<?php

class m140201_193927_settings_table extends CDbMigration
{

	public function safeUp()
	{
		$this->createTable(
			'bg_settings',
			array(
				 'id'                 => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'code'         	  => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'name'				  => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'value'      	      => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			)
		);

		$this->insert('bg_settings', array('code' => 'articles_price_per_letters', 'name' => 'Цена за 1000 символов', 'value' => 100));

	}

	public function safeDown()
	{
		$this->dropTable('bg_settings');
	}

}