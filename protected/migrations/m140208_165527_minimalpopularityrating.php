<?php

class m140208_165527_minimalpopularityrating extends CDbMigration
{
	public function up()
	{
		$this->insert('bg_settings', array('code' => 'minimal_popularity_rating', 'name' => 'Минимальное значение рейтинга для попадания в популярные', 'value' => 1));
	}

	public function down()
	{
		$this->delete('bg_settings', 'code=:code', array('code' => 'minimal_popularity_rating'));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}