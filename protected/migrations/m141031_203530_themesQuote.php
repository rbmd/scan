<?php

class m141031_203530_themesQuote extends CDbMigration
{
	public function up()
	{
		$this->addColumn('chtpz_themes', 'quote_text', 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
		$this->addColumn('chtpz_themes', 'quote_name', 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
		$this->addColumn('chtpz_themes', 'quote_desc', 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('chtpz_themes', 'quote_text');
		$this->dropColumn('chtpz_themes', 'quote_name');
		$this->dropColumn('chtpz_themes', 'quote_desc');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}