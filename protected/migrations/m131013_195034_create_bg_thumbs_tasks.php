<?php

class m131013_195034_create_bg_thumbs_tasks extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_thumbs_tasks',
			array(
				 'id'                 => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'attempts'           => 'INTEGER NOT NULL',
				 'original_path'      => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'settings'           => 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'compressed_path'    => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'hash'               => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'ext'                => 'VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			)
		);

		$this->createIndex('attempts', 'bg_thumbs_tasks', 'attempts');
		$this->createIndex('compressed_path', 'bg_thumbs_tasks', 'compressed_path');
		$this->createIndex('hash', 'bg_thumbs_tasks', 'hash');
	}

	public function safeDown()
	{
		$this->dropTable('bg_thumbs_tasks');
	}
}