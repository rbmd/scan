<?php

class m131028_150639_rating_for_comments extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_comments', 'rating', 'INTEGER NOT NULL DEFAULT 0');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_comments', 'rating');
	}


}