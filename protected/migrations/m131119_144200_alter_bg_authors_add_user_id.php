<?php

class m131119_144200_alter_bg_authors_add_user_id extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_authors', 'user_id', 'INTEGER NOT NULL DEFAULT 0');
		$this->createIndex('user_id', 'bg_authors', 'user_id');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_authors', 'user_id');
	}
}