<?php

class m140410_203311_comments_notice_table extends CDbMigration
{
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(
			'comments_notice',
			array(
				 'id'             => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'user_id'        => 'INTEGER NOT NULL',
				 'entity_name'    => 'VARCHAR(255) NOT NULL',
				 'entity_id'      => 'INTEGER NOT NULL',
				 'comment_id'     => 'INTEGER NOT NULL',
				 'x_timestamp'	  => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			)
		);

		$this->createIndex('user_id', 'comments_notice', 'user_id');
	
	}

	public function safeDown()
	{
		$this->dropTable('comments_notice');
	}
	
}