<?php

class m140216_204232_create_bg_elastic_nested_reindex extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_elastic_nested_reindex',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'entity_id'         => 'INTEGER NOT NULL',
				 'type'              => 'VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
			)
		);

		$this->createIndex('type,entity_id', 'bg_elastic_nested_reindex', 'type,entity_id', true);
	}

	public function safeDown()
	{
		$this->dropTable('bg_elastic_nested_reindex');
	}
}