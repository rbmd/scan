<?php

class m130918_213143_create_bg_blog_iss2 extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_blogs',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'active'            => 'TINYINT(1) DEFAULT 1',
				 'user_id'           => 'INTEGER NOT NULL',
				 'name'              => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'description'       => 'TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'code'              => 'VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'cover'     		 => 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL',
				 'date_create'		 => 'DATETIME',
				 'date_update'		 => 'DATETIME',
			)
		);

		$this->createIndex('user_id', 'bg_blogs', 'user_id');
		$this->createIndex('active', 'bg_blogs', 'active');
		$this->createIndex('code', 'bg_blogs', 'code', true);
		$this->createIndex('date_create', 'bg_blogs', 'date_create');
		$this->createIndex('date_update', 'bg_blogs', 'date_update');
	}

	public function safeDown()
	{
		$this->dropTable('bg_blogs');
	}
}