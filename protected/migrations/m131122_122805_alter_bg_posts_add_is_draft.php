<?php

class m131122_122805_alter_bg_posts_add_is_draft extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_posts', 'is_draft', 'TINYINT NOT NULL DEFAULT 0');
		$this->addColumn('bg_posts', 'draft_for_id', 'INTEGER NOT NULL DEFAULT 0');
		$this->createIndex('is_draft', 'bg_posts', 'is_draft');
		$this->createIndex('draft_for_id', 'bg_posts', 'draft_for_id');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_posts', 'is_draft');
		$this->dropColumn('bg_posts', 'draft_for_id');
	}
}