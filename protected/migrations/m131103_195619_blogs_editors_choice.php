<?php

class m131103_195619_blogs_editors_choice extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_blogs', 'approved', 'TINYINT(1) NOT NULL DEFAULT 0');
		$this->addColumn('bg_posts', 'blog_approved', 'TINYINT(1) NOT NULL DEFAULT 0');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_blogs', 'approved');
		$this->dropColumn('bg_posts', 'blog_approved');
	}
}