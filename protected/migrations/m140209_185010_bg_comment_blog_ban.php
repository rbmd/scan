<?php

class m140209_185010_bg_comment_blog_ban extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_comment_blog_ban',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'blog_id'       	 => 'INTEGER NOT NULL',
				 'user_id'           => 'INTEGER NOT NULL',
			)
		);

		$this->createIndex('user_id', 'bg_comment_blog_ban', 'user_id');
		$this->createIndex('blog_id', 'bg_comment_blog_ban', 'blog_id');
		$this->createIndex('user_id__blog_id', 'bg_comment_blog_ban', 'user_id, blog_id', true);
	}

	public function safeDown()
	{
		$this->dropTable('bg_comment_blog_ban');
	}
}