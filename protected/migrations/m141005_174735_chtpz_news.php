<?php

class m141005_174735_chtpz_news extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			'chtpz_news',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'active'        => 'SMALLINT(1)',
				 'date_active_start'           => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
				 'name'            => 'TEXT',
				 'detail_text'            => 'mediumtext',
				 
			),
			'ENGINE=InnoDB CHARSET=utf8'
		);
	}

	public function down()
	{
		$this->dropTable('chtpz_news');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}