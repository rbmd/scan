<?php

class m131023_113544_update_poll_items extends CDbMigration
{
	public function up()
	{
		$this->addColumn('bg_poll_items', 'img', 'VARCHAR(255) NOT NULL AFTER name');
		$this->addColumn('bg_poll_items', 'description', 'TEXT NOT NULL AFTER img');
		$this->addColumn('bg_poll_items', 'link', 'VARCHAR(255) NOT NULL AFTER description');
	}

	public function down()
	{
		$this->dropColumn('bg_poll_items', 'img');
		$this->dropColumn('bg_poll_items', 'description');
		$this->dropColumn('bg_poll_items', 'link');

	}
}