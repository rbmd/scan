<?php

class m131201_092923_articles_previewimg_show extends CDbMigration
{
	public function up()
	{
		$this->addColumn('bg_articles_draft', 'preview_img_show', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER preview_img_alt');
		$this->addColumn('bg_articles', 'preview_img_show', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER preview_img_alt');
	}

	public function down()
	{
		$this->dropColumn('bg_articles_draft', 'preview_img_show');
		$this->dropColumn('bg_articles', 'preview_img_show');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}