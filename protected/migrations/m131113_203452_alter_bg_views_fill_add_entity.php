<?php

class m131113_203452_alter_bg_views_fill_add_entity extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_views_full', 'entity', 'VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
		$this->createIndex('entity', 'bg_views_full', 'entity');
		$this->update('bg_views_full', array('entity' => 'article'));
	}

	public function safeDown()
	{
		$this->dropColumn('bg_views_full', 'entity');
	}
}