<?php

class m131027_210951_update_rating_for_all_entities extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_ratings',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'entity_name'		=> 'VARCHAR(255) NOT NULL',
				 'entity_id'        => 'INTEGER NOT NULL',
				 'user_id'           => 'INTEGER NOT NULL',
				 'rating'            => 'INTEGER(2) NOT NULL',
			)
		);

		$this->createIndex('user_id', 'bg_ratings', 'user_id');
		$this->createIndex('entity_id', 'bg_ratings', 'entity_id');	
		$this->createIndex('composite user entity', 'bg_ratings', 'entity_name, entity_id, user_id');

		$this->addColumn('bg_posts', 'rating', 'INTEGER NOT NULL DEFAULT 0');
		$this->addColumn('bg_users', 'rating', 'INTEGER NOT NULL DEFAULT 0');
	}

	public function safeDown()
	{
		$this->dropTable('bg_ratings');
		$this->dropColumn('bg_posts', 'rating');
		$this->dropColumn('bg_users', 'rating');	
	}


}