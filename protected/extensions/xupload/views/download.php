<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else { %}
            <td class="preview" width="160">{% if (file.thumbnail_160_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_160_url%}" style="height: 100px;" /></a>
            {% } %}</td>
            <td class="name" width="237">
                <span>Файл: </span>
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_160_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a><br/>
                <span>Размер: </span>
                <span>{%=o.formatFileSize(file.size)%}</span><br/>
            </td>
            <td class="fields">
                <input type="hidden" class="GalleryPhoto_filedir" value="{%=file.dir%}"/>
                <input type="hidden" class="GalleryPhoto_order_num" value="1" />
                <div class="b-photo-edit-textfields">
						  <!--<input name="GalleryPhotos[][name]" class="GalleryPhoto_name" type="text" maxlength="255" placeholder="Название"><br/>-->
                    <input name="GalleryPhotos[][alt]" class="GalleryPhoto_alt" type="text" maxlength="255" placeholder="alt/title"><br/>
                    <input name="GalleryPhotos[][copyright]" class="GalleryPhoto_copyright" type="text" maxlength="255" placeholder="Авторство">
                </div>
                <textarea name="GalleryPhotos[][detail_text]" class="GalleryPhoto_detail-text" type="text" maxlength="1000" placeholder="Описание"></textarea><br/>
            </td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete" width="120">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
                <span>{%=locale.fileupload.destroy%}</span>
            </button>
            <input type="checkbox" name="delete" value="1">
        </td>
    </tr>
{% } %}
</script>