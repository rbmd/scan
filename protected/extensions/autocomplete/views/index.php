<div id="<?= $id ?>" class="ui-sortable"></div>
<script type="text/javascript">
	$('#<?=$id?>').autocmplt({
										 url                : "<?=$url?>",
										 hidden_element_name: '<?=$name?>',
										 max_items          : <?=$maxItems?>,
										 param              : '<?=$param?>',
										 allowNewItems      : <?=($allowNewItems) ? 'true' : 'false'?>
									 });
	$("#<?=$id?>").sortable({items: '.autocmplt_item', axis: 'y', forcePlaceholderSize: true, forceHelperSize: true});
	<?
	if ($data)
	{
		
		if (!is_array($data))
			$data = array($data);
		
		foreach ($data as $item)
		{
			?>
			$("#<?=$id?>").trigger("add", [
				{"key": "<?= $item['id'] ?>", "value": "<?= !empty($item['name']) ? htmlspecialchars($item['name']) : htmlspecialchars($item['firstname']) . ' ' . htmlspecialchars($item['lastname']) ?>"}
			]);
			<?
		}
	}
	?>
</script>
