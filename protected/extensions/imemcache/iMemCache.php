<?php

//YiiBase::import('application.extensions.imemcache.keysSet', true);

/**
 * Created by JetBrains PhpStorm.
 * User: Dmitry Rusakov
 * Date: 23.04.12
 * Time: 11:50
 *
 * Добавляем функционал к стандлартному CMemCache для возможности задлавать для каждого ключа зависимости
 *
 */
class iMemCache extends CMemCache
{
    /*Константы для самых часто используемых таймфремов*/
    const CACHE_DURATION_INFINITY = 0;
    const CACHE_DURATION_30_SECONDS = 30;
    const CACHE_DURATION_1_MINUTE = 60;
    const CACHE_DURATION_3_MINUTES = 180;
    const CACHE_DURATION_5_MINUTES = 300;
    const CACHE_DURATION_10_MINUTES = 600;
    const CACHE_DURATION_15_MINUTES = 900;
    const CACHE_DURATION_20_MINUTES = 1200;
    const CACHE_DURATION_25_MINUTES = 1500;
    const CACHE_DURATION_30_MINUTES = 1800;
    const CACHE_DURATION_45_MINUTES = 2700;
    const CACHE_DURATION_1_HOUR = 3600;
    const CACHE_DURATION_2_HOURS = 7200;
    const CACHE_DURATION_3_HOURS = 10800;
    const CACHE_DURATION_4_HOURS = 14400;
    const CACHE_DURATION_6_HOURS = 21600;
    const CACHE_DURATION_8_HOURS = 28800;
    const CACHE_DURATION_10_HOURS = 36000;
    const CACHE_DURATION_12_HOURS = 43200;
    const CACHE_DURATION_1_DAY = 86400;
    const CACHE_DURATION_2_DAYS = 172800;
    const CACHE_DURATION_3_DAYS = 259200;
    const CACHE_DURATION_4_DAYS = 345600;
    const CACHE_DURATION_5_DAYS = 432000;
    const CACHE_DURATION_1_WEEK = 604800;
    const CACHE_DURATION_2_WEEKS = 1209600;
    const CACHE_DURATION_1_MONTH = 2419200;
    const CACHE_DURATION_2_MONTHS = 4838400;
    const CACHE_DURATION_3_MONTHS = 7257600;
    const CACHE_DURATION_6_MONTHS = 14515200;
    const CACHE_DURATION_1_YEAR = 29030400;

    /**
     * @var $_dependencies - провайдер, который отвечает за хранение зависимостей
     */
    private $_dependencies = null;


	/**
	 * @var bool — Флаг работа ключа. Задается в конфиге.
	 */
	public $disabled = false;


	/**
	 * Переопределяем метод получения даннх из кэша
	 * @param string $id
	 * @return bool|mixed
	 */
	public function get($id)
	{
		if ($this->disabled == false)
		{
			return parent::get($id);
		}
		else
		{
			return false;
		}
	}


	/**
	 * Переопределяем метод записи в кэш.
	 *
	 * @param string $id
	 * @param mixed $value
	 * @param int $expire
	 * @param null $dependency
	 * @return bool
	 */
	public function set($id, $value, $expire = 0, $dependency = null)
	{
		if ($this->disabled == false)
		{
			return parent::set($id, $value, $expire, $dependency);
		}
		else
		{
			return false;
		}
	}


    /**
     * Функция собирает ключ, основываясь на переданных параметрах.
     * Ключ в дальнейшем будет использоваться для кеширования данных.
     *
     * @static
     * @param $key - строка, уникальным образом идентифицирующая ключ
     * @param array $objects - необязательный пареметр, в котором можно передать список айдишников, которые будут
     *              добавлены к ключу. Можно передать в следующих вариантах:
     *              1) array(1,2,3,4,...) - массив айдишников
     *              2) array($objects) - массив объектов(проиндексированных или не проиндексированных).
     *                 Если массив не проиндексирован(ключи объектов не равны айдишникам), будет предпринята попытка
     *                 вытащить айдишники из объектов, храниящихся в поле id.
     *              3) array($objects, 'id') - массив непроиндексированных объектов с указанием поля, которое служит
     *                 айдишников
     * @param string $independentSuffix - суффикс, который не зависит от выбранных данных, и добавляется к ключу,
     *               например авторизован пользователь или нет или день недели на котрый выбирается программа.
     *
     * @return iKey - возвращает объект "Ключ", который имеет меджик метод __toString() и может быть проинтрепритирован
     *                как строка для получения строкового значения ключа.
     */
    public static function buildKey($key, $objects = array(), $independentSuffix = '')
    {
        return new iKey($key, $objects, $independentSuffix);
    }

    /**
     * Добавить зависимость к данным, закешированным по ключу $key
     *
     * @param iKey $key - ключ, экземпляр iKey
     * @param array $dependencies - список зависимостей в следующем формате:
     *              'имя_модели_с_модификаторами'=>массив_со_списком_айдишником,
     *              'имя_модели_с_модификаторами' может включать в себя следующие модификаторы
     *              1)@validator - функция модели, которая возвращает true или false-должен ли объект быть в кеше или нет
     *              2)массив_со_списком_айдишников - id объектов, изменение которых влияет на сброс данного ключа
     *              ПРИМЕРЫ:
     *              'Articles'=>array() - сброс ключа, при изменении любого объекта модели Articles,
     *                                    включая добавление нового объекта.
     *              'Articles'=>array(1,2,3) - сброс ключа только в случае изменения объектов с id=1,2,3
     *              'Articles'=>array($articles) - сброс ключа только в случае изменения объектов $articles, причем
     *                                    $articles - массив объектов Articles, проиндексированных так, что ключ == id объекта,
     *                                    если же массив $articles непроиндексирован, то будет предпринята попытка вытащить
     *                                    id объектов из поля id каждого объекта.
     *              'Articles'=>array($articles, 'id') - то же самое, что и предыдудщий пример, но указывается поле, откуда
     *                                    вытаскивать id объекта.
     *              'Articles@isVisible'=>array() - сброс ключа, при изменении любого объекта модели Articles,
     *                                    включая добавление нового объекта, НО ТОЛЬКО ЕСЛИ ОБЪЕКТ ДОЛЖЕН БЫТЬ В КЕШЕ,
     *                                    ЗА ЧТО ОТВЕЧАЕТ МЕТОД ОБЪЕКТА VALIDATOR, КОТОРЫЙ ВОЗВРАЩАЕТ ИЛИ TRUE ИЛИ FALSE.
     *              'Articles@isVisible(param1, param2)'=>array() - тоже самое, что и предыдущий пример, но можно передать в
     *                                    валидатор 1 или несколько параметров.
     *              '>Articles'=>array(1,2,3) - сброс ключа только в случае изменения объектов с id=1,2,3, а также в случае
     *                                    добавления нового объекта.
     *              '>Articles@isVisible'=>array(1,2,3) - сброс ключа только в случае изменения объектов с id=1,2,3, а также в случае
     *                                    добавления нового объекта.
     *              '>Articles@isVisible'=>array($articles) - сброс ключа только в случае изменения объектов из массива объектов
     *                                    $articles(какой может быть этот массив см в предыдущем примере), а также в случае
     *                                    добавления нового объекта.
     *
     * @throws Exception - в случае ошибки будет выброшено исключение
     */
    public function addDependency(iKey $key, array $dependencies)
    {
		 return;
        try
        {
            if( empty($this->_dependencies) )
            {
                $this->_dependencies = new memcacheDependenciesStorage();
            }

            foreach($dependencies as $mname=>$val)
            {
                //Пропускаем "пустые" зависимости
                if($val === null)
                {
                    continue;
                }

                $model_name = !is_numeric($mname) ? $mname : $val;
                $path = !is_numeric($mname) ? $val : array();
                $ids = array();

                $invalidate_on_insert = false;

                if( mb_substr($model_name, 0, 1) === '>' )
                {
                    $model_name = mb_substr($model_name, 1);
                    $invalidate_on_insert = true;
                }

                $pockets = array();
                $pockets2 = array();
                $validator_params = array();
                $validator = '';

                if( preg_match('/(.+?)@(.+)$/ui', $model_name, $pockets) )
                {
                    $model_name = $pockets[1];
                    $validator = $pockets[2];

                    if( isset($pockets[2]) && preg_match('/([a-z0-9_]+)\((.*?)\)$/ui', preg_replace('/\s+/ui', '', $pockets[2]), $pockets2) )
                    {
                        $validator = $pockets2[1];
                        $validator_params = empty($pockets2[2]) ? array() : explode(',', $pockets2[2]);
                    }
                }

                if( !class_exists($model_name, true) )
                {
                    throw new Exception("Class $model_name doesn't exist!");
                }

                if( !empty($path) )
                {
                    $ids_list_as_string = '';

                    iKey::resolveObjectsIds($path, $ids, $ids_list_as_string);
                }

                $this->_dependencies->addDependencyByKey($key, $model_name, $ids, $validator, $validator_params, $invalidate_on_insert);
            }
        }
        catch(Exception $e)
        {
            Yii::app()->cache->delete($key);

            throw $e;
        }
    }

    /**
     * Сброс ключей на основе зависимостей для всех или для некоторых объектов модели $model_name
     *
     * @param $model_name - имя модели
     * @param array $path - массив айдишников объектов, которые стали невалидными или просто массив этих объектов
     * @param null $object - ссылка на сам объек, который удаляется из кеша
     * @throws Exception - в случае ошибки будет выбрашено исключение.
     */
    public function destroyCacheBasedOnDependencies($model_name, array $path = array(), $object=null)
    {
		
        try
        { 
            if( empty($this->_dependencies) )
            {
                $this->_dependencies = new memcacheDependenciesStorage();
            }

            $ids = array();
            $ids_list_as_string = '';

            iKey::resolveObjectsIds($path, $ids, $ids_list_as_string);

            $data = $this->_dependencies->getDependencyByModelName($model_name);

            foreach($data as $key=>$params)
            {
                /*Если хотим удалить ВООБЩЕ ВСЕ КЛЮЧИ, зависимые от этой модели, без привязки к конктретным айдишникам*/
                if( empty($path) )
                {
                    //$this->deleteKey($key, $params, $object);
                    $this->delete($key);

                    continue;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                /*Если текущая зависимость завист не от конкретных объектов с конкретными id,*/
                /*а от любых изменений в модели $model_name, то удаляем все ключи зависимые от этой модели*/
                if( empty($params['ids']) )
                {
                    $this->deleteKey($key, $params, $object);

                    continue;
                }

                /*Ищем пересечение между объектами для которых надо сбросить все ключи модели и объектами от которых*/
                /*зависит текущий ключ.*/
                $intersect = array_intersect($ids, $params['ids']);

                /*Если вышеуказанное пересечение не пустое, то удаляем ключ*/
                if( !empty($intersect) )
                {
                    $this->deleteKey($key, $params, $object);
                }

                /*Если для данной зависимости указано, что она зависит только от конкретных айдишников, но при этом*/
                /*также добавление любого нового объекта в базу приводит к очистки кеша, то надо грохнуть ключ*/
                if( $params['invalidate_on_insert'] && !empty($object) )
                {
                    if( $object->isNewRecord )
                    {
                        $this->deleteKey($key, $params, $object);
                    }
                }

                /*if( !empty($object)  )
                {
                    if( $object->isNewRecord )
                    {
                        if( $params['invalidate_on_insert'] )
                        {
                            $this->deleteKey($key, $params, $object);
                        }
                    }
                    else
                    {
                        $this->deleteKey($key, $params, $object, false);
                    }
                }*/
            }
			
        }
        catch(Exception $e){
            throw $e;
        }
    }

    /**
     * Удаление ключа и всех хранящихся по нему данных
     *
     * @param $key - ключ в memcache
     * @param $params - параметры зависимости
     * @param $object - ссылка на объект, который изменяется(новый объект, удаляемый объект, изменяемый объект)
     */
    private function deleteKey($key, $params, $object)
    {
		
        //Если указан валидатор, то вызываем его, чтобы понять требуется ли грохать ключ с данными или нет
        if( !empty($params['validator']) && !empty($object) )
        {
            //Вызов валидатора для нового экземпляра объекта(который сохраняется).
            //Смысл валидатора: если данный объект(экземпляр класса модели) влияет на то, что
            //закешировано по ключу $key, то валидатор вернёт true, или false - в противном случае.
            $r = call_user_func_array ( array($object, $params['validator']) , $params['validator_params'] );

            //Получение исходного значения объекта до сохранения (изменения).
            $old_object = $object->getOldObject();

            $r0 = false;

            //Если это вновь созданный объект, то лишнюю проверку делать не надо!
            if(!$object->isNewRecord)
            {
                //Если по каким-то причинам ссылка на исходное значение объекта отсутствует, надо аварийно завершить приложение,
                //так как такая ситуация может быть вызвана только ошибками в логике приложения, допущенными программистами.
                if( empty($old_object) )
                {
                    throw new Exception('Утеряна ссылка на исходное значение объекта!');
                }

                //Вызов валидатора применительно к исходному(старому) значению объекта, до его измения(сохранения)
                //(к объекту который был первоночально получен при чтении из БД)
                $r0 = call_user_func_array ( array($old_object, $params['validator']) , $params['validator_params'] );
            }

            //Если новая или исходное значение объекта влияют на ключ, его(ключ) надо грохнуть.
            if($r || $r0)
            {
                $this->delete($key);
            }
        }
        //если валидатор не указан, то просто грохаем ключ с данными, хранящимися по этому ключу.\
        else
        {
            $this->delete($key);
        }
    }

    /*
    private function deleteKey($key, $params, $object, $anyway_delete = true)
    {
        if( !empty($params['validator']) && !empty($object) )
        {
            //Вызов валидатора. Смысл валидатора: если данный объект(экземпляр класса модели) влияет на то, что
            //закешировано по ключу $key, то валидатор вернёт true или false в противном случае.
            $r = call_user_func_array ( array($object, $params['validator']) , $params['validator_params'] );

            $r = $r || !$object->isNewRecord;

            if($r)
            {
                $this->delete($key);
            }
        }
        else{
            if($anyway_delete)
            {
                $this->delete($key);
            }
        }
    }
    */
}

/**
 * Объект ключа по которому будем сохранять объекты в базе и получать к ним доступ
 */
class iKey{

    const CACHE_KEY_DELIMETER = '::';
    const ELEMS_DELIMETER = ',';
    const INDEPENDENT_DELIMETER = '#';

    private $_data = array('key'=>'', 'independentSuffix'=>'', 'objects'=>array());

    private $_ids_list = array();
    private $_ids_list_as_string = '';
    private $_total_key = '';

    /**
     * Создать ключ
     *
     * @param $key - строка, ключ
     * @param array $objects - масив id или объектов, id которых надо включить в ключ
     * @param string $independentSuffix - суффикс, который будет добавлен к ключу и который не зависит от сохраняемых данных
     */
    public function __construct($key, $objects = array(), $independentSuffix='')
    {
        if( !is_array($objects) )
        {
            throw new Exception('$objects parameter can only be an array!');
        }

        $this->key = trim($key);
        $this->independentSuffix = trim($independentSuffix);
        $this->objects = $objects;

        if( UtilsHelper::isEmpty($this->key) )
        {
            throw new Exception('Wrong parameters are passed! Key can\'t to be an empty.');
        }
    }

    /**
     * Сеттер
     * @param $name - имя свойства
     * @param $value - значение
     * @throws Exception - выбрасывает исключение, если запрошенное свойство не найдено
     */
    public function __set($name, $value)
    {
        if( !isset($this->_data[$name]) )
        {
            throw new Exception("There is no property $name!");
        }

        $this->_data[$name] = $value;
    }

    /**
     * Геттер
     *
     * @param $name - имя свойства
     * @return array|null|string - значение требуемого свойства или null если требуемое свойство отсутствует у объекта
     */
    public function __get($name)
    {
        if( !isset($this->_data[$name]) )
        {
            if($name == 'ids_list')
            {
                return $this->_ids_list;
            }
            elseif($name == 'ids_list_as_string')
            {
                return $this->_ids_list_as_string;
            }

            return null;
        }

        return $this->_data[$name];
    }

    /**
     * @return string - преобразует объект данного класса в строку.
     * Сборка ключа происходит при первом обращении.
     */
    public function __toString()
    {
        if( empty($this->_total_key) )
        {
            $this->_ids_list_as_string = '';
            $this->_ids_list = array();

            /*If passed objects to include their id's in key*/
            self::resolveObjectsIds($this->objects, $this->_ids_list, $this->_ids_list_as_string);

            $this->_total_key =
                $this->key .
                $this->_ids_list_as_string .
                (!UtilsHelper::isEmpty($this->independentSuffix) ? self::INDEPENDENT_DELIMETER . $this->independentSuffix : '') ;
        }

        return $this->_total_key;
    }

    /**
     * @static
     *
     * Вытащить из массива объектов массив айдишников этих объектов
     *
     * @param array $objects - необязательный пареметр, в котором можно передать список айдишников, которые будут
     *              добавлены к ключу. Можно передать в следующих вариантах:
     *              1) array(1,2,3,4,...) - массив айдишников
     *              2) array($objects) - массив объектов(проиндексированных или не проиндексированных).
     *                 Если массив не проиндексирован(ключи объектов не равны айдишникам), будет предпринята попытка
     *                 вытащить айдишники из объектов, храниящихся в поле id.
     *              3) array($objects, 'id') - массив непроиндексированных объектов с указанием поля, которое служит
     *                 айдишников
     * @param array $ids_list - вернёт список айдишников
     * @param $ids_list_as_string - список айдишников склеенных в строку через запятую
     * @return bool - выдернули ли айдишники успешно или нет
     * @throws Exception - в случае ошибки выкинет исключение
     */
    public static function resolveObjectsIds(array $objects, array &$ids_list, &$ids_list_as_string)
    {
        $ids_list = array();
        $ids_list_as_string = '';

        if( !empty($objects) )
        {
            @list($first_elem, $second_elem, ) = $objects;

            if( !empty($first_elem) )
            {
                $ids_list_as_string .= self::CACHE_KEY_DELIMETER;

                /*if defined array of numerical id's, for example array(1,2,3)*/
                if( is_numeric($first_elem) || is_string($first_elem) )
                {
                    sort($objects);
                    $ids_list = $objects;
                    $ids_list_as_string .= implode(self::ELEMS_DELIMETER, $objects);
                }
                /*If defined array indexed or not-indexed objects*/
                elseif( is_array($first_elem) )
                {
                    if( count($first_elem) != 0 )
                    {
                        /*Name of the field where object id is stored, default 'id'*/
                        $id_name = empty($second_elem) ? 'id' : $second_elem;

                        /*If array of objects id IS INDEXED, there is no element at index 0*/
                        if( !isset($first_elem[0]) )
                        {
                            $ids_list = array_keys($first_elem);
                            sort($ids_list);
                            $ids_list_as_string .= implode(self::ELEMS_DELIMETER, $ids_list);
                        }
                        /*If array of objects id ISN'T INDEXED*/
                        else{
                            $ids_list = array();

                            foreach($first_elem as $elem)
                            {
                                if( !isset($elem->$id_name) || empty($elem->$id_name) )
                                {
                                    throw new Exception("There is no field $id_name or field is empty!");
                                }

                                $ids_list[] = $elem->$id_name;
                            }

                            sort($ids_list);

                            $ids_list_as_string .= implode(self::ELEMS_DELIMETER, $ids_list);
                        }
                    }
                    else{
                        $ids_list = array();
                        $ids_list_as_string = '';
                    }
                }
                /*If defined array contains only one object, but not the array*/
                elseif( is_object($first_elem) )
                {
                    /*Name of the field where object id is stored, default 'id'*/
                    $id_name = empty($second_elem) ? 'id' : $second_elem;

                    if( !isset($first_elem->$id_name) || empty($first_elem->$id_name) )
                    {
                        throw new Exception("There is no field $id_name or field is empty!");
                    }

                    $ids_list[] = $first_elem->$id_name;
                    $ids_list_as_string .= $first_elem->$id_name;
                }
                else{
                    throw new Exception('Wrong parameters are are passed!');
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

        return true;
    }
}

/**
 * Провайдер, который хранит зависимости в Мемкешеде
 */
class memcacheDependenciesStorage
{
    const DEPENDENCIES_FOR_MODEL = 'DEPENDENCIES_FOR_MODEL.';
    const ALL_MODELS = 'ALL_MODELS';

    /**
     * Положить зависимость по ключу $key  в мемкешед
     *
     * @param iKey $key
     * @param $model_name
     * @param array $ids
     * @param string $validator
     * @param array $validator_params
     * @param bool $invalidate_on_insert
     * @throws Exception
     */
    public function addDependencyByKey(iKey $key, $model_name, array $ids, $validator='', $validator_params=array(), $invalidate_on_insert = false)
    {
        if( empty($model_name) )
        {
            throw new Exception('Model name can not be empty!');
        }

        $memcache_key = self::DEPENDENCIES_FOR_MODEL . $model_name;

        $model_keys_list = Yii::app()->cache->get($memcache_key);

        if( $model_keys_list === false )
        {
            $model_keys_list = array();
        }

        $model_keys_list[(string)$key] = array(
            'ids' => $ids,
            'validator' => $validator,
            'validator_params' => $validator_params,
            'invalidate_on_insert' => $invalidate_on_insert,
            'last_updated' => time()
        );

        Yii::app()->cache->set($memcache_key, $model_keys_list, iMemCache::CACHE_DURATION_INFINITY);

        $models_list = Yii::app()->cache->get(self::ALL_MODELS);

        if( $models_list === false )
        {
            $models_list = array();
        }

        $models_list[$model_name] = $model_name;

        Yii::app()->cache->set(self::ALL_MODELS, $models_list, iMemCache::CACHE_DURATION_INFINITY);
    }

    /**
     * Удалить ключ из списка зависимостей
     *
     * @param $key
     * @param $model_name
     * @throws Exception
     */
    public function deleteKeyFromDependencyList($key, $model_name)
    {
        if( empty($model_name) )
        {
            throw new Exception('Model name can not be empty!');
        }

        $memcache_key = self::DEPENDENCIES_FOR_MODEL . $model_name;

        $model_keys_list = Yii::app()->cache->get($memcache_key);

        if( $model_keys_list === false )
        {
            return;
        }

        unset($model_keys_list[$key]);

        Yii::app()->cache->set($memcache_key, $model_keys_list, iMemCache::CACHE_DURATION_INFINITY);
    }

    /**
     * Вытащить зависимости по имени модели $model_name
     *
     * @param $model_name - имя модели
     * @return array - список зависимостей, которые привязаны к этой моделе
     */
    public function getDependencyByModelName($model_name)
    {
        $result = array();

        $memcache_key = self::DEPENDENCIES_FOR_MODEL . $model_name;

        $result = Yii::app()->cache->get($memcache_key);

        if( $result === false )
        {
            $result = array();
        }

        return $result;
    }

    /**
     * @return array - получить список моделей для которых для которых мы храним зависимости
     */
    public static function getAllModels()
    {
        $models_list = Yii::app()->cache->get(self::ALL_MODELS);

        if( $models_list === false )
        {
            $models_list = array();
        }

        return $models_list;
    }
}