<?php
/**
 * Выпадашка с категориями сайта в древовидной форме
 */
class TreeCategoriesWidget extends CInputWidget
{
    public $node = 1; /*По умолчанию выбираем всё дерево*/
	
	//Если 1 - то выбираем только то, что соответствует массиву $tree_nodes_for_articles модели Tree.
	public $only_article_nodes = false;
	
    public function init()
    {
        list($this->name, $this->id) = $this->resolveNameID();

        $this->htmlOptions['id'] = $this->id;
        $this->htmlOptions['name'] = $this->name;

        parent::init();
    }

    public function run()
    {
        /*Получаем дерево с требуемого узла*/
        $tree = Tree::model()->findByPk($this->node);

        $base_level = $tree->clevel;
		
		if($this->only_article_nodes)
			$tree = $tree->descendants()->findAllByPk(array_keys(Tree::model()->tree_nodes_for_articles) );
		else
			$tree = $tree->descendants()->findAll();
        

        $this->render("treedropdown", array(
            'name' => $this->name,
            'id' => $this->id,
            'tree' => $tree,
            'base_level' => $base_level,
            'attribute' => $this->attribute,
        ));
    }
}
