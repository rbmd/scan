<?php
class RusDateNormalizerBehavior extends CActiveRecordBehavior
{
    /*
     * Список полей(дата/время) модели, которые должны быть преобразованы к формату даты MySQL перед сохранением.
     * В качестве значения надо указывать, добавлять ли время, например: 'pub_date'=>false - не добавлять время, только дата.
     */
    public $dateAttributes = array();

    /*
     * Перед сохранением для всех указанных в $dateAttributes атрибутов выполянем преобразованием даты в формат MySQL.
     */
    public function beforeSave($event)
    {
        foreach($this->dateAttributes as $attributeName=>$include_time)
        {
            $currentValue = $this->owner->getAttribute($attributeName);
            $normalizedValue = $this->normalize($currentValue, $include_time);
            $this->owner->setAttribute($attributeName, $normalizedValue);
        }

        return parent::beforeSave($event);
    }

    /*
     * После поиска для всех указанных в $dateAttributes атрибутов выполянем преобразованием даты из формата MySQL в русский формат
     */
    public function afterFind($event)
    {
        foreach($this->dateAttributes as $attributeName=>$include_time)
        {
            $currentValue = $this->owner->getAttribute($attributeName);
            $normalizedValue = $this->translate($currentValue, $include_time);
            $this->owner->setAttribute($attributeName, $normalizedValue);
        }

        return parent::afterFind($event);
    }

    /*
     * Преобразуем русскую дату в формат MySQL
     */
    public static function normalize($date, $include_time = false)
    {
        if( empty($date) )
        {
            return new CDbExpression('NULL');
        }

        if( is_numeric($date) )
        {
            return date('Y-m-d' . ($include_time ? ' H:i:s' : ''), (int)$date);
        }

        $_tmp = CDateTimeParser::parse($date, 'dd/MM/yyyy');

        if( empty($_tmp) )
        {
            $_tmp = CDateTimeParser::parse($date, 'dd/MM/yyyy hh:mm:ss');
        }

        if( is_int($_tmp) )
        {
            return date('Y-m-d' . ($include_time ? ' H:i:s' : ''), $_tmp);
        }

        return $date;
    }

    /*
     * Преобразуем дату из формата MySql в русскую дату
     */
    public static function translate($date, $include_time = false)
    {
        if( empty($date) )
        {
            return '';
        }

        if( is_numeric($date) )
        {
            return date('d/m/Y' . ($include_time ? ' H:i:s' : ''), $date);
        }

        $_tmp = CDateTimeParser::parse($date, 'yyyy-MM-dd');

        if( empty($_tmp) )
        {
            $_tmp = CDateTimeParser::parse($date, 'yyyy-MM-dd hh:mm:ss');
        }

        if( is_int($_tmp) )
        {
            return date('d/m/Y' . ($include_time ? ' H:i:s' : ''), $_tmp);
        }

        return $date;
    }
}