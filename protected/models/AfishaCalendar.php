<?php
class AfishaCalendar extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'afisha_calendar';
	}


	public function scopes()
	{
		return array('indexed' => array('index' => 'id'));
	}


	public function rules()
	{
		return array(
			array('', 'required'),
			array('date_start,date_end', 'type', 'type' => 'date', 'dateFormat' => 'yyyy-MM-dd'),
			array('date_start,date_end', 'default', 'setOnEmpty' => true, 'value' => null),
			array('time,event_id', 'safe')
		);
	}


	public function attributeLabels()
	{
		return array(
			'date_start' => 'Дата старта',
			'date_end'   => 'Дата окончания',
			'time'       => 'Время',
		);
	}


	public function beforeSave()
	{
		if (intval($this->date_end) == 0)
		{
			$this->date_end = '3000-00-00';
		}
		return parent::beforeSave();
	}
}