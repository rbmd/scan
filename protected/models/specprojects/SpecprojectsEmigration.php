<?php
class SpecprojectsEmigration extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'specprojects_emigration';
	}

	public function scopes()
	{
		return array(
			'indexed' => array(
				'index' => 'id',
			)
		);
	}
}