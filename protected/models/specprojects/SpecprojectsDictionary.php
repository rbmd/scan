<?php
class SpecprojectsDictionary extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'specprojects_dictionary';
	}


	public function rules()
	{
		return array(
			array('name,text', 'required'),
			array('image', 'safe')
		);
	}


	public function attributeLabels()
	{
		return array(
			'id'         => 'ID',
			'name'       => 'Название',
			'litera'     => 'Литера',
			'text'       => 'Текст',
			'image'      => 'Изображение',
			'image_type' => 'Вид изображения'
		);
	}


	static public function getListGroup()
	{
		$list = self::model()->findAll();
		foreach ($list as $item)
		{
			$res[$item->litera][] = array(
				'id'   => $item->id,
				'name' => $item->name
			);
		}
		return $res;
	}

	static public function getLiters()
	{
		$liters_o = self::model()->findAll(array(
			'select'   => 'litera',
			'group'    => 'litera',
			'distinct' => true,
			'order'    => 'litera'
		));

		foreach ($liters_o as $litera)
		{
			$num = ord($litera->litera);
			if ($num < 65 || $num == 208)
			{
				$liters1[$litera->litera] = $litera->litera;
			}
			else
			{
				$liters2[$litera->litera] = $litera->litera;
			}
		}
		return $liters1 + $liters2;
	}
}