<?php
/**
 * User: Dmitry Rusakov
 * Date: 21.05.13
 * Time: 17:07
 */

class Order extends CActiveRecord {

    /**
     * @param string $className
     *
     * @return Invoice
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'bg_subscribes';
    }

    public function scopes()
    {
        return array(

            'indexed'=>array(
                'index'=>'t.id',
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = array(
            array('in_date, order_text', 'required'),
        );

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            //
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
            'in_date' => 'Дата создания заказа',
            'order_text' => 'Текст с детализацией заказа',
        );

        return $atr;
    }
}