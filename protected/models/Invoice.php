<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dmitry Rusakov
 * Date: 25.03.13
 * Time: 18:41
 * Модель счетов для оплаты подписки на журнал робокассой
 */

class Invoice extends CActiveRecord
{
    /**
     * Статусы счёта
     */
    const INVOICE_STATUS_IN_PROGRESS =   0; //Счёт создан, но ещё НЕ оплачен
    const INVOICE_STATUS_SUCCESS     =   1; //Счёт успешно оплачен(но сумма теоретически может быть и меньше чем нужно)
    const INVOICE_STATUS_ERROR       = 127; //Во время оплаты счёта произошла ошибка

    /**
     * @param string $className
     *
     * @return Invoice
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'bg_invoces';
    }

    public function scopes()
    {
        return array(

            'indexed'=>array(
                'index'=>'t.id',
            ),
        );
    }

    /**
     * Именованный скоуп, накладывает условие, что счета должны иметь определённый указанный статус
     *
     * @param $status
     * @return $this
     */
    public function statusIs($status)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'result = :status',
            'params' => array(':status' => $status + 0),
        ));

        return $this;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = array(
            array('out_sum', 'required'),
            array('in_sum, in_date, result', 'safe'),
        );

        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
           //
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
            'out_sum' => 'На какую сумму выставлен счёт',
            'in_sum' => 'Сколько на самом деле было оплачено',
            'out_date' => 'Дата выставления счёта',
            'in_date' => 'Дата получения ответа от платёжного шлюза',
            'result' => 'Статус счёта(0 - в процессе; 1 - успешно оплачен; 400 - ошибка)',
        );

        return $atr;
    }

    /**
     * Сгенерировать url для оплаты этого счёта счёта
     * @param  float $out_sum
     *
     * @return string - url, который уводит на платёжный шлюз для оплаты этого счёта
     */
    public function getRobokassaPaymentUrl($out_sum)
    {
        $url = Yii::app()->params['robokassa']['mrh_url'];

        $mrh_login = Yii::app()->params['robokassa']['mrh_login'];
        $mrh_pass  = Yii::app()->params['robokassa']['mrh_pass1'];

        //номер счёта
        $inv_id = $this->id;

        //описание заказа
        $inv_desc = Yii::app()->params['robokassa']['mrh_description'];

        //сумма заказа
        $out_summ = sprintf('%.2f', $out_sum);

        //формирование подписи
        $crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass:shp_invoice_id={$inv_id}");

        $in_curr = "BANKOCEAN2R";
        $culture = "ru";
        $encoding = "utf-8";

        $r = "{$url}?MrchLogin={$mrh_login}&OutSum={$out_summ}&InvId={$inv_id}&Desc={$inv_desc}&SignatureValue={$crc}&shp_invoice_id={$inv_id}&IncCurrLabel={$in_curr}&Culture={$culture}&Encoding={$encoding}";

        return $r;
    }

    protected function beforeSave()
    {
        if($this->isNewRecord)
        {
            $this->out_date = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }

    protected function afterSave()
    {
        parent::afterSave();
    }
}
