<?php

/**
 * Статистика по каждой статье за всё время
 */
class ViewsFull extends CActiveRecord
{
    /*
     * Время кеширования статистики в секундах
     */
    const STATISTICS_TTL = 60;

    public $views = 0;
    public $views_all = 0;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'bg_views_full';
    }

    public function scopes()
    {
        return array(
            'Indexed'=>array(
                'index'=>'element_id',
                'select'=>array('*'),
            ),
        );
    }

	
    /*Именованный СКОУП: Выбрать статистику для определённых элементов*/
    public function inList(array $elements_list)
    {
        if( empty($elements_list) )
        {
            return $this;
        }

        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'element_id IN (' . implode(', ', $elements_list) . ')',
            'order'=>'element_id ASC',
        ));

        return $this;
    }


     /**
     * Задать для какой сущности будем выбирать показы
     *
     * @param string $name - имя сущности
     *
     * @return ViewsFull
     */
    public function entity($name = 'article')
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "entity = '{$name}'",
        ));

        return $this;
    }


    public function rules()
    {
        return array(
           //
        );
    }

    public function relations()
    {
        return array(
            //
        );
    }

    public function attributeLabels()
    {
        return array(
           //
        );
    }



    public static function entityHelper($modelName)
    {
        switch ($modelName) {
            case 'Articles':
                return 'article';
                break;

            case 'Post':
                return 'post';
                break;

            default:
                return 'article';
                break;
        }
    }

    /*
     * Получить статистику для списка статей.
     *
     * $elements - статьи, должны быть выбраны с участием скоупа Indexed(), например:
     *    $articles = Articles::model()->with('tree')->Indexed()->findAll($criteria);
     *
     * Использовать так:
     * (1)получить статистику с помощью этого метода, например:
     *    $stat = ViewsFull::model()->getStatistic4Elements( $articles );
     * (2)обращаться к статистике как $stat[id_статьи]->поле, например $stat['153963']->views
     *
     * Примечание: проверять на существование нужного элемента в $stat не требуется, так как если
     * элемента не существует, то вернёт пустой объек ViewsFull, у которого свойства views и views_all будут == 0.
     */
    public function getStatistic4Elements(array $elements, $entity = 'article')
    {
        if( isset($elements[0]) )
        {
            throw new Exception('Нелья получать статистику для непроиндексированных объектов!');
        }

        $elements = array_keys($elements);

        if( empty($elements) )
        {
            return new EasyStat(array(),  __CLASS__);
        }

        $key = md5(implode(':', $elements)).':'.$entity;

        $result = Yii::app()->cache->get($key);

        if( $result === false )
        {
            $result = new EasyStat( $this->entity($entity)->inList($elements)->Indexed()->findAll(), __CLASS__ );

            Yii::app()->cache->set($key, $result, self::STATISTICS_TTL);
        }

        return $result;
    }



    // Named scope - просмотры, относящиеся к конкретному телешоу (ко всем статьям с tree_id = $tree_id)
    // используется для выбора просмотров в рамках отдельных телешоу (просмотры всех выпусков в "Деньги")
    public function childOf($tree_id)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'tree_id = ' . $tree_id,
            )
        );

        return $this;
    }
}
?>