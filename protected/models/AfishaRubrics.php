<?php
class AfishaRubrics extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'afisha_rubrics';
	}


	public function defaultScope()
	{
		return array(
			'condition' => 'is_delete=0'
		);
	}

	public function getList()
	{
		$CACHE_KEY = 'best_summer_rubrics';
		$list      = Yii::app()->cache->get($CACHE_KEY);
		if ($list === false)
		{
			$criteria        = new CDbCriteria();
			$criteria->order = 'order_num';
			$criteria->index = 'id';
			$list            = AfishaRubrics::model()->findAll($criteria);
			Yii::app()->cache->set($CACHE_KEY, $list, 60);
		}
		return $list;
	}
}