<?php
/**
 * Русаков Дмитрий
 * Класс, хранящий массив статистики для определённого списка статей.
 * В классе перегружены операторы [], то есть к объекту класса можно образаться как к массиву
 */
class EasyStat implements ArrayAccess
{
    /**
     * @var $_type - имя класса той статистики, которую мы храним: ViewsFull или ViewsDaily
     */
    protected $_type;

    /**
     * @var array - здесь храним список статистики для определённого списка статей
     */
    protected $_elements = array();

    public function __construct($elements, $type)
    {
        if( !empty($elements) )
        {
            $this->_elements = $elements;
        }

        $this->_type = $type;
    }

    /**
     * @param $offset - элемент статистики, а именно id статьи по которой нужна статистика
     * @return bool - есть ли статистика по указанной статье
     */
    public function offsetExists($offset)
    {
        return isset($this->_elements[$offset]);
    }

    /**
     * @param $offset - элемент статистики, а именно id статьи по которой нужна статистика
     * @return mixed - объект класса ViewsFull или ViewsDaily, содержащий статистику по статье с указанным id
     */
    public function offsetGet($offset)
    {
        if (array_key_exists($offset, $this->_elements)) {
            return $this->_elements[$offset];
        }

        //Если в статистике нет данных по определённому элементу(статье),
        //то возвращаем пустой экземпляр ViewsFull или ViewsDaily у которого
        //поля views и views_all будут == 0
        return new $this->_type();
    }

    public function offsetSet($offset, $value)
    {
        //Менять элементы статистики запрещено, то есть она только для чтения
        throw new Exception('Статистика только для чтения!');
        //return $this->_elements[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        //Менять элементы статистики запрещено, то есть она только для чтения
        throw new Exception('Статистика только для чтения!');
        //unset($this->_elements[$offset]);
    }
}
