<?php
/**
 * User: Русаков Дмитрий
 * Date: 12.03.2013
 * Time: 16:18
 *
 * Логер профайловой информации в БД
 *
 */
class BgDBLogRoute extends CDbLogRoute
{
    /**
     * @var string - имя таблицы куда будем складывать профайловую информацию
     */
    public $logTableName='log';

    /**
     * Создаёт в Базе Данных таблицу для логирования.
     * Этот метод вызывается, если таблицы для логирования ещё не существует.
     *
     * @param CDbConnection $db - экземпляр подключения к БД
     * @param string $tableName - имя таблицы
     */
    protected function createLogTable($db,$tableName)
    {
        $driver=$db->getDriverName();

        $logID = 'id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY';

        $sql="
        CREATE TABLE $tableName
        (
            $logID,
            level VARCHAR(128) COLLATE utf8_unicode_ci,
            category VARCHAR(128) COLLATE utf8_unicode_ci,
            logtime INTEGER,
            message VARCHAR(255) COLLATE utf8_unicode_ci,

            controller_action VARCHAR(255) COLLATE utf8_unicode_ci,
            request_url  VARCHAR(255) COLLATE utf8_unicode_ci,
            request_code VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci,
            user_id INTEGER DEFAULT NULL,
            execution_time NUMERIC(12,8) NOT NULL,
            sql_stat LONGTEXT NULL DEFAULT NULL COLLATE utf8_unicode_ci,

            KEY `category` (`category`),
            KEY `logtime` (`logtime`),
            KEY `message` (`message`),
            KEY `controller_action` (`controller_action`),
            KEY `request_url` (`request_url`),
            KEY `request_code` (`request_code`),
            KEY `user_id` (`user_id`),
            KEY `execution_time` (`execution_time`)
        )";

        $db->createCommand($sql)->execute();
    }

    /**
     * Сохраняет логи в БД.
     * @param array $logs список логов
     */
    protected function processLogs($logs)
    {
        $results = array();

        $stack=array();

        foreach($logs as $log)
        {
            if($log[1]!==CLogger::LEVEL_PROFILE)
            {
                continue;
            }

            $message=$log[0];

            if(!strncasecmp($message,'begin:',6))
            {
                $log[0]=substr($message,6);

                $stack[]=$log;
            }
            else if(!strncasecmp($message,'end:',4))
            {
                $token=substr($message,4);

                if(($last=array_pop($stack))!==null && $last[0]===$token)
                {
                    $delta=floatval($log[3])-floatval($last[3]);

                    $pockets = array();

                    $r = preg_match('/^(begin|end):([a-z0-9_\/\.-]+)(:([a-z0-9_\/\.-]+))?$/ui', $message, $pockets);

                    if(!$r)
                    {
                        throw new Exception('An Error occured during profiling...');
                    }

                    $results[] = array(
                        'level'             => $log[1],
                        'category'          => $log[2],
                        'message'           => empty($pockets[4]) ? $pockets[2] : $pockets[4],
                        'controller_action' => $pockets[2],
                        'execution_time'    => $delta,
                        'request_url'       => Yii::app()->request->userHost . Yii::app()->request->url,
                        'request_code'      => Controller::getRequestCode(),
                        'user_id'           => !empty(Yii::app()->user->id) ? Yii::app()->user->id+0 : 'NULL',
                    );
                }
                else
                {
                    throw new CException(Yii::t('yii','CProfileLogRoute found a mismatching code block "{token}". Make sure the calls to Yii::beginProfile() and Yii::endProfile() be properly nested.',
                        array('{token}'=>$token)));
                }
            }
        }
        //Удаляем старые данные в которых больше нет необходимости
        /*
        $delta = max(Profiler::$PROFILER_INTERVALS) + 1;

        $connection = Yii::app()->db;
        $sql = "DELETE FROM {$this->logTableName} WHERE logtime < UNIX_TIMESTAMP(NOW() - INTERVAL {$delta} MINUTE)";
        $command = $connection->createCommand($sql);
        $command->query();
*/
        //Если список логов пуст, то ничего в БД не пишем.
        if( empty($results) )
        {
            return;
        }

        $sql =
            "
            INSERT INTO {$this->logTableName}
            (level, category, logtime, message, controller_action, request_url, request_code, user_id, execution_time) VALUES
            ";

        $i = 0;

        //Собираем все логи в один запрос, чтобы вставить в БД все за один раз!
        foreach($results as $result)
        {
            $sql .= ($i != 0 ? ',' : '') . sprintf("('%s', '%s', %d, '%s', '%s', '%s', '%s', %s, %f)",
                $result['level'],
                $result['category'],
                time(),
                $result['message'],
                $result['controller_action'],
                $result['request_url'],
                $result['request_code'],
                $result['user_id'],
                $result['execution_time']
            );

            ++$i;
        }

        $sql .= ";";

        Yii::app()->db->createCommand($sql)->execute();
    }
}