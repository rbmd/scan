<?php
/**
 * User: Русаков Дмитрий
 * Date: 06.07.12
 * Time: 12:11
 *
 * Перекрываем наш собственный логгер, чтобы отфильтровать и не логгировать нежелательные ошибки
 * (например исключиить логирование 404 в конфиг).
 *
 */
class RainLogger extends CFileLogRoute
{
    protected function processLogs($logs)
    {
        $filtered_logs = array_filter($logs, function($log) {
            return $log[2] !== 'exception.CHttpException.404';
        });

        parent::processLogs($filtered_logs);
    }
}
