<?php

/**
 * Авторизация внешних пользователей(из соц сетей и т.п.)
 */
class UserExtIdentity extends EAuthUserIdentity
{
    public function __construct($service) { 
        parent::__construct($service);
    }

    public function authenticate(&$is_new_user=false)
    {
        if ($this->service->isAuthenticated)
        {
            $suffix = self::getUserNameSuffix( $this->service->getServiceName() );

            $username = "{$this->service->id}.$suffix";

            $pockets = array();

            $fio_as_list = explode(' ', $this->service->getAttribute('name'));

            $user = User::model()->findByAttributes( array('username'=>$username) );

            //Если у нас в базе ещё нет этого пользователя, то заводим его
            if($user === null)
            {
                $user = new User();

                $user->username = $username;
                $user->avatar = $this->service->getAttribute('photo');
                $user->password = '';
                $user->email = new CDbExpression('NULL');
                $user->firstname = isset($fio_as_list[0]) ? $fio_as_list[0] : '';
                $user->lastname = isset($fio_as_list[1]) ? $fio_as_list[1] : '';
                $user->createtime = new CDbExpression('NOW()');
                $user->lastvisit = new CDbExpression('NOW()');
                $user->superuser = 0;
                $user->status = 1;
                $user->external_type = $this->service->getServiceName();
                $user->external_id = $this->service->id;

                if( !$user->save() )
                {
                    return false;
                }

                $is_new_user = true;
            }

            $this->id = $user->id;
            $this->name = $user->username;
            $user->avatar = $this->service->getAttribute('photo');
			
            $this->saveUserDataInSession($user);

            $this->errorCode = self::ERROR_NONE;
        }
        else {
            $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
        }

        return !$this->errorCode;
    }

    /*
    * Сохраняем все данные юзера в сессии, чтобы они были доступны всегда так:
    * Yii::app()->user->имя_поля
    */
    private function saveUserDataInSession($user = null)
    {
        if( empty($user) )
        {
            return;
        }

        foreach($user->attributes as $name=>$value)
        {
            if(in_array($name, array('id', 'activkey', 'password')))
            {
                continue;
            }

            $this->setState($name, is_numeric($value) ? $value + 0 : $value);
        }

        $this->setState('fio', $user->lastname . ' ' . $user->firstname);

        $this->setState('avatar_url', !empty($user->attributes['photo']) ? User::model()->getPhotoImgSrc($user->attributes['photo'], User::PREVIEW):'');
    }

    /**
     * Возвращаем суффиксы для последующего правильного формирования идентификаторов
     *
     * @static
     * @param $service - имя сервиса
     * @return string - суффикс
     */
    public static function getUserNameSuffix($service)
    {
        if($service == 'vkontakte')
        {
            $service = "vk.com";
        }
        elseif($service == 'facebook')
        {
            $service = "facebook.com";
        }
        elseif($service == 'twitter')
        {
            $service = "twitter.com";
        }
        elseif($service == 'odnoklassniki')
        {
            $service = "odnoklassniki.ru";
        }
        elseif($service == "livejournal")
        {
            $service = "livejournal.com";
        }
        elseif($service == "yandex")
        {
            $service = "yandex.ru";
        }

        return $service;
    }
}