<?
class RainDbHttpSession extends CDbHttpSession {

    /**
     * Время жизни сессии пользователя!
     */
    const SESSION_LIFE_TIME = 3600;

    /**
     * Время жизни сессии пользователя, если он гость
     */
    const SESSION_LIFE_TIME_FOR_GUEST = 1800;

    /**
     * Время жизни запоминательной куки!
     */
    const REMEMBER_ME_COOKIE_LIFE_TIME = 31536000; //3600*24*365;

    /**
     * @var string - как будет называться таблица с сессиями в БД
     */
    public $sessionTableName='tv_sessions';



    /**
     * @return int - задаём своё, отличное от стандартного,
     * время жизни сессии.
     */
    public function getTimeout()
    {
        return (int)self::SESSION_LIFE_TIME;
    }

    /**
     * Переопределяем метод для создания таблицы с сессиями
     *
     * @param CDbConnection $db the database connection
     * @param string $tableName the name of the table to be created
     */
    protected function createSessionTable($db,$tableName)
    {
        $driver=$db->getDriverName();

        if($driver==='mysql')
        {
            $blob='LONGTEXT';
        }
        elseif($driver==='pgsql')
        {
            $blob='TEXT';
        }
        else
        {
            $blob='TEXT';
        }

        $db->createCommand()->createTable($tableName,array(
            'id'      => 'CHAR(32) PRIMARY KEY',
            'user_id' => 'INTEGER NULL DEFAULT NULL',
            'expire'  => 'INTEGER',
            'data'    => $blob,
	        'agent'   => 'VARCHAR(128) NULL DEFAULT NULL',
	        'ip'      => 'VARCHAR(24)  NULL DEFAULT NULL',
        ));


	    $db->createCommand()->addForeignKey(
		   "{$this->sessionTableName}_user_id_fk",
		    $this->sessionTableName,
		    'user_id',

		    'tv_users',
		    'id',

		    "CASCADE",
		    "CASCADE"
	    );

	    $db->createCommand()->createIndex(
		    'expire',
		    $this->sessionTableName,
		    'expire'
	    );
    }

	/**
	 * Перекрываем стандартный regenerateID.
	 * @param bool $deleteOldSession
	 */
	public function regenerateID($deleteOldSession=false)
    {
	    $oldID=session_id();

	    // if no session is started, there is nothing to regenerate
	    if(empty($oldID))
		    return;

	    CHttpSession::regenerateID( false );

	    $newID=session_id();

	    $db=$this->getDbConnection();

	    $row=$db->createCommand()
		    ->select()
		    ->from($this->sessionTableName)
		    ->where('id=:id',array(':id'=>$oldID))
		    ->queryRow();

	    if($row!==false)
	    {
		    //$deleteOldSession в нашей системе будет всегда true
		    if($deleteOldSession)
			    $db->createCommand()->update($this->sessionTableName,array(
				    'id'=>$newID
			    ),'id=:oldID',array(':oldID'=>$oldID));
		    else
		    {
			    $row['id']=$newID;

			    try {
			        $db->createCommand()->insert($this->sessionTableName, $row);
			    }
			    catch(Exception $e) {
				    Yii::log("RainDbHttpSession(134)!", CLogger::LEVEL_ERROR);
				    throw $e;
			    }
		    }
	    }
	    else
	    {
		    //shouldn't reach here normally
		    try {
			    $db->createCommand()->insert($this->sessionTableName, array(
				    'id'       => $newID,
				    'user_id'  => new CDbExpression('NULL'),
				    'expire'   => time()+$this->getTimeout(),
				    'agent'    => mb_substr(Yii::app()->getRequest()->getUserAgent(), 0, 128),
				    'ip'       => User::getIP(),
			    ));
		    }
		    catch(Exception $e) {
			    Yii::log("RainDbHttpSession(152): Shouldn't reach here normally...", CLogger::LEVEL_ERROR);
			    throw $e;
		    }
	    }
    }

    /**
     * Call-back, который php дергает для чтения сессии
     *
     * @param string $id
     * @return string
     */
    public function readSession($id)
    {
        return parent::readSession($id);
    }

    /**
     * Call-back, который php дергает для записи данных сессии
     *
     * Переопределяем стандартный Yii'шный "Session write handler".
     *
     * @param string $id
     * @param string $data
     * @return bool
     */
    public function writeSession($id,$data)
    {
        //exception must be caught in session write handler
        //http://us.php.net/manual/en/function.session-set-save-handler.php

        try
        {
            /**
             * Разное время жизни для неавторизованных и авторизованных
             * пользователей.
             */
            $expire = Yii::app()->user->isGuest
                ?
                time() + self::SESSION_LIFE_TIME_FOR_GUEST
                :
                time() + $this->getTimeout()
            ;









	        $user_id =
		        Yii::app()->user->isGuest ?
		        'NULL' :
			    (int)Yii::app()->user->id;

	        $data  = mysql_real_escape_string($data);
	        $ip    = mysql_real_escape_string(User::getIP());
	        $agent = mysql_real_escape_string(mb_substr(Yii::app()->getRequest()->getUserAgent(), 0, 128));

	        $command = Yii::app()->db->createCommand(
		        "INSERT INTO `{$this->sessionTableName}` (`id`, `user_id`, `expire`, `data`, `agent`, `ip`)
		         VALUES ('{$id}', {$user_id}, {$expire}, '{$data}', '{$agent}', '{$ip}')
		         ON DUPLICATE KEY UPDATE `user_id`={$user_id}, `expire`={$expire}, `data`='{$data}';"
	        );

	        //На нашей строй версии PHP (на сервере) биндинг параметров работает некорректно
	        //$command->bindParam(":id",      $id,       PDO::PARAM_INT);
	        //$command->bindParam(":expire",  $expire,   PDO::PARAM_INT);
	        //$command->bindParam(":data",    $data,     PDO::PARAM_STR);
	        //$command->bindParam(":agent",   $agent,    PDO::PARAM_STR);
	        //$command->bindParam(":ip",      $ip,       PDO::PARAM_STR);

	        try {
	            $command->execute();
	        }
	        catch(Exception $e) {
		        Yii::log("RainDbHttpSession(226)!", CLogger::LEVEL_ERROR);
		        throw $e;
	        }







           /*$_user_id =
	        Yii::app()->user->isGuest ?
		        new CDbExpression('NULL') :
		        (int)Yii::app()->user->id;

           $db = $this->getDbConnection();

           if( $db->createCommand()->
	            select('id')->
	            from($this->sessionTableName)->
	            where('id=:id',array(':id'=>$id))->
	            queryScalar() === false ) {

	        try {
                $db->createCommand()->insert($this->sessionTableName,array(
                    'id'      => $id,
                    'user_id' => $_user_id,
                    'expire'  => $expire,
                    'data'    => $data,
	                'agent'   => mb_substr(Yii::app()->getRequest()->getUserAgent(), 0, 128),
	                'ip'      => User::getIP(),
                ));
	        }
	        catch(Exception $e) {

		        Yii::log("RainDbHttpSession(222)!", CLogger::LEVEL_ERROR);

		        //Поясню этот код: так как другой поток может выполнить insert раньше, то возникает
		        //проблема "Integrity constraint violation: 1062 Duplicate entry". Поэтому, если всё-таки
		        //ошибка случилось, то считаем, что это именно эта проблема и делаем update вместо insert.
		        //А если это была другая проблема, то, в конечном счёте, всё равно будет выкинут
		        //Exception при попытке update.
		        $db->createCommand()->update($this->sessionTableName, array(
			        'user_id' => $_user_id,
			        'expire'  => $expire,
			        'data'    => $data,
			        //Закомментарил, так как есть смысл писать эти данные только при создании сессии
			        //'agent'   => mb_substr(Yii::app()->getRequest()->getUserAgent(), 0, 128),
			        //'ip'      => User::getIP(),
		        ),'id=:id',array(':id'=>$id));

		        throw $e;
	        }
        }
        else
        {
            $db->createCommand()->update($this->sessionTableName, array(
                'user_id' => $_user_id,
                'expire'  => $expire,
                'data'    => $data,
                //Закомментарил, так как есть смысл писать эти данные только при создании сессии
                //'agent'   => mb_substr(Yii::app()->getRequest()->getUserAgent(), 0, 128),
                //'ip'      => User::getIP(),
            ),'id=:id',array(':id'=>$id));
        }*/
        }
        catch(Exception $e)
        {
            if(YII_DEBUG) {
                echo $e->getMessage();
            }

	        try {
		        Yii::log(
			        $e->getMessage() . ": " . $e->getTraceAsString(),
			        CLogger::LEVEL_ERROR
		        );
	        }
	        catch(Exception $e) {}

            // it is too late to log an error message here
            return false;
        }

        return true;
    }
}