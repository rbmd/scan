<?
class Pagination extends CLinkPager
{
	public $width = 3;

	public function run()
	{
		$page = $this->pages->currentPage + 1;
		$total_pages = ceil($this->pages->itemCount / $this->pages->pageSize);

		if ($page - $this->width > 0)
		{
			$start = $page - $this->width;
		}
		else
		{
			$start = 1;
		}
		
		if ($start + $this->width * 2 <= $total_pages)
		{
			$end = $start + $this->width * 2;
		}
		else
		{
			$end = $total_pages;
			$start = $end - $this->width * 2;
			
			if ($start < 1)
				$start = 1;
		}

		if ($page - 1 > 0)
			$move_left = $page - 1;
		else
			$move_left = null;
		
		if ($page + 1 <= $end)
			$move_right = $page + 1;
		else $move_right = null;

		$this->render(
			'application.widgets.paginator.views.generic',
			array(
				'page' => $page,
				'start' => $start,
				'end' => $end,
				'move_left' => $move_left,
				'move_right' => $move_right,
				'total_pages' => $total_pages,
			)
		);
	}
}
?>