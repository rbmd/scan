<?
// Пережималка любых картинок в любой размер
// Пережатое сохраняем в папку /media/рамер/путь_к_изображению

class ThumbsMasterTask extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_thumbs_tasks';
	}

	public function rules()
	{
		$rules = array(
			array('original_path, compressed_path, hash, ext, settings', 'required'),
			array('hash, compressed_path', '_uniqValidator'),
			array('original_path, compressed_path, hash, ext, settings, attempts', 'safe'),
		);

		return $rules;
	}

	// Проверка на уникальность hash + compressed_path
	public function _uniqValidator($attribute, $params)
	{
		if ($this->isNewRecord)
		{
			$test = ThumbsMasterTask::model()->cache(600)->findByAttributes( array('hash' => $this->hash, 'compressed_path' => $this->compressed_path) );

			if (!empty($test))
			{
				$this->addError('compressed_path', 'Задача уже есть');
			}
		}
	}


	public function apply()
	{
		if (strstr($this->ext, 'com/') !== false)
		{
			$this->ext = 'jpg';
			$this->save();
		}
		$filename = $this->hash . '.' . $this->ext;

		$webroot = str_replace('/protected', '', Yii::getPathOfAlias('webroot'));

		if (file_exists($webroot . $this->compressed_path . '/' . $filename) && !$this->isNewRecord)
			$this->delete();

		// Если нет папки под иконку - делаем
		if ( !(file_exists($webroot . $this->compressed_path) && is_dir($webroot . $this->compressed_path)) )
			if (!@mkdir($webroot . $this->compressed_path, 0775, true))
				return false;

		$original_path = $this->original_path;

		if (preg_match('|https?://|', $original_path))
		{
			$remote_file_flag = true;

			$context = stream_context_create(
				array(
					'http' => array(
						'follow_location' => true
					)
				)
			);
			$content = @file_get_contents($original_path, false, $context);

			if ($content !== false)
			{
				// Если нет папки под временные файлы для пережатий с стороннего сайта - делаем
				if ( !(file_exists($webroot . ThumbsMaster::SAVE_REMOTE_PATH) && is_dir($webroot . ThumbsMaster::SAVE_REMOTE_PATH)) )
					mkdir($webroot . ThumbsMaster::SAVE_REMOTE_PATH, 0775, true);

				// Сохраняем стороннюю картинку у нас
				if (file_put_contents($webroot . ThumbsMaster::SAVE_REMOTE_PATH . '/' . $filename , $content))
					$original_path = ThumbsMaster::SAVE_REMOTE_PATH . '/' . $filename;
			}
			else
				$original_path = $webroot . ThumbsMaster::DEFAULT_IMAGE;
		}


		$img = Yii::app()->imagemod->load($webroot . $original_path);
		$img->file_new_name_body = $this->hash;
		$img->file_new_name_ext = $this->ext;

		// Применяем настройки для обжатия
		foreach (json_decode($this->settings) as $key => $value)
		{
			$img->$key = $value;
		}

		$img->process($webroot . $this->compressed_path);


		if ($img->processed)
		{
			// Удаляем блок	
			Yii::app()->cache->delete('ThumbsMaster-' . $this->compressed_path.$this->hash);

			// Удаляем временный файл, сохраненный со стороннего ресурса
			if (isset($remote_file_flag) && file_exists($webroot . '/' . $original_path))
				unlink($webroot . '/' . $original_path);

			if (!$this->isNewRecord)
				$this->delete();

			return $this->compressed_path . '/' . $filename;
		}
		else
		{
			$this->attempts++;
			$this->save();

			return false;
		}
	}
}
?>