<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	const ERROR_EMAIL_INVALID=3;
	const ERROR_STATUS_NOTACTIV=4;
	const ERROR_STATUS_BAN=5;

    public static function makesalt($length = 8)
    {
        if($length > 40 || $length < 1)
            $length = 8;

        return substr(sha1(time()), 0, $length);
    }

    /**
     * @return hash string.
     */
    public static function encrypting($string="", $salt = null)
    {
        if(is_null($salt))
            $salt = self::makesalt();

        return md5($salt.$string);
    }

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = null;

		/*if ( strpos($this->username,"@") ) {
			$user=User::model()->findByAttributes(array('email'=>$this->username));
		} else {
			$user=User::model()->findByAttributes(array('username'=>$this->username));
		}*/

        $user = User::model()->findByAttributes( array('username'=>$this->username) );

        if( empty($user) )
        {
            $user=User::model()->findByAttributes( array('email'=>$this->username) );
        }
		
		if($user === null)
		{
			if (strpos($this->username,"@")) {
				$this->errorCode=self::ERROR_EMAIL_INVALID;
			} else {
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			}
		}
        else if( empty($this->password) )
        {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
		else
		{
			/*salt*/
			$salt = substr($user->password, 0, 8);

			$hash = substr($user->password, -32);

			$entered_hash = self::encrypting($this->password, $salt);

			if( $entered_hash != $hash)
			{
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}
            // Проверка на активацию акка по почте
			// else if($user->status==0)
            // {
				// $this->errorCode=self::ERROR_STATUS_NOTACTIV;
            // }
			else if($user->status==-1)
            {
				$this->errorCode=self::ERROR_STATUS_BAN;
            }
			else {
				$this->_id = $user->id;
				$this->username = $user->username;
				
                $this->saveUserDataInSession($user);
				
				$this->errorCode = self::ERROR_NONE;
			}
		}

		return !$this->errorCode;
	}
    
    /**
    * @return integer the ID of the user record
    */
	public function getId()
	{
		return $this->_id;
	}

    //Авторизовать админа под любым другим пользователем с ID==$user_id
    public function authAdmin($user_id)
    {
        $user = User::model()->findByPk($user_id);

        if( empty($user) )
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else
        {
            //Если пользователь найден, то
            //сохраняем данные пользователя...
            $this->_id = $user->id;
            $this->username = $user->username;

            $this->saveUserDataInSession($user);

            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    /*
     * Сохраняем все данные юзера в сессии, чтобы они были доступны всегда так:
     * Yii::app()->user->имя_поля
     */
    public function saveUserDataInSession($user = null)
    {
		if( empty($user) )
        {
            return;
        }

        foreach($user->attributes as $name=>$value)
        {
            if(in_array($name, array('id', 'activkey', 'password')))
            {
                continue;
            }

            $this->setState($name, is_numeric($value) ? $value + 0 : $value);
        }

        $this->setState('fio', $user->lastname . ' ' . $user->firstname);

        $this->setState('avatar_url', !empty($user->avatar) ? User::model()->getPhotoImgSrc($user->avatar, User::PREVIEW):'');
    }
}