<?php

/**
 * Наш собственный класс юзера
 */
class RainWebUser extends CWebUser /*ВАЖНО! Наследуемся от модуля Rights*/
{
	/**
	 * Будем принудительно генерировать новый SID через каждый час!
	 * ЧТОБЫ НЕ БЫЛО ЭФФЕКТА, когда у пользователя пол года один и тот же
	 * SID.
	 */
	const SID_REGENERATED_TIMEOUT = 3600;

	/**
	 * Здесь храним время последней регенерации SID'а.
	 * Это нужно для того, что мы будем принудительно регенерировать SID
	 * через определённые интревалы времени, указанные в
	 * SID_REGENERATED_TIMEOUT!
	 */
	const LAST_SESS_REGENERATED = '__last_sess_regenerated';

	public $rightsReturnUrl;


	public function afterLogin($fromCookie)
	{
		parent::afterLogin($fromCookie);

		/**Обновляем дату последнего посещения*/
		$user = User::model()->active()->findByPk( $this->getId() );

		if (!empty($user)) {
			$user->lastvisit = new CDbExpression('NOW()');
			$user->save();
		}


		/**
		 * Если авторизовываемся по куке то восстанавливаем данные пользователя
		 */
		if ($fromCookie) {

			$identity = new UserIdentity("", "");
			$identity->saveUserDataInSession($user);
		}
	}

	public function afterLogout()
	{
		parent::afterLogout();
	}

	//

    /**
     * Админ ли текущий пользователь или нет?
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return isset($this->superuser) ? (boolean)intval($this->superuser) : false;
    }

    /**
     * Перекрываем стандартный метод проверки доступа и заменяем его на свой, более простой и понятный!
     *
     * @param string $operation
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function checkAccess($operation, $params=array(), $allowCaching=true)
    {
        return parent::checkAccess($operation, $params, $allowCaching);
    }

    /**
     * Получить id всех привязанных аккаунтов пользователя:
     * как старых, так и текущего.
     *
     * @return array
     * @throws Exception
     */
    public function getAllLinkedAccountIds() {

        $_current_user_id = (int)Yii::app()->user->id;

        if (empty($_current_user_id)) {
	        return null;
            //throw new Exception("You try use <getOldAccountIds> method but the user is not authorized!");
        }

        $command  = Yii::app()->db->createCommand(
            "SELECT `ls`.`old_user_id`
             FROM `tv_user_linked_socials` `ls`
             WHERE `ls`.`user_id` = :cur_user_id AND `ls`.`old_user_id` IS NOT NULL
            "
        );

        $ret = $command->queryColumn(array(
            ':cur_user_id'=>$_current_user_id
        ));

        $ret = array_merge(
            array($_current_user_id),
            array_map(function($item){return (int)$item;}, $ret)
        );

        return $ret;
    }

	/**************************************************************************/

	/**
	 * Перекрывает стандартный Init() на свой.
	 * Особенности:
	 * (1) В случае, если время последней регенерации сессии > порога self::SID_REGENERATED_TIMEOUT,
	 * генерируем новый SID!
	 */
	public function init()
	{
		$this->setStateKeyPrefix('KAVPOLIT_PROJECT');

		CApplicationComponent::init();

		Yii::app()->getSession()->open();

		if($this->getIsGuest() && $this->allowAutoLogin)
		{
			$this->restoreFromCookie();
		}
		else
		{
			if ($this->autoRenewCookie && $this->allowAutoLogin) {

				/**
				 * Принудительно регенирируем SID каждый час!
				 */
				if( time() - (int)$this->getState(self::LAST_SESS_REGENERATED) > self::SID_REGENERATED_TIMEOUT )
				{
					$app=Yii::app();
					$request=$app->getRequest();
					$cookie=$request->getCookies()->itemAt($this->getStateKeyPrefix());
					if($cookie && !empty($cookie->value) && is_string($cookie->value) && ($data=$app->getSecurityManager()->validateData($cookie->value))!==false)
					{
						$data=@unserialize($data);
						if(is_array($data) && isset($data[0],$data[1],$data[2],$data[3]))
						{
							list($id,$name,$duration,$states) = $data;
							$this->changeIdentity($id, $name, $states);
						}
					}
				}

				$this->renewCookie();
			}
		}

		if ($this->autoUpdateFlash) {
			$this->updateFlash();
		}

		$this->updateAuthStatus();
	}

	/**
	 * Перегружаем базовый changeIdentity.
	 * Добавляем парамер LAST_SESS_REGENERATED, чтобы перегенерировать SID
	 * каждый указанный в настройках промежуток времени.
	 *
	 * @param mixed $id
	 * @param string $name
	 * @param array $states
	 */
	protected function changeIdentity($id, $name, $states)
	{
		Yii::app()->getSession()->regenerateID(true);
		$this->setId($id);
		$this->setName($name);
		$states[self::LAST_SESS_REGENERATED] = time(); //Сохраняем время генерации текущего SID
		$this->loadIdentityStates($states);
	}

	/**
	 * Недостаточно просто продлить время экспирации куки, надо ещё
	 * и сохранить в ней нужные данные, чтобы не получилась ситуация,
	 * когда у нас некие данные поменялись, а в куках их старая версия
	 * и после выполения restoreFromCookie мы получаем сессию со старыми данными,
	 * которые были при первом вызове saveToCookie.
	 */
	protected function renewCookie()
	{
		$this->saveToCookie( RainDbHttpSession::REMEMBER_ME_COOKIE_LIFE_TIME );
	}

	/**
	 * Чтобы сделать "запоминательную куку" http-only, надо перекрыть
	 * метод который отвечает за её сохранение в базовом классе и
	 * дописать всего одну строку: $cookie->httpOnly = true;
	 *
	 * @param int $duration - время на которое выставляем запоминательную куку!
	 */
	protected function saveToCookie($duration)
	{
		$app=Yii::app();
		$cookie=$this->createIdentityCookie($this->getStateKeyPrefix());
		$cookie->httpOnly = true;
		$cookie->expire=time()+$duration;
		$data=array(
			$this->getId(),
			$this->getName(),
			$duration,
			$this->saveIdentityStates(),
		);
		$cookie->value=$app->getSecurityManager()->hashData(serialize($data));
		$app->getRequest()->getCookies()->add($cookie->name,$cookie);
	}
}