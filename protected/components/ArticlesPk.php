<?
/**
 * Реализует хранение списка primary key модели Articles в Yii::app()->params['Articles']['selected_ids']
 * Используется для передачи уже выбранных primary key между виджетами или контроллерами
 * Т.е. если мы имеем два списка на странице, и во втором списке нужно _не_ выводить статьи из первого, то используем для передачи данных этот класс
 * ПОМНИ: Set надо обязательно делать через временную переменную, иначе yii ругается на неправильное обращение к свойствам
 */
class ArticlesPk
{

	/**
	 * Добавляет Indexed массив Activerecord, обычный массив чисел или число в глобальную переменную Yii::app()->params['Articles']['selected_ids']
	 */
	public static function add($value)
	{
		//Если переменной нет, заводим
		if(!isset(Yii::app()->params['Articles']['selected_ids']))		
			Yii::app()->params['Articles'] = array('selected_ids' => array());			
		
		if(is_array($value))
			self::addArray($value);
		elseif(is_numeric($value))
			self::addInt($value);
		else
			throw new Exception('Неподдерживаемый формат данных');
			
	}	
	
	private static function addArray(array $value)
	{
		//Если обычный объект
		if(isset($value[0]))
		{
			if(is_object($value[0]))
				throw new Exception('Необходимо проиндексировать объекты activerecord по primary key id(для Articles просто добавить scope Indexed() )');
				
			self::setParam($value);
		}
		//Если activerecord
		elseif(is_object(current($value))){
			self::setParam(array_keys($value));
		}
	}
	
	private static function addInt($value)
	{
		self::setParam(array(intval($value)));
	}
	
	private static function setParam($final_value)
	{
		
		$selected_ids = array_merge(Yii::app()->params['Articles']['selected_ids'], $final_value);
		$selected_ids = array_unique($selected_ids);			
		Yii::app()->params['Articles'] = array('selected_ids' => $selected_ids);		
	}
	
	/*
	 * Возвращает массив
	 */
	public static function getArray()
	{
		if(count(Yii::app()->params['Articles']['selected_ids']) < 1)
			ArticlesPk::add(array(0));
		
		return Yii::app()->params['Articles']['selected_ids'];
	}
}
?>