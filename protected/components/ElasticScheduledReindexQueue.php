<?php
class ElasticScheduledReindexQueue extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_elastic_sheduled_reindex';
	}

	public function rules()
	{
		$rules = array(
			array('type, entity_id, reindex_date', 'required'),
			array('entity_id', 'numerical', 'integerOnly' => true),
			array('type, reindex_date', 'length', 'max' => 100),
		);

		return $rules;
	}


	public static function addTask($type, $entity_id, $reindex_date)
	{
		try
		{
			$sql = "
					INSERT INTO bg_elastic_sheduled_reindex
					(type, entity_id, reindex_date)
					VALUES
					('" . $type . "', " . $entity_id . ", '" . $reindex_date . "' )
					ON DUPLICATE KEY UPDATE entity_id = entity_id";

			$command = Yii::app()->db->createCommand($sql);
			$command->execute();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
			Yii::log('ElasticScheduledReindexQueue:addTask ' . $e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}


	public function apply()
	{
		try 
		{
			if ($this->type == 'posts')
			{
				$e = Post::model()->findByPk($this->entity_id);
				if (!empty($e))
				{
					$e->name = $e->name;
					$e->save();
				}
			}

			if ($this->type == 'articles')
			{
				$e = Articles::model()->findByPk($this->entity_id);
				if (!empty($e))
				{
					$e->name = $e->name;
					$e->save();
				}
			}

			$this->delete();
		}
		catch (Exception $e)
		{
			Yii::log('ElasticScheduledReindexQueue:apply ' . $e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}
}