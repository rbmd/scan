<?php $this->breadcrumbs = array(
	'Rights'=>Rights::getBaseUrl(),
	Rights::t('core', 'Assignments'),
); ?>

<div id="assignments">

	<h2><?php echo Rights::t('core', 'Assignments'); ?></h2>

	<p>
		<?php echo Rights::t('core', 'Here you can view which permissions has been assigned to each user.'); ?>
	</p>

    <p style="margin-top: 30px;">
        <?php echo CHtml::button(Rights::t('core', 'reset'), array('class' => 'btn btn-primary', 'onclick'=>'window.location.reload();')); ?>
    </p>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
	    'dataProvider'=>$dataProvider,
        'filter' => $model,
	    'template'=>"{items}\n{pager}",
	    'emptyText'=>Rights::t('core', 'No users found.'),
        'enableSorting'=>true,
	    'htmlOptions'=>array('class'=>'grid-view assignment-table'),
	    'columns'=>array(
            array(
                'name'=>'id',
                'header'=>Rights::t('core', 'ID'),
                'type'=>'raw',
                'htmlOptions'=>array('class'=>'name-column'),
                'value'=>'$data->id',
            ),
    		array(
    			'name'=>'username',
    			'header'=>Rights::t('core', 'UserName'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'name-column'),
    			'value'=>'$data->getAssignmentNameLink()',
    		),
            array(
                'name'=>'email',
                'header'=>Rights::t('core', 'EMail'),
                'type'=>'raw',
                'htmlOptions'=>array('class'=>'name-column'),
                'value'=>'$data->email',
            ),
            array(
                'name'=>'firstname',
                'header'=>Rights::t('core', 'FirstName'),
                'type'=>'raw',
                'htmlOptions'=>array('class'=>'name-column'),
                'value'=>'$data->firstname',
            ),
            array(
                'name'=>'lastname',
                'header'=>Rights::t('core', 'LastName'),
                'type'=>'raw',
                'htmlOptions'=>array('class'=>'name-column'),
                'value'=>'$data->lastname',
            ),
    		array(
    			'name'=>'assignments',
    			'header'=>Rights::t('core', 'Roles'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'role-column'),
    			'value'=>'$data->getAssignmentsText(CAuthItem::TYPE_ROLE)',
                'filter' => false,
    		),
			/*array(
    			'name'=>'assignments',
    			'header'=>Rights::t('core', 'Tasks'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'task-column'),
    			'value'=>'$data->getAssignmentsText(CAuthItem::TYPE_TASK)',
                'filter' => false,
    		),*/
			array(
    			'name'=>'assignments',
    			'header'=>Rights::t('core', 'Operations'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'operation-column'),
    			'value'=>'$data->getAssignmentsText(CAuthItem::TYPE_OPERATION)',
                'filter' => false,
    		),
	    )
	)); ?>

</div>