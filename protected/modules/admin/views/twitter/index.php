<a href="/admin/twitter/edit/" class="btn btn-success">Создать сообщение</a>
<br/><br/>
<table class="table table-bordered">
    <thead>
    <tr>
        <th style="text-align: center;"><b>#</b></th>
        <th style="text-align: center;"><b>Сообщение</b></th>
        <th style="text-align: center;"><b>Создатель</b></th>
        <th style="text-align: center;"><b>Последнее редактирование</b></th>
        <th style="text-align: center;"><b>Отправлено</b></th>
        <th style="text-align: center;"><b>Дата создания</b></th>
        <th style="text-align: center;"><b>Отправить</b></th>
    </tr>
    </thead>
    <?foreach($messages as $m):?>
        <tr <?if( !empty($m['send_date']) ):?>style="color: #808080;"<?endif?>>
            <td style="text-align: center;"><a href="/admin/twitter/edit/id/<?=$m['id']?>/" title="Редактировать"><?=$m['id']?></a></td>
            <td style="text-align: left;"><?=$m->getMsg(100)?></td>
            <td style="text-align: center;">
                <?=$m->creator->id?>,
                <?=$m->creator->lastname;?> <?=$m->creator->firstname;?>
            </td>
            <td style="text-align: center;">
                <?if( !empty($m['edit_user_id']) ):?>
                    <?=$m->editor->id?>,
                    <?=$m->editor->lastname;?> <?=$m->editor->firstname;?>
                    в <?=$m['x_timestamp']?>
                <?else:?>
                    ---
                <?endif?>
            </td>
            <td style="text-align: center;">
                <?if( !empty($m['send_date']) ):?>
                    <?=$m['send_date']?>
                <?else:?>
                    ---
                <?endif?>
            </td>
            <td style="text-align: center;"><?=$m['create_date']?></td>
            <td style="text-align: center;">
                <? echo CHtml::ajaxButton(
                    'Отправить в twitter',
                    '/admin/twitter/send/',
                    array(
                        'data' => "id={$m['id']}",
                        'beforeSend' => "function(){\$('#delete_btn_".$m["id"]."').hide(); \$('#loader_".$m["id"]."').show();return true;}",
                        'complete' => "function(){\$('#delete_btn_".$m["id"]."').show();\$('#loader_".$m["id"]."').hide();}",
                        'success' => 'function(){alert("Сообщение успешно отправлено!"); window.location.href="/admin/twitter/";}',
                        'error' => 'function(){alert("Во время отправки сообщения произошла непредвиденная ошибка!");}',
                    ),
                    array(
                        'class' => 'btn btn-primary',
                        'id'=>'delete_btn_' . $m['id'],
                        'confirm'=> empty($m['send_date']) ? 'Вы уверены, что хотите отправить это сообщение?' : 'Это сообщение уже отправлялось в twitter. Повторить отправку?',
                    )
                ); ?>
                <br/>
                <?php echo CHtml::image(
                    '/static/css/admin/images/ajax-loader.gif',
                    'progress...',
                    array('id'=>'loader_'.$m['id'], 'style'=>'display:none;')
            ); ?>
                <br/>
            </td>
        </tr>
    <?endforeach?>
</table>