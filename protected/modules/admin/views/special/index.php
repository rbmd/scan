<a href="/admin/special/create/" class="btn btn-success">Создать спецпроект</a>
<br/><br/>
<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/special/')), 'get'); ?>
<div class="well">
  <table class="b-search-params">
    <tr>
      <td class="b-search-params__value">
        <label for="id" class="text inline">
          ID <? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?>
        </label>

        <label for="name" class="text inline">
          Название <? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?>
        </label>
      </td>
      <td><? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?></td>
    </tr>
  </table>
</div>
<? echo CHtml::endForm(); ?>
<table class="table">
  <thead>
  	<tr>
  		<th>ID</th>
  		<th>&nbsp;</th>
  		<th>Название</th>
  		<th>Ссылка</hd>
	</tr>
  </thead>
	<?
	foreach($special as $st)
	{
	?>
	<tr>
		<td><?=$st['id']?></td>
		<td><?if(strlen($st->image) > 0){?><img src="<?=$st->image?>" width="70"/><?}?></td>
		<td><a href="/admin/special/edit/?id=<?=$st['id']?>"><?=$st->name?></a></td>
		<td><a href="<?=$st->link?>" target="_blank"><?=$st->link?></a></td>
	</tr>
	<?
	}
	?>
	
</table>
<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>