<a href="/admin/tags/create/" class="btn btn-success">Создать тег</a>
<br/><br/>
<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/tags/')), 'get'); ?>
<div class="well">
    <table class="b-search-params">
        <tr>
            <td class="b-search-params__option"><? echo CHtml::Label('ID', 'id'); ?></td>
            <td class="b-search-params__value"><? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?></td>
        </tr>
        <tr>
            <td class="b-search-params__option"><? echo CHtml::Label('Название', 'name'); ?></td>
            <td class="b-search-params__value"><? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?></td>
        </tr>
        <tr>
            <td class="b-search-params__action" colspan="2"><? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?></td>
        </tr>
    </table>
</div>
<? echo CHtml::endForm(); ?>
<table class="table">
    <thead>
        <th>ID</th>
        <th>Название</th>
    </thead>
<?
foreach($tags as $n)
{
?>
	<tr>
		<td>#<?=$n['id']?></td>
		<td><a href="/admin/tags/edit/?id=<?=$n['id']?>"><?=$n['name']?></a></td>		 
	</tr>
<?
}
?>
</table>
<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>