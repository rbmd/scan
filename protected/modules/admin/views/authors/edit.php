<h1><?
if($action == 'create')
{
	?>Создание автора<?
}
else
{
	?>Автор #<?=$model->id?> <?=$model->name?><?
}
?>
</h1>

<div class="span10">
	<div class="tab-content">
		<div class="tab-pane active" id="content">

		<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'authors-form',
			'enableClientValidation' => true,
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,	
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		)); 
		?>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->checkBox($model, 'active') ?>
							<span class="help-inline"><?= $form->error($model, 'active') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'surname', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'surname', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'surname') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'email', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'email', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'email') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'gender', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->radioButtonList($model, 'gender', array(0 => ' Женский', 1 => ' Мужской'), array('style' => 'float: left; margin-right: 5px;', 'separator' => '')) ?>
							<span class="help-inline"><?= $form->error($model, 'gender') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'types', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?php
							$currentValues = CHtml::listData($model->tree, 'id', 'name');
							array_walk($currentValues, function(&$v) {return $v = array('selected' => true);});
							?>
							<?= $form->listBox($model, 'types', Tree::model()->getAuthorsTypes(), array('multiple' => 1, 'options' => $currentValues)) ?>
							<span class="help-inline"><?= $form->error($model, 'types') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label"><?= $form->labelEx($model, 'user_id', array('class' => 'control-label')) ?></label>
				<div class="controls">
					<?php
					$this->widget('AutocompleteWidget', array(
						'data'     => $model->User,
						'name'     => 'Authors[user_id]',
						'url'      => '/admin/users/getbyname/',
					));?>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<div class="input-append"><?
								echo $form->textField($model, 'code', array('data-sync-from' => 'slug', 'class' => 'span7'));
								$cookie_var = Yii::app()->request->cookies['transliterate_lock_state'];
								?><button data-action="trigger-lock" data-sync-to="slug" class="btn icon" onclick="dynamicClick($(this)); return false;"><?= isset($cookie_var->value[ Yii::app()->request->requestUri ]) ? $cookie_var->value[ Yii::app()->request->requestUri ] : 'w' ?></button>
							</div>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_title', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_title', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_title') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_keywords', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_keywords', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_keywords') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_description', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textArea($model, 'seo_description', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_description') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'link', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'link', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'link') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'link_text', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'link_text', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'link_text') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'preview_img', array('class' => 'control-label')); ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textField($model, 'preview_img', array('class' => 'span6'))?>
							<span class="help-inline"><?php echo $form->error($model, 'preview_img'); ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'image', array('class' => 'control-label')); ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textField($model, 'image', array('class' => 'span6'))?>
							<span class="help-inline"><?php echo $form->error($model, 'image'); ?></span>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				$(function() {
					ckfinderManager.create('<?=get_class($model)?>_image');
					ckfinderManager.create('<?=get_class($model)?>_preview_img');
				});
			</script>

			<div class="control-group">
				<?= $form->labelEx($model, 'detail_text', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textArea($model,'detail_text');?>		
							<script>
								$(document).ready(function(){
									wysiwygManager.createEditor('Authors_detail_text', 'default');
								});
							</script>								
							
							<span class="help-inline"><?= $form->error($model, 'detail_text') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'social', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'social', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'social') ?></span>
						</div>
					</div>
				</div>
			</div>
		</fieldset>

		<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>


		<h2>Статьи автора</h2>
		<?php
		foreach ($model->articles as $article)
		{?>
			<a href="/admin/articles/edit/?id=<?=$article->id?>"><?=$article->name?></a> <?=$article->date_publish?><br/>
		<?}
		?>

		<div class="form-actions">
			<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')) ?>
			<?= CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)) ?>
			<?= CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')) ?>
			<div style="float:right;">
				<? echo CHtml::ajaxButton(
					'Удалить',
					'/admin/authors/delete',
					array(
						'data' => "id={$model->id}",
						'beforeSend' => 'function(){}',
						'complete' => 'function(){}',
						'success' => 'function(){alert("Автор успешно удалён!"); window.location.href="/admin/authors";}',
						'error' => 'function(){alert("Во время удаления автора произошла непредвиденная ошибка!");}',
					),
					array(
						'class' => 'btn btn-danger',
						'id'=>'delete_btn',
						'confirm'=>'Вы уверены, что хотите безвозвратно удалить этого автора?',
					)
				); ?>
			</div>
		</div>
 
		<? $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_authors', '
	$("#cancel_btn").click( function(e) {
		window.location.href = "/admin/authors/";
	});
', CClientScript::POS_READY)
?>