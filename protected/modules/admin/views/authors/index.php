<a href="/admin/authors/create/" class="btn btn-success">Создать автора</a>
<br/><br/>
<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/authors/')), 'get'); ?>
<div class="well">
  <table class="b-search-params">
    <tr>
      <td class="b-search-params__option"><? echo CHtml::Label('ID', 'id'); ?></td>
      <td class="b-search-params__value"><? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?></td>
    </tr>
    <tr>
      <td class="b-search-params__option"><? echo CHtml::Label('Имя', 'name'); ?></td>
      <td class="b-search-params__value"><? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?></td>
    </tr>
    <tr>
     <td class="b-search-params__option"><? echo CHtml::Label('Фамилия', 'surname'); ?></td>
     <td class="b-search-params__value"><? echo CHtml::textField('surname', urldecode(Yii::app()->request->getParam('surname',''))); ?></td>
    </tr>
    <tr>
      <td class="b-search-params__action" colspan="2"><? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?></td>
    </tr>
  </table>
</div>
<? echo CHtml::endForm(); ?>

<table class="table">
  <thead>
  	<tr>
  		<th>&nbsp;</th>
  		<th>ID</th>
  		<th>&nbsp;</th>
  		<th>Название</th>
  		<th>Символьный код</th>
  	</tr>
  </thead>
	<?
	foreach($authors as $n)
	{
	?>
	<tr>		
		<td><span class="label label-<?=(($n['active'] == 1) ? 'success' : 'important')?>">&nbsp;&nbsp;</span></td>
		<td><?=$n['id']?></td>		
		<td><?if(strlen($n->preview_img) > 0){?><img src="<?=$n->preview_img?>" width="70"/><?}?></td>		
		<td><a href="/admin/authors/edit/?id=<?=$n['id']?>"><?=$n->fullname()?></a></td>
		<td><?=$n['code']?></td>
	</tr>
	<?
	}
	?>
</table>

<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>