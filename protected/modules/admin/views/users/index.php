<h2>Управление пользователями</h2>

<br/>

<?php echo CHtml::beginForm('', 'get', array()); ?>



    <?php echo CHtml::errorSummary($model); ?>
    <div class="" style="margin-bottom: 20px;">
        <input class="btn btn-success" type="button" name="yt0" value="Создать пользователя" onclick="window.location='/admin/users/create'" />
    </div>
    
    <div class="well">

        <table class="b-search-params">
        <tr>
            <td class="b-search-params__option">
                <?php echo CHtml::activeLabel($model, 'id'); ?>
            </td>
            <td class="b-search-params__value">
                 <?php echo CHtml::activeTextField($model, 'id'); ?>
                <?php echo CHtml::error($model, 'id'); ?>
            </td>
        </tr>
        <tr>
            <td class="b-search-params__option">
                <?php echo CHtml::activeLabel($model, 'email'); ?>
            </td>
            <td class="b-search-params__value">
                <?php echo CHtml::activeTextField($model, 'email'); ?>
                <?php echo CHtml::error($model, 'email'); ?>
            </td>
        </tr>    
        <tr>
            <td class="b-search-params__option">
                <?php echo CHtml::activeLabel($model, 'username'); ?>
            </td>
            <td class="b-search-params__value">
                <?php echo CHtml::activeTextField($model, 'username'); ?>
                <?php echo CHtml::error($model, 'username'); ?>
            </td>
        </tr>    
        <tr>
            <td class="b-search-params__option">
                <?php echo CHtml::activeLabel($model, 'lastname'); ?>
            </td>
            <td class="b-search-params__value">
                <?php echo CHtml::activeTextField($model, 'lastname'); ?>
                <?php echo CHtml::error($model, 'lastname'); ?>
            </td>
        </tr>   
        <tr>
            <td class="b-search-params__option">
                <?php echo CHtml::activeLabel($model, 'firstname'); ?>
            </td>
            <td class="b-search-params__value">

                <?php echo CHtml::activeTextField($model, 'firstname'); ?>
                <?php echo CHtml::error($model, 'firstname'); ?>
            </td>
        </tr> 
          
        <tr>
            <td class="b-search-params__option">
                <label for="User_rule">Роль</label>
            </td>
            <td class="b-search-params__value">
                <?php
                $sql = 'SELECT * FROM AuthItem';
                $data = CHtml::listData(Yii::app()->db->createCommand($sql)->queryAll(), 'name', 'description');
                array_unshift($data, 'Все');
                echo CHtml::DropDownList(get_class($model) . '[rule]', isset($_GET['User']['rule']) ? $_GET['User']['rule'] : '', $data, array('class'=>'b-admin__select'));
                ?>
            </td>
        </tr> 
        <tr>
            <td colspan="2" class="b-search-params__value">
                <label for="superuser" class="checkbox">                
                    <?=CHtml::activeCheckBox($model, 'superuser')?> Админ
                </label>
            </td>
        </tr>      
        <tr>
            <td colspan="2" class="b-search-params__action">
                <?php echo CHtml::submitButton('Искать', array('class' => 'btn btn-primary')); ?>
                <?php echo CHtml::button('Сбросить', array(
                    'onclick' => 'window.location.href="/admin/users/";return false;',
                    'class'   => 'btn btn-inverse'
                )); ?> 
            </td>
        </tr>
        </table>

    </div>    

    <table class="table">
        <thead>
            <tr>
                <td style="text-align: center;">
                    <?if( $sort->getDirection('id') !== null ):?>
                        <?if( $sort->getDirection('id') ):?>
                            <i class="icon-arrow-down"></i>
                        <?else:?>
                            <i class="icon-arrow-up"></i>
                        <?endif?>
                    <?endif?>
                    <?=$sort->link('id', '#')?>
                </td>
    			<td style="text-align: center;"><b>Статус</b></td>
                <td>
                    <?if( $sort->getDirection('username') !== null ):?>
                        <?if( $sort->getDirection('username') ):?>
                            <i class="icon-arrow-down"></i>
                        <?else:?>
                            <i class="icon-arrow-up"></i>
                        <?endif?>
                    <?endif?>
                    <?=$sort->link('username', 'Логин')?>
                </td>
                <td>
                    <?if( $sort->getDirection('email') !== null ):?>
                        <?if( $sort->getDirection('email') ):?>
                            <i class="icon-arrow-down"></i>
                        <?else:?>
                            <i class="icon-arrow-up"></i>
                        <?endif?>
                    <?endif?>
                    <?=$sort->link('email', 'Email')?>
                </td>
                <td>
                    <?if( $sort->getDirection('firstname') !== null ):?>
                        <?if( $sort->getDirection('firstname') ):?>
                            <i class="icon-arrow-down"></i>
                        <?else:?>
                            <i class="icon-arrow-up"></i>
                        <?endif?>
                    <?endif?>
                    <?=$sort->link('firstname', 'Имя')?>
                </td>
                <td>
                    <?if( $sort->getDirection('lastname') !== null ):?>
                        <?if( $sort->getDirection('lastname') ):?>
                                <i class="icon-arrow-down"></i>
                            <?else:?>
                                <i class="icon-arrow-up"></i>
                            <?endif?>
                        <?endif?>
                    <?=$sort->link('lastname', 'Фамилия')?>
                </td>
                <td><b>Время создания</b></td>
                <td><b>Последнее посещение</b></td>
                <td style="text-align: center;"><b>Админ</b></td>
    		    <td><b>Роли</b></td>
                <td><b>Войти под пользователем</b></td>
            </tr>
        </thead>

        <? foreach($users as $user) : ?>
        <tr>
            <td style="text-align: center;">
                <?=CHtml::link($user['id'], '/admin/users/edit/id/'.$user['id'], array())?>
            </td>
			  <td style="text-align: center;">
				  <? if($user['status'] == 1) : ?><i class="icon-ok">
				  <? elseif($user['status'] == 0) : ?><i class="icon-remove">
				  <? else : ?><?=$user['status']?><?endif?>
			  </td>
            <td>
                <?=$user['username']?>
            </td>
            <td>
                <?=$user['email']?>
            </td>
            <td>
                <?=$user['firstname']?>
            </td>
            <td>
                <? if(!empty($user['lastname'])) : ?> <?=$user['lastname']?> <? else : ?>---<?endif?>
            </td>
            <td>
                <?=$user['createtime']?>
            </td>
            <td>
                <?=$user['lastvisit']?>
            </td>
            <td style="text-align: center;">
                <? if(!empty($user['superuser'])) : ?><b>ДА</b><? else : ?>Нет<?endif?>
            </td>
            <td>
				<a target="_blank" href="/admin/rights/assignment/user/?id=<?=$user->id?>">
					<? if(!empty($user->rules)) : ?><?=$user->rules?><? else : ?>—<?endif?>
				</a>
            </td>
            <td>
                <? if($user['id'] == Yii::app()->user->id) : ?>
                    Это Вы
                <? else : ?>
                    <? if( Yii::app()->user->checkAccess('Admin') && Yii::app()->user->checkAccess('canAuthorizeAsAnyUser') ) : ?>
                        <?php echo CHtml::link('Войти', "/admin/default/authby/userid/{$user['id']}"); ?>
                    <? else : ?>
                        Недостаточно прав
                    <? endif ?>
                <? endif ?>
            </td>
        </tr>
        <? endforeach ?>

    </table>

    <?$this->widget('CLinkPager', array(
        'pages' => $pages,
    ))?>

    <br/><br/>
<?php echo CHtml::endForm(); ?>