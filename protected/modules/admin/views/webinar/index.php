<h3>Параметры вебинара на главной странице</h3>

<div style="width: 400px; float: left">
	<?php $form=$this->beginWidget('CActiveForm'); ?>
	
		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'webinar_id') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'webinar_id') ?>
			</div>
		</div>


		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'comments_xid') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'comments_xid') ?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'announce_title') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'announce_title') ?>
			</div>
		</div>		

		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'announce_pic') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'announce_pic') ?>
			</div>
		</div>
		<script type="text/javascript">
			$(function() {
				ckfinderManager.create('<?=get_class($webinar)?>_announce_pic');
			});
		</script>

		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'announce_month') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'announce_month') ?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'announce_day') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'announce_day') ?>
			</div>
		</div>	
		
		<div class="control-group">
			<label class="control-label"><?= $form->labelEx($webinar, 'announce_time') ?></label>
			<div class="controls">
				<?= $form->textField($webinar, 'announce_time') ?>
			</div>
		</div>		
		

		<input type="submit" name="save" value="Сохранить" />
	<?php $this->endWidget(); ?>
</div>
