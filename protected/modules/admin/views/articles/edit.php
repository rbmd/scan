<div id="wysiwyg_is_loading" class="alert">
	Загрузка визредактора...
</div>
<script type="text/javascript">
	var article_id = <?=$model->article_id?>;
</script>
<p id="article_id" style="display:none;"><?=$model->article_id?></p>
<p id="article_draft_id" style="display:none;"><?=$model->id?></p>
<div style="height: 30px">
</div>

<?
$model_name = get_class($model);
if ($action == 'create')
{
	//Если true, значит скрываем большую часть полей
	$create = true;

	?>
<h1>Создание статьи</h1>
<span class="label label-warning">После заполнения вкладки "Основные" нажмите "Сохранить". Тогда станут доступны все остальные поля.</span>
<br/><br/>
<?
}
else
{
	$create = false;
	?>
<h1><?#=$model->name?></h1>

<?
}
?>

<?
if (!empty($lock))
{
	?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">×</a>
	Пользователь <?= CHtml::encode($lock->user_name); ?>
	(<?= $lock->user_id; ?>) заблокировал статью
	<?=date('d-m-Y', strtotime($lock->lock_date))?> в
	<?=date('H:i', strtotime($lock->lock_date))?>!
	<br/>
	Автоматическая разблокировка <?=date("d-m-Y в H:i", strtotime($lock->lock_date) + 600)?>.
</div>
<?
}
else
{
	?>
	<div class="alert" id="alert" style="display:none;"></div>
<script type="text/javascript">
	var lockarticle = false;
	var createpage = '<?=date("H:i:s d.m.Y")?>';
</script>
<?php
}
?>

<? if ($action == 'hist' || !empty($lock))
{
	?>
<script type="text/javascript">
	var lockarticle = true;
</script>
<?
}
$form = $this->beginWidget('CActiveForm', array(
	'action'                 => !empty($model->id) ? "?id={$model->id}" : '',
	'id'                     => 'articles-form',
	'enableClientValidation' => true,
	'enableAjaxValidation'   => true,
	'clientOptions'          => array(
		'validateOnSubmit' => true,
	),
	'htmlOptions'            => array(
		'class' => 'form-horizontal',
	),
));
?>


<div class="article_control_menu b-submenu">
	<div class="menu b-menu">
		<div class="b-menu__col">
			<div class="btn-group" data-toggle="buttons-radio">
				<button type="button" class="btn active" pane="generic">Основные</button>
				<button type="button" class="btn" pane="chapters">Главы</button>
				<button type="button" class="btn" pane="relations">Связи</button>
				<button type="button" class="btn" pane="history">История</button>
			</div>
		</div>
		<div class="b-menu__col">
			<?if (empty($lock->user_name))
		{ ?>
			<?
			if ($action == 'hist')
			{
				?>
				<?= CHtml::button('Восстановить', array('class' => 'btn btn-inverse', 'onclick' => 'location.href="/admin/articles/restore/?id=' . $model->id . '"', 'data-id' => $model->article_id))
				; ?>
				<?
			}
			else
			{
				$ed_class = 'noedited';
				if ($model->edited)
				{
					$ed_class = '';
				}
				?>
				<span class="btn btn-warning <?=$ed_class?>" data-draft_id="<?=$model->id?>" id="edited" title="Отменить изменения">
					<i class="icon-reply"></i>
				</span>
				<?=CHtml::button('Сохранить', array('class' => 'btn btn-success article_ajax_save', 'style' => 'margin: 0 5px 0 10px;', 'data-id' => $model->article_id))?>
				<?
			}
			?>
		<? }?>
			<?=CHtml::link('Preview', '/preview/' . $model->article_id, array('class' => 'btn btn-inverse article_preview', 'target' => '_blank'))?>

		</div>
		<div class="b-menu__col b-menu__col_corrector">
			<? if (empty($lock->user_name)){?>
				<label for="corrector_mode" class="b-menu__corrector-mode"><input type="checkbox" id="corrector_mode" style="margin:0" /> — Режим корректора</label>
			<?}?>
		</div>
		<div class="b-menu__col b-menu__col_right">
			<?= CHtml::button('Выйти', array('class' => 'btn', 'id' => 'cancel_btn')) ?>
		</div>
	</div>

	<?
	/*
	if (empty($lock->user_name))
	{
		if ($model->id > 0)
		{
			if ($action == 'hist')
			{
				#echo CHtml::button('Восстановить', array('class' => 'btn btn-mini btn-inverse', 'onclick' => 'location.href="/admin/articles/restore/?id=' . $model->id . '"', 'data-id' => $model->article_id));
			}
			else
			{
				?>
				<?#= CHtml::button('Сохранить', array('class' => 'btn btn-mini btn-inverse article_ajax_save', 'data-id' => $model->article_id)) ?>
				<?#= CHtml::link('Preview', '/preview' . $model->url(), array('class' => 'btn btn-primary btn-mini article_preview', 'target' => '_blank'))
				; ?>
				<? }
		}
		else
		{ ?>
			<?#= CHtml::submitButton('Сохранить', array('class' => 'btn btn-mini btn-inverse', 'name' => 'apply',)) ?>
			<script>
				$(document).ready(function() {
					$('#chapter_add_btn').trigger('click');
				});
			</script>
			<? } ?>


		<?#= CHtml::button('+Глава', array('class' => 'btn btn-mini', 'id' => 'chapter_add_btn')) ?>

		<? #= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success btn-mini')) ?>
		<?
	}
	?>
	<?#= CHtml::button('Выйти', array('class' => 'btn btn-mini', 'id' => 'cancel_btn'))

	*/
	?>

</div>



<div class="row-fluid">
<div class="tab-content">

<div class="tab-pane active" id="generic">

<div class="control-group">
	<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>
	<div class="controls">
		<?
		$params = array();
		//
		if (Yii::app()->user->checkAccess('author') )
		{
			$params = array('disabled' => 'disabled');
		}
		?>
		<?= $form->checkBox($model, 'active', $params) ?>
		<span class="help-inline"><?= $form->error($model, 'active') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'proof_tested', array('class' => 'control-label')) ?>
	<div class="controls">
		<?
		$params = array();
		if (Yii::app()->user->checkAccess('author'))
		{
			$params = array('disabled' => 'disabled');
		}
		?>
		<?= $form->checkBox($model, 'proof_tested', $params) ?>
		<span class="help-inline"><?= $form->error($model, 'proof_tested') ?></span>
	</div>
</div>

<div class="control-group control-group_x2">
	<?=$form->labelEx($model, 'assignee', array('class' => 'control-label')) ?>
	<div class="controls">
		<?
		$data = User::getListForRole('editor');
		array_unshift($data, array('id' => 0, 'name' => 'Нет'));
		echo CHtml::DropDownList(get_class($model) . '[assignee]', $model->assignee, CHtml::listData($data, 'id', 'name'), array('class' => 'b-admin__select'))?>
		<span class="help-inline"><?= $form->error($model, 'assignee') ?></span>
	</div>
</div>

<div class="control-group control-group_x2">
	<?= $form->labelEx($model, 'comment', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span12">
			<?= $form->textField($model, 'comment', array()) ?>
		</div>
		<span class="help-inline"><?= $form->error($model, 'comment') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'data-limit-length' => 'name', 'class' => 'span8')) ?>
			<span data-limit-length="name" class="add-on"><?= mb_strlen($model->name) ?></span>
		</div>
		<span class="help-inline"><?= $form->error($model, 'name') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'seo_title', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'seo_title', array('data-limit-length' => 'seo_title', 'class' => 'span8')) ?>
			<span data-limit-length="seo_title" class="add-on"><?= mb_strlen($model->seo_title) ?></span>
		</div>
		<span class="help-inline"><?= $form->error($model, 'seo_title') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10"><?
			echo $form->textField($model, 'code', array('data-sync-from' => 'slug', 'class' => 'span8'));
			?>
			<button data-action="trigger-lock" data-sync-to="slug" class="btn icon icon-<?= $model->code ? 'lock' : 'unlock' ?>" onclick="dynamicClick($(this)); return false;"><?= $model->code ? 'x' : 'w' ?></button>
		</div>
		<span class="help-inline"><?= $form->error($model, 'code') ?></span>
	</div>
</div>


<div class="control-group">
	<?= $form->labelEx($model, 'preview_text', array('class' => 'control-label')) ?>
	<div class="controls">
		<?=$form->textArea($model, 'preview_text');?>
		<script>
			$(document).ready(function() {
				wysiwygManager.createEditor('<?=$model_name?>_preview_text', {config: 'mini.js'});
			});
		</script>
		<span class="help-inline"><?= $form->error($model, 'preview_text') ?></span>
	</div>
	<div class="controls">
		<span id="namePreviewtext_length" class="alert alert-success"></span>
		<script>
			//Считаем сумму символов в названии и анонсе
			$(document).ready(function() {
				$('#<?=$model_name?>_name').live('keyup', function() {
					showNamePreviewText_length();
				});

				var ckPredetail = CKEDITOR.instances['<?=$model_name?>_preview_text'];
				ckPredetail.on('change', showNamePreviewText_length);

				function showNamePreviewText_length() {
					var length = wysiwygManager.getCleanDataLength(CKEDITOR.instances['<?=$model_name?>_preview_text']) + $('#<?=$model_name?>_name').val().length;
					if (length > 160)
					{
						$('#namePreviewtext_length').removeClass('alert-success');
						$('#namePreviewtext_length').addClass('alert-error');
					}
					else
					{
						$('#namePreviewtext_length').removeClass('alert-error');
						$('#namePreviewtext_length').addClass('alert-success');
					}
					$('#namePreviewtext_length').html('Название+анонс: ' + length);
				}

				showNamePreviewText_length();
			});
		</script>
	</div>

</div>


<div class="control-group">
	<?= $form->labelEx($model, 'predetail_text', array('class' => 'control-label')) ?>
	<div class="controls">
		<?=$form->textArea($model, 'predetail_text');?>
		<script>
			$(document).ready(function() {
				 wysiwygManager.createEditor('<?=$model_name?>_predetail_text', {config: 'mini.js'});
			});
		</script>
		<span class="help-inline"><?= $form->error($model, 'predetail_text') ?></span>
	</div>
</div>


<?
if (!$create)
{
	?>

<?/*
<div class="control-group">
	<?= $form->labelEx($model, 'preview_img_show', array('class' => 'control-label')) ?>
	<div class="controls">
		<?= $form->checkBox($model, 'preview_img_show') ?>
		<span class="help-inline"><?= $form->error($model, 'preview_img_show') ?></span>
	</div>
</div>
*/?>

<div class="control-group preview_img">
	<?php echo $form->labelEx($model, 'preview_img', array('class' => 'control-label')); ?>
	<div class="controls">

		<?=$form->textField($model, 'preview_img', array('class' => 'b-admin__text'))?>
		<script type="text/javascript">
			$(function() {
				ckfinderManager.create('<?=get_class($model)?>_preview_img');
			});
		</script>

		<span class="help-inline"><?php echo $form->error($model, 'preview_img'); ?></span>
	</div>
    <div class="controls">
        <div class="crop_panel" style="display:block; margin:10px 0px;">
            <input type="button" id="crop_preview" value="Crop" class="btn btn-primary"/>
        </div>
        <div id="imgArticlesDraft_preview_img" style="display:none;">
			  <img src='#'/>
            <div class="bottom_crop_panel" style="display:block; width:200px; margin:10px 0px;">
                <input type="button" id="submit_crop" value="OK" class="btn btn-danger"/>
            </div>
        </div>
    </div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'preview_img_alt', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?
			echo $form->textField($model, 'preview_img_alt', array('class' => 'span8'));
			?>			
		</div>
		<span class="help-inline"><?= $form->error($model, 'preview_img_alt') ?></span>
	</div>
</div>

	<?
}
?>

<div class="control-group">
	<?= $form->labelEx($model, 'parent_id', array('class' => 'control-label')) ?>
	<div class="controls">
		<? $this->widget('ext.TreeCategoriesWidget.TreeCategoriesWidget', array(
			'model'              => $model,
			'attribute'          => 'parent_id',
			'htmlOptions'        => array('class' => 'b-admin__select'),
			'only_article_nodes' => true

		));?>
		<span class="help-inline"><?= $form->error($model, 'parent_id') ?></span>
	</div>
</div>

<!-- </td> -->

<?
//!!!!!!!!
if (0 && isset($magazines))
{
	?>
<div class="control-group">
	<?= $form->labelEx($model, 'magazine_id', array('class' => 'control-label')) ?>
	<div class="controls">
		<?echo CHtml::activeDropDownList($model, 'magazine_id', $magazines, array('class' => 'b-admin__select'));?>
		<span class="help-inline"><?= $form->error($model, 'magazine_id') ?></span>
	</div>
</div>
	<?
}
?>


<div class="control-group control-group_x2">
	<?= $form->labelEx($model, 'date_publish', array('class' => 'control-label')) ?>
	<div class="controls">
		<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
		'model'     => $model,
		'attribute' => 'date_publish',
		'mode'      => 'datetime',
		'language'  => 'ru',
		'options'   => array(
			//'dateFormat' => 'dd/mm/yy',
			'dateFormat' => 'yy-mm-dd',
			'timeFormat' => 'hh:mm:ss',
		),
	));?>
		<span class="help-inline"><?= $form->error($model, 'date_publish') ?></span>
	</div>
</div>

<div class="control-group control-group_x2">
	<?= $form->labelEx($model, 'date_active_start', array('class' => 'control-label')) ?>
	<div class="controls">
		<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
		'model'     => $model,
		'attribute' => 'date_active_start',
		'mode'      => 'datetime',
		'language'  => 'ru',
		'options'   => array(
			//'dateFormat' => 'dd/mm/yy',
			'dateFormat' => 'yy-mm-dd',
			'timeFormat' => 'hh:mm:ss',
		),
	));?>
		<span class="help-inline"><?= $form->error($model, 'date_active_start') ?></span>
	</div>
</div>

<?/*

<div class="control-group">
	<?= $form->labelEx($model, 'chapter_paging_type', array('class' => 'control-label')) ?>
	<div class="controls">
		<?echo CHtml::activeDropDownList($model, 'chapter_paging_type', $model->chapter_paging_types, array('class' => 'b-admin__select'));?>
		<span class="help-inline"><?= $form->error($model, 'chapter_paging_type') ?></span>
	</div>
</div>
*/?>

<div class="control-group">
	<?= $form->labelEx($model, 'show_main_announce', array('class' => 'control-label')) ?>

	<div class="controls">
		<?= $form->checkBox($model, 'show_main_announce') ?>
		<span class="help-inline"><?= $form->error($model, 'show_main_announce') ?></span>
	</div>
</div>


<div class="control-group">
	<?= $form->labelEx($model, 'show_on_indexpage', array('class' => 'control-label')) ?>

	<div class="controls">
		<?= $form->checkBox($model, 'show_on_indexpage') ?>
		<span class="help-inline"><?= $form->error($model, 'show_on_indexpage') ?></span>
	</div>
</div>

<?/*

<div class="control-group">
	<?= $form->labelEx($model, 'has_gallery', array('class' => 'control-label')) ?>
	<div class="controls">
		<?= $form->checkBox($model, 'has_gallery') ?>
		<span class="help-inline"><?= $form->error($model, 'has_gallery') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'has_video', array('class' => 'control-label')) ?>
	<div class="controls">
		<?= $form->checkBox($model, 'has_video') ?>
		<span class="help-inline"><?= $form->error($model, 'has_video') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'has_translation', array('class' => 'control-label')) ?>
	<div class="controls">
		<?= $form->checkBox($model, 'has_translation') ?>
		<span class="help-inline"><?= $form->error($model, 'has_translation') ?></span>
	</div>
</div>
*/?>

<div class="control-group">
	<?= $form->labelEx($model, 'to_rss', array('class' => 'control-label')) ?>
	<div class="controls">
		<?= $form->checkBox($model, 'to_rss') ?>
		<span class="help-inline"><?= $form->error($model, 'to_rss') ?></span>
	</div>
</div>




<?/*
<div class="control-group g-clear">
	<table class="bg-horizontal-table">
		<tr>
			<td>
				<?= $form->labelEx($model, 'show_main_announce', array('class' => 'control-label bg-control-label')) ?>

				<div class="bg-controls">
					<?= $form->checkBox($model, 'show_main_announce') ?>
					<span class="help-inline"><?= $form->error($model, 'show_main_announce') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'is_guide', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_guide', array('class' => 'isGuideChecker')) ?>
					<span class="help-inline"><?= $form->error($model, 'is_guide') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'is_news', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_news') ?>
					<span class="help-inline"><?= $form->error($model, 'is_news') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'is_mininews', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_mininews') ?>
					<span class="help-inline"><?= $form->error($model, 'is_mininews') ?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<?= $form->labelEx($model, 'show_main_announce_section', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'show_main_announce_section') ?>
					<span class="help-inline"><?= $form->error($model, 'show_main_announce_section') ?></span>
				</div>

			</td>
			<td>
				<?= $form->labelEx($model, 'has_gallery', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'has_gallery') ?>
					<span class="help-inline"><?= $form->error($model, 'has_gallery') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'has_video', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'has_video') ?>
					<span class="help-inline"><?= $form->error($model, 'has_video') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'is_archive', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_archive') ?>
					<span class="help-inline"><?= $form->error($model, 'is_archive') ?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<?= $form->labelEx($model, 'is_best', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_best') ?>
					<span class="help-inline"><?= $form->error($model, 'is_best') ?></span>
				</div>

			</td>
			<td>
				<?= $form->labelEx($model, 'to_rss', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'to_rss') ?>
					<span class="help-inline"><?= $form->error($model, 'to_rss') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'is_main_guide', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_main_guide', array('class' => 'isMainGuideChecker')) ?>
					<span class="help-inline"><?= $form->error($model, 'is_main_guide') ?></span>
				</div>
			</td>
			<td>
				<?= $form->labelEx($model, 'radiosvoboda', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'radiosvoboda', array('class' => 'radiosvobodaChecker')) ?>
					<span class="help-inline"><?= $form->error($model, 'radiosvoboda') ?></span>
				</div>
			</td>
		</tr>
		<?php
		$aExtra_params = json_decode($model->extra_params, true);
		?>
		<tr>
			<td>
				<?= $form->labelEx($model, 'is_howto', array('class' => 'control-label bg-control-label')) ?>
				<div class="bg-controls">
					<?= $form->checkBox($model, 'is_howto', array('class' => 'is_howtoChecker')) ?>
					<span class="help-inline"><?= $form->error($model, 'is_howto') ?></span>
				</div>
			</td>
			<td>
				<label class="control-label bg-control-label">Сегодня на районе</label>
				<div class="bg-controls">
					<?= CHtml::checkBox('extra[blogs_digest]', isset($aExtra_params['blogs_digest']) ? 1 : 0, array()) ?>
				</div>
			</td>
            <td>
                <label class="control-label bg-control-label">Cityboom</label>
                <div class="bg-controls">
                    <?= CHtml::checkBox('extra[cityboom]', isset($aExtra_params['cityboom']) ? 1 : 0, array()) ?>
                </div>
            </td>
			<td colspan="1"></td>
		</tr>
	</table>
</div>


<div class="control-group">
	<?= $form->labelEx($model, 'ceil_type', array('class' => 'control-label')) ?>
	<div class="controls">
		<?echo CHtml::activeDropDownList($model, 'ceil_type', $model->ceil_types, array('class' => 'b-admin__select'));?>
		<span class="help-inline"><?= $form->error($model, 'ceil_type') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'ceil_quote_author', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'ceil_quote_author', array('data-limit-length' => 'ceil_quote_author', 'class' => 'span8')) ?>
			<span data-limit-length="ceil_quote_author" class="add-on"><?= mb_strlen($model->ceil_quote_author) ?></span>
		</div>
		<span class="help-inline"><?= $form->error($model, 'ceil_quote_author') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'ceil_quote_text', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'ceil_quote_text', array('data-limit-length' => 'ceil_quote_text', 'class' => 'span8')) ?>
			<span data-limit-length="ceil_quote_text" class="add-on"><?= mb_strlen($model->ceil_quote_text) ?></span>
		</div>
		<span class="help-inline"><?= $form->error($model, 'ceil_quote_text') ?></span>
	</div>
</div>
*/?>
<div class="control-group">
	<?= $form->labelEx($model, 'link', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'link', array('data-limit-length' => 'link', 'class' => 'span8')) ?>
			<span data-limit-length="link" class="add-on"><?= mb_strlen($model->link) ?></span>
		</div>
		<span class="help-inline"><?= $form->error($model, 'link') ?></span>
	</div>
</div>

<?/*
<div class="control-group">
	<?= $form->labelEx($model, 'customer', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'customer', array('data-sync-to' => 'slug', 'data-limit-length' => 'customer', 'class' => 'span8')) ?>
			<span data-limit-length="customer" class="add-on"><?= mb_strlen($model->customer) ?></span>
		</div>
		<span class="help-inline"><?= $form->error($model, 'customer') ?></span>
	</div>
</div>

<div class="control-group">
	<?= $form->labelEx($model, 'price', array('class' => 'control-label')) ?>
	<div class="controls">
		<div class="input-append span10">
			<?= $form->textField($model, 'price', array('class' => 'span8')) ?>
			
		</div>
		<span class="help-inline"><?= $form->error($model, 'price') ?></span>
	</div>
</div>
*/?>

<?/*
<div class="control-group">
	<?= $form->labelEx($model, 'is_digitilized', array('class' => 'control-label')) ?>
	<div class="controls">
		<?= $form->checkBox($model, 'is_digitilized') ?>
		<span class="help-inline"><?= $form->error($model, 'is_digitilized') ?></span>
	</div>
</div>
*/?>

<p><span id="service_field_label">Служебные поля</span></p>
<div id="service_field">
	<div class="control-group">
		<?= $form->labelEx($model, 'view', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="input-append span12">
				<?= $form->textField($model, 'view', array('class' => 'span10')) ?>
			</div>
			<span class="help-inline"><?= $form->error($model, 'link') ?></span>
		</div>
	</div>
	<div class="control-group">
		<?= $form->labelEx($model, 'include_text', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="input-append span12">
				<?= $form->textArea($model, 'include_text', array('style' => 'width:83%;')) ?>
			</div>
			<span class="help-inline"><?= $form->error($model, 'include_text') ?></span>
		</div>
	</div>
</div>

<?
//Удаление статьи
if ($model->id > 0 && empty($lock->user_name))
{
	?>
<div style="text-align:center;">
	<? echo CHtml::ajaxButton(
	'Удалить статью',
	'/admin/articles/deletearticle',
	array(
		'data'       => "id={$model->article_id}",
		'beforeSend' => 'function(){}',
		'complete'   => 'function(){}',
		'success'    => 'function(){alert("Статья успешно удалена!"); window.location.href="/admin/articles/";}',
		'error'      => 'function(){alert("Во время удаления статьи произошла непредвиденная ошибка!");}',
	),
	array(
		'class'   => 'btn btn-danger',
		'id'      => 'delete_btn',
		'confirm' => 'Вы уверены, что хотите безвозвратно удалить эту статью?',
		'style' => 'margin-bottom: 40px;'
	)
); ?>
</div>
	<? }?>
</div>
<div class="tab-pane" id="chapters">
	<?
	//Управление главами и элементами
	$this->widget('application.widgets.chapters.ChaptersWidget', array('layout' => 'base', 'article_id' => $model->id, 'model' => $model ));
	?>
</div>

<div class="tab-pane " id="relations">
	<?
	//Блок связей с авторами

	foreach ($authors as $author_type => $author)
	{
		//Кавполит использует пока только тип авторов 18
		if($author_type != 18)
			continue;
		?>
		<div class="control-group">
			<label class="control-label"><?=((isset(Authors::model()->types[$author_type])) ? Authors::model()->types[$author_type] : '')?>: </label>
			<div class="controls">
				<?php
				$author_auto = array();
				if (is_array($author))
				{
					foreach ($author as $a)
					{
						$author_auto[] = array(
							'id'   => $a->authors->id,
							'name' => $a->authors->fullname()
						);
					}
				}
				$this->widget('AutocompleteWidget', array(
					'data' => $author_auto,
					'name' => 'authors[' . $author_type . '][]',
					'url'  => '/admin/authors/getbyname/',
				));?>
			</div>
		</div>
		<?
	}
	?>


	<div class="control-group">
		<label class="control-label"><?= $form->labelEx($model, 'series_id', array('class' => 'control-label')) ?></label>
		<div class="controls">
			<?php
			$series = is_null($series) ? array() : array($series);
			$this->widget('AutocompleteWidget', array(
				'data'     => $series,
				'name'     => 'serie_id',
				'maxItems' => 1,
				'url'      => '/admin/series/getbyname/',
			));?>
		</div>
	</div>

	<?/*
	<div class="control-group">
		<label class="control-label"><?= $form->labelEx($model, 'theme_week_code', array('class' => 'control-label')) ?></label>
		<div class="controls">
			<?php
			$theme_auto = array();
			if (isset($themeweek->name) && $themeweek->name)
			{
				$theme_auto = array(array('id' => $themeweek->code, 'name' => $themeweek->name));
			}
			$this->widget('AutocompleteWidget', array(
				'data'     => $theme_auto,
				'name'     => 'theme_week_code',
				'maxItems' => 1,
				'url'      => '/admin/themeweek/getbyname/',
			));?>
		</div>
	</div>
	*/?>

	<div class="control-group">
		<label class="control-label"><?= $form->labelEx($model, 'regions', array('class' => 'control-label')) ?></label>
		<div class="controls">
			<?php
			$this->widget('AutocompleteWidget', array(
				'data'     => $regions,
				'name'     => 'regions[]',
				'url'      => '/admin/regions/getbyname/',
			));?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label"><?= $form->labelEx($model, 'tags', array('class' => 'control-label')) ?></label>
		<div class="controls">
			<?php
			$this->widget('AutocompleteWidget', array(
				'data'     => $tags,
				'name'     => 'tags[]',
				'url'      => '/admin/tags/getbyname/',
			));?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label"><?= $form->labelEx($model, 'relations', array('class' => 'control-label')) ?></label>

		<div class="controls">
			<?php
			$this->widget('AutocompleteWidget', array(
				'data' => $relations,
				'name' => 'relations[]',
				'url'  => '/admin/articles/getbyname/',
			));?>
		</div>
	</div>


</div>
<div id="history" class="tab-pane">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th>Дата сохранения</th>
				<th>Название</th>
				<th>Пользователь</th>
				<th></th>
			</tr>
		</thead>
		<?

		$hist = $model->history();
		foreach ($hist as $val)
		{
			?>
			<tr>
				<td>
					<?
					if ($val['is_dummy'])
					{
						echo '<span class="icon icon-arrow-right"></span>';
					}
					?>
				</td>
				<td>
					<?=$val['id']?>
				</td>
				<td><?=$val['date']?></td>
				<td>
					<? if ($val['active'])
				{ ?><img src="/static/css/admin/images/accept.png" alt=""/><? }?>
					<?=$val['name']?>
				</td>
				<td><a href="/admin/users/edit/id/<?=$val['user_id']?>"><?=$val['user_name']?></a></td>
				<td>
					<? if ($action == 'hist' && $model->id == $val['id'])
					{
						?>
						Просматривается сейчас
					<?
					}
					else
					{
						?>
						<a href="?id=<?=$model->article_id?>&articles_draft_id=<?=$val['id']?>">Просмотреть</a>
					<? }?>
				</td>
			</tr>
			<?
		}
		?>
	</table>
</div>
</div>



<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

<?
if (!empty($lock->user_name))
{
	?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">×</a>
	Пользователь <?= $lock->user_id; ?> - <?= CHtml::encode($lock->user_name); ?> заблокировал статью!
</div>

	<?
}
?>

<?php $this->endWidget(); ?>


<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_articles', "
	$('#cancel_btn').click( function(e) {
		window.location.href = '/admin/articles/breaklock/article_id/{$model->article_id}';
	});
", CClientScript::POS_READY)
?>