<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/articles/statistic/')), 'get'); ?>


<div class="well">
  <table class="b-search-params">
      <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('ID', 'id'); ?></td>
        <td class="b-search-params__value"><? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?></td>
      </tr>
      <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Категория', 'category_id'); ?></td>
        <td class="b-search-params__value new-height-select">
            <? $this->widget(
                'TreeWidget',
                array(
                    'name' => 'parent_id',
                    'parent_id' => Yii::app()->request->getParam('parent_id'),
                    'parent_scope' => 1
                )
            ); ?>
        </td>
      </tr>
      <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Название', 'name'); ?></td>
        <td class="b-search-params__value"><? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?></td>
      </tr>
	 <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Регион', 'region_id'); ?></td>
        <td class="b-search-params__value new-height-select">            
			<? 
			$regions_list[0] = 'Любой';
			foreach($regions as $region){
				$regions_list[$region->id] = $region->name;
			}
			echo CHtml::dropDownList('region_id', urldecode(Yii::app()->request->getParam('region_id','')), $regions_list); ?>
        </td>
    </tr>     
   <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Автор', 'author_id'); ?></td>
        <td class="b-search-params__value new-height-select">            
      <? 
      $authors_list[0] = 'Любой';
      foreach($authors as $author){
        $authors_list[$author->id] = $author->fullnameR();
      }
      echo CHtml::dropDownList('author_id', urldecode(Yii::app()->request->getParam('author_id','')), $authors_list); ?>
        </td>
    </tr>      

	<tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Дата на сайте', 'date_publish'); ?></td>
        <td class="b-search-params__value new-height-select">            
			<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
				//'model'     => $model,
				//'attribute' => 'date_publish',
				'name' 		=> 'date_publish_from',
				'value' => Yii::app()->request->getParam('date_publish_from'),
				'mode'      => 'datetime',
				'language'  => 'ru',
				'options'   => array(
					//'dateFormat' => 'dd/mm/yy',
					'dateFormat' => 'yy-mm-dd',
					'timeFormat' => 'hh:mm:ss',
				),
			));?>

			<span>-</span> <? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
				//'model'     => $model,
				//'attribute' => 'date_publish',
				'name' 		=> 'date_publish_to',
				'value' => Yii::app()->request->getParam('date_publish_to'),
				'mode'      => 'datetime',
				'language'  => 'ru',
				'options'   => array(
					//'dateFormat' => 'dd/mm/yy',
					'dateFormat' => 'yy-mm-dd',
					'timeFormat' => 'hh:mm:ss',
				),
			));?>
        </td>
    </tr> 

	<tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Дата создания', 'date_create'); ?></td>
        <td class="b-search-params__value new-height-select">            
			<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
				//'model'     => $model,
				//'attribute' => 'date_publish',
				'name' 		=> 'date_create_from',
				'value' => Yii::app()->request->getParam('date_create_from'),
				'mode'      => 'datetime',
				'language'  => 'ru',
				'options'   => array(
					//'dateFormat' => 'dd/mm/yy',
					'dateFormat' => 'yy-mm-dd',
					'timeFormat' => 'hh:mm:ss',
				),
			));?>

			<span>-</span> <? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
				//'model'     => $model,
				//'attribute' => 'date_publish',
				'name' 		=> 'date_create_to',
				'value' => Yii::app()->request->getParam('date_create_to'),
				'mode'      => 'datetime',
				'language'  => 'ru',
				'options'   => array(
					//'dateFormat' => 'dd/mm/yy',
					'dateFormat' => 'yy-mm-dd',
					'timeFormat' => 'hh:mm:ss',
				),
			));?>
        </td>
    </tr> 

	 <tr>
        <td class="b-search-params__option"><? echo CHtml::Label('Заказчик', 'customer'); ?></td>
        <td class="b-search-params__value"><? echo CHtml::textField('customer', urldecode(Yii::app()->request->getParam('customer',''))); ?></td>
      </tr>
     <tr>
       <td class="b-search-params__value" colspan="2">
        <label for="noactive" class="checkbox inline">
         <? echo CHtml::CheckBox('noactive', Yii::app()->request->getParam('noactive', '')); ?> Не активна
        </label>  
      </td>
    </tr>
    <tr>
      <td class="b-search-params__action" colspan="2">
      	<? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?>
      	<div style="float:right"><a href="?send_csv" target="_blank">Скачать CSV</a></div>
      </td>
    </tr>
  </table>
</div>

<? echo CHtml::endForm(); ?>

<table class="table b-admin__articles-list" >
  <thead>
    <tr>
      <th style="text-align: center; width: 20px;">&nbsp;</th>
      <th style="text-align: center; width: 100px;">Регион</th>     
      <th style="text-align: left;"><b>Название</b></th>
      <th style="text-align: left;"><b>Символов</b></th>
      <th style="text-align: left; width: 150px;"><b>Раздел</b></th>
      <th style="text-align: left; width: 100px;"><i class="icon-eye-open"></i></th>
      <th style="text-align: left; width: 100px;"><b>Дата на сайте</b></th>
      <th style="text-align: left; width: 100px;"><b>Дата создания</b></th>
      <th style="text-align: left; width: 100px;"><b>Дата публикации</b></th>
      <th style="text-align: left; width: 100px;"><b>Заказчик</b></th>
      <th style="text-align: left;">Итоговая стоимость</th>
    </tr>
  </thead>
  <? foreach($news as $n) : ?>
  <tr class="b-admin__articles-list__row">
    <td>
      <div class="b-admin__articles-list__icons">
        <a href="javascript:void(0)" class="article_toggle_active" data-id="<?=$n->id?>">
          <i class="icon-<?=(($n['active'] == 1) ? 'circle' : 'circle-blank' )?>"></i>
          <!--
          <img src="/static/css/admin/images/<?=(($n['active'] == 1) ? 'accept.png' : 'delete.png' )?>" /> --></a>
        <?/*<a class="article_toggle_active btn btn-<?=(($n['active'] == 1) ? 'success' : 'danger')?> btn-mini" data-id="<?=$n->id?>">&nbsp;&nbsp;</a><br />*/?>
        <?if($n->show_main_announce == 1){?><i class="icon-bookmark" title="Главная на морде"></i> <!--<img src="/static/css/admin/images/flag_blue.png" title="Главная на морде"/>--><?}?>
        <?if($n->show_main_announce_section == 1){?><i class="icon-bookmark-empty" title="Главная раздела"></i><!--<img src="/static/css/admin/images/flag_yellow.png" title="Главная раздела" />--><?}?>
      </div>
    </td>
    <td style="text-align: center;"><?foreach($n->regions as $region){?><?=$region->name?>  <?}?></td>
    <td style="text-align: left;">
      <a href="<?=$n->url()?>" target="_blank"><?=CHtml::encode($n['name']);?></a>

      <div>
        <?
        $all_authors = array();
        foreach ($n->authors as $author_type => $author) {
          $all_authors[] = $author->fullname();
        }

        echo implode(', ', $all_authors);
        ?>
      </div>    
    </td>
     <td style="text-align: left;">
     	<?=$n->detail_text_letters?>
     </td>
    <td style="text-align: left;">
      <?=((isset($n->tree)) ? CHtml::encode($n->tree->name) : '')?>
      <?= ($n->has_video == 1) ? '<img src="/static/css/admin/images/video.png" />' : ''?>
      <?= ($n->has_gallery == 1) ? '<img src="/static/css/admin/images/camera.png" />' : ''?>
      
    </td>

        <td style="text-align: left;">
            <?=$stat[$n->id]->views?>
        </td>
      <td style="text-align: left;">
      <?=$n->date_publish?>
     </td>        
     <td style="text-align: left;">
      <?=$n->date_create?>
     </td>
      <td style="text-align: left;">
      <?=$n->getFirstPublishDate()?>
     </td>     
     <td style="text-align: left;"><?=$n->customer?></td>
    <td style="text-align: left;">
      <?= $n->getTotalPrice() ?>
      <div>
    	  (<?=$n->getPrice()?> за 1000)
  		</div>
    </td>
  </tr>
  <? endforeach ?>
  
</table>

<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>