<?if(isset($dot['key'])){?>
<div class="GuideGMap_dot b-guide-map__dot" data-key="<?=$dot['key']?>">
		<span class="highlighter label label-success"><i class="icon-chevron-right"></i></span>
		<div class="input-append ">
			<input name="GuideGMap[<?=$dot['key']?>][title]" class="b-guide-map__dot__address dot_address" data-key="<?=$dot['key']?>" type="text" value="<?=$dot['title']?>" />	
			<input class="GuideGMap_findOnMap btn btn-small" type="button" data-key="<?=$dot['key']?>" value="Найти" />
		</div>		
		<?
		//При рендере уже сохраненных точек добавляем их в массив точек
		if(!isset($is_new)){?>
		<script>		
			$(document).ready(function(){ GuideGMap.addMarker('<?=$dot['key']?>', '<?=$dot['title']?>', new google.maps.LatLng(<?=$dot['x']?>, <?=$dot['y']?>) ); })
		</script>
		<?}?>
		<input type="button" name="dot_show_coord" class="GuideGMap_dot_show_coord btn btn-primary btn-mini"  data-key="<?=$dot['key']?>" value="Координаты" onclick="$(this).parent().find('.GuideGMap_dot_params').toggle()" />
		<span type="button" name="dot_delete" class="GuideGMap_remove_dot btn btn-danger btn-mini"  data-key="<?=$dot['key']?>">
			<i class="icon-remove-sign"></i>
		</span>
		
		<div class="GuideGMap_dot_params">
			<input name="GuideGMap[<?=$dot['key']?>][x]" class="dot_x" data-key="<?=$dot['key']?>" type="text" value="<?=$dot['x']?>" />
			<input name="GuideGMap[<?=$dot['key']?>][y]" class="dot_y" data-key="<?=$dot['key']?>" type="text" value="<?=$dot['y']?>" />		
			<input name="GuideGMap[<?=$dot['key']?>][key]" class="dot_key" data-key="<?=$dot['key']?>" type="hidden" value="<?=$dot['key']?>" />
		</div>		
</div>

<?}?>