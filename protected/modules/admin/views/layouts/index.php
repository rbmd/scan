<!DOCTYPE html>
<html>
<head>
	<? # Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=no"/>

	<title><?php echo ($this->pageTitle != 'Большой город') ? CHtml::encode($this->pageTitle) : 'admin'; ?></title>
	<?/*<link rel="stylesheet" type="text/css" href="/static/css/pub/main.css?20121123_1"/>*/?>
	<link rel="stylesheet" type="text/css" href="/static/css/admin/font-awesome.css"/>

	<link href="/static/bootstrap/css/bootstrap.css?v=4" rel="stylesheet"/>
	<!-- <link href="/static/bootstrap/css/bootstrap-responsive.css?v=2" rel="stylesheet" /> -->
	<link href="/static/js/admin/fcbkcomplete/style.css" rel="stylesheet"/>

	<link rel="stylesheet" type="text/css" href="/static/css/admin/style.css?201302142"/>

	<? Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jquery-ui.min.js')) ?>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-transition.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-alert.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-modal.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-dropdown.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-scrollspy.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-tab.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-tooltip.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-popover.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-button.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-collapse.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-carousel.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap-typeahead.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/application.js"></script>

	<script type="text/javascript" src="/static/js/admin/synctranslit.js"></script>
	<script type="text/javascript" src="/static/js/admin/fcbkcomplete/jquery.fcbkcomplete.js"></script>

	<script type="text/javascript" src="/static/js/admin/jquery.cookie.js"></script>
	<script type="text/javascript" src="/static/js/admin/jquery.hotkeys.js"></script>
	<script type="text/javascript" src="/static/js/admin/jquery.json-2.3.min.js"></script>

	<script type="text/javascript" src="/static/js/admin/script.js?20121129"></script>
	<?/*<script type="text/javascript" src="/static/js/pub/main.js"></script>*/?>

	<script type="text/javascript" src="/3rdparty/ckeditor/ckeditor.js?1348528525"></script>
	<script type="text/javascript" src="/3rdparty/ckfinder/ckfinder.js"></script>
	<script type="text/javascript" src="/static/js/admin/ckfinder_manager.js"></script>
	<script type="text/javascript" src="/static/js/admin/wysiwyg_manager.js"></script>
	<script type="text/javascript" src="/s/js/all.js"></script>

	<script src="/3rdparty/codemirror-3.0/lib/codemirror.js"></script>
	<link rel="stylesheet" href="/3rdparty/codemirror-3.0/lib/codemirror.css">
	<script src="/3rdparty/codemirror-3.0/mode/javascript/javascript.js"></script>


</head>
<body >
<div id="screen_fog"></div>


<div class="milk"></div>
<div class="popup-window">
	<div class="wrap"></div>
</div>


<div class="navbar navbar-fixed-top navbar-inverse b-admin__topbar g-clear">
	<div class="navbar-inner">
		<div class="container b-admin__topbar__inner ">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>

			<div class="nav-collapse">
				<ul class="nav">
					



					<? /*<li>
							<a href="/admin/comments/">Комментарии</a>
						</li>*/
					?>
					
					<li>
						<a href="/admin/projects/">
							Проекты
							<b class="caret"></b>
						</a>
					</li>

					<li>
						<a href="/admin/materials/">
							Материалы
							<b class="caret"></b>
						</a>
					</li>

					<li>
						<a href="/admin/materialrequest/">
							Регистранты
							<b class="caret"></b>
						</a>
					</li>


					<?
					if(false){?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Контент
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<?/*
							<li>
								<a href="/admin/tree/"><i class="icon-list-alt"></i> Дерево сайта</a>
							</li>
							<li>
								<a href="/admin/articles/"><i class="icon-book"></i> Статьи</a>
							</li>

							<?/* if (Yii::app()->user->checkAccess('articlesStatistic') || Yii::app()->user->checkAccess('Admin')) { ?>
							<li>
								<a href="/admin/articles/statistic/"><i class="icon-book"></i> Статистика статей</a>
							</li>			
							<? } */?>				

							<?/*<li>
								<a href="/admin/magazine/"><i class="icon-book"></i> Журналы</a>
							</li>*/?>
							<?/*
							<li>
								<a href="/admin/series/"><i class="icon-th-list"></i> Серии</a>
							</li>
							*/?>
	
							<?/*
							<li>
								<a href="/admin/regions/"><i class="icon-book"></i> Регионы</a>

							</li>	
							*/?>						
							<li>
								<a href="/admin/gallery/"><i class="icon-camera"></i> Фотогалереи</a>
							</li>

							<?/*
							<li>
								<a href="/admin/indexpage/"><i class="icon-align-left"></i> Параметры главной</a>
							</li>	
							*/?>						
							<?/*<li>
								<a href="/admin/special/"><i class="icon-tags"></i> Спецпроекты</a>
							</li>
							<li>
								<a href="/admin/afisha/"><i class="icon-tags"></i> Афиша</a>
							</li>
							<li>
								<a href="/admin/afishatypes/"><i class="icon-tags"></i> Типы афиши</a>
							</li>
							<li>
								<a href="/admin/specprojects/instagramtags/"><i class="icon-tags"></i> Instagram tags</a>
							</li>*/?>

							<li class="divider"></li>
							<li>
								<a href="/admin/manageimages/"><i class="icon-tags"></i> Менеджер картинок</a>
							</li>
							
							<li>
								<a href="/admin/polls/"><i class="icon-align-left"></i> Опросы</a>
							</li>
							

							<li>
								<a href="/admin/banners/"><i class="icon-align-left"></i> Баннеры</a>
							</li>
							
							<li class="divider"></li>
							
							<li class="divider"></li>
							<? if (Yii::app()->user->checkAccess('Admin') && Yii::app()->user->checkAccess('canFlushMemcache'))
							{
								?>
								<?/*
								<li>
									<a href="/admin/settings/"><i class="icon-align-left"></i> Настройки</a>
								</li>
								*/?>
								<li>
									<a href="/admin/default/clearcache/"><i class="icon-align-left"></i> Сброс memcache</a>
								</li>
								<li>
									<a href="/admin/default/enabledebug/"><i class="icon-align-left"></i> Включить debug</a>
								</li>
								<li>
									<a href="/admin/profiler/"><i class="icon-align-left"></i> Профайлер</a>
								</li>
							<? } ?>
						</ul>
					</li>
					<?
					}
					?>

					<? if (Yii::app()->user->checkAccess('Admin'))
					{
						?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								Пользователи
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li>
									<a href="/admin/users/">Пользователи</a>
								</li>
								<li>
									<a href="/admin/rights/">Группы и права</a>
								</li>
							</ul>
						</li>
					<? } ?>

				</ul>
				<p class="navbar-text pull-right">
					<a href="/" class="Перейти к сайту"><i class="icon-external-link"></i></a> |
					Привет, <a href="/user/"><?= Yii::app()->user->name ?></a>!
					<a href="/logout/" title="Выход"><i class="icon-signout"></i></a>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="content-placeholder">
	<div>
		<?= $content; ?>
	</div>
</div>


<?php
$this->widget('ext.ScrollTop.ScrollTopWidget', array(
	'label' => 'Наверх',
	'id'    => 'top-link'
));
?>

</body>
</html>