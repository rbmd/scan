<h1><?
if($action == 'create')
{
	?>Создание<?
}
else
{
	?>#<?=$model->id?><?
}
?>
</h1>
<div class="form">
	<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'tags-form',
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => false, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		));
	?>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>

				<div class="controls">
					<?= $form->checkBox($model, 'active') ?>
					<span class="help-inline"><?= $form->error($model, 'active') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'place_on_top', array('class' => 'control-label')) ?>

				<div class="controls">
					<?= $form->checkBox($model, 'place_on_top') ?>
					<span class="help-inline"><?= $form->error($model, 'place_on_top') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'code', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'title', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'title', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'title') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'title_bg', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'title_bg', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'title_bg') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'activity_dates', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'activity_dates', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'activity_dates') ?></span>
						</div>
					</div>
				</div>
			</div>



			<div class="control-group">
				<?= $form->labelEx($model, 'show_regform', array('class' => 'control-label')) ?>

				<div class="controls">
					<?= $form->checkBox($model, 'show_regform') ?>
					<span class="help-inline"><?= $form->error($model, 'show_regform') ?></span>
				</div>
			</div>







<?/*

		<div class="control-group">
			<?= $form->labelEx($model, 'lead', array('class' => 'control-label')) ?>
			<div class="controls bordered-inline-editor">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'lead', array('class' => 'span12', 'rows'=>5)) ?>
						<span class="help-inline"><?= $form->error($model, 'lead') ?></span>
					</div>
				</div>
			</div>
		</div>	
		<script>
        $(document).ready(function() {
           wysiwygManager.createEditor('<?=get_class($model)?>_lead', {config: 'mini.js', inlineEditor: true});
        });
      </script>


*/?>
      


		</fieldset>



	<?= CHtml::errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
	
	<div class="submit">
        <? echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
        <? echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
        <? echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')); ?>
	</div>

    <div style="float:right;">
        <? echo CHtml::ajaxButton(
        'Удалить',
        '/admin/materials/delete/',
        array(
            'data' => "id={$model->id}",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Элемент успешно удалён!"); window.location.href="/admin/materials/";}',
            'error' => 'function(){alert("Во время удаления элемента произошла непредвиденная ошибка!");}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'delete_btn',
            'confirm'=>'Вы уверены, что хотите безвозвратно удалить этот элемент?',
        )
    ); ?>
    </div>
	 
<? $this->endWidget(); ?>

</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_tags', '
    $("#cancel_btn").click( function(e) {
        window.location.href = "/admin/materials/";
    });
', CClientScript::POS_READY)
?>