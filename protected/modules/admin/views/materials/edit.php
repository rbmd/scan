<h1><?
if($action == 'create')
{
	?>Создание<?
}
else
{
	?>#<?=$model->id?><?
}
?>
</h1>
<div class="form">
	<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'tags-form',
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => false, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		));
	?>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>

				<div class="controls">
					<?= $form->checkBox($model, 'active') ?>
					<span class="help-inline"><?= $form->error($model, 'active') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'project_id', array('class' => 'control-label')) ?>
				<div class="controls">
					<?echo CHtml::activeDropDownList($model, 'project_id', $projects, array('class' => 'b-admin__select'));?>
					<span class="help-inline"><?= $form->error($model, 'project_id') ?></span>
				</div>
			</div>



			<div class="control-group control-group_x2">
				<?= $form->labelEx($model, 'date_active_start', array('class' => 'control-label')) ?>
				<div class="controls">
					<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model'     => $model,
					'attribute' => 'date_active_start',
					'mode'      => 'datetime',
					'language'  => 'ru',
					'options'   => array(
						//'dateFormat' => 'dd/mm/yy',
						'dateFormat' => 'yy-mm-dd',
						'timeFormat' => 'hh:mm:ss',
					),
				));?>
					<span class="help-inline"><?= $form->error($model, 'date_active_start') ?></span>
				</div>
			</div>

			<?/*
			<div class="control-group">
				<?= $form->labelEx($model, 'tree_id', array('class' => 'control-label')) ?>
				<div class="controls">
					<?echo CHtml::activeDropDownList($model, 'tree_id', $tree, array('class' => 'b-admin__select'));?>
					<span class="help-inline"><?= $form->error($model, 'tree_id') ?></span>
				</div>
			</div>
			*/?>

			<div class="control-group ">
				<?php echo $form->labelEx($model, 'pic', array('class' => 'control-label')); ?>
				<div class="controls">

					<?=$form->textField($model, 'pic', array('class' => 'b-admin__text span8'))?>
					<script type="text/javascript">
						$(function() {
							ckfinderManager.create('<?=get_class($model)?>_pic');
						});
					</script>

					<span class="help-inline"><?php echo $form->error($model, 'pic'); ?></span>
				</div>

			</div>

			<?/*
			<div class="control-group ">
				<?php echo $form->labelEx($model, 'pic_small', array('class' => 'control-label')); ?>
				<div class="controls">

					<?=$form->textField($model, 'pic_small', array('class' => 'b-admin__text span8'))?>
					<script type="text/javascript">
						$(function() {
							ckfinderManager.create('<?=get_class($model)?>_pic_small');
						});
					</script>

					<span class="help-inline"><?php echo $form->error($model, 'pic_small'); ?></span>
				</div>

			</div>
			*/?>

			<div class="control-group">
				<?= $form->labelEx($model, 'pic_desc', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'pic_desc', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'pic_desc') ?></span>
						</div>
					</div>
				</div>
			</div>



		<div class="control-group">
			<?= $form->labelEx($model, 'lead', array('class' => 'control-label')) ?>
			<div class="controls bordered-inline-editor">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'lead', array('class' => 'span12', 'rows'=>5)) ?>
						<span class="help-inline"><?= $form->error($model, 'lead') ?></span>
					</div>
				</div>
			</div>
		</div>	
		<script>
        $(document).ready(function() {
           wysiwygManager.createEditor('<?=get_class($model)?>_lead', {config: 'mini.js', inlineEditor: true});
        });
      </script>

	
			
  		<div class="control-group">
			<?= $form->labelEx($model, 'detail_text', array('class' => 'control-label')) ?>
			<div class="controls bordered-inline-editor">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'detail_text', array('class' => 'span12', 'rows'=>10)) ?>
						<span class="help-inline"><?= $form->error($model, 'detail_text') ?></span>
					</div>
				</div>
			</div>
		</div>

      <script>
        $(document).ready(function() {
           wysiwygManager.createEditor('<?=get_class($model)?>_detail_text', {config: 'config.js', inlineEditor: true});
        });
      </script>

      


		</fieldset>



	<?= CHtml::errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
	
	<div class="submit">
        <? echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
        <? echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
        <? echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')); ?>
	</div>

    <div style="float:right;">
        <? echo CHtml::ajaxButton(
        'Удалить',
        '/admin/materials/delete/',
        array(
            'data' => "id={$model->id}",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Элемент успешно удалён!"); window.location.href="/admin/materials/";}',
            'error' => 'function(){alert("Во время удаления элемента произошла непредвиденная ошибка!");}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'delete_btn',
            'confirm'=>'Вы уверены, что хотите безвозвратно удалить этот элемент?',
        )
    ); ?>
    </div>
	 
<? $this->endWidget(); ?>

</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_tags', '
    $("#cancel_btn").click( function(e) {
        window.location.href = "/admin/materials/";
    });
', CClientScript::POS_READY)
?>