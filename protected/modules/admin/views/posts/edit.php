<h1><?= $model->name ?></h1>
<br />
<br />

<div class="span10">
	<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'post-form',
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => false, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		));
	?>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->checkBox($model, 'active') ?>
							<span class="help-inline"><?= $form->error($model, 'active') ?></span>
						</div>
					</div>
				</div>
			</div>


			<div class="control-group">
				<?= $form->labelEx($model, 'approved', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->checkBox($model, 'approved') ?>
							<span class="help-inline"><?= $form->error($model, 'approved') ?></span>
						</div>
					</div>
				</div>
			</div>


			<div class="control-group">
				<?= $form->labelEx($model, 'preview_text', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textArea($model,'preview_text');?>		
							<script>
								$(document).ready(function(){
									wysiwygManager.createEditor('Post_preview_text', 'default');
								});
							</script>								
							
							<span class="help-inline"><?= $form->error($model, 'preview_text') ?></span>
						</div>
					</div>
				</div>
			</div>


    		<div class="control-group">
				<?= $form->labelEx($model, 'detail_text', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textArea($model,'detail_text');?>		
							<script>
								$(document).ready(function(){
									wysiwygManager.createEditor('Post_detail_text', 'default');
								});
							</script>								
							
							<span class="help-inline"><?= $form->error($model, 'detail_text') ?></span>
						</div>
					</div>
				</div>
			</div>

			
		</fieldset>



	<?= CHtml::errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
	
	<div class="submit">
		<? echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
		<? echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
		<? echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')); ?>
	</div>
	 
<? $this->endWidget(); ?>


</div>



<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_blogs', '
	$("#cancel_btn").click( function(e) {
		window.location.href = "/admin/posts/";
	});
', CClientScript::POS_READY)
?>