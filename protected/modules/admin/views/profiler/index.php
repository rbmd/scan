<div style="float:left;">
    <? echo CHtml::ajaxButton(
        Controller::doProfile() ? 'ОТКЛЮЧИТЬ профилирование' : 'ВКЛЮЧИТЬ профилирование',
        '/admin/profiler/switch/',
        array(
            'data' => "",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){/*alert("Профилирование включено!");*/window.location.href="/admin/profiler/";}',
            'error' => 'function(){/*alert("Во время вкшлючения профилирования произошла непредвиденная ошибка...");*/}',
        ),
        array(
            'class' => !Controller::doProfile() ? 'btn btn-danger' : 'btn btn-success',
            'id'=>'start_stop_btn',
            /*'confirm'=>!Controller::doProfile() ? 'Вы уверены, что хотите включить профилирование? Это приведёт к существенному падению производительности!' : 'Вы уверены, что хотите ОТКЛЮЧИТЬ профилирование?',*/
        )
    ); ?>
</div>

<div style="float:right;">
    <? echo CHtml::ajaxButton(
        'Принудительный сброс статистики',
        '/admin/profiler/reset/',
        array(
            'data' => "",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Таблица со статистикой успешно очищена!"); window.location.href="/admin/profiler/";}',
            'error' => 'function(){/*alert("Во время вкшлючения профилирования произошла непредвиденная ошибка...");*/}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'reset_btn',
            'confirm'=>'Вы уверены, что хотите уничтожить всю имеющуюся статистику?',
        )
    ); ?>
</div>
<!--
<div style="float:left; padding-left: 20px;">
    <span>Агрегировать результаты за последние:</span>
    <select name="profiler_interval" style="width:100px;">
        <?foreach(Profiler::$PROFILER_INTERVALS as $interval):?>
            <option <?if(Profiler::getProfilerInterval() == $interval):?>selected="selected" <?endif?> value="<?=$interval?>"><?=$interval?> мин</option>
        <?endforeach?>
    </select>
</div>
-->
<div style="float: left; padding-left: 20px;">
    <?php echo CHtml::image(
        '/static/css/admin/images/ajax-loader.gif',
        'progress...',
        array('id'=>'loader', 'style'=>'display:none;')
    );?>
</div>

<div style="clear: both;"></div>

<div id="statistic_content" style="padding-top: 30px;">
    <?=$statistic?>
</div>

<?php
Yii::app()->getClientScript()->registerScript('profiler_managament', "

		$('select[name=profiler_interval]').bind('change', function() {

            var interval = $('select[name=profiler_interval] option:selected').val();

            $.ajax({
                type: 'POST',
                url: '/admin/profiler/newinterval/',
                data: 'profiler_interval=' + interval,
                dataType: 'json',
                beforeSend: function(){ $('#loader').toggle(); },
                complete: function(){ $('#loader').toggle(); },
                success: function(data, textStatus, jqXHR){
                    /*alert('Интервал профилирования успешно обнавлён!');*/
                },
                error: function(XHR, textStatus, errorThrown){/*alert(\"Произошла непредвиденная ошибка! \"+XHR.responseText);*/},
            });
        });

        function update_statistic() {

            if( !$('#start_stop_btn').hasClass('btn-success'))
            {
                return;
            }

		    $.ajax({
                type: 'POST',
                url: '/admin/profiler/update/',
                data: '',
                dataType: 'json',
                beforeSend: function(){ $('#loader').toggle(); },
                complete: function(){ $('#loader').toggle(); },
                success: function(data, textStatus, jqXHR){
                    $('#statistic_content').html(data.content);
                },
                error: function(XHR, textStatus, errorThrown){/*alert(\"Произошла непредвиденная ошибка! \"+XHR.responseText);*/},
            });

        }

        /*setInterval(update_statistic, 5000);*/

	", CClientScript::POS_END);
?>