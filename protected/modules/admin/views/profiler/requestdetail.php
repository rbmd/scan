<?php
/**
 * User: Dmitry Rusakov
 * Date: 18.03.13
 * Time: 12:52
 */
?>

<div class="col span10" style="text-align: center;">

    <div style="padding-bottom: 10px"><span style="font-weight: bold;">ДЕТАЛЬНАЯ ЗАПРОСА "<?=$request->request_url?>"</span></div>

    <div style="float: left;"><span><a href="/admin/profiler/allrequests/?c=<?=$request->controller_action?>">Назад</a></span></a></span></div>

    <table style="width:100%; border: 1px solid #000000; border-collapse: separate; border-spacing: 2px;">

        <thead style="color: #f5f5f5; background-color: #000000;font-weight: bold;">
        <tr>
            <td style="">Запрос</td>
            <td>Контроллер</td>
            <td style="width: 120px;">Время генерации, сек </td>
            <td style="width: 120px;">Время генерации без виджетов, сек </td>
            <td style="width: 180px;padding-left: 10px;padding-right: 10px;">Время</td>
        </tr>
        </thead>

        <tbody>
        <?$i=0;?>
        <?$requests=array($request);?>
        <?foreach($requests as $r => $data):?>
            <?$total_time = Profiler::sum_widgets_time_for_request($data->request_code);?>
            <tr style="background-color: #0e90d2;">
                <td style="text-align: left;padding-left: 5px;"><?=$data->request_url?></td>
                <td style="width:250px;"><?=$data->controller_action?></td>
                <td><?=sprintf("%.3f", round($data->execution_time, 3))?></td>
                <td><?=sprintf("%.3f", round($data->execution_time, 3)-round($total_time, 3))?></td>
                <td><?=$data->time?></td>
            </tr>
            <tr>
                <td colspan="5">
                    <table style="width: 100%;border-collapse: separate; border-spacing: 2px;">
                        <tbody>
                        <?foreach($widgets as $tdata):?>
                            <tr style="background-color: #ffbd40;">
                                <td style="width: 20px;"></td>
                                <td style="text-align: left"><?=$tdata->message?></td>
                                <td style="width:222px;padding-left: 10px;padding-right: 10px;"><?=sprintf("%.3f", round($tdata->execution_time, 3))?></td>
                                <td style="width:198px;"><?=$tdata->time?></td>
                            </tr>
                        <?endforeach?>
                        </tbody>
                    </table>
                    <table style="width: 100%">
                        <tr style="background-color: #ffbd40;">
                            <td style="width: 20px;"></td>
                            <td style="text-align: left;font-weight: bold;">СУММАРНОЕ ВРЕМЯ ВЫПОЛНЕНИЯ ВСЕХ ВИДЖЕТОВ:</td>
                            <td style="font-weight: bold;width:443px;"><?=sprintf("%.3f", round($total_time, 3))?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?++$i;?>
        <?endforeach?>
        </tbody>

    </table>
</div>

<br/>
<br/>

<div style="padding-bottom: 10px">
    <span style="font-weight: bold;">ДЕТАЛЬНАЯ ИНФОРМАЦИЯ ПО SQL-ЗАПРОСАМ</span>
</div>

<?=$request->sql_stat?>


<br/>
<br/>