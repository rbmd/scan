<div class="col span10" style="text-align: center;">

    <div style="padding-bottom: 10px"><span style="font-weight: bold;">СТАТИСТИКА ЗАПРОСОВ ПО "<?=$c?>"</span></div>

    <div style="float: left;"><span><a href="/admin/profiler/">Назад</a></span></a></span></div>

    <table style="width:100%; border: 1px solid #000000;">

        <thead style="color: #f5f5f5; background-color: #000000;font-weight: bold;">
        <tr>
            <td style="">Запрос</td>
            <td>Контроллер</td>
            <td style="width: 120px;">Время генерации, сек </td>
            <td style="width: 120px;">Время генерации без виджетов, сек </td>
            <td style="width: 180px;padding-left: 10px;padding-right: 10px;">Время</td>
        </tr>
        </thead>

        <tbody>
        <?$i=0;?>
        <?foreach($requests as $r => $data):?>
            <?$total_time = Profiler::sum_widgets_time_for_request($data->request_code);?>
            <tr style="background-color: <?if($i%2):?>#87cefa;<?else:?>#0e90d2;<?endif?>">
                <td style="text-align: left;padding-left: 5px;"><a style="color:#000000;" href="/admin/profiler/requestdetail/request_code/<?=$data->request_code?>/"><?=$data->request_url?></a></td>
                <td style="width:250px;"><?=$data->controller_action?></td>
                <td><?=sprintf("%.3f", round($data->execution_time, 3))?></td>
                <td><?=sprintf("%.3f", round($data->execution_time, 3)-round($total_time, 3))?></td>
                <td><?=$data->time?></td>
            </tr>
            <? /*
            <tr>
                <td colspan="5">
                    <table style="width: 100%">
                        <tbody>
                        <?$widgets = Profiler::model()->widgets_for_request($data->request_code)->findAll();?>
                        <?foreach($widgets as $tdata):?>
                            <tr style="background-color: #ffbd40;">
                                <td style="width: 20px;"></td>
                                <td style="text-align: left"><?=$tdata->message?></td>
                                <td style="width:226px;padding-left: 10px;padding-right: 10px;"><?=sprintf("%.3f", round($tdata->execution_time, 3))?></td>
                                <td style="width:195px;"><?=$tdata->time?></td>
                            </tr>
                        <?endforeach?>
                        </tbody>
                    </table>
                    <table style="width: 100%">
                        <tr style="background-color: #ffbd40;">
                            <td style="width: 20px;"></td>
                            <td style="text-align: left;font-weight: bold;">СУММАРНОЕ ВРЕМЯ ВЫПОЛНЕНИЯ ВСЕХ ВИДЖЕТОВ:</td>
                            <td style="font-weight: bold;width:443px;"><?=sprintf("%.3f", round($total_time, 3))?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            */?>
            <?++$i;?>
            <?endforeach?>
        </tbody>

    </table>
</div>

<br/>
<?$this->widget('CLinkPager', array(
    'pages' => $pages,
))?>

<br/>
<br/>