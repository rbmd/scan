<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dmitry Rusakov
 * Date: 15.03.13
 * Time: 14:49
 */
?>

<div style="float: left;"><span><a href="/admin/profiler/">Назад</a></span></a></span></div>

<div style="text-align: center;margin-top: 40px;">

    <div style="padding-bottom: 10px"><span style="font-weight: bold;">ВИДЖЕТ "<?=$w?>"</span></div>

    <table style="width:100%; border: 1px solid #000000;">

        <thead style="color: #f5f5f5; background-color: #000000;font-weight: bold;">
        <tr>
            <td style="">Виджет</td>
            <td style="width: 400px;padding-left: 10px;padding-right: 10px;">Запрос</td>
            <td style="width: 100px;padding-left: 10px;padding-right: 10px;">Время работы, сек</td>
            <td style="width: 200px;padding-left: 10px;padding-right: 10px;">Время запроса</td>
        </tr>
        </thead>

        <tbody>
        <?$i=0;?>
        <?foreach($widgets as $widget => $data):?>
        <tr style="background-color: <?if($i%2):?>#87cefa;<?else:?>#0e90d2;<?endif?>">
            <td style="text-align: left;padding-left: 5px;"><?=$data->message?></td>
            <td><?=$data->request_url?></td>
            <td><?=sprintf("%.3f", round($data->execution_time, 3))?></td>
            <td><?=$data->time?></td>
        </tr>
        <?++$i;?>
        <?endforeach?>
        </tbody>
</table>
</div>

<br/>

<?$this->widget('CLinkPager', array(
    'pages' => $pages,
))?>

<br/>
<br/>