<a href="/admin/series/create/" class="btn btn-success">Создать серию</a>
<br/><br/>
<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/series/')), 'get'); ?>

<div class="well">
  <table class="b-search-params">
    <tr>
      <td class="b-search-params__value">

        <label class="text inline" for="id">
          ID <? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?>
        </label>

        <label class="text inline" for="name">
         Название <? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?>
        </label>
      </td>
      <td><? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?></td>
    </tr>
  </table>
</div>

<? echo CHtml::endForm(); ?>
<table class="table">

  <thead>
  	<tr>
  		<th>ID</th>
  		<th>&nbsp;</th>
  		<th>Название</th>
  	</tr>
  </thead>
	<?
	foreach($series as $st)
	{
	?>
	<tr>
		<td><?=$st['id']?></td>
		<td><?if(strlen($st->preview_img) > 0){?><img src="<?=$st->preview_img?>" width="70"/><?}?></td>
		<td><a href="/admin/series/edit/?id=<?=$st['id']?>"><?=$st['name']?></a></td>
	</tr>
	<?
	}
	?>
	
</table>
<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>