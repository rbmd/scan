<input type="text" name="filter" value="<?= $filter?>" /> <a href="/admin/series/getseries" class="dynamic" data-action="refresh-popup" data-sortable-model="series" data-popup-type="attach-models" data-attach-to-model="<?= $attachToModel?>" data-variable-name="<?= $variableName?>" onclick="dynamicClick($(this)); return false;">Найти</a> 
<table id="attach-list" class="table table-striped table-condensed">
<?
foreach ($output as $tvr) {
	?>
	<tr>
		<td>
			<?= $tvr['id'] ?>
		</td>
		<td>
			<?= $tvr['name']?>
		</td>
		<td>
			<a href="" class="dynamic" data-action="" data-sortable-model="series" data-variable-name="<?= $variableName?>" data-id="<?= $tvr['id']?>" data-name="<?= $tvr['name']?>" data-attach-to-model="<?= $attachToModel?>" onclick="dynamicClick($(this)); return false;"></a>
		</td>
	</tr>
	<?
}
?>
</table>

<div class="pagination pagination-right">
<?
$this->widget('CLinkPager', array(
	'pages' => $pages,
	'id' => 'ajax-attach-paginator',
	'header' => '',
	'htmlOptions' => array(
		'class' => '',
		'selectedPageCss' => 'active',
	),
));
?>
</div>