<h1><?
if($action == 'create')
{
	?>Создание серии статей<?
}
else
{
	?>Серия статей #<?=$model->id?> <?=$model->name?><?
}
?>
</h1>

<div class="span10">
	<div class="tab-content">
		<div class="tab-pane active" id="content">

		<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'series-form',
			'enableClientValidation' => true,
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		)); 
		?>

		<fieldset>

		   <div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<div class="input-append"><?
								echo $form->textField($model, 'code', array('data-sync-from' => 'slug', 'class' => 'span7'));
								$cookie_var = Yii::app()->request->cookies['transliterate_lock_state'];
								?><button data-action="trigger-lock" data-sync-to="slug" class="btn icon" onclick="dynamicClick($(this)); return false;"><?= isset($cookie_var->value[ Yii::app()->request->requestUri ]) ? $cookie_var->value[ Yii::app()->request->requestUri ] : 'w' ?></button>
							</div>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model, 'preview_img', array('class' => 'control-label')); ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?=$form->textField($model, 'preview_img', array('class' => 'span6'))?>
							<script type="text/javascript">
								$(function() {
									ckfinderManager.create('<?=get_class($model)?>_preview_img');
								});
							</script>
							<span class="help-inline"><?php echo $form->error($model, 'preview_img'); ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_title', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_title', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_title') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_keywords', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_keywords', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_keywords') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_description', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textArea($model, 'seo_description', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_description') ?></span>
						</div>
					</div>
				</div>
			</div>
		</fieldset>

	<?
	if($action == 'edit')
	{
	?>
	<div>
		<h3>Статьи, входящие в серию</h3>
		<?
		foreach($articles as $v)
		{
		?>
			<a href="/admin/articles/edit/?id=<?=$v->id?>"><?=$v->name?></a> <?=$v->date_publish?> <br />
		<?
		}
		?>
	 </div>
	<?
	}
	?>

		<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

		<div class="form-actions">
			<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')) ?>
			<?= CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply')) ?>
			<?= CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')) ?>
			<div style="float:right;">
				<? echo CHtml::ajaxButton(
					'Удалить',
					'/admin/series/delete',
					array(
						'data' => "id={$model->id}",
						'beforeSend' => 'function(){}',
						'complete' => 'function(){}',
						'success' => 'function(){alert("Серия статей успешно удалена!"); window.location.href="/admin/series";}',
						'error' => 'function(){alert("Во время удаления серии произошла непредвиденная ошибка!");}',
					),
					array(
						'class' => 'btn btn-danger',
						'id'=>'delete_btn',
						'confirm'=>'Вы уверены, что хотите безвозвратно удалить эту серию статей?',
					)
				); ?>
			</div>
		</div>
 
		<? $this->endWidget(); ?>
	</div>
</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_story', '
	$("#cancel_btn").click( function(e) {
		window.location.href = "/admin/series/";
	});
', CClientScript::POS_READY)
?>