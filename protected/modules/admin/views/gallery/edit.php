<div class="gallery-form">
	<div class="page-header">
		<h1>Редактирование галереи &laquo;<?=$Gallery->name?>&raquo;</h1>
	</div>
	<input type="hidden" class="Gallery_id" value="<?=$Gallery->id?>" />
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'action' => '/admin/gallery/save',
		'id' => 'new-gallery-form',
		'enableClientValidation' => false,
		'enableAjaxValidation' => false,
		'clientOptions' => array(
			'validateOnSubmit' => false,	
		),
		'htmlOptions' => array(
		    'class' => 'form-vertical',
		),
	)); 
	?>
	<fieldset>
		<div class="control-group">
			<?= $form->labelEx($Gallery, 'name', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row">
					<div class="span8">
						<?= $form->textfield($Gallery, 'name') ?>
						<span class="help-inline"><?=$form->error($Gallery, 'name') ?></span>
					</div>
				</div>
			</div>
		</div>	
	</fieldset>

	<?php $this->endWidget(); ?>
	<h3>Загрузить изображения</h3>
	<br/>
	<?php $this->widget('ext.xupload.XUpload', array(
	        'url' => "/admin/gallery/uploadImages/",
	        'model' => $XUploadForm,
	        'attribute' => 'file',
	        'multiple' => true,
	));?>

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'action' => '/admin/gallery/save',
		'id' => 'new-gallery-form',
		'enableClientValidation' => true,
		'enableAjaxValidation' => false,
		'clientOptions' => array(
			'validateOnSubmit' => true,	
		),
		'htmlOptions' => array(
		    'class' => 'form-vertical',
		),
	)); 
	?>
	<table class="table table-striped photos-list">
		<?php
			$i = 0;
			foreach ($Gallery->photos as $photo) :
				$i++;
		        list(,$subdir, $filename) = explode('/', $photo['path']); ?>
				<tr class="template-download">
			        <td class="preview" width="160">
			            <a href="<?=$photo->img('large')?>" title="<?=$filename?>" rel="gallery"><img src="<?=ThumbsMaster::getThumb($photo->img('large'), ThumbsMaster::$settings['200_auto'], true)?>" style="height: 100px;"></a>
			        </td>
			        <td class="name" width="200">
			            <span>Файл: </span>
			            <a href="<?=$photo->img('large')?>" title="<?=$filename?>" rel="gallery"><?=$filename?></a><br>
			           
			        </td>
			        <td class="fields">
			        	<input type="hidden" class="GalleryPhoto_id" value="<?=$photo['id']?>" />
			        	<input type="hidden" class="GalleryPhoto_order_num" value="<?=$photo->order_num?>" />
			            <input type="hidden" class="GalleryPhoto_filedir" value="<?=$photo['path']?>">
			            <div class="b-photo-edit-textfields">
			                <!--<input name="GalleryPhotos[][name]" class="GalleryPhoto_name" type="text" maxlength="512" placeholder="Название" value="<?=$photo['name']?>"><br>-->
			                <input name="GalleryPhotos[][alt]" class="GalleryPhoto_alt" type="text" maxlength="512" placeholder="alt/title" value="<?=$photo['alt']?>"><br>
			                <input name="GalleryPhotos[][copyright]" class="GalleryPhoto_copyright" type="text" maxlength="512" placeholder="Авторство" value="<?=$photo['copyright']?>">
			            </div>
			           <textarea name="GalleryPhotos[][detail_text]" class="GalleryPhoto_detail-text" type="text" maxlength="512" placeholder="Описание"><?=$photo['detail_text']?></textarea>
						  <br>
			        </td>
			        <td colspan="2"></td>
			        <td class="delete" width="120">
			            <input type="button" class="btn btn-danger deletephoto" value="Удалить" data="<?=$photo['id']?>"/>
			        </td>
			    </tr>
		<?php endforeach; ?>
	</table>
	<?php $this->endWidget(); ?>
	<div class="form-actions">
		<input class="btn btn-success submit" name="save" id="gallery-save" type="button" value="Сохранить">		
		<input class="btn btn-primary apply" name="apply" id="gallery-apply" type="button" value="Применить">		
		<input class="btn btn-danger apply" name="delete" type="button" value="Удалить галерею" onclick="javascript:if (confirm('Вы уверены, что хотите удалить галерею?')) window.location='/admin/gallery/delete/id/<?=$Gallery->id?>/'">		
	</div>
</div>