<?php
?>
<a href="/admin/gallery/create/" class="btn btn-success">Создать галерею</a><br/><br/>
<? echo CHtml::beginForm(CHtml::normalizeUrl(array('/admin/gallery/')), 'get'); ?>

<div class="well">
  <table class="b-search-params">
    <tr>
      <td class="b-search-params__value">
        <label for="id" class="text inline">
          ID <? echo CHtml::textField('id', Yii::app()->request->getParam('id','')); ?>              
        </label>

        <label for="name" class="text inline">
          Название <? echo CHtml::textField('name', urldecode(Yii::app()->request->getParam('name',''))); ?>
        </label>
      </td>
      <td>
        <? echo CHtml::submitButton('Искать', array('name'=>'', 'class' => 'btn btn-primary')) ?>
      </td>
    </tr>
  </table>
</div>
<? echo CHtml::endForm(); ?>
<table class="table galleries-list">
  <thead>
  	<tr>
  		<th width="50">ID</th>
  		<th>Название</th>
  		<th>&nbsp;</th>
  	</tr>
  </thead>
	<?php foreach ($Galleries as $gallery) : ?>
			<tr>
				<td><?=$gallery->id?></td>
				<td>
		            <a class="tree_node_edit" href="/admin/gallery/edit/id/<?=$gallery->id?>/"><?=$gallery->name?></a>
		        </td>
		        <td style="text-align: right;">
		        	<input class="btn btn-danger apply" name="delete" type="button" value="Удалить" onclick="javascript:if (confirm('Вы уверены, что хотите удалить галерею?')) window.location='/admin/gallery/delete/id/<?=$gallery->id?>/'">
		        </td>
			</tr>
	<?php endforeach; ?>
</table>
<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>