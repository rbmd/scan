<h1><?
if($action == 'create')
{
	?>Создание<?
}
else
{
	?>#<?=$model->id?><?
}
?>
</h1>
<div class="form">
	<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'tags-form',
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => false, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		));
	?>

		<fieldset>
			<div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>




			
  		<div class="control-group">
			<?= $form->labelEx($model, 'detail_text', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'detail_text', array('class' => 'span12', 'rows'=>5)) ?>
						<span class="help-inline"><?= $form->error($model, 'detail_text') ?></span>
					</div>
				</div>
			</div>
		</div>

      <script>
        $(document).ready(function() {
           wysiwygManager.createEditor('<?=get_class($model)?>_detail_text', {config: 'full.js'});
        });
      </script>


		</fieldset>



	<?= CHtml::errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
	
	<div class="submit">
        <? echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
        <? echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
        <? echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')); ?>
	</div>

    <div style="float:right;">
        <? echo CHtml::ajaxButton(
        'Удалить',
        '/admin/likbez/rubrics/delete/',
        array(
            'data' => "id={$model->id}",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Элемент успешно удалён!"); window.location.href="/admin/likbez/rubrics/";}',
            'error' => 'function(){alert("Во время удаления элемента произошла непредвиденная ошибка!");}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'delete_btn',
            'confirm'=>'Вы уверены, что хотите безвозвратно удалить этот элемент?',
        )
    ); ?>
    </div>
	 
<? $this->endWidget(); ?>

</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_tags', '
    $("#cancel_btn").click( function(e) {
        window.location.href = "/admin/likbez/rubrics/";
    });
', CClientScript::POS_READY)
?>