<div class="page-header">
	<?
	if($action == 'create')
	{
		?><h1>Создание опроса</h1><?
	}
	else
	{
		?>
		<h1>Опрос #<?=$model->id?></h1>
		<h2><?=$model->name?></h2>  
		<?
	}
	?>
</div>



<?
$form = $this->beginWidget('CActiveForm', array(
	'action' => !empty($model->id) ? "?id={$model->id}" : '',
	'id' => 'votes-form',
	'enableClientValidation' => true,
	'enableAjaxValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,	
	),
	'htmlOptions' => array(
						'class' => 'form-horizontal',
					),
)); 
?>

<fieldset>
	<div class="control-group">
		<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->checkBox($model, 'active') ?>
					<span class="help-inline"><?= $form->error($model, 'active') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
					<span class="help-inline"><?= $form->error($model, 'name') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<div class="input-append"><?
						echo $form->textField($model, 'code', array('data-sync-from' => 'slug', 'class' => 'span7'));
						$cookie_var = Yii::app()->request->cookies['transliterate_lock_state'];
						?><button data-action="trigger-lock" data-sync-to="slug" class="btn icon" onclick="dynamicClick($(this)); return false;"><?= isset($cookie_var->value[ Yii::app()->request->requestUri ]) ? $cookie_var->value[ Yii::app()->request->requestUri ] : 'w' ?></button>
					</div>
					<span class="help-inline"><?= $form->error($model, 'code') ?></span>
				</div>
			</div>
		</div>
	</div>


	<div class="control-group ">
		<?php echo $form->labelEx($model, 'pic', array('class' => 'control-label')); ?>
		<div class="controls">

			<?=$form->textField($model, 'pic', array('class' => 'b-admin__text'))?>
			<script type="text/javascript">
				$(function() {
					ckfinderManager.create('<?=get_class($model)?>_pic');
				});
			</script>

			<span class="help-inline"><?php echo $form->error($model, 'preview_img'); ?></span>
		</div>
	</div>

	<div class="control-group">
		<? echo CHtml::activeLabel($model, 'date_active_start', array('class' => 'control-label')); ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',array(
					'model' => $model,
					'attribute' => 'date_active_start',
					'mode' => 'datetime',
					'language' => 'ru',
					'options' => array(
						//'dateFormat' => 'dd/mm/yy',
						'dateFormat' => 'yy-mm-dd',
						'timeFormat' => 'hh:mm:ss',
					),
				));?>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<? echo CHtml::activeLabel($model, 'date_active_end', array('class' => 'control-label')); ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',array(
					'model' => $model,
					'attribute' => 'date_active_end',
					'mode' => 'datetime',
					'language' => 'ru',
					'options' => array(
						//'dateFormat' => 'dd/mm/yy',
						'dateFormat' => 'yy-mm-dd',
						'timeFormat' => 'hh:mm:ss',
					),
				));?>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'poll_type', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->dropDownList($model, 'poll_type', $model->vote_poll_types) ?>
					<span class="help-inline"><?= $form->error($model, 'poll_type') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'type', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->dropDownList($model, 'type', $model->vote_types) ?>
					<span class="help-inline"><?= $form->error($model, 'type') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'check_revoting_mode', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->dropDownList($model, 'check_revoting_mode', $model->revoting_modes) ?>
					<span class="help-inline"><?= $form->error($model, 'check_revoting_mode') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'seo_title', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->textField($model, 'seo_title', array('class' => 'span8')) ?>
					<span class="help-inline"><?= $form->error($model, 'seo_title') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'seo_keywords', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->textField($model, 'seo_keywords', array('class' => 'span8')) ?>
					<span class="help-inline"><?= $form->error($model, 'seo_keywords') ?></span>
				</div>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'seo_description', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row">
				<div class="span8">
					<?= $form->textArea($model, 'seo_description', array('class' => 'span8')) ?>
					<span class="help-inline"><?= $form->error($model, 'seo_description') ?></span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="page-header">
		<h3>Варианты ответов</h3>
	</div>

	<div id="items_container">
		<?
		foreach($model->items as $item) {
			$this->renderPartial('admin.views.polls.parts.item', array('item' => $item));
		}
		?>
	</div>

	<script>
		//Добавление варианта голосования
		function addPollItem()
		{
			$.ajax({
				url: '/admin/polls/ajaxaddpollitem/',

			}).success(function(data){
				$('#items_container').append(data);
			});
		}
	</script>
	<a href="" class="btn btn-small" onclick="addPollItem(); return false;"><i class="icon-plus"></i></a>
	
</fieldset>

<?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>

<div class="form-actions">
	<?= CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')) ?>
	<?= CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)) ?>
	<?= CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')) ?>
	<div style="float:right;">
		<? echo CHtml::ajaxButton(
			'Удалить',
			'/admin/polls/delete',
			array(
				'data' => "id={$model->id}",
				'beforeSend' => 'function(){}',
				'complete' => 'function(){}',
				'success' => 'function(){alert("Опрос удален"); window.location.href="/admin/polls/";}',
				'error' => 'function(){alert("Во время удаления опроса произошла непредвиденная ошибка!");}',
			),
			array(
				'class' => 'btn btn-danger',
				'id'=>'delete_btn',
				'confirm'=>'Вы уверены, что хотите удалить опрос?',
			)
		); ?>
	</div>
</div>

<?php $this->endWidget(); ?>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_votes', '
	$("#cancel_btn").click( function(e) {
		window.location.href = "/admin/polls/";
	});
', CClientScript::POS_READY)
?>