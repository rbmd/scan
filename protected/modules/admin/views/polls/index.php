<a href="/admin/polls/create/" class="btn btn-success">Создать опрос</a>
<br /><br />

<table class="table table-bordered">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>ID</th>
			<th>Название</th>
		</tr>
	</thead>
<?
foreach($votes as $n)
{
	?>
	<tr>
		<td><span class="btn disabled btn-<?=(($n['active'] == 1) ? 'success' : 'danger')?> btn-mini" data-id="<?=$n->id?>">&nbsp;&nbsp;</span></td>
		<td>#<?=$n['id']?></td>
		<td><a href="/admin/polls/edit/?id=<?=$n['id']?>"><?=$n['name']?></a></td>		 
	</tr>
	<?
}
?>
</table>

<?$this->widget('Pagination', array(
    'pages' => $pages,
))?>