<?php
/**
 * Tree for article
 */

class TreeWidget extends CWidget
{
    public $name = 'Articles[parent_id]';
    public $parent_id;
    public $parent_scope = null;

    public function init()
    {
    }

    public function run()
    {
        if (isset($this->parent_scope))
        {
            $tree_parent = Tree::model()->findByPK($this->parent_scope);
            $tree = $tree_parent->descendants()->findAll();
        }
        else
            $tree = Tree::model()->findAll(array('order' => 'cleft'));

        $this->render('Tree', array('tree'      => $tree,
                                    'name'      => $this->name,
                                    'parent_id' => $this->parent_id)
        );

    }
}