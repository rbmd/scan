<?php

/**
 * Управление пользователями
 */
class UsersController extends Controller
{
	public $layout = 'index';

	public function filters()
	{
		return array(
			'accessControl',
			'preaccess',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', 'actions' => array('index', 'edit'), 'users' => array('@'),),
			array('deny', 'actions' => array('index', 'edit'), 'users' => array('*'),),
			// array('allow', 'actions' => array('setUserActive'), 'roles' => array('commentsManager'),),
		);
	}

	public function actionIndex()
	{
		$criteria = new CDbCriteria();

		$model = new User('search');
		$model->unsetAttributes();

		$pager_attributes = array();

		$get_params = array();

		//Получим массив $get_params переменных, которые пришли нам обычным GET-запросом
		parse_str(parse_url(Yii::app()->request->requestUri, PHP_URL_QUERY), $get_params);

		/**
		 * АДСКИЕ ТАНЦЫ С БУБНОМ!!!
		 * Смысл такой: если переменной уже нет в гете, а она всё есть есть в url в виде /имя_переменной/значение,
		 * то её надо удалить из $_REQUEST, так как Yii парсит урл и складывает всё в $_REQUEST.
		 * Если же переменная есть в гете и есть в $_REQUEST, но в гете у неё уже другое значение, то надо заменить
		 * значение и в $_REQUEST.
		 */
		if (isset($_REQUEST['User']) && !empty($get_params))
		{
			foreach ($_REQUEST['User'] as $key => $val)
			{
				if (empty($get_params['User'][$key]) && !empty($_REQUEST['User'][$key]))
				{
					unset($_REQUEST['User'][$key]);
				}
				else
				{
					if (
						!empty($get_params['User'][$key]) &&
						!empty($_REQUEST['User'][$key]) &&
						$get_params['User'][$key] != $_REQUEST['User'][$key]
					)
					{
						$_REQUEST['User'][$key] = $get_params['User'][$key];
					}
				}
			}
		}

		if (isset($_REQUEST['User']))
		{
			$model->attributes = $_REQUEST['User'];

			$criteria->compare('id', $model->id, false);
			$criteria->compare('email', $model->email, true);
			$criteria->compare('username', $model->username, true);
			$criteria->compare('lastname', $model->lastname, true);
			$criteria->compare('firstname', $model->firstname, true);
			if ($model->superuser)
			{
				$criteria->compare('superuser', $model->superuser, true);
			}

			if ($_REQUEST['User']['rule'])
			{
				$criteria->mergeWith(array(
					'select'    => 't.*, GROUP_CONCAT(i.description SEPARATOR "<br/>") AS `rules`',
					'join'      => 'LEFT JOIN AuthAssignment a ON (t.id=a.userid) LEFT JOIN AuthItem i ON (i.name=a.itemname)',
					'group'     => 't.id',
					'condition' => 'i.name = :name',
					'params'    => array(':name' => $_REQUEST['User']['rule']),
				));
			}
			else
			{
				$criteria->mergeWith(array(
					'select' => 't.*'
				));
			}

			foreach ($_REQUEST['User'] as $key => $val)
			{
				if (!empty($val))
				{
					$pager_attributes[$key] = $val;
				}
			}
		}

		$sort = new CSort();

		$sort->modelClass = get_class($model);

		$sort->attributes = array(
			'id'        => array(
				'asc'     => 'id',
				'desc'    => 'id',
				'default' => 'desc'
			),
			'email'     => array(
				'asc'  => 'email',
				'desc' => 'email'
			),
			'username'  => array(
				'asc'  => 'username',
				'desc' => 'username'
			),
			'lastname'  => array(
				'asc'  => 'lastname',
				'desc' => 'lastname'
			),
			'firstname' => array(
				'asc'  => 'firstname',
				'desc' => 'firstname'
			)
		);

		$sort->defaultOrder = array(
			'id' => true,
		);

		if (!empty($pager_attributes))
		{
			$sort->params['User'] = $pager_attributes;
		}



		$pages           = new CPagination(User::model()->count($criteria));
		$pages->pageSize = 40;
		$pages->params   = array('sort' => $sort->getOrderBy() . (($sort->getDirection($sort->getOrderBy())) ? '.desc' : ''));

		if (!empty($pager_attributes))
		{
			$pages->params['User'] = $pager_attributes;
		}
		$criteria->order = $sort->getOrderBy() . (($sort->getDirection($sort->getOrderBy())) ? ' DESC' : ' ASC');
		$pages->applyLimit($criteria);

		$users = User::model()->findAll($criteria);

		foreach ($users as $user)
		{
			$user_ids[] = $user->id;
		}

		$where  = (isset($user_ids)) ? 'WHERE userid IN (' . implode(',', $user_ids) . ')' : '';
		$sql    = 'SELECT userid, GROUP_CONCAT(description SEPARATOR "<br/>") AS rules
				FROM AuthAssignment a
				LEFT JOIN AuthItem i ON (a.itemname=i.name) ' . $where . ' GROUP BY a.userid';
		$rights = Yii::app()->db->createCommand($sql)->queryAll();


		foreach ($rights as $right)
		{
			$rights_indexer[$right['userid']] = $right['rules'];
		}

		foreach ($users as $user)
		{
			if (isset($rights_indexer[$user->id]))
			{
				$user->rules = $rights_indexer[$user->id];
			}
		}

		$this->render('index',
			array(
				'users'     => $users,
				'pages'     => $pages,
				'model'     => $model,
				'sort'      => $sort,
				'order'     => '',
				'direction' => '',
			)
		);
	}

	public function actionCreate()
	{
		//Подрубаем css со свойствами для валидации полей формы
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);

		$model = new User;

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'adminedit-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		//В случае POST-запроса, производим валидацию и сохранение данных
		if (isset($_POST['User']))
		{
			$model->scenario   = 'adminedit';
			$model->attributes = $_POST['User'];
			$model->createtime = date('Y-m-d H:i:s');

			if ($model->save())
			{
				//В случае успешного обновления данных профайла, - редирект на профайл
				if (!isset($_POST['apply']))
				{
					$this->redirect('/admin/users/');
				}
				else
				{
					$this->refresh();
				}
			}
		}

		$this->render('edit', array('model' => $model));
	}

	public function actionEdit($id = 0)
	{
		//Подрубаем css со свойствами для валидации полей формы
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);

		//Получить данные указанного пользователя
		if ((int)$id)
		{
			$model = User::model()->findByPk($id);
		}
		else
		{
			$model = new User;
		}

		if (empty($model))
		{
			$this->redirect('/admin/users/');
		}

		$model->scenario = 'adminedit';

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'adminedit-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		//В случае POST-запроса, производим валидацию и сохранение данных
		if (isset($_POST['User']))
		{
			$model->attributes = $_POST['User'];

			if ($model->isNewRecord)
			{
				$model->createtime = date('Y-m-d H:i:s');
			}

			if ($result = $model->validate())
			{
				if ($model->save())
				{
					//В случае успешного обновления данных профайла, - редирект на профайл
					if (!isset($_POST['apply']))
					{
						$this->redirect('/admin/users/');
					}
					else
					{
						$this->refresh();
					}
				}
			}
		}

		$new_pwd   = '';
		$pwd_error = '';

		if (isset($_POST['new_pwd']))
		{
			//die($_POST['new_pwd']);
			$new_pwd = trim($_POST['new_pwd']);

			if (mb_strlen($new_pwd) < 6)
			{
				$pwd_error = 'Пароль должен быть не менее 6 символов!';
			}
			else
			{
				if (!preg_match('/^[a-z0-9]+$/ui', $new_pwd))
				{
					$pwd_error = 'Пароль может содержать только латинские буквы и цифры!';
				}
			}

			if (empty($pwd_error))
			{
				$salt            = UserIdentity::makesalt();
				$model->password = $salt . UserIdentity::encrypting($new_pwd, $salt);

				if ($model->save())
				{
					$this->redirect('/admin/users/');
				}
			}
		}

		$this->render('edit', array('model' => $model, 'new_pwd' => $new_pwd, 'pwd_error' => $pwd_error));
	}


	//Для заданого id блокирует/разблокирует клиента
	public function actionSetUserActive($id)
	{
		$model         = User::model()->findByPk($id);
		$model->scenario = 'adminedit';
		$model->status = ($model->status == 1) ? -1 : 1;
		$model->save();
	}



	//Возвращает json массив по введенному названию
	public function actionGetByName($s)
	{
		$q = new CDbCriteria();
		#$q->addSearchCondition('name', $s);
		$q->compare('LOWER(email)',strtolower($s),true, 'OR'); 
		$q->compare('LOWER(username)',strtolower($s),true, 'OR'); 
		$q->compare('LOWER(firstname)',strtolower($s),true, 'OR'); 
		$q->compare('LOWER(lastname)',strtolower($s),true, 'OR'); 
		
		$users = User::model()->findAll($q);
		
		$arr = array();
		
		if(!is_null($users))
		{
			$i = 0;
			
			foreach($users as $s)
			{
				$arr[$i]['id'] = $s->id;
				$arr[$i]['key'] = $s->id;
				$arr[$i]['value'] = $s->firstname. ' ' . $s->lastname;
				$i++;
			}
		}
		
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}	
}
