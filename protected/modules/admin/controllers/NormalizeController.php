<?php
class NormalizeController extends Controller
{
	public function actionIndex()
	{
		//$this->setCommentsEditor();
		$this->actionUpdateCountComments();
		$this->actionUpdateCountTags();
	}


	// Обновить количество комментариев в статьях
	public function actionUpdateCountComments()
	{
		$sql  = 'SELECT `bg_articles`.`id` AS `id`, COUNT(`bg_comments`.`id`) AS `count` FROM `bg_articles` LEFT JOIN `bg_comments` ON (`bg_comments`.`entity_id`=`bg_articles`.`id`) WHERE `bg_comments`.`active`=1 GROUP BY `bg_articles`.`id` HAVING `count` > 0';
		$tags = Yii::app()->db->createCommand($sql)->queryAll();
		Yii::app()->db->createCommand('UPDATE `bg_articles` SET `comments_count`=0')->execute();
		for ($i = 0; $i < count($tags); $i++)
		{
			$sql = 'UPDATE `bg_articles` SET `comments_count`=' . $tags[$i]['count'] . ' WHERE `id`=' . $tags[$i]['id'];
			Yii::app()->db->createCommand($sql)->execute();
		}
		echo count($tags);
		exit;
	}


	// Обновить количество тегов
	public function actionUpdateCountTags()
	{
		$sql = 'SELECT tag_id, COUNT(article_id) AS c FROM bg_articles_to_tags LEFT JOIN bg_articles a ON (a.id=bg_articles_to_tags.article_id) WHERE a.active=1 ';
		Yii::app()->db->createCommand('UPDATE bg_tags SET count=0, count_guides=0')->execute();
		$tags = Yii::app()->db->createCommand($sql . 'GROUP BY tag_id')->queryAll();
		for ($i = 0; $i < count($tags); $i++)
		{
			$sqlup = 'UPDATE bg_tags SET count=' . $tags[$i]['c'] . ' WHERE id=' . $tags[$i]['tag_id'];
			Yii::app()->db->createCommand($sqlup)->execute();
		}
		$tags = Yii::app()->db->createCommand($sql . ' AND a.is_guide=1 AND active=1 GROUP BY tag_id')->queryAll();
		for ($i = 0; $i < count($tags); $i++)
		{
			$sqlup = 'UPDATE bg_tags SET count_guides=' . $tags[$i]['c'] . ' WHERE id=' . $tags[$i]['tag_id'];
			Yii::app()->db->createCommand($sqlup)->execute();
		}
	}


	// Установить для комментариев метку "Редактор БГ"
	public function setCommentsEditor()
	{
		echo __METHOD__ . "\n";
		Yii::app()->db->createCommand('UPDATE bg_comments SET editor=0')->execute();
		// Получаем список пользователей с ролью editor
		$users = Yii::app()->db->createCommand('SELECT userid FROM AuthAssignment WHERE itemname="editor"')->queryColumn();
		foreach ($users as $user)
		{
			Yii::app()->db->createCommand('UPDATE bg_comments SET editor=1 WHERE user_id=' . intval($user))->execute();
		}
	}
}