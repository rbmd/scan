<?php

class DefaultController extends Controller
{
	public $layout = 'index';

    public function filters()
    {
        return array(
            'accessControl',
            'preaccess',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny', 'actions'=>array('index', 'authby'), 'users'=>array('?'),),
        );
    }
	
	public function actionIndex($accessdenied="0")
	{
		$this->render('index', array('accessdenied'=>$accessdenied));
	}

    public function actionAuthBy($userid = null)
    {
        if( empty($userid) || empty(Yii::app()->user->superuser) )
        {
            $this->redirect("/admin/");
        }

        if( !Yii::app()->user->checkAccess('Admin') || !Yii::app()->user->checkAccess('canAuthorizeAsAnyUser') )
        {
            $this->redirect("/admin/");
        }

        $identity = new UserIdentity('', '');

        $identity->authAdmin($userid);

        if($identity->errorCode === UserIdentity::ERROR_NONE)
        {
            $duration = 3600*24*365;
            Yii::app()->user->login($identity, $duration);

            $this->redirect('/');
        }
        else
        {
            throw new CHttpException(404);
        }
    }

    public function actionClearcache()
    {
        if( !Yii::app()->user->checkAccess('Admin') || !Yii::app()->user->checkAccess('canFlushMemcache') )
        {
            $this->redirect("/admin/");
        }

        Yii::app()->cache->flush();

        $this->render('index', array('clearcache'=>true));
    }
	
	
	/**
	 * Включение режима разработчика
	 * Происходит установка обычной куки, которая проверяется в /index.php и затем подключается другой config или если необходимо проводятся другие действия
	 */
	public function actionEnabledebug($action = '')
	{
		if( !Yii::app()->user->checkAccess('Admin') )
		{
			$this->redirect("/admin/");
		}
		
		//Ставим куку на час
		if($action == 'enable')
		{
			setcookie('bgsee', 1, time() + 3600, '/');
			$this->redirect('/admin/default/enabledebug/');
		}
		//Убиваем куку
		elseif($action == 'disable')
		{
			setcookie('bgsee', 1, time() - 3600, '/');
			$this->redirect('/admin/default/enabledebug/');
		}	
		$this->render('enabledebug');		
	}
}