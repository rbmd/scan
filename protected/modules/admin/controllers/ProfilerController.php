<?php
/**
 * User: Русаков Дмитрий
 * Date: 12.03.2013
 * Time: 19:25
 *
 * Контроллер, отвечающий за профилирование.
 *
 */
class ProfilerController extends Controller
{
    /**
     * Количество запросов на одной странице
     */
    const ITEMS_PER_PAGE = 100;

    public $layout = 'index';

    public function __construct($id, $module=null)
    {
        parent::__construct($id, $module);

        //Подрубаем css со свойствами для валидации полей формы создания лекции
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(
                'static/css/form.css'
            )
        );
    }

    /**
     * Страница профайлера
     */
    public function actionIndex()
    {
        $this->render(
            'index',
            array(
                'statistic' => $this->getStatisticBlock(),
            )
        );
    }

    /**
     * Все запросы к определённому контроллеру
     */
    public function actionAllrequests()
    {
        $c = Yii::app()->request->getParam('c', '');

        if( empty($c) )
        {
            $this->redirect('/admin/profiler/');
        }

        $interval = Profiler::getProfilerInterval();

        $items_count = Profiler::model()->
            all_requests()->
            forController($c)->
            latest($interval)->
        count();

        $criteria = new CDbCriteria();

        $pages = new CPagination( $items_count );

        $pages->pageSize = self::ITEMS_PER_PAGE;

        $pages->params = array('c' => $c);

        $pages->applyLimit($criteria);

        $requests = Profiler::model()->
            all_requests()->
            forController($c)->
            latest($interval)->
        findAll($criteria);

        $this->render(
            'allrequests',
            array(
                'c' => $c,
                'pages' => $pages,
                'requests' => $requests,
            )
        );
    }

    /**
     * Все обращения к указанному виджету (подробная статистика по каждому вызову виджета).
     */
    public function actionAllwidgets()
    {
        $w = Yii::app()->request->getParam('w', '');

        if( empty($w) )
        {
            $this->redirect('/admin/profiler/');
        }

        $interval = Profiler::getProfilerInterval();

        $items_count = Profiler::model()->
            widgetIs($w)->
            latest($interval)->
        count();

        $criteria = new CDbCriteria();

        $pages = new CPagination( $items_count );

        $pages->pageSize = self::ITEMS_PER_PAGE;

        $pages->params = array('w' => $w);

        $pages->applyLimit($criteria);

        $widgets = Profiler::model()->
            widgetIs($w)->
            latest($interval)->
        findAll($criteria);

        $this->render(
            'allwidgets',
            array(
                'w' => $w,
                'pages' => $pages,
                'widgets' => $widgets,
            )
        );
    }

    /**
     * Детальная информация по запросу
     * @param string $request_code
     */
    public function actionRequestdetail($request_code = null)
    {
        if( empty($request_code) )
        {
            $this->redirect('/admin/profiler/');
        }

        $request = Profiler::model()->
            requestCodeIs($request_code)->
        find();

        $widgets = Profiler::model()->
            widgets_for_request($request_code)->
        findAll();

        $this->render(
            'requestdetail',
            array(
                'request' => $request,
                'widgets' => $widgets,
            )
        );
    }

    /**
     * Переключить режим профилирования
     */
    public function actionSwitch()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            Controller::SwitchProfilingMode();
            Yii::app()->end();
        }

        $this->redirect('/admin/');
    }

    /**
     * Установить новый интревал профилирования
     */
    public function actionNewinterval()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $profiler_interval = Yii::app()->request->getParam('profiler_interval', 0);

            if( empty($profiler_interval) )
            {
                throw new Exception('Вы пытаетесь задать неверный интервал профилирования!');
            }

            Profiler::setProfilerInterval($profiler_interval+0);

            Yii::app()->end();
        }

        $this->redirect('/admin/');
    }

    /**
     * Обновление статистики
     */
    public function actionUpdate()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            echo CJSON::encode( array('content' => $this->getStatisticBlock()) );

            Yii::app()->end();
        }

        $this->redirect('/admin/');
    }

    /**
     * Принудительный сброс статистики
     */
    public function actionReset()
    {
        $sql = "
                TRUNCATE log;
               ";

        $r = Yii::app()->db->createCommand($sql)->execute();

        exit(0);
    }

    /**
     * Рендерит и возвращает весь блок статистики целиком
     * @return string
     */
    private function getStatisticBlock()
    {
        $interval = Profiler::getProfilerInterval();

        $controllers = Profiler::model()->controllers()->latest($interval)->findAll();

        //Общая статистика по всем виджетам, без привязки к контроллеру
        $widgets = Profiler::model()->widgets()->latest($interval)->findAll();

        return $this->renderPartial(
            'statistic',
            array(
                'controllers' => $controllers,
                'widgets' => $widgets,
            ),
            true
        );
    }
}