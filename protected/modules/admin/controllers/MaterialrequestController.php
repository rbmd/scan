<?php

class MaterialrequestController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'id DESC';
		
		$mr = MaterialRequest::model()->findAll($criteria);

		$this->render('index', array('mr' => $mr) );
	}

}