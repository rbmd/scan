<?php

class TagsController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex()
	{
	
        $criteria = new CDbCriteria();		
		$criteria->order = 'id DESC';

        $id = Yii::app()->request->getParam('id', null);
        $name = Yii::app()->request->getParam('name', null);

        if($id)
        {
            $criteria->addCondition('t.id = ' . (int)$id);
        }

        if($name)
        {
            $criteria->addSearchCondition('t.name', urldecode($name));
        }
		
		$count = Tags::model()->count($criteria);
	 
		$pages = new CPagination($count);
		$pages->pageSize = 40;
		$pages->applyLimit($criteria);	
		
		$tags = Tags::model()->findAll($criteria);

		$this->render('index', array('tags' => $tags, 'pages' => $pages) );
	}
	
	public function actionCreate()
	{
		$model = new Tags();
		
		if(isset($_POST['Tags']))
		{			
			$model->attributes = $_POST['Tags'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/tags/");
				else
					$this->redirect('/admin/tags/edit/?id='.$model->id, true);
			}

		}
	
		$this->render('edit', array('model' => $model, 'action' => 'create') );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = Tags::model()->FindByPk($id);
			
			if(isset($_POST['Tags']))
			{
				$model->attributes = $_POST['Tags'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/tags/');
                    }
                    else
					{
                        $this->redirect('/admin/tags/edit/?id='.$model->id, true);
                    }
				}
			}
			
			$articles = $model->articles;

			$this->render('edit', array('model' => $model, 'action' => 'edit', 'articles' => $articles  ) );		
		}
	}

	public function actionGetHint($tag) {

		if (Yii::app()->request->isAjaxRequest && strlen($tag) > 0) 
		{
			$criteria = new CDbCriteria;
			$criteria->select = array('id', 'name');
			$criteria->condition = 'name LIKE :name';
			$criteria->params = array(':name' => '%'.$tag.'%');
			$tags = Tags::model()->findAll($criteria);

			foreach ($tags as $t) {
				$output[] = array('key' => $t->id, 'value' => $t->name);
			}

			if (is_array($output)) {
				echo CJSON::encode($output);
			}

			Yii::app()->end();
        }
	}



	public function actionGetList($filter = '', $attachToModel = '', $variableName = '')
	{
		$filter = urldecode($filter);
		if (Yii::app()->request->isAjaxRequest) 
		{
			$criteria = new CDbCriteria();
			if (strlen($filter) > 0) {
				$criteria->condition = 'id = :id OR name LIKE :name';
				$criteria->params = array(':id' => $filter, ':name' => '%'.$filter.'%');
			}
			$criteria->order = 'id DESC';
			
			$count = Tags::model()->count($criteria);
		
			$pages = new CPagination($count);
			$pages->pageSize = 9;
			$pages->applyLimit($criteria);

			$output = array();
			$stories = Tags::model()->findAll($criteria);
			foreach ($stories as $tva)
			{
				$output[] = array('id' => $tva->id, 'name' => $tva->name);
			}

			$this->layout = 'ajax';
			$this->render(
							'getlist',
							array(
								'output' => $output,
								'filter' => $filter,
								'attachToModel' => $attachToModel,
								'variableName' => $variableName,
								'pages' => $pages,
							)
						);
		}
	}

	//Возвращает json массив по введенному названию
	public function actionGetByName($s)
	{
		$q = new CDbCriteria();
		#$q->addSearchCondition('name', $s);
		$q->compare('LOWER(name)',strtolower($s),true); 
		
		$tags = Tags::model()->findAll($q);
		
		$arr = array();
		
		if(!is_null($tags))
		{
			$i = 0;
			
			foreach($tags as $s)
			{
				$arr[$i]['id'] = $s->id;
				$arr[$i]['key'] = $s->id;
				$arr[$i]['value'] = $s->name;
				$i++;
			}
		}
		
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}	

    /*
     * Удаление тега
     */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = Tags::model()->dbConnection->beginTransaction();

                try{
                    ArticlesToTags::model()->deleteAll('tag_id=:id', array(':id'=>$id));
                    Tags::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/tags/');
    }
}