<?
class PostsController extends Controller
{
	public $layout = 'index';

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);

		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);
	}


	public function actionIndex()
	{
		$criteria         = new CDbCriteria();
		$criteria->order  = 'date_create DESC';

		$id = Yii::app()->request->getParam('id', null);
		$active = Yii::app()->request->getParam('active', null);
		$name = Yii::app()->request->getParam('name', null);
		$blog_id = (int) Yii::app()->request->getParam('blog_id', null);

		if (!empty($id))
			$criteria->addCondition('t.id = ' . $id);

		if (isset($active) && $active > -1)
			$criteria->addCondition('t.active = ' . $active);

		if (isset($name))
			$criteria->addSearchCondition('t.name', urldecode($name));

		if (isset($blog_id) && $blog_id > 0)
            $criteria->addSearchCondition('t.blog_id', (int) $blog_id);

		$count = Post::model()->not_draft()->count($criteria);

		$pages           = new CPagination($count);
		$pages->pageSize = 80;
		$pages->applyLimit($criteria);

		$posts = Post::model()->not_draft()->findAll($criteria);

		$this->render('index', array('posts' => $posts, 'pages' => $pages));
	}


	public function actionEdit($id)
	{
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/wysiwyg_manager.js'), CClientScript::POS_END);
		
		$post = Post::model()->not_draft()->findByPk($id);
		$active = $post->active;

		if(isset($_POST['Post']))
		{
			$post->attributes = $_POST['Post'];

			if ($active != $post->active)
				$post->banned = (int) !$post->active;

			if ($post->save())
			{
				if (!isset($_POST['apply']))
				{
					Yii::app()->request->redirect('/admin/posts/');
				}
				else
				{
					$this->redirect('/admin/posts/edit/?id='.$post->id, true);
				}
			}
		}
		
		$this->render('edit', array('model' => $post));
	}
}