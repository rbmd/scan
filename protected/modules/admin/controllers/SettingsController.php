<?
class SettingsController extends Controller
{

	public $layout = 'index';

	public function actionIndex()
	{

		//Сохраняем все параметры
		if(Yii::app()->request->isPostRequest)
		{
			$settings = Yii::app()->request->getParam('settings');
			foreach($settings as $s => $v)
			{
				$setting = Settings::model()->findByAttributes(array('code' => $s));
				$setting->value = $v;
				$setting->save();
			}
			$this->redirect('/admin/settings/');
		}

		$settings = Settings::model()->findAll();

		$this->render('index', array('settings' => $settings));
	}
}