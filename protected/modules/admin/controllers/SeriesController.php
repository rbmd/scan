<?php
class SeriesController extends Controller
{
	public $layout = 'index';

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);

		//Подрубаем css со свойствами для валидации полей формы создания лекции
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);
	}
	
	public function actionIndex()
	{
		$criteria = new CDbCriteria();		
		$criteria->order = 't.id DESC';
		$criteria->select = 't.*';

        $id = Yii::app()->request->getParam('id', null);
        $name = Yii::app()->request->getParam('name', null);

        if($id)
        {
            $criteria->addCondition('t.id = ' . (int)$id);
        }

        if($name)
        {
            $criteria->addSearchCondition('t.name', urldecode($name));
        }

        $count = Series::model()->count($criteria);
	 
		$pages = new CPagination($count);
		$pages->pageSize = 20;
		$pages->applyLimit($criteria);	
		
		$series = Series::model()->findAll($criteria);

		$this->render('index', array('series' => $series, 'pages' => $pages) ); 
	}

	public function actionCreate()
	{
		$model = new Series();

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'series-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}
		
		if (isset($_POST['Series']))
		{			
			$model->attributes = $_POST['Series'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/series/");
				else
					$this->redirect('/admin/series/edit/?id='.$model->id, true);
			}

		}
		
		$this->render('edit', array('model' => $model, 'action' => 'create') );
	}

	public function actionEdit($id)
	{
		$model = Series::model()->FindByPk($id+0);

		if ( empty($model) )
			throw new CHttpException(404);

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'series-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}
			
		if (isset($_POST['Series']))
		{
			$model->attributes = $_POST['Series'];

			if( $model->validate() )
			{
				$model->save();

				if( !isset($_POST['apply']) )
				{
					$this->redirect("/admin/series/");
				}
				else
				{
					$this->refresh();
				}
			}
		}
			
		$articles = Articles::model()->inSeries($id)->order('date_publish DESC')->findAll();

		$this->render('edit', array('model' => $model, 'action' => 'edit', 'articles' => $articles  ) );
	}

	public function actionGetSeries($filter = '', $attachToModel = '', $variableName = '')
	{
		$filter = urldecode($filter);
		if (Yii::app()->request->isAjaxRequest) 
		{
			$criteria = new CDbCriteria();
			if (strlen($filter) > 0) {
				$criteria->condition = 'id = :id OR name LIKE :name';
				$criteria->params = array(':id' => $filter, ':name' => '%'.$filter.'%');
			}
			$criteria->order = 'id DESC';
			
			$count = Series::model()->count($criteria);
		
			$pages = new CPagination($count);
			$pages->pageSize = 9;
			$pages->applyLimit($criteria);

			$output = array();
			$series = Series::model()->findAll($criteria);
			foreach ($series as $tva)
			{
				$output[] = array('id' => $tva->id, 'name' => $tva->name);
			}

			$this->layout = 'ajax';
			$this->render(
							'getseries',
							array(
								'output' => $output,
								'filter' => $filter,
								'attachToModel' => $attachToModel,
								'variableName' => $variableName,
								'pages' => $pages
							)
						);
		}
	}

	//Возвращает json массив по введенному названию серии
	public function actionGetByName($s)
	{
		$q = new CDbCriteria();
		$q->compare('LOWER(name)',strtolower($s),true); 
		
		$series = Series::model()->findAll($q);
		
		$arr = array();
		
		if(!is_null($series))
		{
			$i = 0;
			
			foreach($series as $s)
			{
				$arr[$i]['id'] = $s->id;
				$arr[$i]['key'] = $s->id;
				$arr[$i]['value'] = $s->name;
				$i++;
			}
		}
		
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}	
	
	/*
	 * Удаление серии
	 */
	public function actionDelete()
	{
		if( Yii::app()->request->isAjaxRequest )
		{
			$series_id = Yii::app()->request->getParam('id');

			if( !empty($series_id) )
			{
				$transaction = Series::model()->dbConnection->beginTransaction();

				try{
					Articles::model()->updateAll(array('series_id'=>0),'series_id=:id',array('id'=>$series_id));
					Series::model()->findByPk($series_id)->delete();

					$transaction->commit();
				}
				catch(Exception $e)
				{
					$transaction->rollBack();

					throw $e;
				}
			}

			Yii::app()->end();
		}

		$this->redirect('/admin/series/');
	}
}