<?
class BlogsController extends Controller
{
	public $layout = 'index';

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);

		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);
	}


	public function actionIndex()
	{
		$criteria         = new CDbCriteria();
		$criteria->order  = 'date_create DESC';

		$count = Blog::model()->count($criteria);

		$pages           = new CPagination($count);
		$pages->pageSize = 80;
		$pages->applyLimit($criteria);

		$blogs = Blog::model()->findAll($criteria);

		$this->render('index', array('blogs' => $blogs, 'pages' => $pages));
	}


	public function actionEdit($id)
	{
		$blog = Blog::model()->findByPk($id);

		$blog_approved = $blog->approved;

		if (isset($_POST['Blog']))
		{
			$blog->attributes = $_POST['Blog'];

			$transaction = Yii::app()->db->beginTransaction();


			try 
			{
				$blog->save();

				if ($blog->approved != $blog_approved)
				{	
					$post_ids = array_keys($blog->posts(array('index' => 'id')));
					if (count($post_ids) > 0)
					{
						Post::model()->updateAll(array('blog_approved' => $blog->approved), 'id IN (' . implode(',', $post_ids) . ')');
					}
				}
					
				$transaction->commit();

				if (!isset($_POST['apply']))
				{
					Yii::app()->request->redirect('/admin/blogs/');
				}
				else
				{
					$this->redirect('/admin/blogs/edit/?id='.$blog->id, true);
				}
			}
			catch (Exception $e)
			{
				$transaction->rollBack();
			}
		}
		
		$this->render('edit', array('model' => $blog));
	}
}