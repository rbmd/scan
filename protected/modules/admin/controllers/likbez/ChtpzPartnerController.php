<?php

class ChtpzPartnerController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex($id = 1)
	{
	 
		if($id > 0)
		{
			$model = ChtpzPartners::model()->FindByPk($id);
			
			if(isset($_POST['ChtpzPartners']))
			{
				$model->attributes = $_POST['ChtpzPartners'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/specprojects/chtpzPartner/');
                    }
                    else
					{
                        $this->redirect('/admin/specprojects/chtpzPartner/', true);
                    }
				}
			}
			
			

			$this->render('index', array('model' => $model, 'action' => 'edit'  ) );		
		}
	}

	public function actionArticles()
	{
		

		$this->render('articles', array('articles' => $articles) );
	}
	
	public function actionCreate()
	{
		$model = new ChtpzArticles();
		
		if(isset($_POST['ChtpzArticles']))
		{			
			$model->attributes = $_POST['ChtpzArticles'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/specprojects/chtpz/");
				else
					$this->redirect('/admin/specprojects/chtpz/edit/?id='.$model->id, true);
			}

		}
	
		$this->render('edit', array('model' => $model, 'action' => 'create') );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = ChtpzArticles::model()->FindByPk($id);
			
			if(isset($_POST['ChtpzArticles']))
			{
				$model->attributes = $_POST['ChtpzArticles'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/specprojects/chtpz/');
                    }
                    else
					{
                        $this->redirect('/admin/specprojects/chtpz/edit/?id='.$model->id, true);
                    }
				}
			}
			
			

			$this->render('edit', array('model' => $model, 'action' => 'edit'  ) );		
		}
	}


    /*
     * Удаление тега
     */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = ChtpzArticles::model()->dbConnection->beginTransaction();

                try{
                    
                    ChtpzArticles::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/specprojects/fnl2014/');
    }
}