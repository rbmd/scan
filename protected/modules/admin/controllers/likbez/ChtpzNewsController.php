<?php

class ChtpzNewsController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'date_active_start DESC';
		
		$articles = ChtpzNews::model()->findAll($criteria);

		$this->render('index', array('articles' => $articles) );
	}

	public function actionArticles()
	{
		

		$this->render('articles', array('articles' => $articles) );
	}
	
	public function actionCreate()
	{
		$model = new ChtpzNews();
		
		if(isset($_POST['ChtpzNews']))
		{			
			$model->attributes = $_POST['ChtpzNews'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/specprojects/ChtpzNews/");
				else
					$this->redirect('/admin/specprojects/ChtpzNews/edit/?id='.$model->id, true);
			}

		}

		$themes = $this->getThemes();
		$galleries = $this->getGalleries();
	
		$this->render('edit', array('model' => $model, 'action' => 'create',  'themes' => $themes, 'galleries' => $galleries, ) );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = ChtpzNews::model()->FindByPk($id);
			
			if(isset($_POST['ChtpzNews']))
			{
				$model->attributes = $_POST['ChtpzNews'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/specprojects/ChtpzNews/');
                    }
                    else
					{
                        $this->redirect('/admin/specprojects/ChtpzNews/edit/?id='.$model->id, true);
                    }
				}
			}
			
			$themes = $this->getThemes();
			$galleries = $this->getGalleries();

			$this->render('edit', array('model' => $model, 'action' => 'edit', 'themes' => $themes, 'galleries' => $galleries,  ) );		
		}
	}

	private function getThemes()
	{
		$themes_ac = ChtpzThemes::model()->findAll(array('order' => 'year DESC, month DESC'));
		$themes[]  = '--';
		foreach ($themes_ac as $theme)
		{
			$themes[$theme->id] = $theme->name;
		}
		return $themes;
	}

	private function getGalleries()
	{
		$galleries_ac = Galleries::model()->findAll(array('order' => 'id DESC'));
		$galleries[]  = '--';
		foreach ($galleries_ac as $gallery)
		{
			$galleries[$gallery->id] = $gallery->name;
		}
		return $galleries;
	}


    /*
     * Удаление тега
     */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = ChtpzArticles::model()->dbConnection->beginTransaction();

                try{
                    
                    ChtpzArticles::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/specprojects/fnl2014/');
    }
}