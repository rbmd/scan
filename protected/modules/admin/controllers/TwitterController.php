<?php
/**
 * User: Русаков Дмитрий
 * Date: 05.06.12
 * Time: 19:16
 *
 * Контроллер твиттер-сообщений
 *
 */
class TwitterController extends Controller
{
    public $layout = 'index';

    public function __construct($id,$module=null)
    {
        parent::__construct($id, $module);

        //Подрубаем css со свойствами для валидации полей формы создания лекции
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->assetManager->publish(
                'static/css/form.css'
            )
        );
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';

        $pages = new CPagination( Twitter::model()->count($criteria) );
        $pages->pageSize =20;
        $pages->applyLimit($criteria);

        $messages = Twitter::model()->with(array('creator', 'editor'))->findAll($criteria);

        $this->render(
            'index',
            array('messages'=>$messages)
        );
    }

    public function actionEdit($id=0)
    {
        $new_flag = false;

        $model = Twitter::model()->findByPk($id);

        if( empty($model) )
        {
            $model = new Twitter();
            $new_flag = true;
        }

        //Выдать сообщения об ошибке в случае аякс валидации
        if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'twitter-form')
        {
            echo CActiveForm::validate($model);

            Yii::app()->end();
        }

        if( isset($_POST['Twitter']) )
        {
            $model->attributes = $_POST['Twitter'];

            if( $model->validate() )
            {
                if( $new_flag )
                {
                    $model->create_date = new CDbExpression('NOW()');
                    $model->create_user_id = Yii::app()->user->id;
                }
                else{
                    $model->edit_user_id = Yii::app()->user->id;
                    $model->x_timestamp = new CDbExpression('NOW()');
                }

                $model->save();

                if( $new_flag )
                {
                    $this->redirect("/admin/twitter/");
                }
                else
                {
                    if( !isset($_POST['apply']) )
                    {
                        $this->redirect("/admin/twitter/");
                    }
                    else{
                        $this->refresh();
                    }
                }
            }
        }

        $this->render(
            'edit',
            array('model' => $model, 'is_new' => $new_flag)
        );
    }

    /**
     * Отправить сообщение в твиттер
     */
    public function actionSend()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $twitter = Twitter::model()->findByPk($id);

                if( !empty($twitter) )
                {
                    $r = $twitter->send();

                    if( !$r )
                    {
                        throw new Exception("Сбой отправки в твиттер...");
                    }

                    $twitter->send_date = new CDbExpression('NOW()');
                    $twitter->edit_user_id = Yii::app()->user->id;

                    $twitter->save();
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/twitter/');
    }

    /*
    * Удаление лекции
    */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = Twitter::model()->dbConnection->beginTransaction();

                try{
                    Twitter::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/twitter/');
    }
}