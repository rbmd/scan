<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
			'ext.autocomplete.AutocompleteWidget'
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (Yii::app()->user->checkAccess('editor') && Yii::app()->request->url == '/admin/')
		{
			Yii::app()->request->redirect('/admin/articles/');
		}

		if (Yii::app()->user->checkAccess('atlasTvrain') && Yii::app()->request->url == '/admin/')
		{
			Yii::app()->request->redirect('/admin/atlas/guides/');
		}

		//РАБОЧИЙ КОД ПРОВЕРКИ ПРАВ
		//Получаем список ролей зарегеных юзеров

		if (!Yii::app()->user->isGuest && Yii::app()->user->superuser + 0 === 1)
		{
			$_SESSION['skfinder_enable'] = 1;
			$_SESSION['skfinder_type']   = 'admin';
			//$userRoles = Rights::getAssignedRoles(Yii::app()->user->id);

			//Какие контроллеры доступны пользователю если он не имеет роли Admin, но имеет другие роли...
			$accessible_routes = array('default');

			if (Yii::app()->user->checkAccess('editor'))
			{
				$accessible_routes[] = 'articles';
				$accessible_routes[] = 'authors';
				$accessible_routes[] = 'magazine';
				$accessible_routes[] = 'series';
				$accessible_routes[] = 'tags';
				$accessible_routes[] = 'gallery';
				$accessible_routes[] = 'special';
				$accessible_routes[] = 'afisha';
				$accessible_routes[] = 'afishatypes';
				$accessible_routes[] = 'polls';
				$accessible_routes[] = 'themeweek';
				$accessible_routes[] = 'tree';
				$accessible_routes[] = 'manageimages';
				$accessible_routes[] = 'specprojects/santa';
				$accessible_routes[] = 'bestsummer/events';
				$accessible_routes[] = 'specprojects/dictionary';
				$accessible_routes[] = 'comments';

				$accessible_routes[] = 'elements';
				$accessible_routes[] = 'chapters';

				$accessible_routes[] = 'regions';
				$accessible_routes[] = 'indexpage';
			}

			if (Yii::app()->user->checkAccess('author'))
			{
				$accessible_routes[] = 'articles';
				$accessible_routes[] = 'afisha';
				$accessible_routes[] = 'afishatypes';
				$accessible_routes[] = 'polls';
				$accessible_routes[] = 'series';
				$accessible_routes[] = 'tags';
				$accessible_routes[] = 'gallery';
				$accessible_routes[] = 'authors';

				$accessible_routes[] = 'elements';
				$accessible_routes[] = 'chapters';
			}

			if (Yii::app()->user->checkAccess('bannerManager'))
			{
				$accessible_routes[] = 'banners';
			}

			if (Yii::app()->user->checkAccess('commentsManager'))
			{
				$accessible_routes[] = 'comments';

				if ($controller->id == 'users' && $action->id == 'setuseractive')
				{
					$accessible_routes[] = 'users';
				}
			}

			if (Yii::app()->user->checkAccess('atlasManager'))
			{
				$accessible_routes[] = 'atlas/objects';
				$accessible_routes[] = 'atlas/rubrics';
				$accessible_routes[] = 'atlas/guides';
				$accessible_routes[] = 'atlas/campaigns';
			}

			if (Yii::app()->user->checkAccess('atlasTvrain'))
			{
				$accessible_routes[] = 'atlas/guides';
				$accessible_routes[] = 'atlas/objects';
			}

			if (Yii::app()->user->checkAccess('treeManager'))
			{
				$accessible_routes[] = 'tree';
			}


			if (Yii::app()->user->checkAccess('instagramEditor'))
			{
				$accessible_routes[] = 'specprojects/instagramtags';
			}

			if (Yii::app()->user->checkAccess('articlesStatistic'))
			{
				$accessible_routes[] = 'articles';
			}

			// загрузка фото
			if (Yii::app()->user->id > 0)
			{
				$accessible_routes[] = 'uploadimage';
			}


			# доступ к статьям, афише, типам афиши, опросам.
			#var_dump($accessible_routes);

			if (!Yii::app()->user->checkAccess('Admin') && !in_array($controller->id, $accessible_routes))
			{
				$controller->redirect('/admin/default/index/accessdenied/1/');
				die;
			}
		}
        elseif (Yii::app()->user->checkAccess('blog_author') || Yii::app()->user->checkAccess('blog_editor'))
        {

        }
		//Всех гостей редиректим
		else
		{
			if (Yii::app()->user->isGuest)
			{
				$app     = Yii::app();
				$request = $app->getRequest();

				Yii::app()->user->setReturnUrl($request->getUrl());

				Yii::app()->Controller->redirect(Yii::app()->user->loginUrl);
			}
			else
			{
				//Всех авторизованных пользователей, но НЕ АДМИНОВ редиректим на главную
				Yii::app()->Controller->redirect('/');
			}
		}

		/*
		///admin/schedule/
		switch ($controller->id)
		{
			case 'schedule':
				if (!isset($userRoles['scheduleManager'])) {
					Yii::app()->Controller->redirect(Yii::app()->user->loginUrl);
				}
				else
					return true;
			break;

			case 'articles';
				if (!isset($userRoles['newsManager'])) {
					Yii::app()->Controller->redirect(Yii::app()->user->loginUrl);
				}
				else
					return true;
			break;

			default:
				Yii::app()->Controller->redirect(Yii::app()->user->loginUrl);
		}
		*/


		if (parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
