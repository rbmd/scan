<?php
class AuthorsToTree extends CActiveRecord 
{

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName()	{
		return 'bg_authors_to_tree';
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//����� ��� ��������� ������� �� �������� ����(��������� � ������)
			//�������� ��� AuthorsToTree::model()->with('authors')->treeIdIs($id)->findAll();
			'authors'  => array(self::BELONGS_TO, 'Authors', array('author_id' => 'id'), ),
		);
	}	
	
    /**
     * ����� ��� ������ ���� ������� ��� �������� ���� ������, �� relation authors.
     */
    public function treeIdIs($tree_id)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'tree_id = :tree_id',
                'params' => array(':tree_id' => $tree_id),
            )
        );

        return $this;
    }	
	
}