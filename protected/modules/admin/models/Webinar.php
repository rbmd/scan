<?php

/**
 * This is the model class for table "bg_galleries".
 *
 * The followings are the available columns in table 'bg_galleries':
 * @property integer $id
 * @property string $name
 */
class Webinar extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Galleries the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'likbez_webinar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('webinar_id,announce_pic,announce_day,announce_time,announce_month,announce_title,comments_xid', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        return array(
            
 		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'   => 'ID',
			'webinar_id' => 'YOUTUBE ID',
			'announce_pic' => 'Фото',
			'announce_day' => 'День',
			'announce_time' => 'Время',
			'announce_month' => 'Месяц',
			'announce_title' => 'Заголовок анонса',
			'comments_xid' => 'Ссылка на статью комментариев вида /articles/10/',

		);
	}

	
}