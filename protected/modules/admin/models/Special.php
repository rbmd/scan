<?php
class Special extends KeywordsItem
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Story the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_special';
    }

    public function behaviors()
    {
        return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );
    }

    public function scopes()
    {
        return array(
            /*Убирать нельзя, иначе sitemap.xml не сгенерится */
            'visible'=>array(
            ),
            'active' => array(
                'condition' => 'active = 1',
            )
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('link, name, preview_text', 'required'),
            array('active', 'numerical', 'integerOnly'=>true),
            array('link, name, preview_text, image', 'length', 'max'=>255),
            array('seo_title, seo_keywords, seo_description', 'safe'),
            array('id, active, link, name', 'safe', 'on'=>'search')
        );
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
            'active' => 'Активность',
            'name' => 'Название',
            'link' => 'Ссылка',
            'preview_text' => 'Краткое описание',
            'image' => 'Изображение',
        );
		
		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
    }

    protected function afterValidate()
    {
        parent::afterValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('code',$this->code,true);
        $criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
	
	public function getSpecialsList()
	{
			$specialsids = array();
			$specialsids[''] = '';
			$specials = Special::Model()->findAll();
			foreach($specials as $special) {
				$specialsids[$special->id] = $special->name;
			}
			
			return $specialsids;
	}

    /**
     * @return string - вернуть ссылку на страницу этого сюжета
     */
    public function url()
    {
        return $this->link;
    }
}
?>