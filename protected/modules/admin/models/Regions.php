<?php
class Regions extends CActiveRecord
{
    /**
     * Название поискового индекса для тегов в поисковом движке "Sphinx"
     */
    const SPHINX_SEARCH_INDEX = "regions";

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bg_regions';
	}

    /**
     * Функция поиска по тегам на основе поискового движка Sphinx
     * @static
     *
     * @param $query - поисковый запрос
     * @param $before_match - тег который ставить перед подсвечиваемым словом(само слово из поискового запроса)
     * @param $after_match  - тег который ставить после подсвечиваемого слова(само слово из поискового запроса)
     *
     * @return array - метод возвращает ассоциативный массив содержащий
     *      1) ключ "ids" - массив айдишников найденных тегов;
     *      2) ключ "highlight_titles" - ассоциативный массив названий тегов с подсвеченным поисковым запросом;
     *      3) ключ "highlight_texts" - ассоциативный массив с вырезками из детальных текстов с описанием тегов с
     *         подсвеченным поисковым запросом.
     *
     *      (2) и (3) - ассоциативные массивы, где ключами являются айдишниками тегов из ids
     */
  /*  public static function sphinxSearch($query, $before_match, $after_match)
    {
        $result = array();

        $search = Yii::App()->search;

        $search->setLimits(0, 1024, 1024);
        $search->setMatchMode( SPH_MATCH_ANY );
        $search->SetRankingMode( SPH_RANK_SPH04 );
        //Сортируем результаты по релевантности, а затем по алфавиту
        $search->SetSortMode(SPH_SORT_EXTENDED, '@relevance DESC, name_for_sort ASC');
        //Устанавливаем вес заголовка и детального текста
        $search->setFieldWeights( array('name' => 30, 'detail_text' => 3) );

        $resArray = $search->query( $query, self::SPHINX_SEARCH_INDEX );

        if ( !empty($resArray) && isset($resArray['matches']) && !empty($resArray['matches']) )
        {
            //Здесь айдишники всех найденных тегов
            $ids = array_keys($resArray['matches']);
            $ids_list = implode(',', $ids);

            if( !empty($ids) )
            {
                //Опции подсветки
                $options = array
                (
                    'before_match'          => $before_match,
                    'after_match'           => $after_match,
                    'chunk_separator'       => '...',
                    'limit'                 => 256,
                    'around'                => 128,
                    'force_all_words'       => true,
                    'html_strip_mode'       => 'strip',
                );

                $connection = Yii::app()->db;

                //Выдёргиваем сначала все заголовки
                $sql = "SELECT t.`name` FROM `bg_tags` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_titles = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех заголовках
                $res_titles = $search->BuildExcerpts($doc_titles, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_titles = array_combine($ids, $res_titles);

                //Выдёргиваем теперь детальные тексты всех тегов, которые нашлись
                $sql = "SELECT t.`detail_text` FROM `bg_tags` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_detail_texts = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех детальных текстах
                $res_detail_texts = $search->BuildExcerpts($doc_detail_texts, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_detail_texts = array_combine($ids, $res_detail_texts);

                $result = array(
                    'ids' => $ids,
                    'highlight_titles' => $res_titles,
                    'highlight_texts'  => $res_detail_texts,
                );
            }
        }

        return $result;
    }
*/

    public function behaviors()
    {
        return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );
    }

    public function scopes()
    {
        return array(
            /*Убирать нельзя, иначе sitemap.xml не сгенерится */
            'visible'=>array(
            ),
            'indexed'=>array(
                'index'=>'id',
            ),
        );
    }
	
	// Скоуп select
	public function select($str = 'id')
	{
		$this->getDbCriteria()->mergeWith(array(
			'select' => $str,
		));

		return $this;
	}		
	
	// Скоуп выбора по полю name
	public function nameIs($str = '')
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'name=:name',
			'params' => array(':name' => $str),
		));

		return $this;
	}		
	
	// Скоуп сортировки по полю name
	public function byName()
	{
		$this->getDbCriteria()->mergeWith(array(
			'order' => 'name ASC',
		));

		return $this;
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name', 'required'),
            array('detail_text', 'safe'),
			array('name,  code, seo_title, seo_keywords, seo_description', 'length', 'max'=>255),
		);
		
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
			'articles' => array(self::MANY_MANY, 'Articles', 'bg_articles_to_regions(region_id, article_id)', 'index'=>'id'),
           # 'articles_count' => array(self::STAT, 'ArticlesToRegions', 'region_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'name' => 'Название',
            'code' => 'Символьный код',
			
            'detail_text' => 'Описание региона',
		);
		
		#$parent_atr = parent::attributeLabels();		
		return $atr;
	}

    /**
     * @return string - вернуть ссылку на страницу этого тага
     */
    public function url()
    {
        return "/regions/{$this->code}/";
    }
}