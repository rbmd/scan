<?php

class KeywordsItem extends CActiveRecord
{
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('seo_keywords, seo_description, fb_title, fb_description', 'safe', 'on'=>'search'),
			array('seo_keywords', 'length', 'max'=>255),
			array('seo_description', 'length', 'max'=>255),
			array('seo_title', 'length', 'max'=>255),
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$atr = array(
			'seo_keywords' => 'SEO Keywords',
			'seo_description' => 'SEO description',
			'seo_title' => 'SEO Title',
		);
		
		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}
	
	

}
?>