<?php
class ArticlesDraft extends Articles
{
	public $old_draft_id = 0;

	public function tableName()
	{
		return 'bg_articles_draft';
	}


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function rules()
	{
		$rules = array(
			array('name, parent_id, date_active_start, date_publish, code', 'required', 'except' => 'new_article'),
			array('active, show_on_indexpage, preview_img_show, show_main_announce_section, parent_id, series_id, is_howto, is_best, has_gallery, has_video, has_translation, is_archive, ceil_type, is_news, is_mininews, is_guide, is_main_guide, is_digitilized, chapter_paging_type, assignee, proof_tested, magazine_id, to_rss, theme_week_code, price', 'numerical', 'integerOnly' => true),
			array('name, preview_img, preview_img_alt, link, code, ceil_quote_author, seo_title, code, customer', 'length', 'max' => 255),
			array('preview_text, ceil_quote_text, comment, include_text, view, radiosvoboda', 'length', 'max' => 5000),
			array('predetail_text', 'length', 'max' => 5000),
			array('date_active_start', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
			array('date_publish', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
			array('twitter_text', 'length', 'max' => 255),
			array('show_main_announce, code, sendToTwitter, x_timestamp, guidegmap', 'safe'),
		);
		return $rules;
	}

	/**
	*
	*
	* вначале у нас есть только статья без драфта. а в админке мы работаем с драфтами. 
	* поэтому при открытии статьи в админке, мы создаем драфт, копируя для него данные у статьи
	*/
	public static function createdFromOrigin($id)
	{
		$article = Articles::model()->findByPk($id);
		if (is_null($article))
		{
			return null;
		}
		else
		{
			$draft                    = new ArticlesDraft('new_article');
			$draft->save_only_article = true;
			foreach ($article as $key => $val)
			{
				//Пропускаем те поля, которые нет необходимости переносить в драфт версию
				if ($key == 'id' || $key == 'rating')
				{
					continue;
				}
				$draft->$key = $val;
			}
			$draft->is_dummy   = 1;
			$draft->article_id = $article->id;

			if ($draft->save())
			{
				// Заменяем главы
				$chapters = Chapters::model()->findAll('article_id=:id', array(':id' => $article->id));
				foreach ($chapters as $chapter)
				{
					$newChapter = new ChaptersDraft();
					foreach ($chapter as $key => $val)
					{
						if ($key == 'id' || $key == 'detail_text')
						{
							continue;
						}
						$newChapter->$key = $val;
					}
					$newChapter->article_draft_id = $draft->id;
					$newChapter->hist             = 0;
					$newChapter->save();
					$chapter_nohist_id = $newChapter->id;
					$newChapter->setIsNewRecord(true);
					unset($newChapter->id);
					$newChapter->hist = 1;
					$newChapter->save();
					$chapter_hist_id = $newChapter->id;

					// Получаем элементы связанные с главой
					$elements = Elements::model()->findAll('article_id=:id AND chapter_id=:chapter_id', array(
						':id'         => $article->id,
						':chapter_id' => $chapter->id
					));
					foreach ($elements as $element)
					{
						$newElement = new ElementsDraft();
						foreach ($element as $key => $val)
						{
							if ($key == 'id')
							{
								continue;
							}
							$newElement->$key = $val;
						}
						$newElement->chapter_id       = $chapter_nohist_id;
						$newElement->article_draft_id = $draft->id;
						$newElement->hist             = 0;
						$newElement->save();
						// Сохраняем версию для истории
						$newElement->setIsNewRecord(true);
						unset($newElement->id);
						$newElement->hist       = 1;
						$newElement->chapter_id = $chapter_hist_id;
						$newElement->save();
					}
				}
			}
		}
		return $draft;
	}


	public function history()
	{
		return Yii::app()->db->createCommand()
			->select('a.*, CONCAT(firstname, " ", lastname) AS user_name, DATE_FORMAT(x_timestamp, "%d.%m.%Y %H:%i:%s") AS `date`')
			->from('bg_articles_draft a')
			->join('bg_users', 'bg_users.id = a.user_id')
			->where('article_id=:id', array(':id' => $this->article_id))
			->order('x_timestamp DESC')
			->queryAll();
	}


	protected function beforeSave()
	{
		//Если новая запись - проставляем время создания
		if (empty($this->date_create))
		{
			$this->date_create = new CDbExpression('NOW()');
		}

		$this->preview_text    = preg_replace('#^(&nbsp;)#', '', strip_tags($this->preview_text, '<br><a><i><b><em><strong>'));
		$this->predetail_text  = preg_replace('#^(&nbsp;)#', '', $this->predetail_text);
		$this->relations       = is_array(Yii::app()->request->getPost('relations')) ? implode(',', Yii::app()->request->getPost('relations')) : '';
		$this->series_id       = Yii::app()->request->getPost('serie_id', 0);
		$this->theme_week_code = Yii::app()->request->getPost('theme_week_code');
		$this->name            = trim($this->name);
		$this->code            = UtilsHelper::transliterate(trim($this->code));
		return parent::beforeSave();
	}

	private function resizeTo($folder, $photo, $x, $y)
	{
		$x_file = $x;
		$y_file = $y;
		// Костыль связанный с тем что для анонсов на главной страницы необходимы картинки с шириной 300 точек.
		// Если ширина не равна 300 точек, то браузер ее искажает.
		if ($x == 320)
		{
			$x = 300;
			$y = 1000;
		}
		$photo->image_resize       = true;
		$photo->image_ratio        = true;
		$photo->image_x            = $x;
		$photo->image_y            = $y;
		$photo->jpeg_quality       = 90;
		$photo->file_new_name_body = $this->article_id;
		$photo->process(Yii::getPathOfAlias('webroot') . '/media/images/' . $x_file . 'x' . $y_file . '/' . $folder);
	}


	public function saveImages()
	{
		/*if (!empty($this->preview_img))
		{
			$date_create = Articles::model()->findByPk($this->article_id)->date_create;

			$folder = empty($date_create) ? date("Y/m/d") : str_replace('-', '/', substr($date_create, 0, 10));
			Yii::getPathOfAlias('webroot') . $this->preview_img;
			$image = Yii::app()->imagemod->load(Yii::getPathOfAlias('webroot') . $this->preview_img);
			if (!file_exists(Yii::getPathOfAlias('webroot') . $this->preview_img))
			{
				$image->process(Yii::getPathOfAlias('webroot') . $this->preview_img);
			}

			$this->resizeTo($folder, $image, 80, 50);
			$this->resizeTo($folder, $image, 120, 70);
			$this->resizeTo($folder, $image, 240, 120);
			$this->resizeTo($folder, $image, 320, 180);
			$this->resizeTo($folder, $image, 480, 286);

			//Отдельные картинки с вотермарком для фейсбука
			$ext = substr($this->preview_img, strrpos($this->preview_img, '.') + 1);
			$watermark = Yii::getPathOfAlias('webroot') . '/media/patterns/fb_watermark.png';
			$cover     = Yii::getPathOfAlias('webroot') . $this->preview_img;

			$tmpdir = Yii::getPathOfAlias('webroot') . '/media/upload/temp/';
			if (!is_dir($tmpdir))
			{
				mkdir($tmpdir, 0775, true);
			}
			$tmp       = $tmpdir . $this->article_id . '.' . $ext;
			$facebook  = Yii::getPathOfAlias('webroot') . '/media/images/facebook/' . $folder . '/' . $this->article_id . '.' . $ext;

			if (!is_dir(dirname($facebook)))
			{
				mkdir(dirname($facebook), 0775, true);
			}

			$initial_path = Yii::getPathOfAlias('webroot') . $this->preview_img;
			exec(escapeshellcmd('/usr/local/bin/convert ' . $initial_path . ' -resize 200x200^ -gravity center -extent 200x200 ' . $tmp));
			exec(escapeshellcmd('/usr/local/bin/composite -dissolve 95% -gravity center -quality 100 ' . $watermark . ' ' . $tmp . ' ' . $facebook));
			exec(escapeshellcmd('/usr/local/bin/convert ' . $facebook . ' -resize 200x200^ -extent 200x200 ' . $facebook));
			if (is_file($tmp))
			{
				unlink($tmp);
			}

			//Круговое превью
			$circle = Yii::getPathOfAlias('webroot') . '/media/images/circle/' . $folder . '/' . $this->article_id . '.png';
			if (!is_dir(dirname($circle)))
			{
				mkdir(dirname($circle), 0775, true);
			}
			exec(escapeshellcmd('/usr/local/bin/convert ' . $cover . ' -resize 150x150^ -extent 150x150 ' . $circle));
			exec(escapeshellcmd('/usr/local/bin/convert ' . $circle . " ( +clone -threshold -1 -negate -fill white -draw 'circle 74,74 74,0' ) -alpha off -compose copy_opacity -composite " . $circle));
		}*/
	}

	protected function afterSave()
	{
		$this->saveImages();

		if ($this->save_only_article == false)
		{
			$this->afterSaveProcessElements();
			$this->afterSaveProcessAuthors();
			$this->afterSaveProcessTags();
			$this->afterSaveProcessRegions();
			$this->afterSaveRecountComments();
		}
		parent::afterSave();
	}


	/**
	 * Собирает все элементы и собирает из них главы
	 */
	protected function afterSaveProcessElements()
	{
		if (!$this->save_only_article)
		{
			/**
			 * Создаем копии глав и элементов для истоии
			 * В текущей рабочей версии глав и элементов (hist = 0) меняем article_draft_id на новый
			 */
			$chapters = ChaptersDraft::model()->findAll('article_draft_id=:id AND hist = 0', array(':id' => $this->old_draft_id));
			foreach ($chapters as $chapter)
			{
				$chapter->article_draft_id = $this->id;
				$chapter_new               = new ChaptersDraft();
				foreach ($chapter as $key => $val)
				{
					$chapter_new->$key = $val;
				}
				unset($chapter_new->id);
				$chapter_new->hist = 1;
				$chapter_new->save();
				$chapter->save();
				$elements = ElementsDraft::model()->findAll('chapter_id=:id AND hist = 0', array(':id' => $chapter->id));
				foreach ($elements as $element)
				{
					$element->article_draft_id = $this->id;
					$element_new               = new ElementsDraft();
					foreach ($element as $key => $val)
					{
						$element_new->$key = $val;
					}
					unset($element_new->id);
					$element_new->hist       = 1;
					$element_new->chapter_id = $chapter_new->id;
					$element_new->save();
					$element->save();
				}
			}
		}
	}


	/**
	 *   Связь с авторами
	 */
	protected function afterSaveProcessAuthors()
	{
		//Удаляем все из таблицы связей
		ArticlesToAuthors::model()->deleteAll('article_id = :id', array(':id' => $this->article_id));

		$authors = Yii::app()->request->getPost('authors');

		if (!is_null($authors))
		{

			//Проходим по массиву, выбираем по фио нужных авторов и сохраняем связь в таблицу связей
			foreach ($authors as $author_type => $author)
			{
				if (is_array($author))
				{
					$author_tmp = array_unique($author);
					foreach ($author_tmp as $author_id)
					{
						if ($author_id > 0)
						{
							$aa             = new ArticlesToAuthors();
							$aa->article_id = $this->article_id;
							$aa->type       = $author_type;
							$aa->author_id  = $author_id;
							$aa->save();

						}
					}
				}
			}
		}


	}


	protected function afterValidate()
	{
		parent::afterValidate();

		$preview_text   = $this->getAttribute('preview_text');
		$detail_text    = $this->getAttribute('detail_text');
		$predetail_text = $this->getAttribute('predetail_text');

		/*Полю, к которому подключается FCKEditor по умолчанию выставляем пробел, иначе западает валидация*/
		if (empty($preview_text))
		{
			$this->setAttribute('preview_text', "&nbsp;");
		}
		if (empty($detail_text))
		{
			$this->setAttribute('detail_text', "&nbsp;");
		}
		if (empty($predetail_text))
		{
			$this->setAttribute('predetail_text', "&nbsp;");
		}
	}

	//Пересчитываем количество комментариев
	protected function afterSaveRecountComments()
	{
		$comments_count = Comments::model()->countByAttributes(array('entity_id' => $this->article_id));
		Articles::model()->updateByPk($this->article_id, array('comments_count' => $comments_count));
	}


	//Создаем связь с тегами
	protected function afterSaveProcessTags()
	{
		// Список тегов до сохранения
		$list_tags_before_save = Yii::app()->db->createCommand()
			->select('tag_id')->from('bg_articles_to_tags')
			->where('article_id=:id', array(':id' => $this->id))
			->queryColumn();

		//Удаляем все из таблицы связей
		ArticlesToTags::model()->deleteAll('article_id = :id', array(':id' => $this->article_id));
		$tags = Yii::app()->request->getPost('tags');
		if (!is_null($tags) && is_array($tags))
		{
			$tags = array_unique($tags);
			foreach ($tags as $tag_id)
			{
				if (!is_null($tag_id))
				{
					$aa             = new ArticlesToTags();
					$aa->article_id = $this->article_id;
					$aa->tag_id     = $tag_id;
					$aa->save();
				}

			}
		}
		else
		{
			$tags = array();
		}

		// Список тегов для обновления
		$list_tags_to_update = array_unique(array_merge($tags, $list_tags_before_save));
		if (count($list_tags_to_update))
		{
			$sql = 'SELECT `tag_id`, COUNT(`article_id`) AS `count`
				FROM `bg_articles_to_tags`
				LEFT JOIN `bg_articles` `a` ON (`a`.`id`=`bg_articles_to_tags`.`article_id`)
				WHERE `tag_id` IN (' . implode(',', $list_tags_to_update) . ') AND `a`.`active`=1 ';

			// Список тегов с количеством упоминаний
			$list_articles_to_update = Yii::app()->db->createCommand($sql . 'GROUP BY `tag_id`')->queryAll();

			// Обновляем счетчик
			for ($i = 0; $i < count($list_articles_to_update); $i++)
			{
				Yii::app()->db->createCommand()->update('bg_tags', array(
					'count' => $list_articles_to_update[$i]['count'],
				), 'id=:id', array(':id' => $list_articles_to_update[$i]['tag_id']));
			}

			// Список тегов для гидов с количеством упоминаний
			$list_articles_to_update = Yii::app()->db->createCommand($sql . 'AND `a`.`is_guide`=1 GROUP BY `tag_id`')->queryAll();

			// Обновляем счетчик тегов для гидов
			for ($i = 0; $i < count($list_articles_to_update); $i++)
			{
				Yii::app()->db->createCommand()->update('bg_tags', array(
					'count_guides' => $list_articles_to_update[$i]['count'],
				), 'id=:id', array(':id' => $list_articles_to_update[$i]['tag_id']));
			}
		}
	}

	//Создаем связь с регионами
	protected function afterSaveProcessRegions()
	{
		// Список тегов до сохранения
		$list_regions_before_save = Yii::app()->db->createCommand()
			->select('region_id')->from('bg_articles_to_regions')
			->where('article_id=:id', array(':id' => $this->id))
			->queryColumn();

		//Удаляем все из таблицы связей
		ArticlesToRegions::model()->deleteAll('article_id = :id', array(':id' => $this->article_id));
		$regions = Yii::app()->request->getPost('regions');
		if (!is_null($regions) && is_array($regions))
		{
			$regions = array_unique($regions);
			foreach ($regions as $region_id)
			{
				if (!is_null($region_id))
				{
					$aa             = new ArticlesToRegions();
					$aa->article_id = $this->article_id;
					$aa->region_id     = $region_id;
					$aa->save();
				}

			}
		}
		else
		{
			$regions = array();
		}

		// Список тегов для обновления
		$list_regions_to_update = array_unique(array_merge($regions, $list_regions_before_save));
		if (count($list_regions_to_update))
		{
			$sql = 'SELECT region_id, COUNT(`article_id`) AS `count`
				FROM `bg_articles_to_regions`
				LEFT JOIN `bg_articles` `a` ON (`a`.`id`=`bg_articles_to_regions`.`article_id`)
				WHERE `region_id` IN (' . implode(',', $list_regions_to_update) . ') AND `a`.`active`=1 ';

			// Список тегов с количеством упоминаний
			$list_articles_to_update = Yii::app()->db->createCommand($sql . 'GROUP BY `region_id`')->queryAll();

			// Обновляем счетчик
			for ($i = 0; $i < count($list_articles_to_update); $i++)
			{
				Yii::app()->db->createCommand()->update('bg_regions', array(
					'count' => $list_articles_to_update[$i]['count'],
				), 'id=:id', array(':id' => $list_articles_to_update[$i]['region_id']));
			}

		}
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$atr        = array(
			'id'                         => 'ID',
			'parent_id'                  => 'Раздел',
			'name'                       => 'Название',
			'active'                     => 'Опубликовано',
			'assignee'                   => 'Ответственный',
			'proof_tested'               => 'Прочитано корректором',
			'comment'                    => 'Комментарий',
			'code'                       => 'Символьный код',
			'show_main_announce'         => 'Главный анонс морды',
			'show_main_announce_section' => 'Главный анонс раздела',
			'show_on_indexpage'			=>	'Показывать на морде',
			'tags'                       => 'Теги',
			'regions'                       => 'Регионы',
			'relations'                  => 'Связанные статьи',
			'date_publish'               => 'Дата на сайте',
			'date_active_start'          => 'Дата начала активности',
			'authorsids'                 => 'Авторы',
			'presentersids'              => 'Ведущие',
			'preview_text'               => 'Анонс',
			'predetail_text'             => 'Лид',
			'series_id'                  => 'Серия',
			'preview_img'                => 'Анонсная картинка',
			'preview_img_alt'            => 'Подпись к картинке',
			'preview_img_show'			 => 'Показывать анонсную картинку в статье',
			'sendToTwitter'              => 'Отправить в твиттер',
			'snatasks'                   => 'Соцагрегатор',
			'is_best'                    => 'Лучшая статья',
			'has_gallery'                => 'Фотогалерея',
			'has_video'                  => 'Видео',
			'has_translation'            => 'Трансляция',
			'is_archive'                 => 'Архивная статья',
			'link'                       => 'Произвольная ссылка',
			'ceil_type'                  => 'Тип плитки',
			'ceil_quote_author'          => 'Автор цитаты',
			'ceil_quote_text'            => 'Текст цитаты',
			'is_news'                    => 'Новость(в виджет новостей)',
			'is_mininews'                => 'Мини-новость',
			'is_guide'                   => 'Гид',
			'is_howto'                   => 'Инструкция',
			'is_main_guide'              => 'Главный гид',
			'is_digitilized'             => 'Оцифровано из архива',
			'chapter_paging_type'        => 'Тип листалки глав',
			'magazine_id'                => 'Журнал №',
			'to_rss'                     => 'В яндекс.news',
			'seo_title'                  => 'SEO title',
			'view'                       => 'Шаблон',
			'include_text'               => 'Включенный в заголовок текст',
			'radiosvoboda'               => 'Материал радио Свобода',
			'theme_week_code'            => 'Тематическая неделя',
			'customer'            		 => 'Заказчик',
			'price'            			 => 'Цена 1000 знаков',
		);
		$parent_atr = parent::attributeLabels();
		return $atr + $parent_atr;
	}


	/**
	 * Ставим активной версию статьи из истории
	 * @param $id ID ArticlesDraft
	 * @return int ID статьи
	 */
	public static function restore($id)
	{
		$id                       = intval($id);
		$draft                    = ArticlesDraft::model()->findByPK($id);
		$draft->scenario          = 'restore';
		$draft->save_only_article = true;
		// Удаляем is_dummy
		self::model()->updateAll(array('is_dummy' => 0), 'article_id=:id', array(':id' => $draft->article_id));
		$draft->is_dummy = 1;
		$draft->edited   = 0;
		$draft->save();

		// Удаляем все главы и элементы, которые сохранялись не для истории (hist = 0)
		ElementsDraft::model()->deleteAll('article_id=:id AND hist = 0', array(':id' => $draft->article_id));
		ChaptersDraft::model()->deleteAll('article_id=:id AND hist = 0', array(':id' => $draft->article_id));

		// Создаем копии глав и элементов, чтобы их редактировать нетрогая историю
		$chapters = ChaptersDraft::model()->findAll('article_draft_id=:id AND hist = 1', array(':id' => $id));
		foreach ($chapters as $chapter)
		{
			$newChapter = new ChaptersDraft();
			foreach ($chapter as $key => $val)
			{
				$newChapter->$key = $val;
			}
			$newChapter->setIsNewRecord(true);
			unset($newChapter->id);
			$newChapter->hist = 0;
			$newChapter->save();

			$elements = ElementsDraft::model()->findAll('chapter_id=:id AND hist = 1', array(':id' => $chapter->id));
			foreach ($elements as $element)
			{
				$newElement = new ElementsDraft();
				foreach ($element as $key => $val)
				{
					$newElement->$key = $val;
				}
				$newElement->chapter_id = $newChapter->id;
				$newElement->setIsNewRecord(true);
				unset($newElement->id);
				$newElement->hist = 0;
				$newElement->save();
			}
		}
		Articles::restoreFromDraft($draft->id);
		return $draft->article_id;
	}


	/**
	 * Вернуть текущую Draft версию статьи
	 * @param $article_id
	 * @return ActiveRecord ArticlesDraft
	 */
	public static function getCurrentDraft($article_id)
	{
		return self::model()->find('article_id=:id AND is_dummy=1', array(':id' => intval($article_id)));
	}




	public function behaviors()
	{
		return array(
			// 'searchable' => array(
			// 	'class' => 'YiiElasticSearch\SearchableBehavior',
			// ),
		);
	}



	



	/**
	 * @param DocumentInterface $document the document that is providing the data for this record.
	 */
	// public function parseElasticDocument(YiiElasticSearch\Document $document)
	// {
	// 	// You should always set the match score from the result document
	// 	if ($document instanceof SearchResult)
	// 		$this->setElasticScore($document->getScore());

	// 	$this->id       = $document->getId();
	// 	$this->name     = $document->name;
	// 	$this->tags     = $document->tags;
	// }
}