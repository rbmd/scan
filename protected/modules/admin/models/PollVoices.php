<?php

/**
 * This is the model class for table "bg_votes_voces".
 *
 * The followings are the available columns in table 'bg_votes_voices':
 * @property integer $id
 * @property integer $vote_id
 * @property integer $item_id
 * @property integer $user_id
 * @property string $ip_address
 */
class PollVoices extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_poll_voices';
    }

    public function behaviors()
    {
        return array();
        /*return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );*/
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('vote_id, item_id', 'required'),
            array('vote_id, item_id', 'numerical', 'integerOnly'=>true),
            array('user_id, ip_address, uniq_group_key', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            //Опрос к которому  относится данный голос
            'vote' => array(self::BELONGS_TO, 'Polls', 'vote_id'),
            //Вариант ответа к которому относится данный голос
            'vote_item' => array(self::BELONGS_TO, 'PollItems', 'item_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'vote_id' => 'Vote',
            'item_id' => 'Item ID',
        );
    }

    /**
     * Возвращает истину, если данный голос подан за опрос $poll_id
     *
     * @param $poll_id
     * @return bool
     */
    public function pollIs($poll_id)
    {
        return $this->vote_id+0 == $poll_id+0;
    }

    public function beforeSave() {

        $this->user_id = Yii::app()->user->isGuest ? new CDbExpression('NULL') : Yii::app()->user->id;

        $this->ip_address = new CDbExpression('INET_ATON(:ip_address)', array(':ip_address' => $this->ip_address ));

        return parent::beforeSave();
    }
}
?>