<?php
/**
 * User: Русаков Дмитрий
 * Date: 07.06.12
 * Time: 13:16
 */
class ArticlesToGuests extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_articles_to_guests';
	}

	public function behaviors()
	{
		return array(
			'iMemCacheBehavior' => array(
				'class' => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			),
		);
	}

	/**
	 * Проверка, что данная статья связана с гостем $guest_id
	 *
	 * @param $guest_id
	 * @return bool
	 */
	public function guestIs($guest_id)
	{
		return $this->guest_id == $guest_id;
	}

	/**
	 * Проверка, что данный гость связан со статьёй $article_id
	 *
	 * @param $article_id
	 * @return bool
	 */
	public function articleIs($article_id)
	{
		return $this->article_id == $article_id;
	}
}
