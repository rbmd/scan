<?php
class Magazine extends CActiveRecord
{
    /**
     * Название поискового индекса для авторов в поисковом движке "Sphinx"
     */
    const SPHINX_SEARCH_INDEX = "magazine";

    public $articleids;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Authors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Функция поиска по авторам на основе поискового движка Sphinx
     * @static
     *
     * @param $query - поисковый запрос
     * @param $before_match - тег который ставить перед подсвечиваемым словом(само слово из поискового запроса)
     * @param $after_match  - тег который ставить после подсвечиваемого слова(само слово из поискового запроса)
     *
     * @return array - метод возвращает ассоциативный массив содержащий
     *      1) ключ "ids" - массив айдишников найденных авторов;
     *      2) ключ "highlight_titles" - ассоциативный массив имён авторов с подсвеченным поисковым запросом;
     *      3) ключ "highlight_texts" - ассоциативный массив с вырезками из детальных текстов с описанием авторов с
     *         подсвеченным поисковым запросом.
     *
     *      (2) и (3) - ассоциативные массивы, где ключами являются айдишниками авторов из ids
     */
    public static function sphinxSearch($query, $before_match, $after_match)
    {
        $result = array();

        $search = Yii::App()->search;

        $search->setLimits(0, 1024, 1024);
        $search->setMatchMode( SPH_MATCH_ANY );
        $search->SetRankingMode( SPH_RANK_SPH04 );
        //Сортируем результаты по релевантности, а затем по алфавиту
        $search->SetSortMode(SPH_SORT_EXTENDED, '@relevance DESC, name_for_sort ASC');
        //Устанавливаем вес заголовка и детального текста
        $search->setFieldWeights( array('name' => 10, 'preview_text' => 3) );

        $resArray = $search->query( $query, self::SPHINX_SEARCH_INDEX );

        if ( !empty($resArray) && isset($resArray['matches']) && !empty($resArray['matches']) )
        {
            //Здесь айдишники всех найденных журналов
            $ids = array_keys($resArray['matches']);
            $all_ids_list = implode(',', $ids);

            $connection = Yii::app()->db;

            $sql = "SELECT t.`id` FROM `bg_magazine` t WHERE t.`id` IN ({$all_ids_list}) AND t.active=1 ORDER BY FIELD(t.`id`, {$all_ids_list})";
            $command = $connection->createCommand($sql);

            $ids = $command->queryColumn();

            $ids_list = implode(',', $ids);

            if( !empty($ids) )
            {
                //Опции подсветки
                $options = array
                (
                    'before_match'          => $before_match,
                    'after_match'           => $after_match,
                    'chunk_separator'       => '...',
                    'limit'                 => 256,
                    'around'                => 128,
                    'force_all_words'       => true,
                    'html_strip_mode'       => 'strip',
                );

                //Выдёргиваем сначала все заголовки
                $sql = "SELECT t.`preview_text` FROM `bg_magazine` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_titles = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех заголовках
                $res_titles = $search->BuildExcerpts($doc_titles, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_titles = array_combine($ids, $res_titles);

                $result = array(
                    'ids' => $ids,
                    'highlight_titles' => $res_titles,
                    'highlight_texts'  => $res_detail_texts,
                );
            }
        }

        return $result;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bg_magazine';
	}

	public function behaviors()
	{
		return array(
			'iMemCacheBehavior' => array(
				'class' => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			),
		);
	}

	public function scopes()
	{
		return array(
			/*Убирать нельзя, иначе sitemap.xml не сгенерится */
			'visible'=>array(
			),
			'active' => array(
				'condition' => 'active = 1',
			)
		);
	}

    /**
     * Скоуп. Лимит
     */
    public function limit($limit)
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'limit' => $limit,
            )
        );

        return $this;
    }

    /**
     * Скоуп. Сортируем по дате публикации
     */
    public function byPublishDate()
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'order' => 'publish_date DESC',
            )
        );

        return $this;
    }

    /**
     * Скоуп. Год публикации
     */
    public function yearIs($year)
    {
		$date_start = "$year-01-01";
		$date_end = "$year-12-31";

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'publish_date >= :date_start AND publish_date <= :date_end',
				'params' => array(':date_start' => $date_start, ':date_end' => $date_end),
            )
        );

        return $this;
    }

    /**
     * Скоуп. issue_year
     */
    public function issueYearIs($issue_year)
    {

        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'issue_year = :issue_year',
				'params' => array(':issue_year' => $issue_year),
            )
        );

        return $this;
    }


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$rules = array(
			array('issue_year, issue_abs', 'required'),
			array('active, is_special, issue_year, issue_abs', 'numerical', 'integerOnly'=>true),
			array('publish_date, seo_title, seo_keywords, seo_description, image', 'length', 'max'=>255),
			array('preview_text, issuu_code', 'length', 'max'=>10000),
			array('id, active, preview_text, image', 'safe', 'on'=>'search'),
		);
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'issue_year' => 'Номер в году',
			'issue_abs' => 'Сквозной номер',
			'is_special' => 'Спецвыпуск',
			'publish_date' => 'Дата выхода',
			'active' => 'Активность',
			'issuu_code' => 'Код для виджета issuu',
			'preview_text' => 'Описание выпуска',
			'image' => 'Обложка',
			'article_id' => 'Статьи',
		);

		$parent_atr = parent::attributeLabels();
		return $atr + $parent_atr;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('preview_text',$this->preview_text,true);
		$criteria->compare('image',$this->image);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    private function resizeMagazineTo($photo, $x, $y){
        $photo->image_resize = true;
        $photo->image_ratio = true;
        $photo->image_x = $x;
        $photo->image_y = $y;
        $photo->jpeg_quality = 100;
        $photo->file_new_name_body = $this->id;
        $photo->process(Yii::getPathOfAlias('webroot') . '/media/magazine/'.$x.'x'.$y.'/');
    }

	protected function afterSave()
	{
        if( !empty($this->image) )
        {
            $image = Yii::app()->imagemod->load(Yii::getPathOfAlias('webroot') . $this->image);
            if(!file_exists(Yii::getPathOfAlias('webroot') . $this->image)){
                $image->process(Yii::getPathOfAlias('webroot') . $this->image);
            }

            $this->resizeMagazineTo($image, 70, 89);
            $this->resizeMagazineTo($image, 110, 138);
            $this->resizeMagazineTo($image, 236, 292);
        }

		parent::afterSave();
	}

	protected function afterValidate()
	{
		parent::afterValidate();

		$preview_text = $this->getAttribute('preview_text');

		/*Полю, к которому подключается FCKEditor по умолчанию выставляем пробел, иначе западает валидация*/
		if( empty($preview_text) )
			$this->setAttribute('preview_text', "&nbsp;");
	}

	/**
	 * @return string - вернуть ссылку на автора
	 */
	public function url()
	{
		$year = Yii::app()->dateFormatter->format('yyyy', $this->publish_date);
		return "/magazine/{$year}/{$this->issue_year}/";
	}

	public function img($size = 'original')
    {
        if(0 < strlen($this->image)){
            $ext = explode('.', $this->image);
            if('original' == $size){
                $img = "/media/upload/images/magazine/" . $this->id . '.' . $ext[1];
            } else if('70x89' == $size){
                $img = "/media/magazine/70x89/" . $this->id . '.' . $ext[1];
            } else if('110x138' == $size){
                $img = "/media/magazine/110x138/" . $this->id . '.' . $ext[1];
            } else if('236x292' == $size){
                $img = "/media/magazine/236x292/" . $this->id . '.' . $ext[1];
            } else {
                $img = "/media/upload/images/magazine/" . $this->id . '.' . $ext[1];
            }

            if(isset($img))
                return $img.'?'.strtotime($this->x_timestamp);
            else
                return false;
        }
        return false;
    }

    /**
     * @return bool - активность автора
     */
    public function isActive()
    {
        return $this->active + 0 == 1;
    }
}
?>