<?php
class CommentsNotice extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'comments_notice';
	}


	public function relations()
    {
        return array(
			'comment' => array(self::BELONGS_TO, 'Comments', 'comment_id'),
			'article' => array(self::BELONGS_TO, 'Articles', 'entity_id', 'select' => 'id, name, code, parent_id'),
			'post' => array(self::BELONGS_TO, 'Post', 'entity_id', 'select' => 'id, name'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }


	public function select($str = 'id')
	{
		$this->getDbCriteria()->mergeWith(array(
			'select' => $str,
		));
		return $this;
	}

	public function userIdIs($user_id)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.user_id=:user_id',
				'params' => array('user_id' => $user_id),
			)
		);
		return $this;
	}

	// en
	public function entityNameIs($entityName)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'entity_name=:entity_name',
				'params' => array('entity_name' => $entityName),
			)
		);
		return $this;
	}

	// en
	public function entityIdIs($entityId)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'entity_id=:entity_id',
				'params' => array('entity_id' => $entityId),
			)
		);
		return $this;
	}	

	public function addNotice($entity_name, $entity_id, $comment_id, $exclude_user_id)
	{
		$user_ids = Comments::model()->visible()->select('DISTINCT user_id')->entityNameIs($entity_name)->entityIdIs($entity_id)->userIdNot($exclude_user_id)->findAll();

		foreach($user_ids as $u)
		{
			$notice = new CommentsNotice;
			$notice->user_id = $u->user_id;
			$notice->entity_name = $entity_name;
			$notice->entity_id = $entity_id;
			$notice->comment_id = $comment_id;
			$notice->save();
		}
		return true;
	}

	public function deleteNotices($entity_name, $entity_id, $user_id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'entity_name=:entity_name AND entity_id=:entity_id AND user_id=:user_id';
		$criteria->params = array(':entity_name' => $entity_name, ':entity_id' => $entity_id, ':user_id' => $user_id);
		return CommentsNotice::model()->deleteAll($criteria);
	}

	public function getCount($user_id = null)
	{
		if(is_null($user_id))
			$user_id = Yii::app()->user->id;

		$cnt = CommentsNotice::model()->userIdIs($user_id)->count();
		return $cnt;
	}



}