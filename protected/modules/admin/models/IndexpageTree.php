<?php

/**
 * This is the model class for table "bg_galleries".
 *
 * The followings are the available columns in table 'bg_galleries':
 * @property integer $id
 * @property string $name
 */
class IndexpageTree extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Galleries the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'indexpage_tree';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id, tree_id', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        return array(
            'tree' => array(self::BELONGS_TO, 'Tree', 'tree_id')
 		);
	}


	public function scopes()
	{
		return array(
			'indexed' => array(
				'index' => 'id',
			),
		);
	}


	/**
	 * Скоуп. Сортировка по заданому полю
	 */
	public function order($order)
	{
		$this->getDbCriteria()->mergeWith(array(
				'order' => $order,
			)
		);

		return $this;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'   => 'ID',
			'series_id' => 'ID серии',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function behaviors()
	{
		return array(
			'iMemCacheBehavior' => array(
				'class'     => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			)
		);
	}
}