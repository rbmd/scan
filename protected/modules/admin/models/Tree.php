<?php
class Tree extends CActiveRecord
{
    /**
     * Название поискового индекса для телепрограмм(которые являются узлами в дереве) в поисковом движке "Sphinx"
     */
    const SPHINX_SEARCH_INDEX = "tvprogramms";


    /**
     * Идентификатор в дереве для /news/
	 * + СМОТРИ НИЖЕ
     */
    const TREE_NEWS_ID = 7;
	
    /**
     * ID узла дерева с тегами
     */
    const TREE_TAGS_NODE_ID = 24;
	
    /**
     * ID узла Гиды
     */
    const TREE_GUIDES_NODE_ID = 8;
	
    /**
     * ID узла Инструкции
     */
    const TREE_HOWTO_NODE_ID = 7;

    /**
     * ID узла Авторы
     */
    const TREE_AUTHORS_NODE_ID = 9;
	
    /**
     * @var array - массив рубрик, в которые могут помещаться статьи
     */
    public $tree_nodes_for_articles = array(			
		29 => 'news',
		//2 => 'elite',
		3 => 'economy',
		4 => 'world',	
		5 => 'society',
		6 => 'advocacy',	
		7 => 'religion',
		8 => 'politics',
		25 => 'state',	
		26 => 'culture',
		34 => 'sport',
		35 => 'accidents',
    );











    //Активность
    public $active = 0;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Tree the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'bg_tree';
	}

    /**
     * Функция поиска по телепрограммам на основе поискового движка Sphinx
     * @static
     *
     * @param $query - поисковый запрос
     * @param $before_match - тег который ставить перед подсвечиваемым словом(само слово из поискового запроса)
     * @param $after_match  - тег который ставить после подсвечиваемого слова(само слово из поискового запроса)
     *
     * @return array - метод возвращает ассоциативный массив содержащий
     *      1) ключ "ids" - массив айдишников найденных телепрограмм;
     *      2) ключ "highlight_titles" - ассоциативный массив названий телепрограмм с подсвеченным поисковым запросом;
     *      3) ключ "highlight_texts" - ассоциативный массив с вырезками из детальных текстов с описанием телепрограмм с
     *         подсвеченным поисковым запросом.
     *
     *      (2) и (3) - ассоциативные массивы, где ключами являются айдишники телепрограмм из ids
     */
    public static function sphinxSearch($query, $before_match, $after_match)
    {
        $result = array();

        $search = Yii::App()->search;

        $search->setLimits(0, 1024, 1024);
        $search->setMatchMode( SPH_MATCH_ANY );
        $search->SetRankingMode( SPH_RANK_SPH04 );
        //Сортируем результаты по релевантности, а затем по алфавиту
        $search->SetSortMode(SPH_SORT_EXTENDED, '@relevance DESC, name_for_sort ASC');
        //Устанавливаем вес заголовка и детального текста
        $search->setFieldWeights( array('name' => 10, 'detail_text' => 3) );

        $resArray = $search->query( $query, self::SPHINX_SEARCH_INDEX );

        if ( !empty($resArray) && isset($resArray['matches']) && !empty($resArray['matches']) )
        {
            //Здесь айдишники всех найденных телепрограмм
            $ids = array_keys($resArray['matches']);
            $all_ids_list = implode(',', $ids);

            $connection = Yii::app()->db;

            //Так как с момента индексирования какие-то телепрограммы уже могли стать неактивными, то надо отфильтровать
            //полученные из Sphinx айдишники, чтобы не пропустить неактивных телепрограмм.
            $sql = "SELECT t.`id`
                    FROM `bg_tree` t
                    WHERE
                       t.`id` IN ({$all_ids_list}) AND
                       t.`cleft` > 64 AND
                       t.`cright` < 273 AND
                       t.clevel=4 AND
                       t.active=1
                    ORDER BY FIELD(t.`id`, {$all_ids_list})";

            $command = $connection->createCommand($sql);
            //В итоге после фильтрации получаем те же самые айдишники телепрограмм, которые нашёл сфинкс, но
            //при этом неактивные телепрограммы(если они появислись с момента индексации) отсеены.
            $ids = $command->queryColumn();

            $ids_list = implode(',', $ids);

            if( !empty($ids) )
            {
                //Опции подсветки
                $options = array
                (
                    'before_match'          => $before_match,
                    'after_match'           => $after_match,
                    'chunk_separator'       => '...',
                    'limit'                 => 256,
                    'around'                => 128,
                    'force_all_words'       => true,
                    'html_strip_mode'       => 'strip',
                );

                //Выдёргиваем сначала все заголовки
                $sql = "SELECT t.`name` FROM `bg_tree` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_titles = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех заголовках
                $res_titles = $search->BuildExcerpts($doc_titles, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_titles = array_combine($ids, $res_titles);

                //Выдёргиваем теперь детальные тексты всех телепрограмм, которые нашлись
                $sql = "SELECT t.`detail_text` FROM `bg_tree` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_detail_texts = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех детальных текстах
                $res_detail_texts = $search->BuildExcerpts($doc_detail_texts, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_detail_texts = array_combine($ids, $res_detail_texts);

                $result = array(
                    'ids' => $ids,
                    'highlight_titles' => $res_titles,
                    'highlight_texts'  => $res_detail_texts,
                );
            }
        }

        return $result;
    }

    public function scopes()
	{
		return array(
			/*Убирать нельзя, иначе sitemap.xml не сгенерится */
			'visible'=>array(
                'condition' => 't.active = 1',
			),
            'indexed' => array(
                'index' => 'id',
            ),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, code', 'length', 'max'=>255),
            array('eng_name', 'length', 'max'=>255, 'allowEmpty'=>true),
			array('comments_count', 'numerical', 'integerOnly'=>true),
			array('preview_text', 'length', 'max'=>1000000),
			array('code', 'match', 'pattern' => '/[a-z0-9_-]+/i'),
            array('active', 'in', 'range'=>array(0,1)),
            array('seo_title, seo_keywords, seo_description, preview_img, detail_img', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'presenters' => array(self::MANY_MANY, 'Authors', 'bg_authors_to_tree(tree_id, presenter_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
            'active' => 'Активность',
            'eng_name' => 'Английское название',
			'preview_text' => 'Текст',
			'code' => 'Символьный код',
			'preview_img' => 'Маленькое фото',
			'detail_img' => 'Детальное фото',
			'cleft' => 'Cleft',
			'cright' => 'Cright',
			'clevel' => 'Clevel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('cleft',$this->cleft);
		$criteria->compare('cright',$this->cright);
		$criteria->compare('clevel',$this->clevel);
		$criteria->compare('preview_text',$this->preview_text, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function behaviors()
	{
		return array(
			'tree' => array(
				'class' => 'ext.nestedsetbehavior.NestedSetBehavior',
				// хранить ли множество деревьев в одной таблице
				'hasManyRoots' => false,
				// поле для хранения идентификатора дерева при $hasManyRoots=false; не используется
				'rootAttribute' => 'root',
				// обязательные поля для NS
				'leftAttribute' => 'cleft',
				'rightAttribute' => 'cright',
				'levelAttribute' => 'clevel',
			),

		);
	}

	public function afterSave()
	{
		parent::afterSave();

		//Перестраиваем всю цепочку url для раздела и его потомков
		$id = $this->attributes['id'];
		$level = $this->attributes['clevel'];
		$code = $this->attributes['code'];

		//Выбираем текущий раздел
		$node = Tree::model()->findByPk($id);
		
		//Выбираем родителя
		$parent = $node->parent;

		$node_url = ($parent) ? ltrim($parent->url . $code, '/') : '';
		$node_url .= '/';

		if ($node->isLeaf())
		{
			$node->saveAttributes( array('url' => $node_url) );
		}
		else
		{
			//Выбираем всех его детей
			$descendants = $node->descendants()->findAll();

			$leafs = array();
			foreach($descendants as $d)
			{
				if ($d->isLeaf())
				{
					$leafs[] = $d;
				}
			}

			foreach($leafs as $l)
			{
				$ancestors = $l->ancestors()->findAll();
				$tmp_url = '/';
				foreach ($ancestors as $a)
				{
					// if ($a->isRoot())
					// {
					//  $tmp_url = '/';
					// }
					// else
					{
						$tmp_url .= $a->code . '/';
						$tmp_url = ltrim($tmp_url, '/');
					}

					$a->saveAttributes( array('url' => $tmp_url == '' ? '/' : $tmp_url) );
				}

				$l->saveAttributes( array('url' => $tmp_url . $l->code . '/') );
			}
		}
	}

	/**
	 * @return string - вернуть ссылку на страницу этого узла дерева
	 */
	public function url()
	{
		return "/{$this->url}";
	}


	public function getLevel($level)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'clevel=' . $level,
			)
		);
		return $this;
	}


	protected function afterValidate()
	{
		parent::afterValidate();

		$preview_text = $this->getAttribute('preview_text');

		/*Полю, к которому подключается FCKEditor по умолчанию выставляем пробел, иначе западает валидация*/
		if( empty($preview_text) )
			$this->setAttribute('preview_text', "&nbsp;");
	}


	// Возвращает не FLAT дерево, а в виде массива
	public function getTree()
	{
		$struct = array();

		$r = '';
		$children = $this->children()->findAll();
		
		foreach ($children as $key => $c)
		{
			$r .= '<li>' . $c->name . '</li>';
			$struct[] = array(	'data' => array(
												'title' => $c->name, 
												'attr' => array(
														'href' => '/admin/tree/edit/?id=' . $c->id,
													)
											),
								'attr' => array(
												'id' => $c->id,
											)
							);

			if ($c->cright - $c->cleft > 1)
			{
				$struct[ count($struct) - 1 ]['children'] = $c->getTree();
				$r .= '<ul>' . $c->getTree() . '</ul>';
			}

			//$struct[] = $s;
		}

		return $struct;
	}

	/**
	 * Получение списка типов авторов
	 */
	public function getAuthorsTypes() {
		$Node = self::model()->findByPk(self::TREE_AUTHORS_NODE_ID);
		$result = array();
		foreach ($Node->children()->findAll() as $node) $result[$node->id] = $node->name;
		return $result;
	}

	/**
	 * @return int - возвращает количество комментариев
	 * кеширует результат на COMMENTS_COUNT_TTL секунд.
	 * Не берём собственное поле $this->comments_count объекта, так как сам текущий объект
	 * может быть закеширован в кеше на время сущесвенно большее COMMENTS_COUNT_TTL.
	 */
	public function comments_cnt()
	{
		$key = "tree_comments_count_{$this->id}";

		$result = Yii::app()->cache->get($key);

		if( $result === false )
		{
			$model = self::model()->findByPk($this->id, array('select'=>array('t.comments_count')));

			if( empty($model) )
			{
				return null;
			}

			$result = $model['comments_count'];

			Yii::app()->cache->set($key, $result, self::COMMENTS_COUNT_TTL);
		}

		return $result;
	}

	/**
	* Возвращает ноду дерева по заданному пути + кеширует
	*
	*/
	public function getByCode($code = null)
	{
		if(strlen($code) < 1)
			return false;

		$CACHE_KEY = Yii::app()->cache->buildKey('tree', array($code));

		$tree = Yii::app()->cache->get($CACHE_KEY);
		
		if ($tree == false)
		{
			$tree = Tree::model()->find('code=:code', array(':code' => $code));

			Yii::app()->cache->set($CACHE_KEY, $tree, 60);
		}		

		return $tree;
	}

	/**
	* Возвращает ноду дерева по заданному ID + кеширует
	*
	*/
	public function getById($id = 0)
	{
		if(intval($id) < 1)
			return false;

		$CACHE_KEY = Yii::app()->cache->buildKey('tree', array($id));

		$tree = Yii::app()->cache->get($CACHE_KEY);
		
		if ($tree == false)
		{
			$tree = Tree::model()->findByPk($id);

			Yii::app()->cache->set($CACHE_KEY, $tree, 60);
		}		

		return $tree;
	}	
}
?>