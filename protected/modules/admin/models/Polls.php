<?php

/**
 * This is the model class for table "bg_votes".
 *
 * The followings are the available columns in table 'bg_votes':
 * @property integer $id
 * @property bool $active
 * @property string $name
 * @property string $code
 * @property string $type
 * @property string $date_active_start
 * @property string $date_active_end
 */
class Polls extends CActiveRecord
{
    /**
     * Тип опроса, когда возможен только один вариант ответа
     */
    const POLL_TYPE_SINGLE = 'select';

    /**
     * Тип опроса, когда возможны несколько вариантов ответа
     */
    const POLL_TYPE_MULTIPLE = 'checkbox';

    /**
     * Режим: не выполнять проверку, проголосовал ли пользователь или нет
     */
    const REVOTING_VALIDATION_NONE = 'none';

    /**
     * Режим: выполнять проверку проголосовал пользователь или нет с помощью куков
     */
    const REVOTING_VALIDATION_COOKIE = 'cookie';

    /**
     * Режим: выполнять проверку проголосовал пользователь или нет с помощью куков и ip адреса
     */
    const REVOTING_VALIDATION_IP_AND_COOKIE = 'ip_and_cookie';

    /**
     * Раз в сколько минут можно голосовать с одного и того же ip адреса,
     * если включён режим REVOTING_VALIDATION_IP_AND_COOKIE
     */
    const UNIQ_IP_TIMEOUT = 60;

    /**
     * @var array - варианты опросов - Обычный, Точка зрения и тп, влияет на шаблон 
     */
    public $vote_poll_types = array(
        0 => 'Стандартный',
        1 => 'Точка зрения',
    );


    /**
     * @var array - типыопроса
     */
    public $vote_types = array(
        self::POLL_TYPE_SINGLE => 'Один вариант ответа',
        self::POLL_TYPE_MULTIPLE => 'Несколько вариантов ответа'
    );

    /**
     * @var array - типы режимов валидации повторного голосования
     */
    public $revoting_modes = array(
        self::REVOTING_VALIDATION_NONE   => 'Без валидации',
        self::REVOTING_VALIDATION_COOKIE => 'Валидация на базе куков',
        self::REVOTING_VALIDATION_IP_AND_COOKIE => 'Валидация на базе IP и куков',
    );

    /**
     * @var - список вопросов из которых состоит опрос
     */
    public $voteitems;

    /**
     * @var - список ответов на вопросы из которых состоит голосование для СМС голосования
     */
    public $voteitemsanswers;
	
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    protected function afterConstruct()
    {
        parent::afterConstruct();

        $this->date_active_start = date('Y-m-d H:i:s');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_polls';
    }

    public function behaviors()
    {
        return array();
        /*return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),

            'TTL' => array(
                'class' => 'application.extensions.DateActiveStartEndBehavior',
                'date_active_start' => 'date_active_start',
                'date_active_end' => 'date_active_end',
                'condition' => 'active=1',
            ),
        );*/
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = array(
            array('name, code, type, date_active_start, check_revoting_mode', 'required'),
            array('active', 'numerical', 'integerOnly'=>true),
            array('name, code, poll_type', 'length', 'max'=>255),
            array('type', 'in', 'range'=>array_keys($this->vote_types)),
            array('check_revoting_mode', 'in', 'range'=>array_keys($this->revoting_modes)),
            array('date_active_start, date_active_end', 'date', 'format'=>'yyyy-MM-dd HH:mm:ss', 'message'=>'Время начала опроса указано некорректно!'),
            array('name', 'length', 'max'=>255),
            array('seo_title, seo_keywords, seo_description, pic', 'length', 'max'=>255, 'allowEmpty'=>true),
        );

		return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            //Варинты ответа для данного опроса
			'items' => array(self::HAS_MANY, 'PollItems', 'vote_id', 'order'=>'sort ASC'),
            //Количество вариантов ответа для данного опроса
            'items_count'=>array(self::STAT, 'PollItems', 'vote_id'),
            //Сколько людей всего уже приняло участие в этом опросе
            'users_voted_count'=>array(
                self::STAT,
                'PollVoices',
                'vote_id',
                'select'=>'COUNT(DISTINCT(uniq_group_key))'
            ),
            //Общее количество голосов за все варианты ответа, поданное в рамках этого опроса
            'voices_count'=>array(
                self::STAT,
                'PollVoices',
                'vote_id',
            ),
            //Проголосовал ли тиекущий авторизованный пользователь или нет
            'i_am_voted'=>array(
                self::STAT,
                'PollVoices',
                'vote_id',
                'condition'=>'user_id=:user_id',
                'params'=>array(':user_id' => Yii::app()->user->id + 0),
            ),
            //Голосовал ли кто-то по этому опросу с текущего IP-адреса(откуда пришёл запрос) в течении self::UNIQ_IP_TIMEOUT секунд
            'ip_restriction'=>array(
                self::STAT,
                'PollVoices',
                'vote_id',
                'condition'=>'ip_address=INET_ATON(:ip_address) AND NOW() BETWEEN x_timestamp AND x_timestamp +:delta',
                'params'=>array(':ip_address' => Yii::app()->request->userHostAddress, ':delta'=>self::UNIQ_IP_TIMEOUT),
            ),
        );
    }

    public function scopes()
    {
        return array(
            'indexed' => array(
                'index' => 'id',
            ),
            'active' => array(
                'condition' => 'active = 1',
            ),
            'visible'=>array(
                'condition'=>'t.active=1 AND NOW() >= t.date_active_start AND NOW() <= IFNULL(t.date_active_end, NOW() + INTERVAL 1 DAY)',
            ),
        );
    }

    /**
     * Скоуп. Выбирает заданое голосование
     */
    public function pollTypeIs($pollType)
    {
        if(array_key_exists($pollType, $this->vote_poll_types) )
        {
            $this->getDbCriteria()->mergeWith(array(
                'condition' => 'poll_type=:poll_type',
                'params' => array(':poll_type' => $pollType),
                )
            );
        }
        return $this;       

    }   

	/**
     * Скоуп. Выбирает заданое голосование
     */
    public function pkIs($poll_id)
    {
        if(intval($poll_id) < 1)
            return $this;

        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'id=:id',
			'params' => array(':id' => $poll_id),
            )
        );

        return $this;           
    }	

	/**
     * Скоуп. Сортировка по заданому полю
     */
    public function order($order)
    {
        $this->getDbCriteria()->mergeWith(array(
            'order' => $order,
            )
        );

        return $this;           
    }	
	
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
            'active' => 'Активность',
            'name' => 'Название',
            'code' => 'Символьный код',
            'poll_type' => 'Вид опроса',
            'type' => 'Выбор ответа',
            'check_revoting_mode' =>'Режим валидации повторного голосования',
            'date_active_start' => 'Дата начала активности',
            'date_active_end' => 'Дата окончания активности',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
            'seo_title' => 'SEO Title'
        );

		return $atr;
    }

	

	
	
    /**
     * Обработка данных перед сохранением
     *
     * @return bool
     */
    protected function beforeSave()
    {
        $ret = parent::beforeSave();

        $this->name = trim($this->name);
        $this->code = trim($this->code);

        $this->date_active_end = ($this->date_active_end === '') ? new CDbExpression('NULL') : $this->date_active_end;

        return $ret && true;
    }

    /**
     * После сохранения опроса, сохраняем также и варианты ответа, привязанные к этому опросу
     */
	protected function afterSave()
    {
		parent::afterSave();

		$this->afterSaveProcessItems();
	}

    /**
     * После сохранения данных самого опроса сохраняем данные о вариантах ответа на опрос
     */
	protected function afterSaveProcessItems()
    {
        if( !is_array($this->voteitems) )
        {
            $this->voteitems = array();
        }

        $sort = 0;
        $vote_items_ids_list = array();

        foreach($this->voteitems as $item_id => $item)
        {
            ++$sort;

            $name = trim($item['name']);
            $description = trim($item['description']);
            $link = trim($item['link']);
            $img = trim($item['img']);


            if($name === '')
            {
                continue;
            }

            $vote_item = PollItems::model()->findByAttributes( array('vote_id'=>$this->id, 'id'=>$item_id) );

            if ( empty($vote_item) )
            {
                $vote_item = new PollItems();
            }

            $vote_item->vote_id = $this->id;
            $vote_item->name = $name;
            $vote_item->description = $description;
            $vote_item->link = $link;
            $vote_item->img = $img;
            $vote_item->sort = $sort;

            $vote_item->save();

            $vote_items_ids_list[] = $vote_item->id;
        }

        if ( !count($vote_items_ids_list) )
        {
            //Если удалили все варианты ответов, то чистим их и из базы
            PollItems::model()->deleteAll(
                'vote_id=:vote_id', array(':vote_id' => $this->id)
            );
            //Также удаляем ответы пользователей, если они были
            PollVoices::model()->deleteAll(
                'vote_id=:vote_id', array(':vote_id' => $this->id)
            );
        } elseif( count($vote_items_ids_list) > 0) {

            //Если мы режиме редактирования и удалили некоторые варианты, которые были до этого,
            //то вычищаем их и из базы
            PollItems::model()->deleteAll(
                'vote_id=:vote_id AND id NOT IN(' . implode(', ', $vote_items_ids_list) . ')', array(':vote_id' => $this->id)
            );
            //Также удаляем ответы пользователей, если они были
            PollVoices::model()->deleteAll(
                'vote_id=:vote_id AND item_id NOT IN(' . implode(', ', $vote_items_ids_list) . ')', array(':vote_id' => $this->id)
            );
        }
	}

    /**
     * Возвращает истину, если пользователь авторизован и уже проголосовал в этом опросе,
     * в противном случае вовзвращает ложь. Результат кешируется в сессии пользователя.
     * @return bool
     */
    public function am_i_auth_and_voted()
    {
        if( Yii::app()->user->isGuest )
        {
            return false;
        }

        $session = Yii::app()->getSession();

        if( isset($session['voted_polls']) && isset($session['voted_polls'][$this->id]) )
        {
            return $session['voted_polls'][$this->id];
        }

        $r = $this->i_am_voted;

        $r = !empty($r);

        if( !isset($session['voted_polls']) )
        {
            $session['voted_polls'] = array($this->id => $r);
        }
        else{
            $_t = $session['voted_polls'];
            $_t[$this->id] = $r;
            $session['voted_polls'] = $_t;
        }

        return $r;
    }

    /**
     * Возвращает истину, если с этого IP адреса времено запрещено голосование.
     * Результат кешируется в мемкешед, чтобы на какждый запрос к главной не дёргать базу
     * Примечание: $this->ip_restriction - реализовано через релейшен, см relations().
     * @return bool
     */
    public function is_ip_restricted()
    {
       /* $CACHE_KEY = Yii::app()->cache->buildKey(
            keysSet::CACHE_KEY_IP_RESTRICTED,
            array(),
            $this->id . '/' . Yii::app()->request->userHostAddress
        );*/

       # $r = Yii::app()->cache->get($CACHE_KEY);

        #if($r === false)
        {
            $r = $this->ip_restriction+0;

          #  Yii::app()->cache->set($CACHE_KEY, $r, Votes::UNIQ_IP_TIMEOUT);
        }

        return $r;
    }

    /*Активен ли опрос или нет?*/
    public function isActive()
    {
        return $this->active+0 == 1;
    }
	
    /**
     * Вернуть ключ куки, в которой будем хранить инфу о том, что пользователь уже проголосовал
     *
     * @static
     * @param $vote_id
     * @return mixed
     */
    public static function getCookieKey($vote_id)
    {
        return str_replace('.', '_', $_SERVER['HTTP_HOST'] . '_user_vote_on_poll_' . $vote_id);
    }

    /**
     * Показывать пользователю результаты голосования или саму голосовалку
     * @return bool
     */
    public function showResultsToUser()
    {
        //Получаем режим защиты от повторного голосования для данного опроса
        $check_revoting_mode = $this->check_revoting_mode;

        //Если защита от повторного голосования выключена,
        if($check_revoting_mode == self::REVOTING_VALIDATION_NONE)
        {
            //то надо показывать опрос, а не результаты голосования
            return false;
        }

        //Если защита включена, то проверяем сначала
        //а)Стоит ли у пользователя кука, подтверждающая факт голосования,
        //б)или, если кука не обнаружена, проверяем авторизован ли текущий пользователь и проголосовал ли он уже.
        //Если у нас подтвердится условие (а), то благодоря оптимизации PHP условие (б) проверяться уже не будет.
        $result = isset(Yii::app()->request->cookies[self::getCookieKey($this->id)]) ||
                  $this->am_i_auth_and_voted();

        return $result;
    }

    /**
     * Проверяет, может ли голосовать текущий пользователь по дпнному опросу
     */
    public function Can_I_Vote()
    {
        //Если опрос неактивен, то голосовать по нему нельзя
        if (!$this->active){
            return false;
        }

        //Будем считать, что голосовать можно
        $result = true;

        //Получаем режим защиты от повторного голосования для данного опроса
        $check_revoting_mode = $this->check_revoting_mode;

        //Если защита от повторного голосования выключена,
        if($check_revoting_mode == self::REVOTING_VALIDATION_NONE)
        {
            //то голосовать можно
            return $result;
        }

        //Если защита включена, то проверяем сначала
        //а)Не стоит ли у пользователя кука, подтверждающая факт голосования,
        //б)а затем, если кука не обнаружена, проверяем авторизован ли текущий пользователь и проголосовал ли он уже.
        //Если у нас подтвердится условие (а), то благодоря оптимизации PHP условие (б) проверяться уже не будет.
        $result = !(isset(Yii::app()->request->cookies[self::getCookieKey($this->id)])) &&
                  !$this->am_i_auth_and_voted();

        //Если защита от повтрного голосования находится в режиме проверки только по куке,
        if($check_revoting_mode == self::REVOTING_VALIDATION_COOKIE)
        {
            //то вернём результат нащего теста
            return $result;
        }

        //Если режим защиты от повторного голосования предусматривает проверку ещё и по IP адресу с которого пришёл
        //запрос, то проверим ещё и IP и вернём результат.
        return $result && !$this->is_ip_restricted();
    }
	
	//Производим голосование
	public function doVote($poll, $answers)
	{
        $r = null;

		//Получить уникальный идентификатор группы ответов.
		//Смысл в том, чтобы потом мы могли отличить ситуацию, когда пользователи присылают на один опрос
		//несколько вариантов ответов и соответвенно надо делать COUNT(DISTINCT(uniq_group_key)),
		//чтобы правильно посчитать сколько людей проголосовало всего
		//(чтобы скажем 2 ответа от одного юзера не считались за двух проголосовавших).
		$uniq_group_key = User::getUniqueValue();


		
		//Оборачиваем в транзакцию, чтобы в случае ошибки в одном из ответов онулировать все предыдущие
		$transaction = PollVoices::model()->dbConnection->beginTransaction();

		try
		{
			//Заносим все ответы в БД
			foreach($answers as $answer)
			{
				$item = PollItems::model()->findByPk($answer+0, 'vote_id=:poll_id', array(':poll_id'=>$poll->id));

				//Проверим, а существует ли такой вариант ответа на этот опрос
				if( empty($item) )
				{
					throw new Exception('Такого варианта ответа не существует!');
				}

				$voice = new PollVoices();

				$voice->vote_id = $poll->id + 0;
				$voice->item_id = $answer + 0; 
				$voice->user_id = !Yii::app()->user->isGuest ? Yii::app()->user->id : new CDbExpression('NULL');
				$voice->ip_address = Yii::app()->request->userHostAddress;
				$voice->uniq_group_key = $uniq_group_key;

				if( !($r = $voice->save()) )
				{
					//В случае неудачи прерываем процесс сохранения ответов
					throw new Exception('Неизвестная ошибка во время сохранения результатов голосования!');
				}

				//Если опрос подразумевает только один вариант ответа, то в любом случае цикл выполнится
				//только 1 раз, даже если прислали больше ответов
				if($poll->type == Polls::POLL_TYPE_SINGLE)
				{
					break;
				}
			}

			//Если не было ошибок коммитим транзакцию
			$transaction->commit();
		}
		catch(Exception $e)
		{
			//Если произошла ошибка откатываем транзакцию
			$transaction->rollBack();

			$r = false;
		}
		return $r;
	}

	
} 
?>