<?php
class Settings extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_settings';
	}

	public function attributeLabels()
	{
		$atr        = array(
			//'name'                         => 'ID',	
		);

		$parent_atr = parent::attributeLabels();
		return $atr + $parent_atr;
	}


	public function getParam($code)
	{
		$s = Settings::model()->findByAttributes(array('code' => $code));

		return $s->value;
	}

	public function getMinimalPopularityRating()
	{
		return $this->getParam('minimal_popularity_rating');
	}
}