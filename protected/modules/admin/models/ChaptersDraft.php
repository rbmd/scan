<?php
class ChaptersDraft extends Chapters
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_chapters_draft';
	}


	/**
	 * Скоуп возвращает все элементы по заданому article_draft_id с сортировкой по order_num ASC
	 */
	public function allByArticleDraftPk($article_draft_id, $hist = 0)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.article_draft_id=:article_draft_id AND hist = :hist',
				'order'     => 'order_num ASC',
				'params'    => array(':article_draft_id' => $article_draft_id, ':hist' => $hist),
			)
		);
		return $this;
	}

	protected function beforeSave()
	{
		unset($this->x_timestamp);
		return parent::beforeSave();
	}
}