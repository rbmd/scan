<?php
/**
 * User: Русаков Дмитрий
 * Date: 15.06.12
 * Time: 18:08
 *
 * Модель модуля для вставки баннеров на сайт
 *
 */

class Banners extends CActiveRecord
{
    public $active = 0;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_banner';
	}

	public function behaviors()
	{
		return array(

			'iMemCacheBehavior' => array(
				'class' => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			),

		);
	}

	public function rules()
	{
		return array(

			array('active', 'boolean', 'falseValue'=>'0', 'trueValue'=>'1', 'allowEmpty'=>true),
			array('name, code', 'length', 'min'=>1),
            array('name', 'unique', 'message'=>'Баннер с таким именем уже существует'),
            array('code', 'unique', 'message'=>'Баннер с таким уникальным кодом уже существует'),
            array('code', 'match', 'pattern'=>'/^[a-z0-9@_\.-]+$/ui', 'message'=>'Поле содержит запрещённые символы! Разрешено использовать: английские буквы и цифры, а также знак подчёркивания, собаку, точку и тире'),
			array('edit_user_id', 'numerical', 'min'=>1, 'integerOnly'=>true),
			array('html_code', 'length', 'min'=>8, 'allowEmpty'=>true),
			array('code, name', 'required'),
		);
	}

	public function defaultScope()
	{
		return array(
			'select'=>array(
				'*',
			),
		);
	}

	public function scopes()
	{
		return array(
			'active' => array(
				'condition'=>'active=1',
			),

			//Cписок всех баннеров
			'sort_by_id_desc'=>array(
				'order'=>'id DESC',
			),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'active' => 'Активность',
			'name' => 'Название баннера',
			'code' => 'Символьный уникальный код баннера',
			'html_code' => 'Embed-код баннера',
			'edit_user_id' => 'Последний раз редактировал',
		);
	}

    protected function beforeSave()
    {
        $ret = parent::beforeSave();

        $this->name = trim($this->name);
        $this->code = trim($this->code);

        $this->edit_user_id = Yii::app()->user->id;

        return $ret && true;
    }

	/*Активен ли текущий экземпляр баннера*/
	public function isActive()
	{
		return $this->active == 1;
	}
}