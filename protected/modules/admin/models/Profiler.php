    <?php
/**
 * User: Русаков Дмитрий
 * Date: 12.03.2013
 * Time: 18:59
 *
 * Модель профайлера
 *
 */

class Profiler extends CActiveRecord
{
    public static $PROFILER_INTERVALS = array(1,5,10,20,30,60);

    public $widgets = null;

    public $cnt, $avg_exec_time, $max_exec_time, $message, $request_url, $time;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'log';
    }

    public function behaviors()
    {
        return array(
            //
        );
    }

    public function rules()
    {
        return array(
            array('level, category, message, controller_action, request_url', 'length', 'min'=>1),
            array('user_id, logtime, execution_time', 'numerical', 'min'=>0, 'integerOnly'=>true, 'allowEmpty'=>true),
        );
    }

    public function defaultScope()
    {
        return array(
        );
    }

    public function scopes()
    {
        return array(
            //Все контроллеры, отсортированные по времени выполнения
            'controllers' => array(
                'index' => 'controller_action',
                'select' => 'controller_action, count(*) as cnt, AVG(execution_time) as avg_exec_time, MAX(execution_time) as max_exec_time',
                'condition' => "category = 'bg.profiling.controllers' ",
                'group' => 'controller_action', //WITH ROLLUP не работает...
                'order' => 'avg_exec_time DESC',
            ),

            //Все виджеты, отсортированные по времени выполнения
            'widgets' => array(
                'index' => 'message',
                'select' => 'message, count(*) as cnt, AVG(execution_time) as avg_exec_time, MAX(execution_time) as max_exec_time',
                'condition' => "category = 'bg.profiling.widgets' ",
                'group' => 'message', //WITH ROLLUP не работает...
                'order' => 'avg_exec_time DESC',
            ),
        );
    }


    /**Именованный СКОУП: Выбирать только данные за последние $interval минут*/
    public function latest($interval)
    {
		 /*
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "logtime >= UNIX_TIMESTAMP(NOW() - INTERVAL {$interval} MINUTE)",
        ));
		*/
        return $this;
    }

    /**Вытащить все заданные реквесты без группировки*/
    public function all_requests()
    {
        $this->getDbCriteria()->mergeWith(array(
            'select' => 'request_url, request_code, controller_action, FROM_UNIXTIME(logtime, "%Y %D %M %H:%i:%s") as time, execution_time',
            'condition' => "category = 'bg.profiling.controllers'",
            'order' => 'logtime DESC',
        ));

        return $this;
    }

    /**Вытащить все виджеты для запроса с указанным кодом*/
    public function widgets_for_request($request_code)
    {
        $this->getDbCriteria()->mergeWith(array(
            'select' => 'message, execution_time, FROM_UNIXTIME(logtime, "%Y %D %M %H:%i:%s") as time',
            'condition' => "request_code = '{$request_code}' AND category = 'bg.profiling.widgets'",
            'order'  => 'execution_time DESC',
        ));

        return $this;
    }

    /*Задать контроллер для которого будет идти последующая выборка*/
    public function forController($controller)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "controller_action = '{$controller}' ",
        ));

        return $this;
    }

    /**
     * Задать виджет для которого будет идти последующая выборка
     *
     * @param $widget
     * @return $this
     */
    public function widgetIs($widget)
    {
        $this->getDbCriteria()->mergeWith(array(
            'select'    => 'message, request_url, execution_time, FROM_UNIXTIME(logtime, "%Y %D %M %H:%i:%s") as time',
            'condition' => "message = '{$widget}' ",
            'order'     => 'logtime DESC',
        ));

        return $this;
    }

    /**
     * Вытащить запрос по указанному коду
     *
     * @param $code
     * @return $this
     */
    public function requestCodeIs($code)
    {
        $this->getDbCriteria()->mergeWith(array(
            'select' => 'request_url, request_code, controller_action, FROM_UNIXTIME(logtime, "%Y %D %M %H:%i:%s") as time, execution_time, sql_stat',
            'condition' => "category = 'bg.profiling.controllers' AND request_code = '{$code}'",
        ));

        return $this;
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'level' => 'Уровень',
            'category' => 'Категория',
            'message' => 'Сообщение',
            'controller_action' => 'Контроллер и экшен',
            'request_url' => 'Запрошенный URL',
            'user_id' => 'ID пользователя',
            'logtime' => 'Время логгирования',
            'execution_time' => 'Время выполнения',
        );
    }

    /**
     * @static
     *
     * Функция возвращает текущий интревал на котором учитывается статистика  для профилирования
     *
     * @return int - интервал профилирования
     */
    public static function getProfilerInterval()
    {
        $profiler_interval = Yii::app()->cache->get(KeysSet::CACHE_KEY_PROFILER_INTERVAL);

        if($profiler_interval === false)
        {
            $profiler_interval = 60;

            Yii::app()->cache->set(
                KeysSet::CACHE_KEY_PROFILER_INTERVAL,
                $profiler_interval,
                0
            );
        }

        return $profiler_interval;
    }

    /**
     * @static
     *
     * Функция устанавливает текущий интревал профилирования профайлера
     *
     * @param int $profiler_interval - интервал профилирования, может быть 1,5,10,20 минут
     * @throws Exception
     */
    public static function setProfilerInterval($profiler_interval)
    {
        if( !in_array($profiler_interval, self::$PROFILER_INTERVALS) )
        {
            throw new Exception('Вы пытаетесь задать неверный интервал профилирования!');
        }

        Yii::app()->cache->set(KeysSet::CACHE_KEY_PROFILER_INTERVAL, $profiler_interval, 0);
    }

    /**
     * @static
     * Считает суммарное среднее время выполнения всех виджетов нутри одного контроллера
     *
     * @param $controller
     * @return float
     */
    public static function getTotalWidgetsTimePerController($controller)
    {
        $interval = self::getProfilerInterval();

        $sql = "
                SELECT SUM(t.avg_exec_time) as total
                FROM
                (
                SELECT AVG(execution_time) as avg_exec_time
                FROM log t
                WHERE
                    (category = 'bg.profiling.widgets') AND
                    (controller_action = '{$controller}') AND
                    (logtime >= UNIX_TIMESTAMP(NOW() - INTERVAL {$interval} MINUTE))
                GROUP BY message
                ORDER BY avg_exec_time DESC
                ) t;
               ";

        $r = Yii::app()->db->createCommand($sql)->queryScalar();

        return floatval($r);
    }

    /**
     * Суммарное время отработки всех виджетов в запросе
     *
     * @param $request_code
     * @return float
     */
    public static function sum_widgets_time_for_request($request_code)
    {
        $sql = "
                SELECT SUM(round(t.execution_time, 3)) as total
                FROM log t
                WHERE request_code = '{$request_code}' AND category = 'bg.profiling.widgets';
               ";

        $r = Yii::app()->db->createCommand($sql)->queryScalar();

        return floatval($r);
    }
}