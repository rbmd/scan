<?php
class Comments extends CActiveRecord
{
	//Настройки виджета
	const SECRET      = 'bigbigsecret';
	const WIDGET_TYPE = 'Stream';
	const WIDGET_ID   = 817;


	public $nested = array();

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_comments';
	}

	public function rules()
	{
		return array(
			array('active, entity_id, user_id, message', 'required'),
			array('active, user_id, parent_id, entity_id, root_id', 'numerical', 'integerOnly' => true),
			array('user_name, title, entity_name', 'length', 'max' => 255),
			array('user_ip', 'length', 'max' => 100),
			array('message', 'length', 'min' => 1, 'max' => 100000),
		);
	}


	public function relations()
	{
		return array(
			'Profile' => array(self::BELONGS_TO, 'User', 'user_id'),
			'Article' => array(self::BELONGS_TO, 'Articles', 'entity_id', 'on' => 't.entity_name = "Articles"'),
			'Post' => array(self::BELONGS_TO, 'Post', 'entity_id', 'on' => 't.entity_name = "Post"'),
		);
	}



	public function behaviors()
	{
		return array(
			'iMemCacheBehavior' => array(
				'class'     => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id',
			),
		);
	}

	// public function behaviors()
	// {
	// 	return array(
	// 		'NestedSetBehavior' => array(
	// 			'class' => 'ext.nestedsetbehavior.NestedSetBehavior',
	// 			'hasManyRoots' => true,
	// 			'rootAttribute' => 'ns_root',
	// 			'leftAttribute' => 'ns_left',
	// 			'rightAttribute' => 'ns_right',
	// 			'levelAttribute' => 'ns_level',
	// 		),
	// 	);
	// }

	public function attributeLabels()
	{
		return array(
			'id'           => 'ID',
			'active'       => 'Active',
			'entity_id'    => 'Element',
			'entity_name'    => 'Element name',
			'date_publish' => 'Comment Date',
			'user_id'      => 'User',
			'user_name'    => 'User Name',
			'title'        => 'Title',
			'message'      => 'Message',
		);
	}

	public function search()
	{

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('active', $this->active);
		$criteria->compare('element_id', $this->element_id);
		$criteria->compare('date_publish', $this->date_publish, true);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('user_name', $this->user_name, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('message', $this->message, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function afterValidate()
	{
		$stoplist   = CommentsStoplist::model()->findAll();
		$errorCount = 0;
		foreach ($stoplist as $word)
		{
			if (strpos($this->message, $word->word) !== false)
			{
				$errorCount++;
			}
		}

		if ($errorCount)
		{
			$this->addError('error', 'Ваша запись похожа на спам. Мы не можем ее опубликовать');
		}
		return parent::afterValidate();
	}


	// Обновить счетчики комментариев у статей
	public static function UpdateCounts()
	{
		$sql      = 'SELECT bg_articles.id AS id, COUNT(bg_comments.id) AS c FROM bg_articles LEFT JOIN bg_comments ON (bg_comments.entity_id=bg_articles.id) WHERE bg_comments.active=1 GROUP BY bg_articles.id HAVING c > 0';
		$articles = Yii::app()->db->createCommand($sql)->queryAll();
		Yii::app()->db->createCommand('UPDATE bg_articles SET comments_count=0')->execute();
		for ($i = 0; $i < count($articles); $i++)
		{
			$sql = 'UPDATE bg_articles SET comments_count=' . $articles[$i]['c'] . ' WHERE id=' . $articles[$i]['id'];
			Yii::app()->db->createCommand($sql)->execute();
		}


		$sql      = 'SELECT bg_posts.id AS id, COUNT(bg_comments.id) AS c FROM bg_posts LEFT JOIN bg_comments ON (bg_comments.entity_id=bg_posts.id) WHERE bg_comments.active=1 GROUP BY bg_posts.id HAVING c > 0';
		$articles = Yii::app()->db->createCommand($sql)->queryAll();
		Yii::app()->db->createCommand('UPDATE bg_articles SET comments_count=0')->execute();
		for ($i = 0; $i < count($articles); $i++)
		{
			$sql = 'UPDATE bg_posts SET comments_count=' . $articles[$i]['c'] . ' WHERE id=' . $articles[$i]['id'];
			Yii::app()->db->createCommand($sql)->execute();
		}
	}


	public function beforeSave()
	{
		// Обновление счётчика кол-ва комментов для статей
		if (mb_strtolower($this->entity_name) == 'articles' && $this->isNewRecord)
		{
			Articles::model()->updateCounters(
				array('comments_count' => 1),
				"id = :id",
				array(':id' => $this->entity_id)
			);

			// Сброс кеша кол-ва комментов для статьи
			Yii::app()->cache->delete('article_comments_count_' . $this->entity_id);
		}



		// Обновление счётчика кол-ва комментов для статей
		if (mb_strtolower($this->entity_name) == 'post' && $this->isNewRecord)
		{
			Post::model()->updateCounters(
				array('comments_count' => 1),
				"id = :id",
				array(':id' => $this->entity_id)
			);

			// Сброс кеша кол-ва комментов для статьи
			Yii::app()->cache->delete('post_comments_count_' . $this->entity_id);
		}



		return parent::beforeSave();
	}


	/**
	 * Есть ли ещё комментарии к статье (отображать ли кнопку "Загрузить ещё")
	 * @param
	 * @return bool
	 */
	public function isMoreAvailable($params)
	{
		$commentsTable = $this->tableName();

		$sql = "
            SELECT count(*) AS root_count FROM {$commentsTable}
            WHERE 
                parent_id = 0 AND
                entity_name = '{$params['entity_name']}' AND
                entity_id = {$params['entity_id']} AND
                date_publish > '{$params['date_from']}'
            LIMIT {$params['limit']}
        ";

		$res = Yii::app()->db->createCommand($sql)->queryRow();
		return false;
		//return $res['root_count'] > 0 ? true : false;
	}

	/**
	 * Получение дерева комментариев
	 * @param array $params массив параметров для выборки
	 * @return array $result массив отсортированных по дате комментариев
	 */
	public function getList($params)
	{
		$result    = false;
		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::COMMENTS_LIST, array(), 'ENTITY_NAME_' . $params['entity_name'] . '_ENTITY_ID_' . $params['entity_id'] );
		$result    = Yii::app()->cache->get($CACHE_KEY);
		if ($result === false)
		{
			$commentsTable = $this->tableName();
			$count         = 0;
			$date_from     = (isset($params['date_from'])) ? " AND date_publish < '{$params['date_from']}'" : "";
			$date_active   = (isset($params['active_only']) && $params['active_only']) ? 'active=1 AND' : '';

			$sql      = "SELECT {$commentsTable}.*, UNIX_TIMESTAMP(date_publish) AS x_timestamp FROM {$commentsTable}
                WHERE
                	  {$date_active}
                    entity_name = '{$params['entity_name']}' AND
                    entity_id = {$params['entity_id']}
                    {$date_from} 
                    ORDER BY date_publish ASC
                LIMIT {$params['limit']}";

			/*
			$sql      = "SELECT {$commentsTable}.*, UNIX_TIMESTAMP(date_publish) AS x_timestamp FROM {$commentsTable}
                WHERE
                	  {$date_active}
                    entity_name = '{$params['entity_name']}' AND
                    entity_id = {$params['entity_id']}
                    {$date_from} AND (active=1 OR NOW()-INTERVAL 1 DAY <= date_publish)
                    ORDER BY date_publish ASC
                LIMIT {$params['limit']}";
                */

			$rootsIDs = Yii::app()->db->createCommand($sql)->queryAll();
			$comments = array();
			if (count($rootsIDs))
			{
				foreach ($rootsIDs as $comment)
				{
					if ($comment['active'] == 1)
					{
						$count++;
					}
					$comment['rating'] = Ratings::model()->getRating('Comments', $comment['entity_id']);
					$comments[$comment['parent_id']][] = $comment;
				}

				$lastComment     = end($comments);
				$lastCommentDate = $lastComment[0]["date_publish"];
				$result          = array('count' => $count, 'Comments' => $comments, 'lastCommentDate' => $lastCommentDate);
			}

			Yii::app()->cache->set($CACHE_KEY, $result, 60);

			Yii::app()->cache->addDependency($CACHE_KEY, array(
					"Comments@isEntity({$params['entity_name']}, {$params['entity_id']})" => array()
				)
			);
		}
		return $result;
	}


	public function isEntity($entityName, $entityId)
	{
		return ($this->entity_name == $entityName && $this->entity_id == $entityId);
	}


	public function scopes()
	{
		return array(
			'visible' => array(
				'condition' => 'active=1',
			)
		);
	}


	// Скоуп select
	public function select($str = 'id')
	{
		$this->getDbCriteria()->mergeWith(array(
			'select' => $str,
		));
		return $this;
	}

	// en
	public function entityNameIs($entityName)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'entity_name=:entity_name',
				'params' => array('entity_name' => $entityName),
			)
		);
		return $this;
	}

	// en
	public function entityIdIs($entityId)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'entity_id=:entity_id',
				'params' => array('entity_id' => $entityId),
			)
		);
		return $this;
	}	


	public function userIdNot($exclude_user_id)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'user_id != :user_id',
				'params' => array('user_id' => $exclude_user_id),
			)
		);
		return $this;
	}	

	public function getLink()
	{
		if($this->entity_name == 'Articles')
		{
			return '/articles/article-'.$this->entity_id.'/#comment'.$this->id;
		}

		if($this->entity_name == 'Post')
		{
			return Post::model()->url($this->entity_id).'#comment'.$this->id;
		}

		return '/';
	}

	/**
	* Возвращает заданое поле родительской сущности
	* Например, для того, чтобы получить название статьи или поста, к которой относится текущий комментарий
	* делаем $comment->getEntityField('name');
	*/
	public function getEntityField($field = 'name')
	{
		$criteria=new CDbCriteria;
		$criteria->select = $field;
		$criteria->condition = 'id=:id';
		$criteria->params = array(':id' => $this->entity_id);

		if($this->entity_name == 'Articles')
		{
			$article = Articles::model()->find($criteria);
			if(!is_null($article))
				return $article->name;
		}

		if($this->entity_name == 'Post')
		{
			$post = Post::model()->find($criteria);
			if(!is_null($post))
				return $post->name;
		}	
		return '';	
	}

}