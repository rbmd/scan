<?php
class ChtpzNews extends CActiveRecord
{


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chtpz_news';
	}


	public function scopes()
	{
		return array(

			'active'  => array(
				'condition' => 't.active = 1',
			),
		);
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name', 'required'),
            array('active, date_active_start, detail_text', 'safe'),
			array('name', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}


	public function url()
	{
		return '/news/'.$this->id.'/';
	}



}