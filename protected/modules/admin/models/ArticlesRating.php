<?

class ArticlesRating extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_articles_ratings';
	}


	public function rules()
	{
		$rules = array(
			array('user_id, article_id, rating', 'numerical', 'integerOnly' => true),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}


	public function relations()
	{
		return array(
			'articles'  => array(self::HAS_MANY, 'Articles', 'article_id', 'order' => 'articles.date_create DESC'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}



	public function attributeLabels()
	{
		$r = array(
			
		);

		return $r;
	}



	/**
	* Функция оценки статей
	* param int $article_id
	* param int $rate +1 -1
	* @return array() of messages
	*/
	public function rate($article_id, $rate)
	{
		$user_id = Yii::app()->user->id;

		if($rate != 1 && $rate != -1)
			throw new CHttpException(404);

		//Проверяем, не голосовал ли уже юзер
		$criteria = new CDbCriteria;
		$criteria->select = 'user_id';
		$criteria->condition = 'article_id=:article_id AND user_id=:user_id';
		$criteria->params = array(':article_id' => $article_id, ':user_id' => $user_id);
		$already_rate = ArticlesRating::model()->find($criteria);

		if(is_null($already_rate))
		{
			//Проверяем, есть ли указаная статья
			$article = Articles::model()->findByPk($article_id);

			if(is_null($article))
				throw new CHttpException(404);

			//Незарегеным нельзя голосовать
			if(Yii::app()->user->isGuest)
				throw new CHttpException(403);

			//Добавляем голос
			$rating = new ArticlesRating();
			$rating->user_id = $user_id;
			$rating->article_id = $article_id;
			$rating->rating = $rate;
			$rating->save();

			//Считаем новую сумму голосов
			$criteria = new CDbCriteria;
			$criteria->select = 'sum(rating) as rating';  // подходит только то имя поля, которое уже есть в модели
			$criteria->condition = 'article_id=:article_id';
			$criteria->params = array(':article_id' => $article_id);
			$new_rating = ArticlesRating::model()->find($criteria)->getAttribute('rating'); 		

			//Обновляем значение в статье
			$sql = 'UPDATE bg_articles SET rating=:rating WHERE id=:article_id LIMIT 1';
			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(':rating', $new_rating);
			$command->bindParam(':article_id', $article_id);
			$command->execute();


			$ret['status'] = 'success';
			$ret['rating'] = $new_rating;			
		}
		else
		{
			$ret['status'] = 'already';
		}
		

		return $ret;
	}

}





