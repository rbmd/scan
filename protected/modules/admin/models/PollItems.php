<?php

/**
 * This is the model class for table "bg_votes_items".
 *
 * The followings are the available columns in table 'bg_votes_items':
 * @property integer $id
 * @property integer $vote_id
 * @property string $name
 * @property integer $voices_count
 */
class PollItems extends CActiveRecord
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_poll_items';
    }

    public function behaviors()
    {
        return array();
        /*return array(
            'iMemCacheBehavior' => array(
                'class' => 'application.extensions.imemcache.iMemCacheBehavior',
                'entity_id' => 'id'
            ),
        );*/
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('vote_id, name', 'required'),
            array('vote_id, sort', 'numerical', 'integerOnly'=>true),
            array('name, img, link', 'length', 'max'=>255),
            array('description', 'length', 'max'=>10000),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            //Какому опросу пренадлежит этот вариант ответа
			'vote' => array(self::BELONGS_TO, 'Polls', 'vote_id'),
            //Количество голосов за каждый варинт ответа
            'voices_count'=>array(self::STAT, 'PollVoices', 'item_id'),
        );
    }

    public function defaultScope()
    {
        return array(
            'select' => '*',
            'index' => 'id',
            'order' => 'sort ASC',
            'with' => 'voices_count',
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'vote_id' => 'Опрос',
            'name' => 'Имя',
			'sort' => 'Порядок следования этого вопроса в опросе',
        );
    }

    /**
     * Возвращает истину, если данный вариант ответа относится к опросу $poll_id
     *
     * @param $poll_id
     * @return bool
     */
    public function pollIs($poll_id)
    {
        return $this->vote_id+0 == $poll_id+0;
    }
}
?>