<?php
class Chapters extends CActiveRecord
{
    /**
     * @var Articles - ссылка на статью к которой относится эта глава.
     * Используйте метод getParentArticle, чтобы получить доступ к родительской статье.
     */
    private $_parent_article = null;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_chapters';
	}

	public function behaviors()
	{
		return array();
	}

	public function scopes()
	{
		return array(
			'visible'        => array(),
			'Indexed'        => array(
				'index' => 'id',
			),
			'indexedByOrder' => array(
				'index' => 'order_num',
			),
		);
	}

	/**
	 * Скоуп для указания полей для выборки
	 */
	public function select($select)
	{
		$this->getDbCriteria()->mergeWith(array(
				'select' => $select,
			)
		);
		return $this;
	}

	/**
	 * Скоуп возвращает все элементы по заданому article_id с сортировкой по order_num ASC
	 */
	public function allByArticlePk($article_id)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.article_id=:article_id',
				'order'     => 'order_num ASC',
				'params'    => array(':article_id' => $article_id),
			)
		);
		return $this;
	}

	/**
	 * Скоуп возвращает все элементы по заданому article_id
	 */
	public function articlePkIs($article_id)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.article_id=:article_id',
				'params'    => array(':article_id' => $article_id),
			)
		);
		return $this;
	}

	/**
	 * Скоуп возвращает все элементы по заданому order_num
	 */
	public function orderNumIs($order_num)
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 't.order_num=:order_num',
				'params'    => array(':order_num' => $order_num),
			)
		);
		return $this;
	}


	public function rules()
	{
		return array(
			array('article_id', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 255),
			array('image', 'length', 'max' => 255),
			//array('detail_text', 'length', 'max' => 100000),
			array('seo_title, seo_keywords, seo_description', 'length', 'max' => 255),
			array('order_num', 'safe'),

		);
	}


	public function attributeLabels()
	{
		$atr        = array(
			'id'   => 'ID',
			'name' => 'Название',
		);
		$parent_atr = parent::attributeLabels();
		return $atr + $parent_atr;
	}

    /**
     * @return Articles|CActiveRecord|null - получить ссылку на статью, к которой принадлежит эта глава.
     */
    public function getParentArticle()
    {
        return $this->_parent_article ? $this->_parent_article : ($this->_parent_article=Articles::model()->findByPk($this->article_id));
    }

    public function url()
    {
        return $this->getParentArticle()->url() . "?chapter=" . $this->order_num;
    }
}