<?php
class Authors extends CActiveRecord
{
    /**
     * Название поискового индекса для авторов в поисковом движке "Sphinx"
     */
    const SPHINX_SEARCH_INDEX = "authors";

    /**
     * Типы автора. Совпадают по ключу с id раздела из дерева
     */
    public $types = array(18 => 'Автор', 
						19 => 'Фотографии',
		 				24 => 'Текст и фотографии',
						20 => 'Видео', 
						21 => 'Иллюстрации', 
						22 => 'Продюсер',
						23 => 'Перевод',
						);
						
	public $selected_types;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Authors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Функция поиска по авторам на основе поискового движка Sphinx
     * @static
     *
     * @param $query - поисковый запрос
     * @param $before_match - тег который ставить перед подсвечиваемым словом(само слово из поискового запроса)
     * @param $after_match  - тег который ставить после подсвечиваемого слова(само слово из поискового запроса)
     *
     * @return array - метод возвращает ассоциативный массив содержащий
     *      1) ключ "ids" - массив айдишников найденных авторов;
     *      2) ключ "highlight_titles" - ассоциативный массив имён авторов с подсвеченным поисковым запросом;
     *      3) ключ "highlight_texts" - ассоциативный массив с вырезками из детальных текстов с описанием авторов с
     *         подсвеченным поисковым запросом.
     *
     *      (2) и (3) - ассоциативные массивы, где ключами являются айдишниками авторов из ids
     */
    public static function sphinxSearch($query, $before_match, $after_match)
    {
        $result = array();

        $search = Yii::App()->search;

        $search->setLimits(0, 1024, 1024);
        $search->setMatchMode( SPH_MATCH_ANY );
        $search->SetRankingMode( SPH_RANK_SPH04 );
        //Сортируем результаты по релевантности, а затем по алфавиту
        $search->SetSortMode(SPH_SORT_EXTENDED, '@relevance DESC, name_for_sort ASC');
        //Устанавливаем вес заголовка и детального текста
        $search->setFieldWeights( array('name' => 10, 'detail_text' => 3) );

        $resArray = $search->query( $query, self::SPHINX_SEARCH_INDEX );

        if ( !empty($resArray) && isset($resArray['matches']) && !empty($resArray['matches']) )
        {
            //Здесь айдишники всех найденных авторов
            $ids = array_keys($resArray['matches']);
            $all_ids_list = implode(',', $ids);

            $connection = Yii::app()->db;

            //Так как с момента индексирования какие-то авторы уже могли стать неактивными, то надо отфильтровать
            //полученные из Sphinx айдишники, чтобы не пропустить неактивных авторов.
            $sql = "SELECT t.`id` FROM `bg_authors` t WHERE t.`id` IN ({$all_ids_list}) AND t.active=1 ORDER BY FIELD(t.`id`, {$all_ids_list})";
            $command = $connection->createCommand($sql);
            //В итоге после фильтрации получаем те же самые айдишники авторов, которые нашёл сфинкс, но
            //при этом неактивные авторы(если они появислись с момента индексации) отсеены.
            $ids = $command->queryColumn();

            $ids_list = implode(',', $ids);

            if( !empty($ids) )
            {
                //Опции подсветки
                $options = array
                (
                    'before_match'          => $before_match,
                    'after_match'           => $after_match,
                    'chunk_separator'       => '...',
                    'limit'                 => 256,
                    'around'                => 128,
                    'force_all_words'       => true,
                    'html_strip_mode'       => 'strip',
                );

                //Выдёргиваем сначала все заголовки
                $sql = "SELECT t.`name` FROM `bg_authors` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_titles = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех заголовках
                $res_titles = $search->BuildExcerpts($doc_titles, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_titles = array_combine($ids, $res_titles);

                //Выдёргиваем теперь детальные тексты всех авторов, которые нашлись
                $sql = "SELECT t.`detail_text` FROM `bg_authors` t WHERE t.`id` IN ({$ids_list}) ORDER BY FIELD(t.`id`, {$ids_list})";
                $command = $connection->createCommand($sql);
                $doc_detail_texts = $command->queryColumn();

                //Подсвечиваем поисковый запрос во всех детальных текстах
                $res_detail_texts = $search->BuildExcerpts($doc_detail_texts, self::SPHINX_SEARCH_INDEX, $query, $options);
                $res_detail_texts = array_combine($ids, $res_detail_texts);

                $result = array(
                    'ids' => $ids,
                    'highlight_titles' => $res_titles,
                    'highlight_texts'  => $res_detail_texts,
                );
            }
        }

        return $result;
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bg_authors';
	}

	public function behaviors()
	{
		return array(
			'iMemCacheBehavior' => array(
				'class' => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			),
		);
	}

	public function scopes()
	{
		return array(
			/*Убирать нельзя, иначе sitemap.xml не сгенерится */
			'visible'=>array(
			),
			'active' => array(
				'condition' => 'active = 1',
			)
		);
	}
	
	// Скоуп select
	public function select($str = 'id')
	{
		$this->getDbCriteria()->mergeWith(array(
			'select' => $str,
		));

		return $this;
	}	
	
	// Скоуп. Выбирает автора, в качестве входящего параметра использует "фамилия имя"
	public function fullNameIs($str = '')
	{
		$e = explode(' ', $str);
		$name = isset($e[1]) ? $e[1] : '';
		$surname = isset($e[0]) ? $e[0] : '';
		
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'name=:name AND surname=:surname',
			'params' => array(':name' => $name, ':surname' => $surname),
		));

		return $this;
	}	
	
	// Скоуп с сортировкой по фамилии, затем по имени
	public function bySurnameName()
	{
		$this->getDbCriteria()->mergeWith(array(
			'order' => 't.surname ASC, t.name ASC',
		));

		return $this;
	}
	
    /**
     * Возвращает массив AC всех авторов для заданой статьи, разбитых по типу авторов
     */
    public function authorsByArticleId($article_id)
    {
		$authors = ArticlesToAuthors::model()->with('authors')->articleIdIs($article_id)->findAll();

		foreach($authors as $a)
		{
			$return[$a->type][] = $a;
		}
		
		if(isset($return))			
			return $return;
		else
			return null;
			
    }
	
    /**
	 * Аналогична authorsByArticleId, но возвращает _все_ типы авторов, даже если у данной статьи авторов такого типа нет
     * Возвращает массив AC всех авторов для заданой статьи, разбитых по типу авторов
     */
    public function authorsByArticleIdAT($article_id)
    {
		$authors = ArticlesToAuthors::model()->with('authors')->articleIdIs($article_id)->findAll();
		
		//Чтобы вернуть все типы
		$return = array_flip(array_keys($this->types));

		foreach($authors as $a)
		{
			if(!is_array($return[$a->type]))
				$return[$a->type] = array();
				
			$return[$a->type][] = $a;
		}
		
		if(isset($return))			
			return $return;
		else
			return null;
			
    }		
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name, code', 'required'),
			array('active, gender, user_id', 'numerical', 'integerOnly'=>true),
			array('name, surname, code, email, social, seo_title, seo_keywords, seo_description, preview_img, image, link, link_text', 'length', 'max'=>255),
			array('detail_text', 'length', 'max'=>10000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, active, name, types, surname, code, preview_img, image, link, link_text', 'safe', 'on'=>'search'),
		);

	#	$parent_rules = parent::rules();
	#	$res = array_merge($rules, $parent_rules);
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tree' => array(self::MANY_MANY, 'Tree',
				'bg_authors_to_tree(author_id, tree_id)'
			),
			#'author_type'  => array(self::MANY_MANY, 'ArticlesToAuthors', 'bg_articles_to_authors(article_id, author_id)'),
			'ArticlesToAuthors' => array(self::HAS_MANY, 'ArticlesToAuthors', 'author_id'),
			'articles'  => array(self::HAS_MANY, 'Articles', array('article_id' => 'id'), 'through' => 'ArticlesToAuthors'),
			'User' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'name' => 'Имя',
			'surname' => 'Фамилия',
			'code' => 'Символьный код',
			'preview_img' => 'Маленькая фотография',
			'image' => 'Фотография',
			'detail_text' => 'Описание',
			'active' => 'Активность',
			'social' => 'Социальные сети',
			'link' => 'Ссылка',
			'link_text' => 'Текст ссылки',
			'email' => 'E-mail',
			'gender' => 'Пол',
			'seo_title' => 'SEO title',
			'seo_keywords' => 'SEO keywords',
			'seo_description' => 'SEO description',
			'types' => 'Типы автора',
			'user_id' => 'Аккаунт на сайте'
		);

		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('preview_img',$this->preview_img);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('link_text',$this->link_text,true);
		$criteria->compare('image',$this->image);
		$criteria->compare('detail_text',$this->detail_text,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function afterSave()
	{
		parent::afterSave();
		// Author trees
		
		$AuthorsTypes = AuthorTrees::model()->deleteAll(
			'author_id=:author_id',
			array(':author_id' => $this->id)
		);
		
		if ( isset($this->selected_types) && is_array($this->selected_types) ) {
			foreach ($this->selected_types as $v) {
				$at = new AuthorTrees();
				$at->author_id = $this->id;
				$at->tree_id = $v;
				$at->save();
			}

		}

		// ElasticNestedReindexQueue::addTask('authors', array('should' => array('match' => array('authors.id' => $this->id))));
	}

	protected function afterValidate()
	{
		parent::afterValidate();

		$detail_text = $this->getAttribute('detail_text');

		/*Полю, к которому подключается FCKEditor по умолчанию выставляем пробел, иначе западает валидация*/
		if( empty($detail_text) )
			$this->setAttribute('detail_text', "&nbsp;");
	}

	/**
	 * @return string - возвращает склеенную строку surname + name
	 */
	public function fullname()
	{
		return $this->name.' '.$this->surname;
	}	

	public function fullnameR()
	{
		return $this->surname.' '.$this->name;
	}
	
	/**
	 * @return string - вернуть ссылку на автора
	 */
	public function url()
	{
		return "/authors/{$this->code}-{$this->id}/";
	}

	public function getAuthorsList()
	{
			$authorlist = array();
			//$authorlist[''] = '';
			$authors = Authors::Model()->findAll();
			foreach($authors as $author) {
				$authorlist[$author->id] = $author->name . ' ' . $author->surname;
			}
			
			return $authorlist;
	}
}
?>