<?

class Post extends CActiveRecord
{

	const COMMENTS_COUNT_TTL = 60;


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_posts';
	}


	public function rules()
	{
		$rules = array(
			array('name, preview_text, detail_text', 'required'),
			array('active, banned, approved, blog_approved, blog_id, comments_count, is_draft, draft_for_id', 'numerical', 'integerOnly' => true),
			array('name, cover', 'length', 'max' => 255),
			array('preview_text', 'length', 'max' => 1000),
			array('detail_text', 'length', 'max' => 50000),
			array('date_create', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
			array('date_create', 'default', 'value' => new CDbExpression('NOW()')),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}


	public function relations()
	{
		return array(
			'blog' => array(self::BELONGS_TO, 'Blog', 'blog_id'),
			'draft' => array(self::HAS_ONE, 'Post', 'draft_for_id', 'alias' => 'draft'),
		);
	}


	public function attributeLabels()
	{
		$r = array(
			'id' => 'ID',
			'active' => 'Опубликован',
			'name' => 'Заголовок поста',
			'date_create' => 'Дата создания',
			'preview_text' => 'Анонсный текст',
			'detail_text' => 'Основной текст',
			'approved' => 'Выбор редакции',
			'cover' => 'Анонсная картинка',
			'is_draft' => 'Черновик',
		);

		return $r;
	}





	/**
	 * @return int - возвращает количество комментариев
	 * кеширует результат на COMMENTS_COUNT_TTL секунд.
	 * Не берём собственное поле $this->comments_count объекта, так как сам текущий объект
	 * может быть закеширован в кеше на время сущесвенно большее COMMENTS_COUNT_TTL.
	 */
	public function comments_cnt()
	{
		$key    = "post_comments_count_{$this->id}";
		$result = Yii::app()->cache->get($key);
		if ($result === false)
		{
			$c = Comments::model()->visible()->entityNameIs('post')->entityIdIs($this->id)->count();
			
			if (empty($c))
			{
				return null;
			}
			$result = $c;
			Yii::app()->cache->set($key, $result, self::COMMENTS_COUNT_TTL);
		}
		return $result;
	}


	

	public function afterSave()
	{
		if ($this->isNewRecord)
		{
			$this->date_create = date('Y-m-d H:i:s');
			// $this->saveAttributes(array('blog_approved' => $this->blog->approved));
		}

		parent::afterSave();
	}



	public function beforeDelete()
	{
		Comments::model()->deleteAllByAttributes(array('entity_name' => 'Post', 'entity_id' => $this->id));

		return parent::beforeDelete();
	}


	public function defaultScope()
	{
		return array(
			// 'condition' => 'is_draft = 0',
		);
	}



	public function scopes()
	{
		return array(
			'Indexed' => array(
				'index' => 'id',
			),

			'not_draft' => array(
				'condition' => 'is_draft = 0',
			),

			'active' => array(
				'condition' => 't.active = 1 AND t.banned = 0',
			),
			'visible'=>array(
				'condition' => 't.active = 1 AND is_draft = 0 AND draft_for_id = 0',
			),
		);
	}


	/*
	public function url()
	{
		return $this->blog->url() . $this->id . '/';
	}
	*/

	public function url($post_id = null)
	{
		if($this->id == null && $post_id > 0)
		{
			$post = Post::model()->findByPk($post_id);
			return $post->blog->url() . $post->id . '/';
		}
		else
			return $this->blog->url() . $this->id . '/';
	}





	public function behaviors()
	{
		return array(
			'searchable' => array(
				'class' => 'YiiElasticSearch\SearchableBehavior',
			),
		);
	}


	public function getElasticIndex()
    {
        return 'kavpolit';
    }

	public function getElasticType()
    {
        return 'posts';
    }

	/**
	 * @param DocumentInterface $document the document where the indexable data must be applied to.
	 */
	public function populateElasticDocument(YiiElasticSearch\Document $document)
	{
		$authors[] = array(
						'id' => $this->blog->user->id,
						'name' => $this->blog->user->firstname . ' ' . $this->blog->user->lastname,
						'image' => $this->blog->user->avatar,
						'url' => $this->blog->user->url(),
					);

		$authors_txt = $this->blog->user->firstname . ' ' . $this->blog->user->lastname;

		$document->setId($this->id);
		$document->name     = $this->name;
		$document->active   = (int) ($this->active && $this->is_draft == 0 && $this->draft_for_id == 0 && $this->banned == 0);
		$document->url = $this->url();
		$document->authors  = $authors;
		$document->authors_txt  = $authors_txt;
		$document->date_active_start  = $this->date_create;
		$document->preview_text  = $this->preview_text;
		$document->detail_text  = $this->detail_text;
		$document->preview_img  = $this->cover;
	}

}













