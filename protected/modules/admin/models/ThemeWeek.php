<?php
class ThemeWeek extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_theme_week';
	}


	public function scopes()
	{
		return array(
			'indexed' => array(
				'index' => 'id',
			),
		);
	}


	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 255),
		);
	}

	public function relations()
	{
		return array(
			'articles' => array(self::HAS_MANY, 'Articles', 'theme_week_code')
		);
	}


	public function attributeLabels()
	{
		return array(
			'id'   => 'ID',
			'name' => 'Название'
		);
	}


	public function url()
	{
		return "/themeweek/{$this->code}/";
	}
}