<?php
class CommentsStoplist extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_comments_stoplist';
	}

	public function defaultScope()
	{
		return array(
			'condition' => '"delete"=0',
		);
	}
}