<?php

/**
 * This is the model class for table "bg_galleries".
 *
 * The followings are the available columns in table 'bg_galleries':
 * @property integer $id
 * @property string $name
 */
class Galleries extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Galleries the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bg_galleries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'length', 'max' => 255),
			array('name', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'photos' => array(self::HAS_MANY, 'GalleryPhotos', 'gallery_id',
				'order' => 'order_num ASC'
			),
		);
	}


	public function scopes()
	{
		return array(
			'indexed' => array(
				'index' => 'id',
			),
			'active'  => array(
				'condition' => 'active = 1',
			),
		);
	}


	/**
	 * Скоуп. Сортировка по заданому полю
	 */
	public function order($order)
	{
		$this->getDbCriteria()->mergeWith(array(
				'order' => $order,
			)
		);

		return $this;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'   => 'ID',
			'name' => 'Название',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function behaviors()
	{
		return array(
			'iMemCacheBehavior' => array(
				'class'     => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			)
		);
	}
}