<?php
class ArticlesSaveLog extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'a_debug';
	}


	protected function beforeSave()
	{
		$this->post      = print_r($_POST, true);
		$this->user_id   = Yii::app()->user->id;
		$this->session   = substr(session_id(), 0, 6);
		$this->useragent = Yii::app()->request->getUserAgent();
		return parent::beforeSave();
	}


	public static function saveArticle($model, $type = '', $msg = '', $runtime = 0)
	{
		$debug                   = new self;
		$debug->type             = $type;
		$debug->desc             = $msg;
		$debug->article_id       = $model->article_id;
		$debug->runtime          = $runtime;
		$debug->article_draft_id = $model->id;
		$debug->save();
	}
}