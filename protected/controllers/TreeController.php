<?php
class TreeController extends Controller
{
	public $layout = 'index';
	public $tree = null;

	//индексы для верхней плашки
	public $indexes;


	//инфа об анонсе вебинара или уже идущем вебинаре
	public $webinar;


	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	public function actionIndex($code = 'index')
	{

		$tree_id = 1;		
		$tree = Tree::model()->getById($tree_id);

		$this->setSeo($tree);

		$this->rss_feeds = array();
		#$this->rss_feeds[] = array('title' => 'Все новости', 'code' => 'yandex');
		

		

		if($code == 'index')
		{
			$criteria = new CDbCriteria();
			$criteria->condition = 'place_on_top=0';
			$criteria->order = 'id ASC';
			$projects = Project::model()->findAll($criteria);
			$_render['projects'] = $projects;

			$criteria = new CDbCriteria();
			$criteria->condition = 'place_on_top=1';
			$criteria->order = 'id ASC';
			$announce = Project::model()->find($criteria);
			$_render['announce'] = $announce;

			$this->render('index_projects', $_render);

		}
		else
		{
			$criteria = new CDbCriteria();
			$criteria->condition = 'code=:code';
			$criteria->params = array(':code' => $code);
			$project = Project::model()->find($criteria);
			if(is_null($project))
			{
				$this->redirect('/');
				exit;
			}


			$criteria = new CDbCriteria();
			$criteria->condition = 'project_id=:project_id';
			$criteria->params = array(':project_id' => $project->id);
			$criteria->order = 'date_active_start ASC';
			$_render['materials'] = LikbezMaterials::model()->with('tree')->active()->findAll($criteria);
			$_render['project'] = $project;
			$this->render('index_dynamic', $_render);
		}

	}



	public function actionAddMaterialRequest()
	{

		$mq = new MaterialRequest();
		$mq->attributes = Yii::app()->request->getPost('message');
		$mq->save();
		$this->renderPartial('addmaterialrequest');
	}


}