<?
class SearchController extends Controller
{
	public $layout = 'inner';


	public function actionIndexYandex()
	{
		$node = Tree::model()->visible()->findByPk(11);
		$this->setSeo($node);
		$this->render('index');
	}



	public function actionIndex()
	{
		$limit = 12;

		$page = intval(Yii::app()->request->getParam('page', 0));
		$query = Yii::app()->request->getParam('q');
		$dateFrom = Yii::app()->request->getParam('date-from');
		$dateTo = Yii::app()->request->getParam('date-to');
		$author = Yii::app()->request->getParam('author');
		$section = Yii::app()->request->getParam('section');

		if (!empty($author))
		{
			$author_id = intval($author);
		}

		if (!empty($section))
		{
			$section_id = intval($section);
		}


		if (strtotime($dateFrom) === false)
		{
			$dateFrom = null;
		}

		if (strtotime($dateTo) === false)
		{
			$dateTo = null;
		}


		
		if ($page > 0)
			$page--;

		$search = new \YiiElasticSearch\Search('fnl', 'articles');		
		$search->size = $limit;
		$search->from = $limit * $page;
		
		if (!empty($query))
		{
			$query_fields = array(
						'name^10', 
						'tags^7', 
						'predetail_text^5', 
						'detail_text^5', 
						'preview_text^8',
						// 'authors_txt^1'
					);


			$search->query = array(
				"multi_match" => array(
					'query' => $query, 
					'fields' => $query_fields
				),
			);
		}


		

		$must_filters = array();

		if (!empty($author))
		{
			$must_filters[] = array(
				'nested' => array(
					'path' => 'authors',
					'query' => array(
						'bool' => array('must' => array('match' => array('authors.name' => $author)))
					)
				)
			);
		}



		if (!empty($section))
		{
			$must_filters[] = array(
				'term' => array('parent_id' => $section)
			);
		}


		$filter_date = array();
		if (!empty($dateFrom))
			$filter_date['from'] = date('Y-m-d 00:00:00', strtotime($dateFrom));

		if (!empty($dateTo))
			$filter_date['to'] = date('Y-m-d 23:59:59', strtotime($dateTo));


		if (!empty($filter_date))
		{
			$must_filters[] = array(
							'range' => array(
								'date_active_start' => array(
									$filter_date
								)
							)
						);
		}


		// if (!empty($author_id))
		// {
		// 	$must_filters[] = array(
		// 					'nested' => array(
		// 						'path' => 'authors',
		// 						'query' => array(
		// 							'bool' => array('must' => array('match' => array('authors.id' => $author_id)))
		// 						)
		// 					)
		// 				);
		// }
		


		$pages = new CPagination();
		$result_set = null;

		if (!empty($query) || !empty($must_filters))
		{

			$must_filters[] = array(
				'term' => array('active' => 1)
			);

			if (!empty($must_filters))
			{	
				$search->filter = array(
					'bool' => array(
						'must' => $must_filters,
					)
				);
			}

			$result_set = Yii::app()->elasticSearch->search($search);
			$total = $result_set->getTotal();

			if ($page > $total)
			{
				$page = $total;
			}

		
			$pages->itemCount = $total;
			$pages->pageSize = $limit;
			$pages->currentPage = $page;
		}

		$node = Tree::model()->cache(600)->visible()->findByPk(11);
		$this->setSeo($node);

		$this->layout = 'search';

		if (!Yii::app()->request->isAjaxRequest)
			$this->render('elastic', array('result_set' => $result_set, 'query' => $query, 'author' => $author, 'section' => $section, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'pages' => $pages));
		else
		{
			$this->layout = false;
			$this->renderPartial('elastic', array('result_set' => $result_set, 'query' => $query, 'author' => $author, 'section' => $section, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'pages' => $pages));
		}
	}





	public function actionSuggestAuthor($term = null)
	{
		if (strlen($term) < 3)
			return;


		$criteria = new CDbCriteria();
		$criteria->condition = 'active = 1 AND (name LIKE :match OR surname LIKE :match2)';
		$criteria->params = array(':match' => '%'.$term.'%', ':match2' => '%'.$term.'%');
		$criteria->limit = 10;

		$authors = Authors::model()->findAll($criteria);


		$used = array();
		$a = array();

		foreach ($authors as $au)
		{
			if (!in_array($au->name . ' ' . $au->surname, $used))
			{
				$a[] = array('label' => $au->name . ' ' . $au->surname, 'category' => '');
				$used[] = $au->name . ' ' . $au->surname;
			}
		}


		$criteria = new CDbCriteria();
		$criteria->condition = 'status = 1 AND (firstname LIKE :match OR lastname LIKE :match2)';
		$criteria->params = array(':match' => '%'.$term.'%', ':match2' => '%'.$term.'%');
		$criteria->limit = 10;

		$users = User::model()->findAll($criteria);


		foreach ($users as $au)
		{
			if (!in_array($au->firstname . ' ' . $au->lastname, $used))
			{
				$a[] = array('label' => $au->firstname . ' ' . $au->lastname, 'category' => '');
				$used[] = $au->firstname . ' ' . $au->lastname;
			}
		}


		echo CJSON::encode($a);

	}

}