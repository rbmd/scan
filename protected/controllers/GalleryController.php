<?php
class GalleryController extends Controller
{
	public $layout = '';


	//Показывает основу галереи - первую фотку, количество фоток
	public function actionPlace($id = null, $size = 'small')
	{
		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::GALLERY_PLACE, array(), $id . $size);
		$_render   = Yii::app()->cache->get($CACHE_KEY);

		if ($_render == false)
		{
			$gallery = Galleries::model()->findByPk($id, array('select' => 'id,name'));

			//Общее количество фоток в галерее
			$criteria         = new CDbCriteria();
			$_render['count'] = GalleryPhotos::model()->galleryIdIs($id)->count($criteria);

			if (!is_null($gallery))
			{
				$photos = GalleryPhotos::model()->galleryIdIs($gallery->id)->findAll(array('order' => 'order_num ASC'));
				/*foreach ($photos as $photo)
				{
					if (!isset($first_photo))
					{
						$first_photo = $photo;
					}

					if ($size == 'large')
					{
						$photo_img_path = $photo->img('840x560');
					}
					else
					{
						$photo_img_path = $photo->img('660x440');
					}

					
					//$_render['photos_js'][] = array('img' => $photo_img_path, 'detail_text' => $photo->detail_text, 'copyright' => addslashes($photo->copyright));
				}*/
				$_render['photos'] = $photos;
				$_render['gallery']     = $gallery;
				//$_render['first_photo'] = $first_photo;
				Yii::app()->cache->set($CACHE_KEY, $_render, 60);
				Yii::app()->cache->addDependency($CACHE_KEY, array('Galleries' => array($id)));
			}
		}

		if (isset($_render['photos']))
		{

			$this->renderPartial('place', array('gallery' => $_render['gallery'], 'photos' => $_render['photos'], 'count' => $_render['count'], 'type' => $size));
		}
	}

	//Получаем конкретное фото через аякс
	public function actionGetPhoto($gallery_id, $order_num)
	{
		$photo = $this->getPhoto($gallery_id, $order_num);
		if (!is_null($photo))
		{
			$p['path'] = $photo->path;
			echo CJSON::encode($p);
		}
		Yii::app()->end();
	}
}