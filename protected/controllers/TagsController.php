<?php

class TagsController extends Controller
{
	public $layout = 'static';

	public function actionDetail($code)
	{
		//Если указаны параметры фильтра регион и тип сортировки
		$region_id = Yii::app()->request->getParam('region', 0);

		//Популярное-последние
		$type = Yii::app()->request->getParam('type', 'last');

		$CACHE_KEY = 'Tags.detail'.$code.$type.$region_id.Yii::app()->request->getParam('page');
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{

			$tag = Tags::model()->find(array('condition' => 'code = \'' . $code . '\''));
			

			if (!empty($tag))
			{
				//Всего статей
				$articles_total = Articles::model()->visible()->tagIs($tag->id)->count();

				$criteria = new CDbCriteria();

				$criteria->order = 'date_active_start DESC';

				$pages           = new CPagination($articles_total);
				$pages->pageSize = 8;
				$pages->applyLimit($criteria);
				

				if(Yii::app()->request->getParam('page') > $pages->pageCount)
					throw new CHttpException(404);
					

				$articles = Articles::model()->visible()->with(array('tree'))->tagIs($tag->id)->Indexed()->findAll($criteria);

				$_render['tag'] = $tag;
				$_render['articles'] = $articles;
				$_render['pages'] = $pages;
			}

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}




		$this->setSeo($_render['tag']);
				
		$this->render('detail', array('tag' => $_render['tag'], 'articles' => $_render['articles'], 'pages' => $_render['pages']) );
	}

}