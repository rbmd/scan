<?
class ChtpzController extends Controller
{
	public $layout = 'inner';


	
	public function actionArticle($id)
	{
		$article = ChtpzArticles::model()->active()->findByPk($id);

		if(is_null($article))
			$this->redirect('/');

		$criteria = new CDbCriteria();
		$criteria->condition = 'theme_id=:theme_id';
		$criteria->params = array(':theme_id' => $article->theme_id);
		$criteria->order = 'date_active_start DESC';

		$other_articles = ChtpzArticles::model()->findAll($criteria);

		$theme = ChtpzThemes::model()->findByPk($article->theme_id);

		$partner = ChtpzPartners::model()->findByPk(1);

		$_render['article'] = $article;
		$_render['other_articles'] = $other_articles;
		$_render['theme'] = $theme;
		$_render['partner'] = $partner;


		if($article->gallery_id > 0)
		{
			$gallery = Galleries::model()->findByPk($article->gallery_id);
			$photos = GalleryPhotos::model()->galleryIdIs($article->gallery_id)->order('order_num ASC')->findAll();
			$_render['gallery'] = $gallery;
			$_render['photos'] = $photos;
		}

		$this->setSeo($_render['article']);

		$this->render('article', $_render );
	}

	public function actionNewsIndex()
	{

		$criteria = new CDbCriteria();
		$criteria->order = 'date_active_start DESC';

		$count = ChtpzNews::model()->count($criteria);
		
		$pages = new CPagination($count);
		$pages->pageSize = 10;
		$pages->applyLimit($criteria);

		$news = ChtpzNews::model()->findAll($criteria);
		
		$_render['pages'] = $pages;
		$_render['news'] = $news;

		$this->render('news', $_render);
	}

	public function actionNewsDetail($id)
	{

		$criteria = new CDbCriteria();
		$criteria->order = 'date_active_start DESC';

		$n = ChtpzNews::model()->findByPk($id);
		
		

		$_render['n'] = $n;

		$this->setSeo($_render['n']);

		$this->render('newsDetail', $_render);
	}


}