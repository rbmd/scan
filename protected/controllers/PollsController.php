<?php
/**
 * Контроллер голосования
 *
 * Created by JetBrains PhpStorm.
 * User: Русаков Дмитрий
 * Date: 15/05/2012
 * Time: 13:40
 */
class PollsController extends Controller
{
    /**
     * Пользователь голосует в каком-то конкретном голосовании
     * @param null $poll_id - id голосования
     */
    public function actionVote($poll_id = null)
    {
        //Проверяем, что это аякс запрос и что передан id опроса
        if( Yii::app()->request->isAjaxRequest && !empty($poll_id) )
        {
            //Проверяем, что такой опрос существует
            if( $poll = Polls::model()->visible()->FindByPk($poll_id) )
            {
                $r = false; //Результат сохранения ответов(удачно/неудачно)

                //Проверяем, может ли голосовать текущий пользователь
                if( $poll->Can_I_Vote() )
                {
					//Получаем ответы пользователя на опрос
					$answers = Yii::app()->request->getParam('answers', array());	
					
					$r = Polls::model()->doVote($poll, $answers);
                }

                //Если занесение в БД всех ответов прошло успешно,
                if( $r )
                {
                    //то ставим конктрольную куку
                    $key = Polls::getCookieKey($poll_id);
                    setcookie($key, 1, time()+3600*24*60, '/');

                  /*  $CACHE_KEY = Yii::app()->cache->buildKey(
                        keysSet::CACHE_KEY_IP_RESTRICTED,
                        array(),
                        $poll_id . '/' . Yii::app()->request->userHostAddress
                    );

                    Yii::app()->cache->delete($CACHE_KEY);*/

                    //Если голосует авторизованный пользователь,
                    //то отмечаем ему в сессии, что он уже проголосовал
                    if( !Yii::app()->user->isGuest )
                    {
                        $session = Yii::app()->getSession();

                        if( !isset($session['voted_polls']) )
                        {
                            $session['voted_polls'] = array($this->id => $r);
                        }
                        else{
                            $_t = $session['voted_polls'];
                            $_t[$this->id] = $r;
                            $session['voted_polls'] = $_t;
                        }
                    }

                    //Если голосовалка находилась в правой колонке
                    /*if(Yii::app()->request->getParam('pollFromRightCol', false) )
                        $tpl = 'poll_rightcol_results';
                    //Для точки зрения другой шаблон
                    elseif($poll->poll_type == 1)
                        $tpl = 'viewpoint_results';
                    else*/
                        $tpl = 'poll_chtpz_results';

                    $tpl = 'application.widgets.polls.views.results.'.$tpl;


                    //и рендорим сводные результаты опроса для отправки пользователю
                    $poll_results = $this->renderPartial($tpl, array('poll'=>$poll, 'light' => (isset($_GET['light']) && $_GET['light'] == true)), true);
					
					//Отправляем ответ клиенту
					echo CJSON::encode(
						array(
							'result' => $r, //Успешно проголосовал пользователь или нет
							'poll_results' => $poll_results, //В случае успешного голосования здесь будет отрендореный шаблон с общими результатами голосования
							'poll_is_closed' => 0, 
						)
					);					
					
                }

                //Завершаем приложение
                Yii::app()->end();
            }
            else
            {
                //Отправляем ответ клиенту, что опрос закрыт для голосования
                echo CJSON::encode(
                    array(
                        'result' => false, 
                        'poll_is_closed' => 1,  
                    )
                );

                //Завершаем приложение
                Yii::app()->end();
            }
        }

        //Если не ajax-запрос, то редиректим на главную
       # $this->redirect('/');
    }
	
	//Возвращает голосование для вставтки в статью
	public function actionPlace($current_poll_id = null, $light = false)
	{
        $current_poll = $poll = Polls::model()->findByPk($current_poll_id);

        $criteria        = new CDbCriteria;
        $criteria->condition = 'date_active_start < :date_active_start';
        $criteria->params = array(':date_active_start' => $current_poll->date_active_start);
        $criteria->order = "date_active_start DESC";
        $criteria->limit = 1;

        $poll = Polls::model()->visible()->find($criteria);

        if(is_null($poll))
        {
            $criteria->condition = '';
            $criteria->params = array();
            $poll = Polls::model()->visible()->find($criteria);
        }
        


		$this->renderPartial('place', array('poll_id' => $poll->id, 'light' => $light) );
	}
}