<?php
//exit;
return array(
        //'http://2014.faceslaces.com/' => 'fnl2014/index',

        // 'http://www1.fnl.codemedia.ru/' => 'fnl2014/index',
        // 'http://www2.fnl.codemedia.ru/' => 'fnl2014/index',
        '' => 'tree/index',
        //Админка
        'admin/' => 'admin/default',
        'admin/rights' => 'rights/assignment/view',
        'admin/profiler/allrequests'=>'admin/profiler/allrequests',
        'admin/profiler/allwidgets'=>'admin/profiler/allwidgets',
        'admin/rights/<controller:\w+>/<action:\w+>' => 'rights/<controller>/<action>',
        '<code:([A-z0-9-_]+)>' => 'tree/index',

        
        'articles/<id:([0-9]+)>' => 'tree/article',
        '<code:(interview|news|main|infographics|video|polls)>' => 'tree/rubric',
        'contest' => 'tree/contest',


        //Авторизация, профайл, восстановление пароля
        'reactivate' => 'site/reactivate',
        'activate/<code:[0-9cdefABCDEF]{32}>' => 'site/activate',
        'registration' => 'site/registration',
        'registration/confirm' => 'site/confirmRegistration',
        'recovery' => 'site/recovery',
        'recovery/code/<code:[0-9abcdefABCDEF]{32}>' => 'site/recovery',
        'recovery/confirmation' => 'site/recovery/success/1',
        'recovery/success' => 'site/recovery/success/2',
        'login' => 'site/login',
        'logout' => 'site/logout',
        'site/authexternals/<service:([A-z0-9_-]+)>' => 'site/authexternals',
        'site/authexternals/<service:([A-z0-9_-]+)>/<identity:([A-z0-9_\.:\/-]+)>' => 'site/authexternals',

        //Теги
        'tags/<code:[0-9A-z_-]+>' => 'tags/detail',

        //Серии
        #'series/<id:[0-9]+>' => 'series/detail',

        // Темы недели
        #'themeweek/<code:[A-z0-9_-]+>' => 'themeweek/detail',

        //Голосование
        'polls/vote/<poll_id:[0-9]+>' => 'polls/vote/<poll_id:[0-9]+>',



        //Поиск
        'search' => 'search/index',

        // Карта сайта
        'map/' => 'map/index',
        'map/<id:.+>' => 'map/parts',


        //Предпросмотр статей
        '<preview:(preview)>/<id:[0-9]+>' => 'articles/detail',

        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

        //Все остальные страницы кидаем на статику
        '(?!admin|blogs|atlas|site|profile|specprojects|specials|comments|test|api|ajax|elements)([\w-_/]+)' => 'site/static',


        //Стандартные правила роутинга, которые должны отрабатывать по умолчанию
        /*'<controller:\w+>' => '<controller>/index',
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',*/
);