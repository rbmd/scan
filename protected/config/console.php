<?php
$_SERVER['SERVER_NAME'] = 'facesandlaces.com';
// require_once('/home/kp.codemedia.ru/secure/db.php');



#require realpath(dirname(__FILE__).'/../../../secure/db.php');
require realpath(dirname(__FILE__).'/../../../../shared/secure/db.php');



require_once(__DIR__.'/../components/YiiElasticSearch-master/vendor/autoload.php');

ini_set("memory_limit","1024M");

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'       => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'sourceLanguage' => 'en_US',
	'language'       => 'ru',
	'charset'        => 'utf-8',
	'homeUrl'           => '/',
	'name'           => 'Rbmd Console Application',
	'timeZone'       => "Europe/Moscow",
	'preload'        => array(
		'log',
	),

	'commandMap' => array(
		    'elastic' => array(
		        'class' => 'YiiElasticSearch\ConsoleCommand',
		    ),
		),

	// application components
	'components'     => array(
		'db'    => array(
			'class'            => 'CDbConnection',
			'connectionString' => 'mysql:host=' . CONFIG_YII_DB_HOST . ';dbname=' . CONFIG_YII_DB_NAME,
			'emulatePrepare'   => false,
			'username'         => CONFIG_YII_DB_USER,
			'password'         => CONFIG_YII_DB_PASSWORD,
			'charset'          => 'utf8',
			'autoConnect'      => false,
		),

		'elasticSearch' => array(
	        'class' => 'YiiElasticSearch\Connection',
	        'baseUrl' => 'http://localhost:9200/',
	    ),

		'imagemod' => array(
			'class' => 'application.extensions.imagemodifier.CImageModifier',
		),


		'log'   => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'       => 'RainLogger',
					'levels'      => 'error, warning',
					'maxFileSize' => 1024, #kbytes
					'maxLogFiles' => 10,
				)
			),
		),

		'CURL'  => array(
			'class'   => 'ext.curl.Curl',
			'options' => array(
				'setOptions' => array(
					CURLOPT_SSL_VERIFYPEER => false,
				),
			),
		),

		'mail'  => array(
			'class' => 'application.components.Mail',
			'from'  => array('noreply@faceslaces.com' => 'Faces&Laces')
		),

		'cache' => array(
			'class'   => 'application.extensions.imemcache.iMemCache',
			'servers' => array(
				array('host' => 'localhost', 'port' => 11211, 'weight' => 100),
			)
		)
	),
	'import'         => array(
		'application.modules.admin.models.*',
		'application.models.*',
		'application.components.*',
		'application.commands.*',
		'application.helpers.*',
		'application.modules.user.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.modules.rights.*',
		'application.modules.rights.models.*',
		'application.modules.rights.components.*',
		'application.modules.rights.components.behaviors.*',
		'ext.nestedsetbehavior.NestedSetBehavior',
		'ext.imemcache.*',
		'ext.*',
		'ext.eoauth.*',
		'ext.eoauth.lib.*',
		'ext.lightopenid.*',
		'ext.eauth.*',
		'ext.eauth.services.*',
	),

	'params'         => array(
		'use_pre_auth'  => false,
		// this is used in contact page
		'keepLoginTime' => 3600,
		'adminEmail'    => 'registiy@gmail.com',
		'baseUrl'       => 'http://faceslaces.com',
	)
);
