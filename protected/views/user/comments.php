<?php

/*$this->breadcrumbs=array(
	'Профайл',
);*/
?>

<h1>Комментарии пользователя</h1>

<? $this->widget('application.widgets.profile.summary.UserSummaryWidget',
				array(
					'user_id' => $user->id,
				)
			); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.userMenu.UserMenuWidget',
				array(
					'user_id' => $user->id,
				)
			);
	?>


	<?
	if (count($comments) > 0)
	{
		?>
		<div class="tabwrap">
			<div id="tab-comment" class="box _optional">
				<div class="commentBlock js_loadmore">

					<?
					foreach ($comments as $c)
					{
						if (!empty($c->Article))
						{
							$commented_entity = $c->Article;

							$authors = array_map(
											function ($a) {
												return '<a href="' . $a->url() . '" class="_blue">' . $a->name . ' ' . $a->surname . '</a>';
											}, 
											$commented_entity->authors
										);
							if (!empty($authors))
								$authors = implode(',', $authors);

							$pub_date = Yii::app()->dateFormatter->format('dd MMMM yyyy, HH:mm', $commented_entity->date_publish);
						}
						else if (!empty($c->Post))
						{
							$commented_entity = $c->Post;

							$authors = '<a href="' . $commented_entity->blog->url() . '" class="_blue">' . $commented_entity->blog->user->firstname . ' ' . $commented_entity->blog->user->lastname . '</a>';

							$pub_date = Yii::app()->dateFormatter->format('dd MMMM yyyy, HH:mm', $commented_entity->date_create);
						}

						

						$r = Ratings::model()->getEntityVoices('Comments', $c->id);
						?>
						<div class="comment">
							<div><a href="<?= $commented_entity->url() ?>" class="_blue"><?= $commented_entity->name ?></a></div>
							<div class="publ">
								<span class="_grey">Опубликовано <?= $pub_date ?> <?= !empty($authors) ? 'автором: ' . $authors : '' ?></span> 
							</div>
							<span class="date">Опубликован<br/><?= Yii::app()->dateFormatter->format('dd MMMM yyyy, HH:mm', $c->date_publish) ?></span>
							<p class="commtext"><?= $c->message ?></p><!-- /.commtext -->
							<div class="action">
								<span class="_grey">Вас поддержали: <a href="#" class="_blue"><?= $r->total ?> человек</a><div class="hint"><span class="_grey">За:</span><a href="#" class="_blue"><?= $r->pros ?></a> <span class="_grey">Против:</span><a href="#" class="_blue"><?= $r->cons ?></a></div></span>
							</div>
						</div><!-- /.comment -->
						<?
					}

					$this->widget('application.widgets.pager.PagerWidget', array('pages' => $pages, ));
					?>
				</div>

			</div>			
		</div>
		<?
	}
	else
	{
		?>
		<p>У пользователя нет ни одного комментария</p>
		<?
	}
	?>
</div>









