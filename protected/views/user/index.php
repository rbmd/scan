<?php

/*$this->breadcrumbs=array(
	'Профайл',
);*/
?>

<h1>Профиль пользователя</h1>

<? $this->widget('application.widgets.profile.summary.UserSummaryWidget',
				array(
					'user_id' => $user->id,
				)
			); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.userMenu.UserMenuWidget',
				array(
					'user_id' => $user->id,
				)
			);
	?>


	<div class="tabwrap">
		<div id="tab-blog" class="box">
			<?
			if (!empty($user->blog))
			{
				if (count($posts) > 0)
				{
					?>
					<div class="profile_page">
						<ul class="newslist-main">
							<?
							foreach ($posts as $i => $p)
							{
								?>
								<li>
									<?
									if (!empty($p->cover))
									{
										?>
										<div class="photo">
											<img src="<?= ThumbsMaster::getThumb($p->cover, $i == 0 ? ThumbsMaster::$settings['300_300'] : ThumbsMaster::$settings['166_166']) ?>" alt="">
										</div>
										<?
									}
									?>
									<div class="newstext">
										<span class="date"><?=  Yii::app()->dateFormatter->format('dd MMMM yyyy, HH:mm', $p->date_create) ?></span>
										<h3><a href="<?= $p->url() ?>"><?= $p->name ?></a></h3>
										<p><?= strip_tags($p->preview_text) ?></p>
										<div class="sub">
											<span class="views"><?= $stat[$p->id]->views ?></span>
											<span class="comments"><?= $p->comments_cnt() ?></span>
											<span class="_grey">Рейтинг: <?= $p->rating ?></span>
										</div>
									</div>
								</li>
								<?
							}
							?>
						</ul>
					</div>
					<?
				

					$this->widget('application.widgets.pager.PagerWidget', array('pages' => $pages, ));
				}
				else
				{
					?>
					<p>У пользователя нет ни одного поста</p>
					<?
				}
			}
			else
			{
				?>
				<p>У пользователя нет ни одного поста</p>
				<?
			}
			?>
		</div>
	</div>
</div>




