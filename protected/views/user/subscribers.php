<h1>Подписчики пользователя</h1>

<? $this->widget('application.widgets.profile.summary.UserSummaryWidget',
				array(
					'user_id' => $user->id,
				)
			); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.userMenu.UserMenuWidget',
				array(
					'user_id' => $user->id,
				)
			);
	?>

	<?
	if (count($subscribers) > 0)
	{
		?>
		<div class="tabwrap">
			<div id="tab-subs" class="box _optional">
				<table class="blogTb">
					<tbody>
						<?
						foreach ($subscribers as $i => $s)
						{
							if (~ $i & 1)
							{
								?><tr><?
							}
							?>
								<td>
									<img src="<?= ThumbsMaster::getThumb($s->Watcher->avatar, ThumbsMaster::$settings['avatar_81'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="<?= !empty($s->Watcher->blog) ? $s->Watcher->blog->code : '' ?>">
								</td>
								<td>
									<?
									if (!empty($s->Watcher->blog))
									{
										?>
										<span class="login"><a href="/user/<?= $s->Watcher->id ?>/"><?= $s->Watcher->blog->code ?></a></span> <span>(<?= $s->Watcher->lastname ?> <?= $s->Watcher->firstname ?>)</span>
										<span class="_grey"><?= $s->Watcher->profession ?></span>
										<?
									}
									else
									{
										?>
										<span class="login"><span><?= $s->Watcher->lastname ?> <?= $s->Watcher->firstname ?></span>
										<span class="_grey"><?= $s->Watcher->profession ?></span>
										<?
									}
									?>
								</td>
							<?
							if ($i & 1)
							{
								?></tr><?
							}
						}

						if (~ $i & 1)
						{
							?></tr><?
						}
						?>
				</table>
			</div>
		</div>
		<?
	}
	else
	{
		?>
		<p>У пользователя нет ни одного подписчика</p>
		<?
	}
	?>
</div>
