<?
$uid = uniqid();

if($type == 'triple')
{
	?>
	<div class="gallery-triple">
		<div class="gallery-triple-wrap">
			<?
			$active = 'active';
			foreach($photos as $photo)
			{
				?>
				<a href="<?=$photo->img('large')?>" class="gallery-triple-slide">
				<div class="gallery-triple-slide-wrap">
					<span class="gallery-triple-slide-zoom" data-icon-name="lens"></span>
					<img src="<?=ThumbsMaster::getThumb($photo->img('large'), ThumbsMaster::$settings['200_auto'], true)?>">
				</div>
				</a>
				<?
				$active = '';
			}
			?>
		</div>
	</div>	
	<?
}
?>

<?
if($type == 'fotorama_big')
{
	?>
	<div class="gallery-slider">
		<div class="fotorama fotorama-<?=$uid?>">
			<?
			$active = 'active';
			foreach($photos as $photo)
			{
				?>
				<img src="<?=$photo->img('large')?>" alt="<?=addslashes($photo->copyright)?>">
				<?
				$active = '';
			}
			?>
		</div>
	</div>
	<?
}
?>

<?
if($type == 'fotorama_small')
{
	?>
	<div class="gallery-slider gallery-slider--inarticle">
		<div class="fotorama-<?=$uid?>">
		  <?
			$active = 'active';
			foreach($photos as $photo)
			{
				?>
				<img src="<?=ThumbsMaster::getThumb($photo->img('large'), ThumbsMaster::$settings['500_auto'], true)?>" alt="<?=addslashes($photo->copyright)?>">
				<?
				$active = '';
			}
			?>
		</div>
	</div>
	<?
}
?>

<script>
	$('.fotorama-<?=$uid?>').each(function(){

		var f = $(this);
		var fotorama = f.fotorama().data('fotorama');

		fotorama.setOptions({
			nav: false,
			margin: 0,
			loop: true
		});

		f.find('.fotorama__nav-wrap').append('<div class="fotorama__nav fotorama__nav--numeric"><div class="fotorama__nav__frame">' + (fotorama.activeIndex + 1) + ' из ' + (fotorama.size) + '</div></div>')

		f.on(
				'fotorama:showend',
				function (e, fotorama, extra) {
					f.find('.fotorama__nav__frame').html( (fotorama.activeIndex + 1) + ' из ' + (fotorama.size) );
					f.find('.fotorama__stage__shaft').removeClass('fotorama__stage__shaft--transition');
				}
			);

		f.on(
				'fotorama:show',
				function (e, fotorama, extra) {
					f.find('.fotorama__stage__shaft').removeClass('fotorama__stage__shaft--transition');
				}
			);

		f.find('.fotorama__stage__shaft, .fotorama__arr--next').hover(
			function(){
				var shaft = f.find('.fotorama__stage__shaft');
				shaft.css({
						'-webkit-transform': 'translate3d(-10%, 0px, 0px)',
						'-ms-transform': 'translate3d(-10%, 0px, 0px)'
				});
				shaft.addClass('fotorama__stage__shaft--transition');
			},
			function(){
				var shaft = f.find('.fotorama__stage__shaft');
				shaft.css({
						'-webkit-transform': 'translate3d(0px, 0px, 0px)',
						'-ms-transform': 'translate3d(0px, 0px, 0px)'
				});
			}
		);

		f.find('.fotorama__arr--prev').hover(
			function(){
				var shaft = f.find('.fotorama__stage__shaft');
				shaft.css({
						'-webkit-transform': 'translate3d(10%, 0px, 0px)',
						'-ms-transform': 'translate3d(10%, 0px, 0px)'
				});
				shaft.addClass('fotorama__stage__shaft--transition');
			},
			function(){
				var shaft = f.find('.fotorama__stage__shaft');
				shaft.css({
						'-webkit-transform': 'translate3d(0px, 0px, 0px)',
						'-ms-transform': 'translate3d(0px, 0px, 0px)'
				});
			}
		);
	});
</script>