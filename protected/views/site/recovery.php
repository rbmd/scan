
<h1>Восстановление пароля</h1>
<?
if ($success == 1)
{
	?>
	<p>Вам на почту отпрпавлено письмо с инструкцией для восстановления пароля</p>
	<?
}
elseif ($success == 2)
{
	?>
	<p>
		Ваш пароль изменён.<br />
		<a href="/login/" class="_blue">Войти на сайт</a>.
	</p>
	<?
}
else
{
	?>
	<div class="sign-up-form">

	<?
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'recovery-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'action' => '/recovery/',
	));
	?>

	<div class="form-wrapper">

		<div class="row">
			<label><?= $model->attributeLabels()['email'] ?> <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'username', array('class' => 'field', 'style' => 'width: 300px')) ?>
				<?= $form->error($model, 'username', array('class' => 'error', 'style' => 'width: 300px')) ?>
			</span>
		</div>

		<div class="row">
			<label><?= $model->attributeLabels()['verifyCode'] ?> <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'verifyCode', array('class' => 'field', 'style' => 'width: 300px')) ?>
				<?= $form->error($model, 'verifyCode', array('class' => 'error', 'style' => 'width: 300px')) ?>
			</span>
		</div>


		<div class="row">
			<label for=""></label>
			<div class="field-wrapper">
				<? $this->widget('CCaptcha', array('buttonLabel' => '<span style="font-size: 11px; top: -22px; position: relative;">обновить код</span>', 'buttonOptions' => array('class' => '_blue'))); ?>
			</div>
		</div>

		<div class="row">
			<label for=""></label>
			<div class="field-wrapper">
				<?= CHtml::submitButton('Восстановить пароль', array('class' => 'button _blue-button')) ?>
			</div>
		</div>
	</div>
	<?
	$this->endWidget();
	?>
	</div>
	<?
}
?>