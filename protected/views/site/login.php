<?
if (!Yii::app()->request->isAjaxRequest)
{
	?><h1>Вход</h1><?
}
?>


	<div class="sign-up-form">

		<div class="soc-enter">
			<div class="soc-enter_label">Войти через соцсети:</div>
			<div class="soc-links">
				<a href="/site/authexternals/twitter/" class="tw"></a>
				<a href="/site/authexternals/facebook/" class="fb"></a>
				<a href="/site/authexternals/vkontakte/" class="vk"></a>
			</div>
		</div>

		<?
		CHtml::$errorCss = '';
		$form = $this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableAjaxValidation'=>true,
			'enableClientValidation'=>true,
			'action' => '/login/',
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => true,
				'errorCssClass' => 'error',
				'inputContainer' => 'div.row',
				'afterValidate' => 'js:function(form, data, hasError) { 
					if (hasError) {
						for(var i in data) $("#"+i).closest(".row").addClass("error");
						return false;
					}
					else {
						return true;
					}
				}',
				'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
					if (hasError) $("#"+attribute.id).closest(".row").addClass("error");
				}'
			)
		));
		?>

		<div class="form-wrapper">

			<div class="row">
				<label for="reg_nick">Email или Nick</label>
				<span class="field-wrapper">
					<?= $form->textField($model, 'email', array('class' => 'field')) ?>
				</span>
				<?= $form->error($model, 'email', array('class' => 'error')) ?>
			</div>
			<div class="row">
				<label for="reg_pass">Пароль</label>
				<span class="field-wrapper">
					<?= $form->passwordField($model, 'password', array('class' => 'field')) ?>
				</span>
				<?= $form->error($model, 'password', array('class' => 'error')) ?>
			</div>

			<div class="row">
				<a href="/recovery/" class="_blue">Забыли пароль?</a>
			</div>

			<div class="row">
				<label for=""></label>
				<div class="field-wrapper">
					<?= CHtml::submitButton('Войти', array('class' => 'button _blue-button')) ?>
				</div>
			</div>

		</div>


		<? 
		$this->endWidget();
		?>


	</div>




