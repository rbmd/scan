<div class="col c12 first">
	<div class="alpha">
		<? $this->widget('application.widgets.breadcrumbs.Breadcrumbs'); ?>
		<h1>Подсмотри за дождем</h1>
		<div class="plan">
			<div class="cameras">
				<a href="/player/office/iframe.php?cam=3" onclick="switchCam($(this)); return false;" title="Центр офиса" class="cam" style="left: 300px; top: 95px;">Центр офиса</a>
				<a href="/player/office/iframe.php?cam=5" onclick="switchCam($(this)); return false;" title="Студия №2" class="cam" style="left: 420px; top: 90px;">Студия №2</a>
				<a href="/player/office/iframe.php?cam=2" onclick="switchCam($(this)); return false;" title="Студия" class="cam" style="left: 500px; top: 72px;">Студия</a>
				<a href="/player/office/iframe.php?cam=4" onclick="switchCam($(this)); return false;" title="Крыша" class="cam small" style="left: 636px; top: 120px;">Крыша</a>
				<a href="/player/office/iframe.php?cam=6" onclick="switchCam($(this)); return false;" title="Арт-студия" class="cam" style="left: 20px; top: 25px;">Арт-студия</a>
			</div>
		</div>
	</div>
</div>

<script>
function switchCam($click)
{
	$('#player').attr('src', $click.attr('href'));
	$('.cameras a').removeClass('active');
	$click.addClass('active');
};
</script>