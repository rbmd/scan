<?
if (!Yii::app()->request->isAjaxRequest)
{
	?><h1>Регистрация</h1><?
}
?>

<div class="sign-up-form">

	<?
	if (Yii::app()->request->isAjaxRequest)
	{
		?><header>Регистрация</header><?
	}
	?>

	<div class="soc-enter">
		<div class="soc-enter_label">Зарегестрируйтесь через соцсети:</div>
		<div class="soc-links">
			<a href="/site/authexternals/twitter/" class="tw"></a>
			<a href="/site/authexternals/facebook/" class="fb"></a>
			<a href="/site/authexternals/vkontakte/" class="vk"></a>
		</div>
	</div>

	<?
	CHtml::$errorCss = '';
	$form=$this->beginWidget('CActiveForm', array(
		'id' => 'register-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'action' => '/registration/',
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'errorCssClass' => 'error',
			'inputContainer' => 'div.row',
			'afterValidate' => 'js:function(form, data, hasError) { 
				if (hasError) {
					for(var i in data) $("#"+i).closest(".row").addClass("error");
					return false;
				}
				else {
					return true;
				}
			}',
			'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
				if (hasError) $("#"+attribute.id).closest(".row").addClass("error");
			}'
		)
	)); 
	?>

	<div class="form-wrapper">

		<div class="row">
			<label for="reg_surname">Фамилия <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'lastname', array('class' => 'field', 'autofocus' => 'autofocus')) ?>
			</span>
			<?= $form->error($model, 'lastname', array('class' => 'error')) ?>
		</div>
		<div class="row">
			<label for="reg_name">Имя <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'firstname', array('class' => 'field')) ?>
			</span>
			<?= $form->error($model, 'firstname', array('class' => 'error')) ?>
		</div>
		<div class="row">
			<label for="reg_mail">E-mail <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'email', array('class' => 'field')) ?>
			</span>
			<?= $form->error($model, 'email', array('class' => 'error')) ?>
		</div>
		<div class="row">
			<label for="reg_pass">Пароль <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->passwordField($model, 'password', array('class' => 'field')) ?>
				<a href="#" class="switchpass _blue">Показать пароль</a>
			</span>
			<?= $form->error($model, 'password', array('class' => 'error')) ?>
		</div>
		<div class="row">
			<label for="reg_pass">Пароль еще раз <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->passwordField($model, 'password2', array('class' => 'field')) ?>
				<a href="#" class="switchpass _blue">Показать пароль</a>
			</span>
			<?= $form->error($model, 'password2', array('class' => 'error')) ?>
		</div>
		<div class="row">
			<label for="reg_pass">Код с картинки <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'verifyCode', array('class' => 'field')) ?>
			</span>
			<?= $form->error($model, 'verifyCode', array('class' => 'error')) ?>
		</div>
		<div class="row">
			<label></label>
			<span class="field-wrapper">
				<? $this->widget('CCaptcha', array('buttonLabel' => '<span class="resetcaptcha _blue">Обновить код</span>')); ?>
			</span>
		</div>


		<div class="row">
			<label for=""></label>
			<span class="field-wrapper">
				<?= CHtml::submitButton('Зарегистрироваться', array('class' => 'button _blue-button')) ?>
			</span>
		</div>

	</div>

	



	<div class="col c8 first">
		<div class="col c4 first">
			<div class="wrap">
				<div class="input-nest required">
					
					
				</div>
			</div>
		</div><!--
		--><div class="col c4 first">
			<div class="wrap _align center">
				
			</div>
		</div>
	</div>


	<?php $this->endWidget(); ?>

</div>