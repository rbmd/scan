<div class="article">
	<h1><?=$article->name?></h1>

	<?
	if (!empty($article->pic))
	{
		?>
		<div class="article-photo">
			<img src="<?= $article->pic ?>" class="article-photo__image">
			<div class="article-photo__copy"><?=$article->pic_desc?></div>
		</div>
		<?
	}
	?>

	<h2><?=$article->lead?></h2>

	<?
	if(isset($photos)){
		?>
		<div class="fotorama-nest">
			<div class="fotorama" data-width="100%">
			<?
			foreach($photos as $photo)
			{
				?>
				<img src="<?=$photo->img('large')?>" data-caption="<?=$photo->detail_text?>">
				<?
			}
			?>
			</div>
		</div>
		<?
		
	}
	?>
	
	<?= $article->detail_text ?>

	<?/*
	<div class="article-author">
		Интервью: <a href="">Константин Константинопольский</a>, <a href="">Константин Константинопольский</a>, <a href="">Константин Константинопольский</a>
	</div>*/?>

	<div class="shares">
		<?
		$this->widget('application.widgets.likes.SharesWidget');
		?>
	</div>
</div>



<div class="wrap">
	<?/*
	<div class="interviews">
		<span class="grid-item__label">Другие интервью</span>
		<div class="grid">
			<div class="grid-col">
				<a href="" class="grid-item grid-item--3x">
					<span class="grid-item__wrap">
						<span class="grid-item__photo"></span>
						<span class="grid-item__name">Оптимизация — это доступность медицины или сокращение врачей?</span>
						<span class="grid-item__intro">О том, что сегодня происходит в отечественном здравоохранении, корреспондент «Интерфакс» побеседовал с известным врачом и президентом Национальной медицинской палаты Леонидом Рошалем</span>
					</span>
				</a>
			</div>

			<div class="grid-col">
				<a href="" class="grid-item grid-item--3x">
					<span class="grid-item__wrap">
						<span class="grid-item__photo"></span>
						<span class="grid-item__name">Оптимизация — это доступность медицины или сокращение врачей?</span>
						<span class="grid-item__intro">О том, что сегодня происходит в отечественном здравоохранении, корреспондент «Интерфакс» побеседовал с известным врачом и президентом Национальной медицинской палаты Леонидом Рошалем</span>
					</span>
				</a>
			</div>

			<div class="grid-col">
				<a href="" class="grid-item grid-item--3x">
					<span class="grid-item__wrap">
						<span class="grid-item__photo"></span>
						<span class="grid-item__name">Оптимизация — это доступность медицины или сокращение врачей?</span>
						<span class="grid-item__intro">О том, что сегодня происходит в отечественном здравоохранении, корреспондент «Интерфакс» побеседовал с известным врачом и президентом Национальной медицинской палаты Леонидом Рошалем</span>
					</span>
				</a>
			</div>
		</div>

		<div class="_align_center">
			<a href="" class="button-more">Все интервью <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	*/?>

	<?/*
	<div class="latest">
		<span class="grid-item__label">Последние новости</span>
		<div class="grid">
			<div class="grid-col">
				<a href="" class="grid-item">
					<span class="grid-item__date">12 апреля</span>
					<span class="grid-item__name">За шесть месяцев в Москве появятся 13 университетских клиник</span>
				</a>
			</div>

			<div class="grid-col">
				<a href="" class="grid-item">
					<span class="grid-item__date">12 апреля</span>
					<span class="grid-item__name">За шесть месяцев в Москве появятся 13 университетских клиник</span>
				</a>
			</div>

			<div class="grid-col">
				<a href="" class="grid-item">
					<span class="grid-item__date">12 апреля</span>
					<span class="grid-item__name">За шесть месяцев в Москве появятся 13 университетских клиник</span>
				</a>
			</div>
		</div>

		<div class="_align_center">
			<a href="" class="button-more">Все новости <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	*/?>
</div>