<?/*<div class="feature">
	<div class="feature__inner ">
		<div class="hero-block hero-block--feature inner">
			<p>Получите уникальные материалы по&nbsp;итогам <span class="text-bold">Петербургского международного экономического форума </span>от&nbsp;аналитиков &laquo;Интерфакса&raquo;</p>
		</div>
	</div>
</div>
*/?>

<?/*
<div class="b-block inner">
	<p>Для продолжения нажмите кнопку</p>
	<a href="<?#= $href ?>" class="btn btn--large">Скачать отчет</a>
</div>


<div class="b-block inner">
	<p>Ссылка для скачивания отправлена на&nbsp;указанный вами почтовый ящик</p>
</div>
*/?>

<div class="inner">
	<hr class="separator-line">
	<h2 class="h2 text-extrabold">
		Рейтинг упоминаемости участников RIW 2015 
	</h2>
</div>




<div class="toplist">
	<div class="tabs">
		<div class="tabs__nav inner">
			<?
			$day = Yii::app()->request->getParam('day', 0);
			
			if($day < 1 || $day > 3)
			{
				
				foreach($materials as $m)
				{
					if(strlen($m->detail_text) > 2)
						$day = $m->id;
				}
				
			}

			
			foreach($materials as $m)
			{
				if($day == $m->id)
					$active = 'active';
				else
					$active = '';

				?>

				<?
				if(strlen($m->detail_text) < 2)
				{
				?>
				<div class="tabs__nav__item">
					<span class="tabs__nav__item__inner">
						<?=$m->name?>
					</span>
				</div>
				<?
				}			
				else
				{
				?>
				<a href="<?=$m->id?>" class="tabs__nav__item <?=$active?>">
					<span class="tabs__nav__item__inner">
						<?=$m->name?>
					</span>
				</a>
				<?
				}

			}
			?>


		</div>
		<div class="tabs__content inner">

			<?
			foreach($materials as $m)
			{	
				if($day == $m->id)
					$active = 'active';
				else
					$active = '';			
				?>
				<div class="tabs__content__tab <?=$active?>">					
					<p class="text-large">						
						<?=$m->lead?>
					</p>
					<img src="<?=$m->pic?>">
					<div><?=$m->pic_desc?></div>
					<?=$m->detail_text?>
				</div>
				<?
			}
			?>



		</div>
	</div>
</div>



