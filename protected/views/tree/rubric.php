<div class="wrap">
	<div class="grid">
		<div class="grid-flow">
		<?
		foreach($materials as $m)
		{
			if ($tree->code == 'news' || $tree->code == 'infographics')
			{
				?>
				<a href="<?=$m->url()?>" class="grid-item grid-item--flow">
					<span class="grid-item__date"><?=Yii::app()->dateFormatter->format('d MMMM', $m->date_active_start)?></span>
					<span class="grid-item__name"><?=$m->name?></span>
				</a>
				<?
			}

			if ($tree->code == 'interview' || $tree->code == 'video')
			{
				?>
				<a href="<?=$m->url()?>" class="grid-item grid-item--flow grid-item--3x">
					<span class="grid-item__wrap">
						<span class="grid-item__photo" data-image="<?=$m->pic_small?>"></span>
						<span class="grid-item__name"><?=$m->name?></span>
						<span class="grid-item__intro"><?= strip_tags($m->predetail_text) ?></span>
					</span>
				</a>
				<?
			}
		}
		?>
		</div>
	</div>
</div>