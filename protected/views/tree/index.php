<?/*<div class="feature">
	<div class="feature__inner ">
		<div class="hero-block hero-block--feature inner">
			<p>Получите уникальные материалы по&nbsp;итогам <span class="text-bold">Петербургского международного экономического форума </span>от&nbsp;аналитиков &laquo;Интерфакса&raquo;</p>
		</div>
	</div>
</div>
*/?>

<?/*
<div class="b-block inner">
	<p>Для продолжения нажмите кнопку</p>
	<a href="<?#= $href ?>" class="btn btn--large">Скачать отчет</a>
</div>


<div class="b-block inner">
	<p>Ссылка для скачивания отправлена на&nbsp;указанный вами почтовый ящик</p>
</div>
*/?>

<div class="inner">
	<hr class="separator-line">
	<h2 class="h2 text-extrabold">
		Топ-20 самых цитируемых участников Петербургского международного экономического форума
	</h2>
</div>




<div class="toplist">
	<div class="tabs">
		<div class="tabs__nav inner">
			<?
			$day = Yii::app()->request->getParam('day', 0);
			
			if($day < 1 || $day > 3)
			{
				
				foreach($materials as $m)
				{
					if(strlen($m->detail_text) > 2)
						$day = $m->id;
				}
				
			}

			
			foreach($materials as $m)
			{
				if($day == $m->id)
					$active = 'active';
				else
					$active = '';

				?>

				<?
				if(strlen($m->detail_text) < 2)
				{
				?>
				<div class="tabs__nav__item">
					<span class="tabs__nav__item__inner">
						<?=$m->name?>
					</span>
				</div>
				<?
				}			
				else
				{
				?>
				<a href="<?=$m->id?>" class="tabs__nav__item <?=$active?>">
					<span class="tabs__nav__item__inner">
						<?=$m->name?>
					</span>
				</a>
				<?
				}

			}
			?>


		</div>
		<div class="tabs__content inner">

			<?
			foreach($materials as $m)
			{	
				if($day == $m->id)
					$active = 'active';
				else
					$active = '';			
				?>
				<div class="tabs__content__tab <?=$active?>">					
					<p class="text-large">						
						<?=$m->lead?>
					</p>
					<img src="<?=$m->pic?>">
					<div><?=$m->pic_desc?></div>
					<?=$m->detail_text?>
				</div>
				<?
			}
			?>



		</div>
	</div>
</div>


<div class="hero-block inner">
	<p>Получите уникальные материалы по&nbsp;итогам <span class="text-bold">Петербургского международного экономического форума </span>от&nbsp;аналитиков &laquo;Интерфакса&raquo;</p>
</div>


<div class="b-block inner">
	<form class="request-form js-form" name="request_form" id="request_form" action="/tree/addmaterialrequest/" method="POST">
		<p class="request-form__title text-center">
			Заполните форму, чтобы получать материалы в числе первых
		</p>
		
		<div class="row">
			
			<div>
				<input type="text" 
					class="request-form__input js-input js-input-name"				    							  
					name="username" 		
					id="username"			
					placeholder="Имя, фамилия">
				<div class="request-form__notify"></div>
			</div>

			<div>
				<input type="text" 
					class="request-form__input js-input js-input-company" 
					name="usercompany" 	
					id="usercmopany" 	
					placeholder="Компания">
				<div class="request-form__notify"></div>
			</div>
		
			<div class="ibox ibox--half">
				<input type="text" 
					class="request-form__input js-input js-input-phone" 
					name="userphone" 	
					id="userphone" 	
					placeholder="Номер телефона">
				<div class="request-form__notify"></div>
			</div>
		
			<div class="ibox ibox--half">
				<input type="text" 
					class="request-form__input js-input js-input-email" 
					name="usermail" 		
					id="usermail" 		
					placeholder="E-mail">
				<div class="request-form__notify"></div>
			</div>
		</div>
		<button class="request-form__input btn btn--large request-form__input--send request-form__send js-sendform">Отправить</button>
	</form>
</div>