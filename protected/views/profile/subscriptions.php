<?php

/*$this->breadcrumbs=array(
	'Профайл',
);*/
?>

<h1>Личный кабинет</h1>

<? $this->widget('application.widgets.profile.summary.ProfileSummaryWidget'); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.profileMenu.ProfileMenuWidget',
				array(
					'user_id' => Yii::app()->user->id,
				)
			);
	?>
	
	<div class="tabwrap">
		<div id="tab-subts" class="box _optional">
			<table class="blogTb">
				<tbody>
					<?
					foreach ($subscriptions as $i => $s)
					{
						if (~ $i & 1)
						{
							?><tr><?
						}
						?>
							<td>
								<img src="<?= ThumbsMaster::getThumb($s->Author->avatar, ThumbsMaster::$settings['avatar_81'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="<?= $s->Author->blog->code ?>">
							</td>
							<td>
								<?
								if (!empty($s->Author->blog))
								{
									?>
									<span class="login"><a href="/user/<?= $s->Author->id ?>/"><?= $s->Author->blog->code ?></a></span> <span>(<?= $s->Author->lastname ?> <?= $s->Author->firstname ?>)</span>
									<span class="_grey"><?= $s->Author->profession ?></span>
									<?
								}
								else
								{
									?>
									<span class="login"><span><?= $s->Author->lastname ?> <?= $s->Author->firstname ?></span>
									<span class="_grey"><?= $s->Author->profession ?></span>
									<?
								}
								?>
								
								<div>
									<a href="/blogs/authors/<?= $s->Author->id ?>/unsubscribe/" class="_red" onclick="subscription.toggle($(this)); return false;">Отписаться</a>

									<a href="/blogs/authors/<?= $s->Author->id ?>/subscribe/" class="_blue hidden" onclick="subscription.toggle($(this)); return false;">Подписаться</a>
								</div>
							</td>
						<?
						if ($i & 1)
						{
							?></tr><?
						}
					}

					if (!empty($subscriptions) && ~ $i & 1)
					{
						?></tr><?
					}
					?>
				</tbody>
			</table>
		</div>

	</div>
</div>
