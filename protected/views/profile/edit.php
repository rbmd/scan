<h1>Редактирование профиля</h1>

<div class="sign-up-form">
	<?
	CHtml::$errorCss = '';
	$form = $this->beginWidget('CActiveForm', array(
	'action' => '/my/edit/',
	'id'=>'profile-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'validateOnChange' => true,
		'errorCssClass' => 'error',
		'inputContainer' => 'div.row',
		'afterValidate' => 'js:function(form, data, hasError) { 
			if (hasError) {
				for(var i in data) $("#"+i).closest(".row").addClass("error");
				return false;
			}
			else {
				return true;
			}
		}',
		'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
			if (hasError) $("#"+attribute.id).closest(".row").addClass("error");
		}'
	),
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	)); ?>



	<div class="form-wrapper form-wrapper-wide">
		<div class="row">
			<label><?= $model->attributeLabels()['email'] ?> <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'email', array('class' => 'field')) ?>
				<?= $form->error($model, 'email', array('class' => 'error error-wide')) ?>
			</span>
		</div>


		<div class="row">
			<label><?= $model->attributeLabels()['firstname'] ?> <sup>*</sup></label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'firstname', array('class' => 'field')) ?>
				<?= $form->error($model, 'firstname', array('class' => 'error error-wide')) ?>
			</span>
		</div>



		<div class="row">
			<label><?= $model->attributeLabels()['lastname'] ?> </label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'lastname', array('class' => 'field')) ?>
				<?= $form->error($model, 'lastname', array('class' => 'error error-wide')) ?>
			</span>
		</div>




		<div class="row">
			<label><?= $model->attributeLabels()['birthday'] ?> </label>
			<span class="field-wrapper">
				<?
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model' => $model,
					'attribute'=>'birthday',
					'language' => 'ru',
					'options'=>array(
						'showAnim'=>'fold',
						'dateFormat' => 'dd/mm/yy',
						'minDate'=>'-90y',
						'yearRange' => '1917:c',
						'defaultDate' => '-20y',
						'changeMonth' => 'true',
						'changeYear' => 'true',
					),
					'htmlOptions'=>array(
						'class' => 'field',
					)
				));
				?>
				<?= $form->error($model, 'birthday', array('class' => 'error error-wide')) ?>
			</span>
		</div>




		<div class="row">
			<label><?= $model->attributeLabels()['profession'] ?> </label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'profession', array('class' => 'field')) ?>
				<?= $form->error($model, 'profession', array('class' => 'error error-wide')) ?>
			</span>
		</div>




		<div class="row">
			<label><?= $model->attributeLabels()['country'] ?> </label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'country', array('class' => 'field')) ?>
				<?= $form->error($model, 'country', array('class' => 'error error-wide')) ?>
			</span>
		</div>




		<div class="row">
			<label><?= $model->attributeLabels()['city'] ?> </label>
			<span class="field-wrapper">
				<?= $form->textField($model, 'city', array('class' => 'field')) ?>
				<?= $form->error($model, 'city', array('class' => 'error error-wide')) ?>
			</span>
		</div>




		<div class="row">
			<label class="label-top"><?= $model->attributeLabels()['avatar'] ?></label>
			<span class="field-wrapper">
				<?
				// if (empty($model->avatar))
				{
					?>
					<div class="clearfix">
						<img src="<?= ThumbsMaster::getThumb($model->avatar, ThumbsMaster::$settings['260_260'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="" align="left">
					</div>
					<br>
					<?
				}
				?>
				<?= CHtml::activeFileField($model, 'avatar', array('class' => 'field')) ?>
				<?= $form->error($model, 'avatar', array('class' => 'error error-wide')) ?>
			</span>
		</div>


		<div class="row">
			<label><?= $model->attributeLabels()['password'] ?> </label>
			<span class="field-wrapper">
				<?= $form->passwordField($model, 'password', array('class' => 'field', 'value' => '')) ?>
				<?= $form->error($model, 'password', array('class' => 'error error-wide')) ?>
			</span>
		</div>


		<div class="row">
			<label><?= $model->attributeLabels()['password2'] ?> </label>
			<span class="field-wrapper">
				<?= $form->passwordField($model, 'password2', array('class' => 'field')) ?>
				<?= $form->error($model, 'password2', array('class' => 'error error-wide')) ?>
			</span>
		</div>


		<div class="row">
			<label><?= $model->attributeLabels()['comments_notify'] ?> </label>
			<span class="field-wrapper">
				<?= $form->checkBox($model, 'comments_notify') ?>
				<?= $form->error($model, 'comments_notify', array('class' => 'error error-wide')) ?>
			</span>
		</div>


		<div class="row">
			<label for=""></label>
			<span class="field-wrapper">
				<?= CHtml::submitButton('Сохранить', array('class' => 'button _blue-button')) ?>
			</span>
		</div>
	</div>


	<?php $this->endWidget(); ?>

</div>