<?php

/*$this->breadcrumbs=array(
	'Профайл',
);*/
?>

<h1>Личный кабинет</h1>

<? $this->widget('application.widgets.profile.summary.ProfileSummaryWidget'); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.profileMenu.ProfileMenuWidget',
				array(
					'user_id' => Yii::app()->user->id,
				)
			);
	?>
	<div class="tabwrap">
		<div id="tab-subs" class="box _optional">
			<table class="blogTb">
				<tbody>
					<?
					foreach ($subscribers as $i => $s)
					{
						if (~ $i & 1)
						{
							?><tr><?
						}
						?>
							<td>
								<img src="<?= ThumbsMaster::getThumb($s->Watcher->avatar, ThumbsMaster::$settings['avatar_81'], false, '/static/css/pub/i/noavatar.jpg'); ?>" alt="">
							</td>
							<td>
								<?
								if (!empty($s->Watcher->blog))
								{
									?>
									<span class="login"><a href="/user/<?= $s->Watcher->id ?>/"><?= $s->Watcher->blog->code ?></a></span> <span>(<?= $s->Watcher->lastname ?> <?= $s->Watcher->firstname ?>)</span>
									<span class="_grey"><?= $s->Watcher->profession ?></span>
									<?
								}
								else
								{
									?>
									<span class="login"><span><?= $s->Watcher->lastname ?> <?= $s->Watcher->firstname ?></span>
									<span class="_grey"><?= $s->Watcher->profession ?></span>
									<?
								}
								?>
							</td>
						<?
						if ($i & 1)
						{
							?></tr><?
						}
					}

					if (!empty($subscribers) && ~ $i & 1)
					{
						?></tr><?
					}
					?>
			</table>
		</div>

	</div>
</div>
