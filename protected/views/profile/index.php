<?php

/*$this->breadcrumbs=array(
	'Профайл',
);*/
?>

<h1>Личный кабинет</h1>

<? $this->widget('application.widgets.profile.summary.ProfileSummaryWidget'); ?>

<div class="profile_page">
	<?
	$this->widget(
				'application.widgets.profile.profileMenu.ProfileMenuWidget',
				array(
					'user_id' => Yii::app()->user->id,
				)
			);
	?>
</div>

<div>
	<div class="tabwrap">
		<div id="tab-blog" class="box">
			<?
			if (!empty($model->blog))
			{
				?>
				<div class="profile_page">
					<a href="<?= $model->blog->url() . 'newpost/' ?>" class="button _blue-button">написать в блог</a>
					<ul class="newslist-main">
						<?
						foreach ($posts as $i => $p)
						{
							?>
							<li>
								<?
								if (!empty($p->cover))
								{
									?>
									<div class="photo">
										<img src="<?= ThumbsMaster::getThumb($p->cover, $i == 0 ? ThumbsMaster::$settings['300_300'] : ThumbsMaster::$settings['166_166']) ?>" alt="">
									</div>
									<?
								}
								?>
								<div class="newstext">
									<span class="date"><?=  Yii::app()->dateFormatter->format('dd MMMM yyyy, HH:mm', $p->date_create) ?></span>

									&mdash;

									<?
									if (!empty($p->is_draft))
									{
										?>
										<span class="date"><span class="_red">черновик</span></span>
										<?
									}
									else
									{
										?>
										<span class="date"><span class="_blue">опубликован</span></span>
										<?
									}
									?>

									<h3><a href="<?= $p->url() ?>"><?= $p->name ?></a></h3>
									<p><?= strip_tags($p->preview_text) ?></p>
									<div class="sub">
										<span class="views"><?= $stat[$p->id]->views ?></span>
										<span class="comments"><?= $p->comments_cnt() ?></span>
										<span class="_grey">Рейтинг: <?= $p->rating ?></span>
										&nbsp;&nbsp;&nbsp;
										<a href="<?= $p->url() . 'edit/' ?>" class="_blue">Редактировать</a>
										<?
										if (!empty($p->draft))
										{
											?>
											&nbsp;&nbsp;&nbsp;
											<a href="<?= $p->draft->url() . 'delete/' ?>" class="_red" onclick="return confirm('Удалить?')">Удалить черновик</a>
											<?
										}
										?>
									</div>
								</div>
							</li>
							<?
						}
						?>
					</ul>
				</div>
				<?
				

				$this->widget('application.widgets.pager.PagerWidget', array('pages' => $pages, ));

			}
			else if (Yii::app()->user->status != User::STATUS_ACTIVE)
			{
				?>
				Можно завести свой блог, но для этого нужно пройти активацию. <a href="/reactivate/" class="js-ajaxPopup _blue">Выслать письмо с активацией</a>
				<?
			}
			else
			{
				CHtml::$errorCss = '';
				$form = $this->beginWidget('CActiveForm', 
											array(
													'action' => '/my/',
													'id'=>'profile-form',
													'enableClientValidation'=>true,
													'enableAjaxValidation'=>true,
													'clientOptions' => array(
														'validateOnSubmit' => true,
														'validateOnChange' => true,
														'errorCssClass' => 'error',
														'inputContainer' => 'div.row',
														'afterValidate' => 'js:function(form, data, hasError) { 
															if (hasError) {
																for(var i in data) $("#"+i).closest(".row").addClass("error");
																return false;
															}
															else {
																return true;
															}
														}',
														'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
															if (hasError) $("#"+attribute.id).closest(".row").addClass("error");
														}'
													)
												)
											);
				?>
				<div class="sign-up-form">
					<div class="form-wrapper form-wrapper-wide">

						<div class="row">
							<label><?= $model->attributeLabels()['blog_code'] ?> <sup>*</sup></label>
							<span class="field-wrapper">
								<?
								if (!empty($model->blog_code))
								{
									echo Yii::app()->request->hostInfo . '/blogs/' . $model->blog_code . '/';
								}
								else
								{
									echo $form->textField($model, 'blog_code', array('class' => 'field', 'onkeyup' => "$('#blog_path').text( $(this).val() + ($(this).val().length > 0 ? \"/\" : \"\") )"));
									?>
									<?= $form->error($model, 'blog_code', array('class' => 'error error-wide')) ?>
									<div>
										<br />
										<?= Yii::app()->request->hostInfo . '/blogs/<span id="blog_path"></span>' ?>
									</div>
									<?
								}
								?>
							</span>
						</div>

						<div class="row">
							<label><?= $model->attributeLabels()['blog_name'] ?> <sup>*</sup></label>
							<span class="field-wrapper">
								<?= $form->textField($model, 'blog_name', array('class' => 'field')) ?>
								<?= $form->error($model, 'blog_name', array('class' => 'error error-wide')) ?>
							</span>
						</div>


						<div class="row">
							<label class="label-top"><?= $model->attributeLabels()['blog_description'] ?></label>
							<span class="field-wrapper">
								<?= $form->textArea($model, 'blog_description', array('class' => 'field field-wide')) ?>
								<?= $form->error($model, 'blog_description', array('class' => 'error error-wide')) ?>
							</span>
						</div>

					</div>
				

					<?= CHtml::submitButton('Создать блог', array('class' => 'button _blue-button')) ?>
				</div>

				<?
				$this->endWidget();
			}
			?>
		</div>
	</div>
</div>




