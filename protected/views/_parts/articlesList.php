<ul class="newslist-main">
<?
if(is_array($articles)){
    
    $last_dname = '';

    $i = 0;
    foreach($articles as $article)
    {

        //Голос юзера за данную статью
        $userRatingForComment = Ratings::model()->getUserRateForEntity('Articles', $article->id);


        $plusClass = '';
        $minusClass = '';
        if($userRatingForComment == 1)
            $plusClass = 'style="border: 1px solid #f00"';
        elseif($userRatingForComment == -1)
            $minusClass = 'style="border: 1px solid #f00"';


        if($i == 0 && $pages->currentPage == 0)
        {
            $class = 'specific';
            $thumb_size = '433_284';
            $thumb_width = '433';
        }
        else
        {
            $class = '';
            $thumb_size = '253_166';
            $thumb_width = '253';
        }
        ?>
        
        <?
        if (date('Y-m-d') == date('Y-m-d', strtotime($article->date_publish)))
        {
            $dname = 'Сегодня';
        }
        else if (date('Y-m-d', strtotime('yesterday')) == date('Y-m-d', strtotime($article->date_publish)))
        {
            $dname = 'Вчера';
        }
        else
        {
            $dname = Yii::app()->dateFormatter->format('dd MMMM', $article->date_publish);
        }

        if ($last_dname != $dname)
        {
            ?>
            <li class="dateline"><?= $dname ?></li>
            <?
            $last_dname = $dname;
        }
        ?>
        
        <li class="<?=$class?>">
            <?if(strlen($article->preview_img) > 5)
            {
            ?>
            <div class="photo">
                <span class="photolabel"><?=$article->tree->name?></span>
                <img src="<?=ThumbsMaster::getThumb($article->preview_img, ThumbsMaster::$settings[$thumb_size])?>" width="<?=$thumb_width?>" alt="">
            </div>
            <?
            }
            ?>
            <div class="newstext">
                <span class="date"><?=Yii::app()->dateFormatter->format('dd MMMM y HH:mm', $article->date_publish)?></span>
                <h3><a href="<?=$article->url()?>"><?=$article->name?></a></h3>
                <?=$article->preview_text?>
                <div class="sub">
                    <span class="views"><?=$stat[$article->id]->views?></span>
                    <span class="comments"><?=$article->comments_count?></span>
                    <span class="rate">
                        <a href="/ratings/rate/Articles/<?=$article->id?>/-1/" class="minus" <?=$minusClass?> onclick="rating.push($(this)); return false;"></a>
                        <span class="raitnum"><?=Ratings::model()->getRating('Articles', $article->id);?></span>                                      
                        <a href="/ratings/rate/Articles/<?=$article->id?>/1/" class="plus" <?=$plusClass?> onclick="rating.push($(this)); return false;"></a>
                       
                    </span>
                </div>
            </div>
        </li>
        <?
    

        $i++;

    }
}
?>

    <?$this->widget('application.widgets.pager.PagerWidget', array('pages' => $pages, )); ?>

</ul>