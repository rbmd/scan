<div class="content-nest content-nest-white">

	<div class="article">
		<div class="wrapper wrapper-article">
			<div class="content-banner content-banner-240">

			</div>
			
			<div class="article-content-wrap">
				<div class="article-lead">
					<?
					if (!empty($article->predetail_text))
					{
						?>						
							<?= strip_tags($article->predetail_text, '<br><a><b>') ?>						
						<?
					}
					?>
				</div>



				<div class="article-meta">

					<div class="shares _float_left">
						<?
						$this->widget('application.widgets.articles.SharesCountWidget');
						?>
					</div>
				
					<?
					if ($article->parent_id != 7)
					{
						?>
						<div class="author _float_right">
							<b>Автор статьи:</b> 
							<?php
							if (isset($authors))
							{
								
								foreach ($authors as $author_type => $author)
								{
									?>
								
										<?php
										$last = count($author) - 1;
										foreach ($author as $i => $a)
										{
											?>										
											<?=$a->authors->fullname()?>
											<?

										}
										?>
									
								<?php
								}
								
							}
							?>
						</div>
						<?
					}
					else
					{
						?>
						<div class="author _float_right">
							<b>Дата:</b> 
							<?= Yii::app()->dateFormatter->format('d MMMM yyyy', $article->date_publish) ?>
						</div>
						<?
					}
					?>
				</div>

				<?= $chapter->detail_text ?>

				<div class="article-meta article-meta-line">
					<div class="shares _float_right">
						<?
						$this->widget('application.widgets.articles.SharesCountWidget');
						?>
					</div>
					<div class="tags _float_left">
						<b>Теги:</b>
						<?
						if (count($tags) > 0) 
						{	
								$last = count($tags) - 1;
								foreach ($tags as $i => $t) {
									echo sprintf(
										'<a href="%s">%s</a>%s',
										$t->url(),
										mb_strtoupper(mb_substr($t->name, 0, 1)) . mb_substr($t->name, 1),
										$i < $last ? ', ' : ''
									);
								}
						}
						?>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>


<div class="content-nest content-nest-white">

    <div id="disqus_thread" class="wrapper wrapper-article wrapper--comments"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'faceslacescomm'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <?/*<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>*/?>
    
</div>


<?/*
<div class="sub">
	<span class="date"><?=UtilsHelper::timeToNiceString($article->date_publish)?> <?//22 октября 2013, 22:45?></span>
	<span class="views"><?=$stat[$article->id]->views?></span>
	<span class="comments"><?=$article->comments_cnt()?></span>
</div>*/?>







<?#$this->widget('application.widgets.bestbysection.BestbysectionWidget', array() )?>