<div class="col c12 first">
		<div class="col c12 first">
			<div class="alpha">
                <? if($this->beginCache(keysSet::CACHE_KEY_BREADCRUMBS . 'NEWS_PAGE'.$tree_node_id, array('duration'=>iMemCache::CACHE_DURATION_1_HOUR))) : ?>
                    <?$this->widget('application.widgets.breadcrumbs.Breadcrumbs', array('tree_node_id' => $tree_node_id) );?>
                    <?$this->endCache();?>
                <? endif ?>

                <div class="heading-with-nav">
                	<h1><?=$title_h1?></h1>
                	<nav>
                		<a class="rss" href="<?=Articles::rssNews()?>" target="_blank">&nbsp;</a>
                	</nav>
                </div>


					<?
					foreach($articles as $a)
					{
						?>
						<article class="list">
							<table>
								<tr>
									<td>
										<div class="baseline-wrapper">
											<time datetime="<?= $a->date_active_start ?>">
												<?= Yii::app()->dateFormatter->format('HH:mm', $a->date_active_start) ?> 
												&nbsp;<i class="ico rain l"></i> 
												<?= Yii::app()->dateFormatter->format('dd.MM.yyyy', $a->date_active_start) ?>
											</time>
										</div>
										<?
										if (strlen($a->preview_img) > 0)
										{
											?>
											<a href="/articles/<?=$a->code?>-<?=$a->id?>/" class="thumb"><img src="<?= $a->preview_img ?>" alt="" /><span class="play"></span></a>
											<?
										}
										?>
									</td>
									<td>
										<div class="baseline-wrapper">
											<a href="/articles/<?=$a->code?>-<?=$a->id?>/" class="name"><?= $a->name ?></a>
											<span class="meta">
												<i class="ico camera"></i>&nbsp;<span><?= isset($a->video_tvigle_duration) ? UtilsHelper::humanizeDuration($a->video_tvigle_duration / 1000) : 0 ?></span>
											</span>
										</div>

										<p><?= strip_tags($a->preview_text, '<br>') ?></p>

										<div>
											<span class="meta">
												<i class="ico views"></i> <span><?= $stat[$a->id]->views ?></span>
												<a href="<?= $a->url() ?>"><i class="ico comments"></i> <span><?= $a->comments_cnt() ?></span></a>
											</span>
										</div>
									</td>
								</tr>
							</table>
						</article>
						<div class="hr"></div>
						<?
					}

					$this->widget(	'Pagination',
									array(
										'pages' => $pages,
									)
								);

					?>

			</div>
		</div><?/*<!--
		--><div class="col c3">
				<? $this->widget('application.widgets.mainnews.MainNewsWidget'); ?>
		</div>		*/?>			
</div><!--
--><div class="col c4 gelded">
	<? #$this->widget('application.widgets.thisday.ThisDayWidget'); ?>
	
	<? $this->widget('application.widgets.banners.BannerWidget', array('banner_code'=>'krutipedali')); ?>

	<?//ria obmenka?>
	<!--AdFox START-->
	<!--slon_obmen-->
	<!--��������: slon.ru / ���������� �������� / ������� 1-->
	<!--���������: <�� ������>-->
	<!--��� �������: ��������-����������� ����-->
	<script type="text/javascript">
	<!--
	if (typeof(pr) == 'undefined') { var pr = Math.floor(Math.random() * 1000000); }
	if (typeof(document.referrer) != 'undefined') {
	  if (typeof(afReferrer) == 'undefined') {
		afReferrer = escape(document.referrer);
	  }
	} else {
	  afReferrer = '';
	}
	var addate = new Date(); 
	document.write('<scr' + 'ipt type="text/javascript" src="http://ads.adfox.ru/159305/prepareCode?p1=bluz&amp;p2=ehfk&amp;pct=a&amp;pfc=a&amp;pfb=a&amp;pr=' + pr +'&amp;pt=b&amp;pd=' + addate.getDate() + '&amp;pw=' + addate.getDay() + '&amp;pv=' + addate.getHours() + '&amp;prr=' + afReferrer + '"><\/scr' + 'ipt>');
	// -->
	</script>
	<!--AdFox END-->	
	
	
    <? $this->widget('application.widgets.lectures.LecturesWidget'); ?>

	<? $this->widget('application.widgets.schedule.ScheduleWidget'); ?>

    <? $this->widget('application.widgets.polls.VotesWidget'); ?>
	
	<? $this->widget('application.widgets.socnet.SocnetWidget'); ?>
</div>