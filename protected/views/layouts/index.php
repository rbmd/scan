<?=Yii::app()->controller->renderPartial('application.views.layouts.parts.head')?>
<body>

	<div class="container wrap">

		<header class="header row inner">
			<div class="header__logo">
				<a href="//scan-interfax.ru/" title="" target="_blank">
					<img src="http://scan-interfax.ru/content/shared-images/promo/logo_top.png" alt="">
				</a>
			</div>
			<div class="header__desc">
				<div class="header__title">Система комплексного анализа новостей</div>
				<div class="header__phone text-light">Поддержка: <span>+ 7 (495) 648-3269</span></div>
			</div>
		</header>

		<div class="main">

			<?=$content?>


			<?= Yii::app()->controller->renderPartial('application.views.layouts.parts.footer') ?>

	<!-- end of wrap -->
		</div>
	</div>
	
	<script src="/js/script.js"></script>
	<script>


	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){


	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),


	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)


	  })(window,document,'script','//http://www.google-analytics.com/analytics.js','ga');



	 

	  ga('create', 'UA-63923145-1', 'auto');


	  ga('send', 'pageview');



	 

	</script>
	
</body>
</html>