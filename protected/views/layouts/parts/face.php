<div class="ribbon ribbon--blue">
	<div class="inner inner--small">
		<h2 class="text-extrabold text-center title title--smooth title--large">
			Рейтинги цитируемости от&nbsp;Интерфакса
		</h2>
	</div>
</div>

<section class="face-block">

	<div class="inner">
		<header class="face-block__header">
			<div class="btn btn--small face-block__header__soon">Скоро</div>
			<h2 class="h2 face-block__header__title title title--smooth text-extrabold">
				Молодежный инновационный форум 2015
			</h2>
			<div class="face-block__header__date">
				Москва, 2&mdash;3 октября 2015
			</div>
		</header>
	</div>

	<div class="inner inner--small">
		<div class="separator-line"></div>
		<h2 class="h2 text-center">
			Все рейтинги цитируемости по&nbsp;версии SCAN
		</h2>
		<div class="face-block__rating-table">
			
			<!-- item -->
				<div class="face-block__rating-table__cell">
					<a href="" class="face-block__rating-table__cell__link">
						<span class="text-extrabold face-block__rating-table__cell__title">
							XIII Международный банковский форум "Банки России - XXI век"
						</span>
						<span class="face-block__rating-table__cell__info">
							Санкт-Петербург, 12-15 июня 2015
						</span>
					</a>
				</div>
			<!-- /item -->

		</div>
	</div>

</section>