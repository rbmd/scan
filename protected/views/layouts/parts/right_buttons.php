<div class="right-menu">
  <ul>
    <li class="advice" data-href="http://likbez.quote.rbc.ru/rubric/education/">
      <div class="inner">
        <div class="front">
          <div class="t-table">
            <div class="t-row">
              <div class="t-cell">
                <div class="icon">
                  <img src="/img/advices.png" alt=""/>
                </div>
                <span>Смотри</span>
              </div>
            </div>
          </div>
        </div>
        <div class="back">
          <div class="t-table">
            <div class="t-row">
              <div class="t-cell">
                Регулярные вебинары о том, как зарабатывать на финансовых рынках
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
    <li class="lesson" data-href="http://likbez.quote.rbc.ru/rubric/articles/">
      <div class="inner">
        <div class="front">
          <div class="t-table">
            <div class="t-row">
              <div class="t-cell">
                <div class="icon">
                  <img src="/img/lesson.png" alt=""/>
                </div>
                <span>Читай</span>
              </div>
            </div>
          </div>
        </div>
        <div class="back">
          <div class="t-table">
            <div class="t-row">
              <div class="t-cell">
                Успешные люди рассказывают о самом важном уроке в их жизни
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
    <li class="remote" data-href="http://www.fxclub.org/lp/ru-ru/lbx-demo/simplereg/trenazher-millionera-nauchites-zarabatyvat-bez-vlozhenii/?utm_id=1T9&utm_source=rbc&utm_country=ru&utm_mediumtype=bn&utm_medium=media&utm_campaign=selfbuy&utm_term=trenazher&utm_content=navbar">
      <div class="inner">
        <div class="front">
          <div class="t-table">
            <div class="t-row">
              <div class="t-cell">
                <div class="icon">
                  <img src="/img/remote.png" alt=""/>
                </div>
                <span>Тренируйся</span>
              </div>
            </div>
          </div>
        </div>
        <div class="back">
          <div class="t-table">
            <div class="t-row">
              <div class="t-cell">
                Научитесь зарабатывать, не рискуя собственными деньгами
              </div>
            </div>
          </div>
        </div>

      </div>
    </li>
<!--    <li class="membership">-->
<!--      <div class="inner">-->
<!--        <div class="front">-->
<!--          <div class="t-table">-->
<!--            <div class="t-row">-->
<!--              <div class="t-cell">-->
<!--                <div class="icon">-->
<!--                  <img src="/img/membership.png" alt=""/>-->
<!--                </div>-->
<!--                <span>Участвуй в конкурсе</span>-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--        <div class="back">-->
<!--          <div class="t-table">-->
<!--            <div class="t-row">-->
<!--              <div class="t-cell">-->
<!--                Каждую неделю &ndash; новая возможность сделать самый точный прогноз-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </li>-->
  </ul>
</div>