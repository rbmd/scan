<div class="footer inner row">
	<div class="footer__logo ibox">
		<a href="//interfax.ru/" target="_blank" title="">
			<img src="/img/interfax-logo.png" alt="">
		</a>
	</div>
	<div class="footer__info ibox">
		<p>
			© 2007-<?= date('Y') ?> 
			<br/>
			Вся информация, размещенная на&nbsp;данном веб-сайте, предназначена только для персонального пользования и&nbsp;не&nbsp;подлежит дальнейшему воспроизведению и/или распространению в&nbsp;какой-либо форме, иначе как с&nbsp;письменного разрешения Интерфакса.
		</p>
	</div>
</div>