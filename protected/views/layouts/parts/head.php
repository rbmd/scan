<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=$this->pageTitle?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge," />
	<meta name="description" content="Описание страницы">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
	
	<link rel="stylesheet" href="/css/reset.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<script src="//use.typekit.net/tyx6kzb.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	<script src="/js/jquery-1.11.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js"></script>
	
</head>