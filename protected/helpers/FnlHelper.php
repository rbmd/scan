<?
//функции для faces laces
class FnlHelper
{

	public static function makeHumanDate($date)
	{
		$today = date('Y-m-d');
		$yesterday = date('Y-m-d', time() - 60*60*24);
			

		$date_day = date('Y-m-d', strtotime($date) );

		if($date_day == $today)
		{
			$str_date = 'Сегодня в '.Yii::app()->dateFormatter->format('HH:mm', $date);
		}
		elseif($date_day == $yesterday)
		{
			$str_date = 'Вчера в '.Yii::app()->dateFormatter->format('HH:mm', $date);
		}
		else {
			$str_date = Yii::app()->dateFormatter->format('d.MM.yyyy', $date);
		}

		return $str_date;
	}

	public static function utf_json_encode($arr)
	{
		if(is_array($arr) && count($arr) > 0)
		{
			//convmap since 0x80 char codes so it takes all multibyte codes (above ASCII 127). So such characters are being "hidden" from normal json_encoding
			array_walk_recursive($arr, function (&$item, $key) { if (is_string($item)) $item = mb_encode_numericentity($item, array (0x80, 0xffff, 0, 0xffff), 'UTF-8'); });
			return mb_decode_numericentity(json_encode($arr), array (0x80, 0xffff, 0, 0xffff), 'UTF-8');
		} 
		//return false;
	}

}