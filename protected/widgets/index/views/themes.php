<div class="col _w74-5 _blackbt _greybb">
	<?
	$n = 0;
	$all_themes_count = count($themes);
	foreach ($themes as $theme_key => $theme)
	{
		//first theme
		if ($theme_key == 0)
		{
			?>
			<div class="columns">
				<div class="col _w64">

					<div class="columns">

						<div class="col _w240p">

							<div class="section-title _mtb15p">
								<h2>
									<?if(isset($theme->series)){?>
									<a href="<?= $theme->series->url() ?>" class="_red"><?= $theme->series->name ?></a>
									<?}?>
								</h2>
							</div>

							<?
							//first item of first theme
							if (isset($items_holder[$theme_key]) && is_array($items_holder[$theme_key]) && count($items_holder[$theme_key]) >= 1)
							{
								$f_item = array_shift($items_holder[$theme_key]);
								?>
								<div class="anons-list _secondary-promo">
									<div class="anons">
										<a href="<?= $f_item->url() ?>" class="piclink">
											<img class="pic" src="<?=ThumbsMaster::getThumb($f_item->preview_img, ThumbsMaster::$settings['220_127'])?>" alt="" width="220" height="127">
										</a>
										<a href="<?= $f_item->url() ?>" class="title"><?= $f_item->name ?></a>
										<a href="<?= $f_item->url() ?>" class="note"><?= strip_tags($f_item->preview_text) ?></a>
									</div>
								</div><!-- /.anons-list -->
								<?
							}
							?>

						</div><!-- /.col -->

						<div class="col">

							<?
							if (isset($items_holder[$theme_key]) && is_array($items_holder[$theme_key]))
							{		
								?>
								<div class="section-title-spacer"></div>
								<div class="anons-list _simple">
									<?
									foreach ($items_holder[$theme_key] as $item_key => $item)
									{
										?><div class="anons"><a href="<?= $item->url() ?>" class="title"><?= $item->name ?></a></div><?
									}
									?>
								</div><!-- /.anons-list -->
								<?
							}
							?>

						</div><!-- /.col -->

					</div><!-- /.columns -->

				</div><!-- /.col -->

			<?

		}
		else if ($theme_key == 1)
		{
			?>

				<div class="col _w32">

					<div class="section-title _mtb15p">
						<h2>
							<?if(isset($theme->series)){?>
							<a href="<?= $theme->series->url() ?>" class="_red"><?= $theme->series->name ?></a>
							<?}?>
						</h2>
					</div>

					<div class="anons-list _simple">
						<?
						//first item
						if (isset($items_holder[$theme_key]) && is_array($items_holder[$theme_key]) && count($items_holder[$theme_key]) >= 1)
						{ 
							$f_item = array_shift($items_holder[$theme_key]);

							?>
							<div class="anons special">
								<a href="<?= $f_item->url() ?>" class="piclink"><img src="<?=ThumbsMaster::getThumb($f_item->preview_img, ThumbsMaster::$settings['220_127'])?>" alt="" class="pic" width="220" height="127"></a>
								<a href="<?= $f_item->url() ?>" class="title"><?= $f_item->name ?></a>
							</div>
							<?
						}
						?>

						<?
						if(isset($items_holder) && is_array($items_holder) && isset($items_holder[$theme_key]))
						{
							foreach($items_holder[$theme_key] as $item_key => $item)
							{
								?>
								<div class="anons"><a href="<?= $item->url() ?>" class="title"><?= $item->name ?></a></div>
								<?
							}
						}
						?>
					</div><!-- /.anons-list -->

				</div><!-- /.col -->

			</div><!-- /.columns -->
			<?

		} 
		else
		{
			if ($n == 0)
			{
				?>
				<div class="columns _borders">
					<div class="col _w32">
					</div>
					<div class="col _w32">
					</div>
					<div class="col _w32">
					</div>
				</div><!-- /.columns._borders -->

				<div class="columns">
				<?	
			}
			?>
		

			<div class="col _w32">
				<div class="section-title _mtb15p">
					<h2>
						<?if(isset($theme->series)){?>
						<a href="<?= $theme->series->url() ?>" class="_red"><?= $theme->series->name ?></a>
						<?}?>
					</h2>
				</div>

				<div class="anons-list _simple">
				<?
				//first item
				if (isset($items_holder[$theme_key]) && is_array($items_holder[$theme_key]) && count($items_holder[$theme_key]) >= 1)
				{ 
					$f_item = array_shift($items_holder[$theme_key]);

					?>
					<div class="anons special">
						<a href="<?= $f_item->url() ?>" class="piclink"><img src="<?=ThumbsMaster::getThumb($f_item->preview_img, ThumbsMaster::$settings['220_127'])?>" alt="" class="pic" width="220" height="127"></a>
						<a href="<?= $f_item->url() ?>" class="title"><?= $f_item->name ?></a>
					</div>
					<?
				}
				?>

				<?
				if(is_array($items_holder) && isset($items_holder[$theme_key]))
				{
					foreach($items_holder[$theme_key] as $item_key => $item)
					{
						?>
						<div class="anons"><a href="<?= $item->url() ?>" class="title"><?= $item->name ?></a></div>
						<?
					}
				}
				?>
				</div><!-- /.anons-list -->
			</div>


			<?
			++$n;
			
			//Если показали все темы, то показываем оставшееся количество заглушек и закрываем див
			if($n < 3 && $theme_key + 1 == $all_themes_count)
			{
				for($k = 0; $k < 3 - $n; $k++)
				{
				?>
				<div class="col _w32">
					<div class="section-title _mtb15p">
						<h2></h2>
					</div>
					<div class="anons-list _simple">				
					</div><!-- /.anons-list -->
				</div>
				<?
				}
			} 			

			//Закрываем, если показали 3 или если показали все темы
			if ($n == 3 || $theme_key + 1 == $all_themes_count)
			{
				$n = 0;
				?></div><?
			}
		}
	}
	?>
</div>


