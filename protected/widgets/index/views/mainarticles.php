<div class="columns">

	<div class="col _w300p">
		<div class="anons-list _main-promo">
			<div class="anons">
				<a href="<?= $main_item->url() ?>" class="piclink">
					<img class="pic" src="<?=ThumbsMaster::getThumb($main_item->preview_img, ThumbsMaster::$settings['300_252'])?>" alt="<?= htmlspecialchars($main_item->name, ENT_QUOTES) ?>" width="300" height="252">
				</a>
				<div class="section" <?//=$main_item->tree->url()?>><?= $main_item->tree->name ?></div>
				<a href="<?= $main_item->url() ?>" class="title"><?= $main_item->name ?></a>
			</div>
		</div>
	</div>

	<div class="col">
		<div class="anons-list _main-list">
		<?
		foreach($items as $item)
		{
			?>
			<div class="anons">
				<div class="section" <?//=$item->tree->url()?>><?= $item->tree->name ?></div>
				<a href="<?= $item->url() ?>" class="title"><?= $item->name ?></a>
			</div>
			<?
		}
		?>
		</div>
	</div>

</div>
