<div class="columns">

	<div class="col _w350p">
		<div class="anons-list _main-promo">
			<div class="anons">
				<a href="<?= $main_item->url() ?>" class="piclink">
					<img class="pic" src="<?=ThumbsMaster::getThumb($main_item->preview_img, ThumbsMaster::$settings['350_252'])?>" alt="<?= htmlspecialchars($main_item->name, ENT_QUOTES) ?>" width="350" height="252">
				</a>
				<div class="section">
					<?
					$authors = array();
					foreach ($main_item->authors as $a)
					{
						$authors[] = $a->name . ' ' . $a->surname;
					}

					$authors = implode($authors, ', ');

					echo $authors;
					?>
				</div><div class="meta sub">
					<span class="date"><?= Yii::app()->dateFormatter->format('HH:mm', $main_item->date_active_start) ?></span>
					<span class="views"><?= $stat[$main_item->id]->views ?></span>
					<?
					$cc = (int) $main_item->comments_cnt();
					?>
					<span class="comments"><?= $cc ?></span>
				</div>
				<a href="<?= $main_item->url() ?>" class="title"><?= $main_item->preview_text ?></a>
			</div>
		</div>
	</div>

	<div class="col">
		<div class="anons-list _main-list">
			<div class="anons hover" data-views="<?= $stat[$main_item->id]->views ?>" data-comments="<?= (int) $main_item->comments_cnt() ?>" data-author="<?= htmlspecialchars($authors) ?>" data-text="<?= htmlspecialchars($main_item->preview_text) ?>" data-img="<?= ThumbsMaster::getThumb($main_item->preview_img, ThumbsMaster::$settings['350_252'])?>" data-date="<?= Yii::app()->dateFormatter->format('HH:mm', $main_item->date_active_start) ?>" data-href="<?= $main_item->url() ?>">
				<a href="<?= $main_item->url() ?>">
					<span class="section" <?//=$main_item->tree->url()?>><?= $main_item->tree->name ?></span>
					<span class="title"><?= $main_item->name ?></span>
				</a>
			</div>
		<?
		foreach($items as $item)
		{
			$authors = array();
			foreach ($item->authors as $a)
			{
				$authors[] = $a->name . ' ' . $a->surname;
			}

			$authors = implode($authors, ', ');

			?>
			<div class="anons" data-views="<?= $stat[$item->id]->views ?>" data-comments="<?= (int) $item->comments_cnt() ?>" data-author="<?= htmlspecialchars($authors) ?>" data-text="<?= htmlspecialchars($item->preview_text) ?>" data-img="<?= ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['350_252'])?>" data-date="<?= Yii::app()->dateFormatter->format('HH:mm', $item->date_active_start) ?>" data-href="<?= $item->url() ?>">
				<a href="<?= $item->url() ?>">
					<span class="section" <?//=$item->tree->url()?>><?= $item->tree->name ?></span>
					<span class="title"><?= $item->name ?></span>
				</a>
			</div>
			<?
		}
		?>
		</div>
	</div>


	<script type="text/javascript">
		$(document).ready(function(){
			$('.anons-list._main-list .anons').mouseover(function(){
				$o = $(this);
				$a = $('.anons-list._main-promo .anons');

				$a.find('a').attr('href', $o.attr('data-href'));
				$a.find('.section').text($o.attr('data-author'));
				$a.find('.date').text($o.attr('data-date'));
				$a.find('.views').text($o.attr('data-views'));
				$a.find('.comments').text($o.attr('data-comments'));
				$a.find('img').attr('src', $o.attr('data-img'));
				$a.find('.title').html( $o.attr('data-text'));

				$('.anons-list._main-list .anons').removeClass('hover');
				$o.addClass('hover');
			})
		})
	</script>

</div>
