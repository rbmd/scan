<div class="b-preview b-preview_size_medium b-preview_type_best-articles">
	<table class="b-preview__head">
		<tbody>
			<tr>
				<td class="b-preview__head__left">
					<span class="g-font-small-1">Лучшие статьи</span>
				</td>
				<td class="b-preview__head__right">
					<span class="g-font-small-1 g-color-text"></span>
				</td>
			</tr>
		</tbody>
	</table>
	<?
	if(isset($items))
	{
		$i = 0;
		foreach ($items as $item) {
			switch ($item->tree->id) {
				case 7: $type = 'howto'; break;
				case 8: $type = 'guide'; break;
				default: $type = 'article'; break;
			}
			if($item->is_guide == 1)
				$type = 'guide';

			$class = 'b-preview__item';
			if ($i == 0) {
				$class .= ' g-active';
			}
		?>
		<div class="<?=$class?> g-color-<?=$type?>">
			<span class="g-margin-uncollapse"></span>
			<a href="/<?=$item->tree->url?>" class="b-preview__item__section g-font-small-2 g-color-text-hover"><?=$item->tree->name?>&nbsp;&nbsp;<i class="g-icon g-icon_triangle_right"></i></a>
			<a href="<?=$item->url()?>" class="g-color-text-hover">
				<img src="<?=$item->img('circle')?>" width="88" alt="<?=$item->name?>" class="b-preview__item__image" />
				<p class="b-preview__item__title g-font-title-3"><?=$item->name?></p>
				<div class="b-preview__item__arrow-handler"><i class="g-icon g-icon_arrow_right"></i></div>
			</a>
			<ul class="b-preview__item__meta b-meta g-color-text g-font-small-1">
				<?Yii::app()->controller->renderPartial('application.views.layouts.parts.article_info_block', array('article' => $item, 'stat' => $stat))?>
			</ul>
			<span class="g-margin-uncollapse"></span>
		</div>
	<?
		$i++;
		}
	}
?>
</div><?php ?>