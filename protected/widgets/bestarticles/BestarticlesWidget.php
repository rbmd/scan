<?
/**
 * Виджет блока лучших статей
 */
class BestarticlesWidget extends CWidget
{
	//Подключаемый шаблон
	public $template = 'index';
	
    public function init(){}
 
    public function run()
    {
	
		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::WIDGET_BEST_ARTICLES, array(), serialize(ArticlesPk::getArray()) );
			
		$items = Yii::app()->cache->get($CACHE_KEY);	
		if($items === false)
		{		
			$items = Articles::model()->with('tree')->Indexed()->visible()->pkNotIn(ArticlesPk::getArray())->recently(3)->isBest()->findAll();
			
			Yii::app()->cache->set($CACHE_KEY, $items, 60);
			
			Yii::app()->cache->addDependency($CACHE_KEY, array(
					'Articles@getIsActive'=>array(),
					//'Articles@getIsBest' => array(),
				)
			);				
		}			
			
		
		//Вытаскиваем статистику для выбранных статей
		$stat = ViewsFull::model()->getStatistic4Elements($items);
		
		$this->render($this->template, array('items' => $items, 'stat' => $stat) );	
    }
}
?>