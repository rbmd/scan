<?
abstract class CAbstractElement {
	
	protected $article_id;
	
	//Тип элемента. смотри CElements
	protected $type;
	
	//Параметры отображения. Устанавливаются через $this->getAdditionalRenderParams(); - эта функция должна быть определена в классе элемента
	protected $additionalRenderParams;
	
	public function __construct( $type)
	{		
		#$this->article_id = $article_id;
		$this->type = $type;
	}
	
	/**
	 * Обрабатывает элемент при сохранении, преобразует его в вид для публички
	 * @param $content - json
	 * @return array
	 */
	public function parseToAdmin($content)
	{		
		#var_dump($content);		
		if(is_array($content))
		{
		
			//В зависимости от типа элемента необходимо либо выводить "вид публички", либо "вид публички" с разметкой областей редактирования
			switch($this->type)
			{
				case 'wysiwyg':
				case 'blockquote':
					$mode = 'admin';
				break;
				default:
					$mode = 'pub';
			}

			$render = Yii::app()->controller->renderPartial('application.widgets.elements.views.'.$mode.'.'.$this->type, array('c' => $content), true);
			#var_dump($render);
			return $render;
		}		
	}
	
	/**
	 * Обрабатывает элемент при сохранении, преобразует его в вид для публички
	 * @param $content - json
	 * @return array
	 */	
	public function parseToDb($article_id, $content)
	{
		if(is_array($content))
		{
			$render = Yii::app()->controller->renderPartial('application.widgets.elements.views.pub.'.$this->type, array('c' => $content, 'article_id' => $article_id), true);
			return $render;
		}		
	}

	/**
	 * Возвращает массив параметров для элемента в админке
	 * @param $content - json или обычная строка(wysiwyg)
	 * @return array('params' => основные параметры, преобразованные из $content, 'a_params' => параметры, опциональные для элемента, полученые из $additionalRenderParams)
	 */
	public function getRenderParams($content)
	{		
		//Вызваем  функцию получения доп. параметров(функция должна быть определена в классе элемента)
		if(is_null($this->additionalRenderParams))
			$this->getAdditionalRenderParams($content);	

		$params = json_decode($content);

		return array('params' => $params, 'a_params' => $this->additionalRenderParams);
	}
	
	
}
?>