<?
class ElementWysiwyg extends Element {	
	
	protected $elementType = 'wysiwyg';
	protected $title = 'Визредактор';
	protected $editMode = 'inline';
	
	
	public function afterRender(&$render)
	{
		$render = preg_replace("!(&quot;)(?=[a-zA-Z0-9АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЬьЫыЭэЮюЯя])!s", "&laquo;", $render);
		$render = preg_replace("!([a-zA-Z0-9АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЬьЫыЭэЮюЯя\?\!., ])(&quot;)([^Ёё]{0,1})!", "\\1&raquo;\\3", $render);
		$render = str_replace(' - ',' &ndash; ', $render);	
	}
	
}
?>