<?
$uid = uniqid();
?>
<div class="quote-wrap">
	<a href="#" class="twit-quote <?= $uid ?>">твитнуть цитату</a>
	<blockquote><?=$E->fields->text?></blockquote>
</div>
<script>
$('.twit-quote.<?= $uid ?>').click(function (e) {
	var width  = 575,
		height = 400,
		left   = ($(window).width()  - width)  / 2,
		top    = ($(window).height() - height) / 2,
		url    = this.href,
		opts   = 'status=1' +
				 ',width='  + width  +
				 ',height=' + height +
				 ',top='    + top    +
				 ',left='   + left;
	window.open('https://twitter.com/intent/tweet?url=' + location.protocol+'//'+location.host+location.pathname + '&text=<?= $E->fields->text ?>', "Поделиться", opts);
	e && e.preventDefault();
	return false;
});
</script>