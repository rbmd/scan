<?php
class ElementBuilder {
	
	static function build($params)
	{
	
		try {
			
			$classname = 'Element'.ucfirst($params['elementType']);

			if(!file_exists(YiiBase::getPathOfAlias('application.widgets.elements.celements/' . $classname)  . '.php') )
				throw new Exception('Класс ' . $classname . ' не существует. ');
				
			//Создаем новый элемент на основе входящих параметров
			$e = new $classname();
			
			if(isset($params['content']))
				$e->setContent($params['content']);
				
			if(isset($params['elementId']))
				$e->setElementId($params['elementId']);
				
			if(isset($params['chapterId']))
				$e->setChapterId($params['chapterId']);
								
			if(isset($params['chapterOrderNum']))
				$e->setChapterOrderNum($params['chapterOrderNum']);
				
			if(isset($params['elementOrderNum']))
				$e->setElementOrderNum($params['elementOrderNum']);

			if(isset($params['renderMode']))
				$e->setRenderMode($params['renderMode']);				
				
			if(isset($params['refreshFromDb']))
				$e->refreshFromDb();
			
			if(isset($params['create']) && is_null($e->getElementId()) )
				$e->create();				
				
			return $e;
			
		} catch (Exception $e) {
			echo $e->getMessage();			
		}
		
	}
	
}