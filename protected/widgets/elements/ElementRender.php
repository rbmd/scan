<?php

class ElementRender {
	
	//Результат рендера
	protected $render;
	
	/**
	 * 
	 * Выводит элемент в соответствующем ему шаблоне
	 * @param Element $element
	 * @param bool $return
	 * @param bool $onlyTemplate
	 */
	
	public function render(Element $element, $return = false, $onlyTemplate = false)
	{
		if(!$onlyTemplate)
		{	
			$view_path = $element->getRenderMode();
	
			//Если режим вывода - публичка и это инлайн элемент
			if($view_path == 'admin_preview' && $element->getEditMode() == 'inline')
			{
				$view_path = 'admin';
			}		
		}
		else
		{
			$view_path = 'pub';
		}
		
		$this->render = '';
		
		//Верхняя часть виджета для админки
		if(!$onlyTemplate)
			$this->render .= Yii::app()->controller->renderPartial('application.widgets.elements.views.parts.widgetBegin', array('E' => $element), true );		
		
		//Выводим шаблон элемента
		$this->render .= Yii::app()->controller->renderPartial('application.widgets.elements.views.'.$view_path.'.'.$element->getElementType(), array('E' => $element ), true );
		
		//Нижняя часть виджета для админки
		if(!$onlyTemplate)
			$this->render .= Yii::app()->controller->renderPartial('application.widgets.elements.views.parts.widgetEnd', array('E' => $element), true );	
	
		//Вызываем пост-рендер
		if($view_path == 'pub')
			$element->afterRender($this->render);
	
		if($return)
			return $this->render;
		else
			echo $this->render;	
	}
	
}