<?php

abstract class Element {	
	
	//ID элемента
	protected $elementId;
	
	//ID главы 
	protected $chapterId;

	//Порядковый номер главы в статье
	protected $chapterOrderNum;	
	
	//Порядковый номер элемента в главе
	protected $elementOrderNum;
	
	//Содержимое элемента. JSON массив
	protected $content;
	
	//Объект, содержащий раскодированное содержимое элемента. $E->fields->field
	public $fields;	
	
	//Режим отображения. admin или pub
	protected $renderMode = 'pub';
	
	public function __construct()
	{
		$this->renderer = new ElementRender();	
		$this->setContent('{}');	
	}
	
	/**
	 * Обновляет данные текущего элемента из БД
	 */
	public function refreshFromDb()
	{
		if($this->elementId < 1)
			return;

		$e = ElementsDraft::model()->findByPk($this->elementId);
		$this->setContent($e->content);
	}
	/**
	 * Сохраняет элемент в базу
	 * 
	 */
	public function save()
	{
		
		$article_id = ElementsDraft::model()->findByPk($this->elementId)->article_id;
		if (WorkflowLock::isLockArticle($article_id) == false)
		{
			ElementsDraft::model()->updateByPk($this->elementId, array('content' => $this->content) );
		}		
	}
	
	/**
	 * Создает новый элемент в заданой главе
	 */	
	public function create()
	{
		if($this->chapterId < 1)
			return false;
			
		$chapter             = ChaptersDraft::model()->findByPk($this->chapterId);
		$e                   = new ElementsDraft();
		$e->article_draft_id = $chapter->article_draft_id;
		$e->article_id      = $chapter->article_id;
		$e->chapter_id       = $chapter->id;
		$e->type             = $this->elementType;
		$e->hist             = 0;
		$order_num                 = Yii::app()->db->createCommand()->select('order_num')->from(ElementsDraft::model()->tableName())
			->where('chapter_id=:chapter_id', array(':chapter_id' => $chapter->id))
			->order('order_num desc')->limit(1)
			->queryScalar() + 1;
		$e->order_num        = $order_num;
		$e->save();
		
		$this->setElementId($e->id);
		
		return true;
	}
	
	//Функция вызывается перед рендером
	protected function beforeRender()
	{

	}
	/**
	 * 
	 * Отображает элемент
	 * @param bool $return возвращать ли результат в виде строки или сразу вывести на экран
	 * @param bool $onlyTemplate - добавлять ли при выводе обрамляющие блоки для админки (если false)
	 */
	public function render($return = false, $onlyTemplate = false)
	{
		$this->beforeRender();
		return $this->renderer->render($this, $return, $onlyTemplate);
	}
		
	/**
	 * Генерирут параметр NAME для формы в админском интерфейсе
	 * Важно - в ElementsController при сохранении конкретного элемента идет сбор из POST по заданому element_id.
	 * @param string $field - имя поля
	 */
	public function fieldFormName($field)
	{
		return $this->elementId . '[' . $field . ']';
	}
	
	/**
	 * Генерирут параметр ID для формы в админском интерфейсе
	 * Важно - в ElementsController при сохранении конкретного элемента идет сбор из POST по заданому element_id.
	 * @param string $field - имя поля
	 */
	public function fieldFormId($field)
	{
		return $this->elementId . '_' . $field;
	}	
	
	/**
	 * 
	 * Установка режима отображения элемента 
	 * @param string $mode - pub или admin
	 */
	public function setRenderMode($mode)
	{
		$this->renderMode = $mode;
		$this->afterSetRenderMode();
	}
	
	/**
	 * Заглушка для функции, которая вызывается после установки режима отображения
	 * В этой функции внутри классов конкретных элементов можно вызывать доп функции. Например, получение списка галерей для выбора и прочее.
	 */
	protected function afterSetRenderMode()
	{		
	}
	
	public function getRenderMode()
	{
		return $this->renderMode;
	}
	
	
	/**
	 * 
	 * Устанавливает ID текущего элемента
	 * @param int $elementId
	 */
	public function setElementId($elementId)
	{
		if(intval($elementId) > 0)
			$this->elementId = intval($elementId);		
	}
	
	public function getElementId()
	{
		return $this->elementId;
	}
	
	public function setChapterId($chapterId)
	{
		$this->chapterId = $chapterId;		
	}	
	
	public function getChapterId()
	{
		return $this->chapterId;		
	}		
	
	//@param $content string OR json string.
	public function setContent($content, $encode = false)
	{
		if($encode)
			$content = ElementFields::encode($content);
			
		$this->content = $content;	

		$this->fields = new ElementFields($this); 
	}
	
	/**
	 * Возвращает контент текущего элемента
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}	

	/**
	 * 
	 * Возвращает название текущего элемента (устанавливается в классе соответствующего элемента)
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	public function getElementType()
	{
		return $this->elementType;
	}
	
	
	public function getFromDB()
	{		
		if(!is_null($this->elementId))
		{
			$element       = ElementsDraft::model()->findByPk($this->elementId);
			$this->content = $element->content;		
		}
	}
	
	public function getEditMode()
	{
		return $this->editMode;		
	}

	//****/
	public function setChapterOrderNum($chapterOrderNum)
	{
		$this->chapterOrderNum = $chapterOrderNum;
	}
	
	public function setElementOrderNum($elementOrderNum)
	{
		$this->elementOrderNum = $elementOrderNum;
	}
		
	
	
	public function getChapterOrderNum()
	{
		return $this->chapterOrderNum;
	}
	
	public function getElementOrderNum()
	{
		return $this->elementOrderNum;
	}
	
	/**
	 * 
	 * Вызывается для renderMode=pub после рендера.
	 * @param string $render - результат рендера элемента (строка, содержащая весь шаблон)
	 */
	public function afterRender(&$render)
	{

	}
	
}

?>