<div class="intro" style="background-image: url(<?=$obj->detail_img?>);">
	<div class="wrapper wrapper-adaptive wrapper-relative">
		<div class="intro__text">
			<div class="intro__text-title">
				<span class="intro__text-title__icon intro__text-title__icon-<?=$obj->code?>" data-icon-name="<?=$obj->code?>"></span>
				<?=$obj->name?>
			</div>
			<div class="intro__text-txt">
				<?=$obj->preview_text?>
			</div>
		</div>
	</div>
</div>