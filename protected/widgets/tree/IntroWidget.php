<?
class IntroWidget extends CWidget
{
	
	public $obj;

	public function run()
	{
		//Если передаем дерево, то отобразится один шаблон, если статью, то другой и тп
		$class = get_class($this->obj);

		$this->render('intro/'.$class, array('obj' => $this->obj, ));
	}
}