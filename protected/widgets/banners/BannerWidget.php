<?php
/**
 * User: Русаков Дмитрий
 * Date: 18.06.12
 *
 * Виджет для отображения баннера по указанному коду баннера
 */

class BannerWidget extends CWidget
{
    /**
     * @var int - код баннера, который будет отображаться виджетом
     */
    public $banner_code = null;
	 public $return = false;
	 public $output = '';

    public function init()
    {
    }

    public function run()
    {
        if( empty($this->banner_code) )
        {
            return;
        }

        $CACHE_KEY = Yii::app()->cache->buildKey(
            keysSet::WIDGET_BANNER,
            array($this->banner_code)
        );

        $banner = Yii::app()->cache->get($CACHE_KEY);

        if ($banner === false)
        {
            $banner = Banners::model()->active()->findByAttributes(array('code'=>$this->banner_code));

            if($banner === null)
            {
                return;
            }

            Yii::app()->cache->set($CACHE_KEY, $banner, 300);

            Yii::app()->cache->addDependency($CACHE_KEY, array(
                    //Сбрасываем кеш, когда баннер с указанным кодом меняется из админки
                    '>Banners@isActive' => array($banner->id),
                )
            );
        }

        if( !empty($banner) )
        {
            $this->output = $this->render('application.widgets.banners.views.banner', array('banner' => $banner), $this->return);
        }
    }
}