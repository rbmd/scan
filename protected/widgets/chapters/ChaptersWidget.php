<?
/**
 * Виджет для работы с главами в админской части
 */
class ChaptersWidget extends CWidget
{
	//Шаблон
	public $layout = 'base';

	//Статья, элементы и главы которой выбираем
	public $article_id;

	//Главы
	public $chapters;

	//Элементы
	public $elements;

	// Модель

	public $model;

	public function init()
	{
	}

	public function run()
	{
		//Базовый шаблон
		if ($this->layout == 'base')
		{
			$hist = (Yii::app()->controller->action_type == 'hist') ? 1 : 0;

			//Выбираем главы
			$chapters = ChaptersDraft::Model()->allByArticleDraftPk($this->article_id, $hist)->findAll();

			//Выбираем элементы глав
			$elements = ElementsDraft::Model()->articleDraftPk($this->article_id, $hist)->findAll();

			$elements_a = array();

			if (!is_null($elements))
			{
				//Приводим элементы к массиву
				foreach ($elements as $el)
				{
					$elements_a[$el->chapter_id][$el->order_num] = array(
						'id'      => $el->id,
						'type'    => $el->type,
						'content' => $el->content
					);
				}
			}
			$this->render('base', array('chapters' => $chapters, 'elements' => $elements_a, 'model' => $this->model ));
		}
		//Полный шаблон: глава+элементы. Используется для создания новых глав и редактирования существующих.
		elseif ($this->layout == 'chapters')
		{
			$this->render('chapters', array('chapters' => $this->chapters, 'elements' => $this->elements));
		}
	}
}