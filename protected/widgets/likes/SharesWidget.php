<?
class SharesWidget extends CWidget
{
	// Виджет использует скрипт http://sapegin.github.io/social-likes/

	public $look = 'horizontal'; // horizontal/vertical/single
	public $skin = 'birman'; // birman
	public $show_counters = true; // показывать счетчик шеров
	public $url = '';
	private $skins = array('flat', 'birman');

	public function init()
	{
		$cs = Yii::app()->getClientScript();

		if (!in_array($this->skin, $this->skins)) {
			$this->skin = $this->skins[0];
		}

	  	$cs->registerCssFile("/static/css/social-likes_{$this->skin}.css");
		$cs->registerScriptFile('/static/js/pub/social-likes/social-likes.min.js', CClientScript::POS_END);
	}

	public function run()
	{
		$this->render('shares');
	}
}

