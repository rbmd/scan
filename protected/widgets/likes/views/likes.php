<div class="sociallink">
	<table>
		<tr>
			<td style="vertical-align: top;">
				<?
				$url = $host . $uri;
				$r = $this->widget(
					'ext.yii-facebook-opengraph.plugins.LikeButton',
					array(
						'href'       => $url,
						'show_faces' => false,
						'send'       => false,
						'action'     => 'like',
						'layout'     => 'button_count',
						'skin'		 => 'light',
					)
				);
				?>
			</td>
			<td style="padding-left: 15px;">
				<div id="vk_like_<?= $uniq ?>"></div>
				<script
					type="text/javascript">VK.Widgets.Like("vk_like_<?= $uniq ?>", {type: "mini", height: 20,pageUrl: '<?=$url?>'}<?=!empty($this->vk_page_id) ? ', '.$this->vk_page_id : ''?>);</script>
			</td>
			<td>
				<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?= $url ?>"
				   data-text="<?= $this->twitter ?>" data-lang="ru">Твитнуть</a>
				<script>
					!function (d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
						if (!d.getElementById(id)) {
							js = d.createElement(s);
							js.id = id;
							js.src = p + '://platform.twitter.com/widgets.js';
							fjs.parentNode.insertBefore(js, fjs);
						}
					}(document, 'script', 'twitter-wjs');
				</script>
			</td>
		</tr>
	</table>
</div>