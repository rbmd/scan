<?
class LikesWidget extends CWidget
{
	public $host = 'http://kavpolit.com';
	public $uri;
	public $share = false;
	public $comments = false;
	public $likeandpay = 0;
	public $twitter; //текст твиттера
	public $vk_page_id; //page_id для сбрасывания кеша и количества лайков в vk
	private $uniq;

	public function init()
	{
		$this->uniq = md5(rand());
		if (empty($this->uri))
		{
			$this->uri = Yii::app()->request->requestUri;
		}
		if (empty($this->twitter))
		{
			$this->twitter = Yii::app()->facebook->ogTags['title'];
		}
	}

	public function run()
	{
		$this->render(
			'likes',
			array(
				'host' => $this->host,
				'uniq' => $this->uniq,
				'uri'  => $this->uri,
			)
		);
	}
}

