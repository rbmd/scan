<div class="b-filter">
	<ul class="b-filterlist _tabs">
		<li class="tablink"><a href="/blogs/">лента</a> (<?= $posts ?>)</li>
		<li class="tablink"><a href="/blogs/authors/">блогеры</a> (<?= $bloggers ?>)</li>
		<li class="tablink active"><a href="/blogs/subscriptions/">подписки</a> (<span id="subscriptions_cnt"><?= $subscriptions ?></span>)</li>
		<li class="sort" data-tabs-id="tab-blog">
			Сортировать:   
			<?
			if (Yii::app()->request->getParam('sort') == 'popular')
			{
				?>
				<a href="#" class="curent">Популярные</a>
				<a href="<?= UtilsHelper::mergeGetRequest(array('sort' => null)) ?>">Последние</a>
				<?
			}
			else
			{
				?>
				<a href="<?= UtilsHelper::mergeGetRequest(array('sort' => 'popular')) ?>">Популярные</a>
				<a href="#" class="curent">Последние</a>
				<?
			}
			?>
		</li>
	</ul>
</div>