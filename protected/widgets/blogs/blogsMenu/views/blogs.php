<div class="b-filter">
	<ul class="b-filterlist _tabs">
		<li class="tablink active"><a href="/blogs/">лента</a> (<?= $posts ?>)</li>
		<li class="tablink"><a href="/blogs/authors/">блогеры</a> (<?= $bloggers ?>)</li>
		<?
		if (!Yii::app()->user->isGuest)
		{
			?>
			<li class="tablink"><a href="/blogs/subscriptions/">подписки</a> (<span id="subscriptions_cnt"><?= $subscriptions ?></span>)</li>
			<?
		}
		?>
		<li class="sort" data-tabs-id="tab-blog">
			Сортировать:   
			<?
			if (empty($_GET['sort']) || $_GET['sort'] == 'popular')
			{
				?>
				<a href="#" class="curent">Популярные</a>
				<a href="<?= UtilsHelper::mergeGetRequest(array('sort' => 'last')) ?>">Последние</a>
				<?
			}
			else
			{
				?>
				<a href="<?= UtilsHelper::mergeGetRequest(array('sort' => null)) ?>">Популярные</a>
				<a href="#" class="curent">Последние</a>
				<?
			}
			?>
		</li>
	</ul>
</div>