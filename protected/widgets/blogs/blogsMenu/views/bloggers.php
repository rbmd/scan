<div class="b-filter">
	<ul class="b-filterlist _tabs">
		<li class="tablink"><a href="/blogs/">лента</a> (<?= $posts ?>)</li>
		<li class="tablink active"><a href="/blogs/authors/">блогеры</a> (<?= $bloggers ?>)</li>
		<?
		if (!Yii::app()->user->isGuest)
		{
			?>
			<li class="tablink"><a href="/blogs/subscriptions/">подписки</a> (<span id="subscriptions_cnt"><?= $subscriptions ?></span>)</li>
			<?
		}
		?>
		<li class="searchli" data-tabs-id="tab-bloggers">
			<span class="bloggers-hide">Поиск блоггера:&nbsp;</span>
			<form class="search">
				<input type="text" class="search_text" placeholder="Начните вводить данные" name="search">
				<input type="submit" class="search_submit" value="">
			</form>
		</li>
	</ul>
</div>