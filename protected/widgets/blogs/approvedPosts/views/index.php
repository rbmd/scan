<?
if (empty(Yii::app()->user->blog_code) && !Yii::app()->user->isGuest)
{
	?>
	<a href="/my/" class="create-blog">
		Создать блог
	</a>
	<?
}
?>

<!--
	Табы генерятся автоматически из этой семантичной разметки
	Соответствие - ссылка-айдишник.
	Класс _optional скрывается при высоких разрешениях.
-->
<header class="section-title">
	<h2><a href="#tab-blogs">Блоги</a></h2>
</header><!-- /.section-title -->

<div class="anons-list _blogposts" id="tab-blogs">

	<?
	foreach ($posts as $p)
	{
		?>
		<div class="anons">
			<a href="<?= $p->blog->url() ?>" class="author-pic-link">
				<img src="<?= ThumbsMaster::getThumb($p->blog->user->avatar, ThumbsMaster::$settings['avatar_81'], false, '/static/css/pub/i/noavatar.jpg'); ?>" class="author-pic" alt="<?= htmlspecialchars($p->blog->user->firstname . ' ' . $p->blog->user->lastname) ?>" width="30" height="30">
			</a>
			<a class="author-name" href="<?= $p->blog->url() ?>"><?= $p->blog->user->firstname ?> <?= $p->blog->user->lastname ?></a>
			<a href="<?= $p->url() ?>" class="title"><?= $p->name ?></a>
		</div>
		<?
	}
	?>

	<a class="all-link" href="/blogs/">Все блоги</a>

</div><!-- /.anons-list -->