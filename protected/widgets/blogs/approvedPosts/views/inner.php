<header class="section-title">
	<h2><a class="_blue" href="/blogs/">Блоги</a></h2>
</header><!-- /.section-title -->

<div class="anons-list _blogposts _inner">

	<?
	foreach ($posts as $p)
	{
		?>
		<div class="anons">
			<a class="author-name" href="<?= $p->blog->url() ?>"><?= $p->blog->user->firstname ?> <?= $p->blog->user->lastname ?></a>
			<a href="<?= $p->url() ?>" class="title"><?= $p->name ?></a>
		</div>
		<?
	}
	?>

</div><!-- /.anons-list -->

<?
if (empty(Yii::app()->user->blog_code) && !Yii::app()->user->isGuest)
{
	?>
	<a href="/my/" class="create-blog">
		Создать блог
	</a>
	<?
}