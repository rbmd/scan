<?

class ApprovedPostsWidget extends CWidget 
{
	public $tpl;

	public function run()
	{
		$CACHE_KEY = 'ApprovedPostsWidget';

		$posts = Yii::app()->cache->get($CACHE_KEY);

		if ($posts === false)
		{
			$posts = Post::model()->with(
									array(
										'blog' => array(
													'with' => array('user'),
													'joinType' => 'inner join',
												)
										)
									)->findAll(
											array(
												'condition' => 't.active = 1 AND (t.approved = 1 OR t.blog_approved = 1) AND t.is_draft = 0',
												'order' => 't.date_create DESC', 
												'limit' => 7,
											)
										);

			Yii::app()->cache->set($CACHE_KEY, $posts, 60);
		}

		$this->render($this->tpl, array('posts' => $posts));
	}
}