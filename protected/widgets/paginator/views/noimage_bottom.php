<div class="b-article-element b-article-element_shiftable b-article-element-nav b-article-element-nav_neighbour g-font-small-3">
	<table class="b-article-element-nav__layout">
		<tbody>
			<tr>
				<td class="b-article-element-nav__prev g-color-border">
					<? if (!is_null($prev)) { ?>
						<i class="b-article-element-nav__line"></i>
						<a href="<?=$prev_link?>" class="b-article-element-nav__title"><?=$chapters[$prev]->name?><br /><br /><i class="g-icon g-icon_arrow_left"></i></a>
					<? } ?>
				</td>
				<td class="b-article-element-nav__next">
					<? if (!is_null($next)) { ?>
						<i class="b-article-element-nav__line"></i>
						<a href="<?=$next_link?>" class="b-article-element-nav__title"><?=$chapters[$next]->name?><br /><br /><i class="g-icon g-icon_arrow_right"></i></a>
					<? } ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>