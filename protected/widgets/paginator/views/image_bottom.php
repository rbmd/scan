<div class="b-article-element b-article-element_shiftable b-article-element-nav b-article-element-nav_neighbour g-font-small-3">
	<table class="b-article-element-nav__layout">
		<tbody>
			<tr>
				<? if (count($chapters_pages) > 2 || $order_num != 1) {?>
				<td class="b-article-element-nav__prev g-color-border">
					<? if (!is_null($prev)) { ?>
						<a href="<?=$prev_link?>#menu">
							<img src="<?=$chapters[$prev]->image?>" width="118" alt="<?=addslashes($chapters[$prev]->image)?>" class="b-article-element-nav__image" />
							<i class="b-article-element-nav__line"></i>
							<span class="b-article-element-nav__title"><?=$chapters[$prev]->name?><br /><br /><i class="g-icon g-icon_arrow_left"></i></span>
						</a>
					<? } ?>
				</td>
				<?}?>
				<? if (count($chapters_pages) > 2 || $order_num != 2) {?>
				<td class="b-article-element-nav__next">
					<? if (!is_null($next)) { ?>
						<a href="<?=$next_link?>#menu">
							<img src="<?=$chapters[$next]->image?>" width="118" alt="<?=addslashes($chapters[$next]->image)?>" class="b-article-element-nav__image" />
							<i class="b-article-element-nav__line"></i>
							<span class="b-article-element-nav__title"><?=$chapters[$next]->name?><br /><br /><i class="g-icon g-icon_arrow_right"></i></span>
						</a>
					<? } ?>
				</td>
				<?}?>
			</tr>
		</tbody>
	</table>
</div>