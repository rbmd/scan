<div class="pagination">
	<ul>
	<?
	if (isset($move_left))
	{
		if ($move_left == 1)
		{
			$left_page_link = UtilsHelper::mergeGetRequest( array('page' => null) );
		}
		else
		{
			$left_page_link = UtilsHelper::mergeGetRequest( array('page' => $move_left) );
		}

		?><li><a href="<?=$left_page_link?>">&laquo;</a></li><?
	}
	else
	{
		?><li><span>&laquo;</span></li><?
	}

	if($start != 1)
	{
		?><li><a href="<?= UtilsHelper::mergeGetRequest( array('page' => null) ) ?>">1</a></li><?
		
		if($start != 2)
		{
			?><li class="hellip"><span>&hellip;</span></li><?
		}
	}


	for ($p = $start; $p <= $end; $p++)
	{
		if($p == $page)
		{
			?><li class="active"><a href="<?= UtilsHelper::mergeGetRequest( array('page' => $p) )?>"><?=$p?></a></li><?
		}
		else
		{
			if($p > 1)
			{					
				?><li><a href="<?= UtilsHelper::mergeGetRequest( array('page' => $p) )?>"><?=$p?></a></li><?
			}
			else
			{
				?><li><a href="<?= UtilsHelper::mergeGetRequest( array('page' => null) ) ?>"><?=$p?></a></li><?
			}
		}
	}	

	//Выводим последнюю цифру с троеточием, если конец диапазона не равен общему количеству страниц
	if ($end != $total_pages)
	{
		if($end != $total_pages - 1)
		{
			?><li><span class="hellip">&hellip;</span></li><?
		}

		?><li><a href="<?= UtilsHelper::mergeGetRequest( array('page' => $total_pages) )?>"><?=$total_pages?></a></li><?
	}

	if (isset($move_right))
	{
		?><li><a href="<?= UtilsHelper::mergeGetRequest( array('page' => $move_right) )?>">&raquo;</a></li><?
	}
	else
	{
		?><li class="disabled"><span>&raquo;</span></li><?
	}
	?>
	</ul>
</div>