<table class="b-article-element-per-page-switcher__layout">
	<tbody>
		<tr>
			<?php if (is_null(Yii::app()->request->getParam('all', null))) { ?>
			<td class="b-article-element-per-page-switcher__item b-article-element-per-page-switcher__item_state_active g-color-border">
				<a href="/<?=Yii::app()->request->pathInfo?>/" rel="nofollow"><i class="g-icon g-icon_chapter-per-page g-icon_chapter-per-page_one"></i><br /><span class="g-color-text g-font-small-1">Одно<br />место на<br />странице</span></a>
			</td>
			<td class="b-article-element-per-page-switcher__item b-article-element-per-page-switcher__item_state_inactive">
				<a href="?all" rel="nofollow"><i class="g-icon g-icon_chapter-per-page g-icon_chapter-per-page_all"></i><br /><span class="g-font-small-1">Все<br />на одной<br />странице</span></a>
			</td>
			<?php } else { ?>
			<td class="b-article-element-per-page-switcher__item b-article-element-per-page-switcher__item_state_inactive">
				<a href="/<?=Yii::app()->request->pathInfo?>/" rel="nofollow"><i class="g-icon g-icon_chapter-per-page g-icon_chapter-per-page_one"></i><br /><span class="g-font-small-1">Одно<br />место на<br />странице</span></a>
			</td>
			<td class="b-article-element-per-page-switcher__item b-article-element-per-page-switcher__item_state_active g-color-border">
				<a href="?all" rel="nofollow"><i class="g-icon g-icon_chapter-per-page g-icon_chapter-per-page_all"></i><br /><span class="g-color-text g-font-small-1">Все на<br />одной<br />странице</span></a>
			</td>
			<?php } ?>
		</tr>
	</tbody>
</table>