<div class="b-article-element b-article-element_shiftable b-article-element-nav b-article-element-nav_all g-font-small-3">
	<table class="b-article-element-nav__layout">
		<tbody>
<?php
$base_page = '/'.Yii::app()->request->getPathInfo().'/';
$CELLS_PER_ROW = 4;
$width = 100 / $CELLS_PER_ROW;
$i = 0;
foreach ($chapters_pages as $page) {
	$link = $page == 1 ? $base_page : '?chapter='.$page;
	$class = 'b-article-element-nav__item g-color-border';
	$titleClass = 'b-article-element-nav__title';
	$lineClass = 'b-article-element-nav__line';
	if ($i % $CELLS_PER_ROW == 0) {
		echo '<tr>';
	}
	if ($page == $order_num) {
		// $class .= ' g-highlight-bg';
		$titleClass .= ' g-color-text';
		$lineClass .= ' g-color-border';
	}
	?>
		<td class="<?=$class?>" width="<?=$width?>%">
			<a href="<?=$link?>">
				<div class="b-article-element-nav__arrow-wrapper">
				<?php if ($page == $order_num) : ?><i class="g-icon g-icon_arrow_down b-article-element-nav__arrow"></i><?php endif; ?>
				</div>
				<img src="<?=$chapters[$page]->image?>" width="88" alt="<?=addslashes($chapters[$page]->name)?>" class="b-article-element-nav__image" />
				<i class="<?=$lineClass?>"></i>
				<span class="<?=$titleClass?>"><?=$chapters[$page]->name?><span>
			</a>
		</td>
	<?php
	if (($i + 1) % $CELLS_PER_ROW == 0) {
		echo '</tr>';
	}
	$i++;
}
if (count($chapters_pages) % $CELLS_PER_ROW != 0) {
	$class = 'b-article-element-nav__item b-article-element-nav__item_empty';
	$additional = ((floor(count($chapters_pages) / $CELLS_PER_ROW) + 1) * $CELLS_PER_ROW) - count($chapters_pages);
	for ($j = 0; $j < $additional; $j++) {
	?>
		<td class="<?=$class?>" width="<?=$width?>%"></td>
	<?php
	}
	echo '</tr>';
}
?>
		</tbody>
	</table>
</div>