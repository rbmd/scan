<?
/**
 * Выводит постраничку для статей
 */
class PaginatorWidget extends CWidget
{
	//Порядковый номер просматриваемой главы
	public $order_num;
	
	//Массив activerecord глав с индексом = номеру главы
	public $chapters;	
	
	//Тип постранички, от этого зависит шаблон вывода
	public $chapter_paging_type;
	
	//Шаблоны. Ключем является тип постранички - $this->chapter_paging_type
	private $templates = array('image', 'noimage', 'paginator', '', 'without_first_chapter');
	
	//Расположение виджета(над или под текстом статьи)
	public $position;	
	
	//Если true, то выводим переключатель один гид на странице" или "все на одной"
	public $switcher = false;

	public $is_guide = false;
	
    public function init()
    {

    }
 
    public function run()
    {
	
		//Выводим переключатель один гид на странице" или "все на одной"
		if($this->switcher)
		{
			$this->render('switcher');
			return;
		}		

		if ($this->chapter_paging_type == 3) return;

		//Если тип paginator и расположение сверху - не выводим
		if(
			$this->templates[$this->chapter_paging_type] == 'paginator' && $this->position == 'top')
			return;	
	
		//Если гид и верхняя постраничка, то не выводим
		if($this->is_guide == 1 && $this->position == 'top')
			return;
			
		//Страницы
		$chapters_pages = array_keys($this->chapters);	
		
		//Показываем постраничку только если глав более одной.
		if(count($chapters_pages) <= 1)
			return;
		
		//Текущая страница
		$base_page = '/'.Yii::app()->request->getPathInfo().'/';
		
		//Предыдущее и следующее
		$prev = (in_array( ($this->order_num-1), $chapters_pages)) ? ($this->order_num-1) : null;
		$next = (in_array( ($this->order_num+1), $chapters_pages)) ? ($this->order_num+1) : null;
		
		if(!is_null($prev) && $prev > 1)
			$prev_link = '?chapter='.$prev;
		else
			$prev_link = $base_page;

		if(!is_null($next))
			$next_link = '?chapter='.$next;
		else
			$next_link = $base_page;			
			
		
		//Для ВСЕХ зацикливаем постраничку
		#if($this->is_guide == 1)
		{
			if(is_null($prev))
			{
				$prev = count($chapters_pages);
				$prev_link = '?chapter='.$prev;
			}
			elseif(is_null($next))
			{
				$next = 1;
			}
		}
		

		//Если нет такого типа постранички
		if(!isset($this->templates[$this->chapter_paging_type]))
			return;		
	
		$template = $this->templates[$this->chapter_paging_type];

		
		//Для нижних постраничек дописываем автоматом _bottom
		if($this->position == 'bottom')
			$template .= '_bottom';		
			
		

		
				
		$this->render($template, array('chapters_pages' => $chapters_pages, 
										'chapters' => $this->chapters,
										'order_num' => $this->order_num,
										'next' => $next,
										'prev' => $prev,
										'next_link' => $next_link,
										'prev_link' => $prev_link,
										'base_page' => $base_page,
										) 
					);
    }
}
?>