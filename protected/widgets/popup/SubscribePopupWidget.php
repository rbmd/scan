<?php
class SubscribePopupWidget extends CWidget
{
	public function init() { }

	public function run()
	{
		$mobileDetect = new Mobile_Detect();
		if ($mobileDetect->isMobile() == false && isset($_COOKIE['subscribe_popup']) == false && strpos(Yii::app()->request->getRequestUri(), '/subscribe') === false)
		{
			$this->render('subscribe');
		}
	}
}