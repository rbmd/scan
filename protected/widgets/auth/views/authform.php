<div class="form">
	<?
	if(Yii::app()->user->isGuest)
	{
		$form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableAjaxValidation'=>true,
			'enableClientValidation'=>true,
			'action' => '/login/',
			'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true)
		));
		?>

		<div class="col c8 first">
			<div class="col c4 first">
				<div class="col c4 first">
					<div class="wrap">
						<div class="input-nest required">
							<?= $form->textField($model, 'email', array('placeholder' => 'Логин или Email')) ?>
							<?= $form->error($model, 'email') ?>
						</div>
					</div>
				</div>
				<div class="col c4 first">
					<div class="wrap">
						<div class="input-nest required">
							<?= $form->passwordField($model, 'lastname', array('placeholder' => 'Пароль')) ?>
							<?= $form->error($model, 'password') ?>
						</div>
					</div>
				</div>
				<div class="col c4 first">
					<div class="wrap">
						<input id="ytUser_rememberMe" type="hidden" value="0" name="User[rememberMe]" />
						<label><input name="User[rememberMe]" id="User_rememberMe" value="1" checked="checked" type="checkbox" />Запомнить меня</label>
					</div>
				</div>
			</div><!--
			--><div class="col c4 first">
				<? $this->widget('ext.eauth.EAuthWidget', array('action' => 'site/authexternals')); ?>
			</div>
		</div>

		<div class="row buttons">
			<?php echo CHtml::submitButton('Войти'); ?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<?php echo CHtml::link('Регистрация', '/registration'); ?>
		</div>

		<? 
		$this->endWidget();
	}
	else
	{
		
	}
	?>
</div>