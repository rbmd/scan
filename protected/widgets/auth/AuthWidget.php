<?php

/**
 * Виджет авторизации
 */

class AuthWidget extends CWidget
{
    public $model = null;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->render(
            'authform',
            array('model' => empty($this->model) ? new User() : $this->model)
        );
    }
}