<?php
if (isset($items))
{


	$blockIcon  = $this->type == 'by_views' ? 'g-icon_most_viewed' : 'g-icon_most_commented';
	$class = '';
	$ga = "_gaq.push(['_trackEvent', 'rcol_mostRead', 'show']);";

	if ($this->type == 'by_readalso')
	{
		$class = ' readalso';
		$ga = "_gaq.push(['_trackEvent', 'rcol_readAlso', 'show']);";
	}

	if ($this->type == 'by_views')
	{
		$duration= $this->days == 7 ? 'views-by-week' : 'views-by-month';
		$class = 'b-popular_framed b-popular_by-views' . ' ' . $duration;
	}

	if ($this->type == 'by_best')
	{
		$class = 'b-popular_framed b-popular_by-best best';
	}

	?>
<div class="b-popular <?=$class?>" >
<script type="text/javascript"><?=$ga?></script>
<noindex>
	<p class="b-popular__head g-highlight-bg">
	 <? if ($this->type =='by_views') {?>
		 Самое популярное <nobr><span class="g-bcsansbold"><?= $this->days == 7 ? 'на этой неделе' : 'за месяц' ?></span></nobr>
	 <?}?>
	<? if ($this->type =='by_best') {?>
		Лучшее <nobr><span class="g-bcsansbold">на этой неделе</span></nobr>
	<?}?>
	</p>
	<?php
	$i = 0;

	$article_id = isset(Yii::app()->controller->article_id) ? Yii::app()->controller->article_id : 0;
	?>

	<div class="b-popular__container">

	<?
	foreach ($items as $item)
	{
		if ($item->id == $article_id || $i >= $this->limit)
		{
			continue;
		}
		$i++;

		if (isset($item->tree->id))
			$tree_id = $item->tree->id;
		else
				{
					$tree_id = null;
				}

		switch ($tree_id)
		{
			case 7:
				$type = 'howto';
				break;
			case 8:
				$type = 'guide';
				break;
			default:
				$type = 'article';
				break;
		}
		?>
		<table class="b-popular__item b-popular__item_<?=$type?> g-color-<?=$type?>">
			<tbody>
			<tr>
				<?
				$img = $item->img('120x70');

				if ($this->type == 'by_best')
				{
					$img = $item->img('circle');
				}
?>
				<td class="b-popular__item__info-holder">
      <? if ($img) { ?>
        <img src="<?=$img?>" alt="<?=addslashes($item->name)?>" title="<?=addslashes($item->name)?>" class="b-popular__item__image" />
      <? } ?>
					<a rel="nofollow" href="<?=$item->url()?>" class="b-popular__item__title g-font-title-3 g-color-text-hover"><?=$item->name?>
						<br/></a>
					<ul class="b-popular__item__meta b-meta g-font-small-1">
						<?Yii::app()->controller->renderPartial('application.views.layouts.parts.article_info_block', array('article' => $item,'stat' => $stat))?>
					</ul>
				</td>
			</tr>
			</tbody>
		</table>
		<?php
	} ?>
	</div>
<? }
?>
</noindex>
</div>