<form action="">
<div class="b-filter">
	<ul class="b-filterlist">						
		<li class="filter-cell">								
				<label for="search_regions">Выберите регион:</label>
				
				<div class="custom-select">
					<select name="region" id="search_regions" onchange="submit();">
						<option value="0">Весь Кавказ</option>
						<?
						foreach($regions as $region){
							if($region->id == $currentRegion)
								$selected = 'selected';
							else
								$selected = '';
						?>
						<option value="<?=$region->id?>" <?=$selected?> ><?=$region->name?></option>
						<?
						}
						?>
					</select>
					<input id="listFilterType" type="hidden" name="type" value="<?=$currentType?>" />
				</div>
			
		</li>
		<li class="sort">
			Сортировать: 
			<?
			foreach($types as $type => $type_name)
			{
				if($type == $currentType)
				{
				?>
				<a href="#" class="curent" onclick="$('input#listFilterType').val('<?=$type?>'); submit(); "><?=$type_name?></a> 
				<?
				} else {
				?>
				<a href="#" onclick="$('input#listFilterType').val('<?=$type?>'); $(this).closest('form').submit(); "><?=$type_name?></a> 

				<?
				}
			}
			?>
		</li>
	</ul>
</div>
</form>