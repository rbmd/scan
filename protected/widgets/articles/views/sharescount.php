<a href="http://vkontakte.ru/share.php?url=<?= $url ?>" class="share-button">
	<span class="share-icon share-icon-vk" data-icon-name="vk"></span>
	<span class="shares__count"><?= $vk ?></span>
</a>
<a href="https://www.facebook.com/sharer/sharer.php?u=<?= $url ?>" class="share-button">
	<span class="share-icon share-icon-fb" data-icon-name="fb"></span>
	<span class="shares__count"><?= $fb ?></span>
</a>
<a href="https://twitter.com/share?url=<?= $url ?>" class="share-button">
	<span class="share-icon share-icon-tw" data-icon-name="tw"></span>
	<span class="shares__count"><?= $tw ?></span>
</a>



<script>
	$('.share-button').click(function (e) {
		var width  = 575,
		    height = 400,
		    left   = ($(window).width()  - width)  / 2,
		    top    = ($(window).height() - height) / 2,
		    url    = this.href,
		    opts   = 'status=1' +
		             ',width='  + width  +
		             ',height=' + height +
		             ',top='    + top    +
		             ',left='   + left;

		window.open(url, 'share', opts);

		return false;
	})
</script>