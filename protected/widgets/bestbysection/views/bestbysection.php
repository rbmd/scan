<div class="best columns">
	<div class="col section-title _w19">
			<h2><a href="#" class="_grey">лучшее по разделам</a></h2>
			<a href="javascript:void(0)" class="all _grey loadmore" onclick="$('#bestbysectionOther').toggle();"><span>Показать все</span></a>
	</div>
	<div class="col _w70-5">


		<div class="columns">
			<?

			//Первые два раздела
			foreach($nodes_main as $tree_id)
			{
				
				
				if(!array_key_exists($tree_id, Tree::model()->tree_nodes_for_articles))
					continue;
				?>
				<div class="col _w49">
					<header class="section-title _marked _mtb30p">
						<h2><a href="<?=$tree[$tree_id]->url()?>"><?=$tree[$tree_id]->name?></a></h2>
					</header>

					<div class="anons-list _simple">
						<?
						foreach($items[$tree_id] as $item)
						{
							?>
							<div class="anons">
								<?if(strlen($item->preview_img) > 0){?>
								<a href="<?=$item->url()?>" class="piclink">
									<img class="pic" src="<?=ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['110_64'])?>" alt="" width="110" height="64">
								</a>
								<?}?>
								<a href="<?=$item->url()?>" class="title"><?=$item->name?></a>
							</div>
							<?
						}
						?>
					</div><!-- /.anons-list._simple -->
				</div>
				<?
			}
			?>

		</div>

		<div id="bestbysectionOther" style="display: none;">

			<?
			//Выводим остальные разделы
			$k = 0;
			foreach($nodes as $tree_id)
			{
				
				if(!array_key_exists($tree_id, Tree::model()->tree_nodes_for_articles))
				{
					continue;
				}
				
				$k++;
			?>

			<?if($k%2 == 1){?>
			<div class="columns _blackborders">
				<div class="col _w49"></div>
				<div class="col _w49"></div>
			</div><!-- /.columns._borders -->
			<div class="columns">
			<?}?>

				<div class="col _w49">
					<header class="section-title _marked _mtb30p">
						<h2><a href="<?=$tree[$tree_id]->url()?>"><?=$tree[$tree_id]->name?></a></h2>
					</header>

					<div class="anons-list _simple">
						<?
						foreach($items[$tree_id] as $item)
						{
							?>
							<div class="anons">
								<?if(strlen($item->preview_img) > 0){?>
								<a href="<?=$item->url()?>" class="piclink">
									<img class="pic" src="<?=ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['110_64'])?>" alt="" width="110" height="64">
								</a>
								<?}?>
								<a href="<?=$item->url()?>" class="title"><?=$item->name?></a>
							</div>
							<?
						}
						?>
					</div><!-- /.anons-list._simple -->
				</div>			
				

			<?if($k%2 == 0){?>
			</div>		
			<?}?>

			<?
			}
			?>
		</div>
		<?//HERE?>
	</div>




</div>


