<?php
class AuthorSummaryWidget extends CWidget
{
	public $author;

	public function init() { }

	public function run()
	{
		$user = $this->author->User;

		if (!Yii::app()->user->isGuest && !empty($user))
		{
			$criteria = new CDbCriteria();
			$criteria->condition = 'author_id = ' . $user->id . ' AND watcher_id = ' . Yii::app()->user->id;
			$subscription = Subscription::model()->find($criteria);
		}
		
		$watcher_is_subscribed = !empty($subscription);

		$this->render('author', array('author' => $this->author, 'user' => $this->author->User, 'watcher_is_subscribed' => $watcher_is_subscribed));
	}
}