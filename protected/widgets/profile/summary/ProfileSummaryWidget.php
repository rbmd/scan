<?php
class ProfileSummaryWidget extends CWidget
{
	public function init() { }

	public function run()
	{
		$user = User::model()->cache(60)->findByPk(Yii::app()->user->id);
		$this->render('default', array('user' => $user));
	}
}