<?

class AuthorMenuWidget extends CWidget 
{
	public $tpl = 'default';
	public $author;

	public function run()
	{
		$CACHE_KEY = 'AuthorMenuWidget'.$this->author->id;
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$user = $this->author->User;

			try {
				$blog = @$user->blog;
				$_render['posts'] = @count($blog->posts);
			}
			catch (Exception $e)
			{
				$_render['posts'] = 0;
			}


			try {
				$criteria = new CDbCriteria();
				$criteria->select = '*';
				$criteria->condition = 'user_id = :user_id';
				$criteria->params = array(':user_id' => @$user->id);

				$_render['comments'] = Comments::model()->count( $criteria );
			}
			catch (Exception $e)
			{
				$_render['comments'] = 0;
			}


			try {
				$_render['articles'] = @count($this->author->articles());
			}
			catch (Exception $e)
			{
				$_render['articles'] = 0;
			}


			try {
				$criteria = new CDbCriteria();
				$criteria->condition = 'author_id = :author_id';
				$criteria->params = array(':author_id' => @$user->id);

				$_render['subscribers'] = Subscription::model()->count($criteria);
			}
			catch (Exception $e)
			{
				$_render['subscribers'] = 0;
			}

			
			try {
				$criteria = new CDbCriteria();
				$criteria->condition = 'watcher_id = :watcher_id';
				$criteria->params = array(':watcher_id' => @$user->id);

				$_render['subscriptions'] = Subscription::model()->count($criteria);
			}
			catch (Exception $e)
			{
				$_render['subscriptions'] = 0;
			}

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render($this->tpl, array('author' => $this->author, 'articles' => $_render['articles'], 'posts' => $_render['posts'], 'comments' => $_render['comments'], 'subscribers' => $_render['subscribers'], 'subscriptions' => $_render['subscriptions']));
	}
}











