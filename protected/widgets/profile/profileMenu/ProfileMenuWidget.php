<?

class ProfileMenuWidget extends CWidget 
{
	public $tpl = 'default';
	public $user_id;

	public function run()
	{
		$CACHE_KEY = 'ProfileMenuWidget:'.$this->user_id;
		$_render = Yii::app()->cache->get($CACHE_KEY);


		if ($_render === false)
		{
			$criteria = new CDbCriteria();
			$criteria->select = '*';
			$criteria->condition = 'user_id = :user_id';
			$criteria->params = array(':user_id' => $this->user_id);

			$_render['comments'] = Comments::model()->count( $criteria );


			$_render['articles'] = Articles::model()->with(array('authors' => array('condition' => 'user_id = ' . $this->user_id, 'joinType' => 'INNER JOIN')))->count($criteria);
			

			$criteria = new CDbCriteria();
			$criteria->condition = 'watcher_id = :watcher_id';
			$criteria->params = array(':watcher_id' => $this->user_id);

			$_render['subscriptions'] = Subscription::model()->count($criteria);


			$criteria = new CDbCriteria();
			$criteria->condition = 'author_id = :author_id';
			$criteria->params = array(':author_id' => $this->user_id);

			$_render['subscribers'] = Subscription::model()->count($criteria);

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render($this->tpl, array('comments' => $_render['comments'], 'articles' => $_render['articles'], 'subscriptions' => $_render['subscriptions'], 'subscribers' => $_render['subscribers']));
	}
}