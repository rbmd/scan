<?
/**
 * Виджет выводит ссылку "Перейти в раздел администратора", если пользователь админ
 */
class AdminPanel extends CWidget
{
    public function init()
    {
        // этот метод будет вызван методом CController::beginWidget()
    }
 
    public function run()
    {
		//Получаем список ролей зарегеных юзеров
		if (!Yii::app()->user->isGuest)
		{
			if ( Yii::app()->user->superuser )
			{
				$this->render('adminpanel');
			}
		}
    }
}
?>