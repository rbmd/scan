<form id="form_poll_<?=$poll->id?>" class="personsBlock" method="POST">
	<div class="_greybg columns">
		<?
		$i = 0;
		foreach($poll->items as $item_id => $item)
		{
				if($poll->voices_count > 0)
				{
					$percent[$i] = intval(round(($item->voices_count / $poll->voices_count) * 100, 2));
					$voices_count[$i] = $item->voices_count;
				}
				else 
				{
					$percent[$i] = 0;		
					$voices_count[$i] = 0;
				}	
				$i++;
			?>

		<div class="col _w50">
			<div class="personitem">
				<div class="voit">
					<img src="<?=$item->img?><?#=ThumbsMaster::getThumb($item->img, ThumbsMaster::$settings[$thumb_size])?>" alt="" width="210">
				</div>
				<div class="textblock">
					<h3 class="title-a"><?=$item->name?></h3>
					<p style="height: 260px; position: relative;"><?=$item->description?></p>
				</div>								
				
			</div>
			
		</div>
		<?
		}
		?>
	</div>

	<div class="results _greybg">
		<div class="results_cnt">
			<div class="text-wrap-a">
				<div class="num-a"><?=$percent[0]?>%</div>
				<div class="voted-a">Проголосовало <?=$voices_count[0]?> <?=UtilsHelper::pluralize($voices_count[0], 'человек', 'человека', 'человек')?></div>
			</div>
			<div class="line">
				<div class="barwrap">
					<div class="bar">
						<!-- В ширину надо написать сколько процентов проголосовало за левого чувака -->
						<div class="barvoted" style="width: <?=$percent[0]?>%"></div>
					</div>
				</div>
			</div>
			<div class="text-wrap-b">
				<div class="num-b"><?=$percent[1]?>%</div>
				<div class="voted-b">Проголосовало <?=$voices_count[1]?> <?=UtilsHelper::pluralize($voices_count[1], 'человек', 'человека', 'человек')?></div>
			</div>
		</div>
	</div>

</form>
