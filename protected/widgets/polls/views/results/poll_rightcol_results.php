<div class="sidebox">
	<div class="poll">
			
		<div class="question">
			<?=$poll->name?>
		</div>

		<div >
			<?php
				foreach($poll->items as $item_id => $item) {
					if($poll->voices_count > 0)
				{
					$percent = intval(round(($item->voices_count / $poll->voices_count) * 100, 2));
					$voices_count = $item->voices_count;
				}
				else 
				{
					$percent = 0;		
					$voices_count = 0;
				}
			?>
			<div class="results" style="margin-top: 0px; margin-left: -20px;">
				<div class="voted"><?=$item->name?></div>
				<div class="line" style="display: inline-block; width: 100%;">
					<div class="barwrap" style="padding: 0px">
						<div class="bar">
							<div class="barvoted" style="width: <?=$percent?>%"></div>
						</div>
					</div>
					<div class="num"><?=$percent?>%</div>
				</div>
			</div>
			<?php
				}
			?>
		</div>

	</div>
	
</div>