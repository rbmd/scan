<div class="big-card poll">
	<div class="card-content">
		<div class="desc">
			<div class="result-form">
				<?php
					foreach($poll->items as $item_id => $item) {
						if($poll->voices_count > 0)
					{
						$percent = intval(round(($item->voices_count / $poll->voices_count) * 100, 2));
						$voices_count = $item->voices_count;
					}
					else 
					{
						$percent = 0;		
						$voices_count = 0;
					}
				?>
				<div class="poll-result">
					<div class="poll-bar">
						<div class="poll-bar__fill" style="width: <?=$percent?>%;"></div>
						<div class="poll-percent">
							<?=$percent?>%
						</div>
					</div>
				</div>

				<div class="row">
					<div class="percentage">
						<div class="bar" style="width: <?=$percent?>%"></div>
						<div class="value"><?=$percent?>%</div>
					</div>
					<span class="l">
						<?=$item->name?>
					</span>
				</div>

				<?php
				}
				?>
				
			</div>
		</div>
	</div>
</div>