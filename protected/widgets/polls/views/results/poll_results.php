<?/*<div class="b-article-element b-article-element_shiftable b-article-element-poll b-article-element-poll_state_voted">
	<!-- Visible when already voted: poll results -->
	<p class="b-article-element-poll__title"><?=$poll->name?></p>
	<div class="b-article-element-poll__results">
		<?php
			foreach($poll->items as $item_id => $item) {
				if($poll->voices_count > 0)
					$percent = intval(round(($item->voices_count / $poll->voices_count) * 100, 2));
				else
					$percent = 0;
		?>
		<div class="b-article-element-poll__result">
			<p class="b-article-element-poll__result__title g-font-default-2"><?=$item->name?></p>
			<div class="b-article-element-poll__result__bar g-color-bg" style="width: <?=$percent?>%;">
				<span class="b-article-element-poll__result__bar-cap g-color-bg"></span>
				<span class="b-article-element-poll__result__votes-count g-font-small-1"><?=$item->voices_count?></span>
			</div>
		</div>
		<?php
			}
		?>
	</div>
</div>*/?>


<form class="voitForm results">
	<br><br><p class="b-article-element-poll__title"><?=$poll->name?></p>
	<div class="voit _greybg">

			<?php
				foreach($poll->items as $item_id => $item) {
					if($poll->voices_count > 0)
						$percent = intval(round(($item->voices_count / $poll->voices_count) * 100, 2));
					else
						$percent = 0;
			?>

			<p>
				<span class="pc-ball"><?= $percent ?></span><?=$item->name?>
			</p>
			<?php
				}
			?>

			<div class="result">Ваш голос принят. Всего проголосовало <span><?= $poll->voices_count ?></span> человек</div>
	</div>

</form>