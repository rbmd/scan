<div class="poll_block" id="poll_<?=$poll->id?>">
<?
if( $poll->showResultsToUser() )
{
	$this->controller->renderPartial('application.widgets.polls.views.results.poll_chtpz_results', array('poll'=>$poll, 'light' => (!empty($light) && $light == true)) );

} else {
?>
	<div class="big-card poll">
		<div class="card-content">

			<div class="desc">
				<form class="poll-form" id="form_poll_<?=$poll->id?>" action="" method="POST">
					<?if($poll->type == 'select'){?>
						<?
						foreach($poll->items as $item_id => $item) {
							?>
							<div class="row">
								<label>
									<input type="radio" name="answers[]" value="<?=$item_id?>"/>
									<span class="l">
										<?=$item->name?>
									</span>
								</label>
							</div>
						<? 
						} 
						?>
					<?} else {?>
						<?
						foreach($poll->items as $item_id => $item) {
							?>
							<div class="row">
								<label>
									<input type="checkbox" name="answers[]" value="<?=$item_id?>"/>
									<span class="l">
										<?=$item->name?>
									</span>
								</label>
							</div>
						<? 
						} 
						?>			
					<?}?>					
					<div class="row">
						<input type="hidden" name="pollFromRightCol" value="1" />
						<input class="vote active" id="pollSubmit<?=$poll->id?>" type="button" value="Голосовать!" />
					</div>
				</form>
			</div>
		</div>
	</div>








<?
}
?>
</div>

<script>
	$('#pollSubmit<?=$poll->id?>').click(function(){
		var answers = $('#form_poll_<?=$poll->id?> input').serialize();
		$.ajax({
			url: "/polls/vote/<?=$poll->id?>/" + "<?= !empty($light) ? '?light=true' : '' ?>",
			dataType: 'json',
			data: answers,
			beforeSend: function(){
				
			},
			success: function(data){
				$('#poll_<?=$poll->id?>').html(data.poll_results);
			}
		});					
	});
</script>