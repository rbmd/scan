<?
// Tag list (for `/guides` and `/how-to`)
class TaglistWidget extends CWidget
{
	public $type;

	public function run()
	{
		$items = Tags::model()->isGuideTag()->findAll(array('limit' => 15));


		$this->render('index', array('items' => $items, 'type' => $this->type));
	}
}