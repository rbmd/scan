<?php
switch ($type)
{
	case 'guides':
		$color = 'g-color-guide';
		break;
	case 'howto':
		$color = 'g-color-howto';
		break;
	default:
		$color = 'g-color-article';
		break;
}
?>
<ul class="b-tag-list <?=$color?> g-color-border g-only-w-1200">
	<li class="b-tag-list__head g-font-small-1 g-color-text g-color-border">Теги</li>
	<?php
	if (isset($items))
	{
		foreach ($items as $item)
		{
			if ($type == 'guides')
			{
				$count = $item->count_guides;
				$link_prefs = '?only_guides=1';
			}
			else
			{
				$count = $item->count;
				$link_prefs = '';
			}
			?>
			<li class="b-tag-list__item g-highlight-bg g-font-small-3">
				<a href="<?=$item->url() . $link_prefs?>"><span class="b-tag-list__item__label"><?=$item->name?></span><span class="b-tag-list__item__counter g-color-text"><?=$count?></span></a>
			</li>
			<?php
		}
	}
	?>
</ul>