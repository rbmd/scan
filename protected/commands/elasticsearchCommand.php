<?
// php /home/kp1.codemedia.ru/www/protected/console.php elasticsearch update
class elasticsearchCommand extends CConsoleCommand
{
	public function actionUpdate()
	{
		echo "elasticsearchCommand->actionUpdate started...\r\n";

		$enrq = ElasticNestedReindexQueue::model()->findAll();
		foreach ($enrq as $e) {
			$e->apply();
		}

		echo "elasticsearchCommand->actionUpdate finished\r\n";
	}



	public function actionScheduled()
	{
		echo "elasticsearchCommand->actionScheduled started...\r\n";

		$enrq = ElasticScheduledReindexQueue::model()->findAll(array('condition' => "reindex_date <= '" . date('Y-m-d H:i:s') . "'" ));
		foreach ($enrq as $e) {
			$e->apply();
		}

		echo "elasticsearchCommand->actionScheduled finished\r\n";
	}
}