<?php
/**
 * User: Русаков Дмитрий
 * Date: 30.05.12
 * Time: 18:38
 *
 * Комманда для генерации RSS файлов 
 * Запускать так: /usr/bin/php /home/kp.codemedia.ru/www/protected/console.php rss update
 * Запускать по крону 1 раз в 5 минут
 */


// TODO: Вообще не совсем кошерно хранить в Articles метод rssNews для получения ссылки на RSS
// и для получения места хранения XML на серве
//
// Эту область ответственности стоит всеже возложить на сам процесс экспорта, на rssCommand


class rssCommand extends CConsoleCommand
{
	/**
	 * Количество последних сущностей, которое будем отдавать в RSS
	 */
	const ITEMS_FOR_RSS_COUNT        = 30;
	const ITEMS_FOR_RSS_GOOGLE_COUNT = 5;

	private $siteTitle = 'Кавказская политика';
	private $siteUrl = 'http://kavpolit.com';
	private $siteHostname = 'kavpolit.com';

	/*
	 * Обновление RSS
	 */
	public function actionUpdate()
	{
		//Yii::log("RSS started...", CLogger::LEVEL_ERROR);

		echo "rss->update started...\r\n";

		$rss_dir = Yii::getPathOfAlias('webroot') . '/../export/rss';

		//Получаем все новости
		$news = Articles::model()->visible()->recently(self::ITEMS_FOR_RSS_COUNT)->findAll();

		$title       = $this->siteTitle.' — Все материалы';
		$link        = $this->siteUrl.'/export/rss/news.xml';
		$description = $this->siteTitle.' — Все материалы';
		$path        = $rss_dir . '/news.xml';

		//Генерируем RSS новостей
		$this->generateRss($news, $title, $link, $description, $path);


		// RSS для разделов
		$arTree = Tree::model()->findByPk(1)->descendants()->findAllByPk(array_keys(Tree::model()->tree_nodes_for_articles));
		foreach ($arTree as $tree)
		{
			$title       = $this->siteTitle.' — ' . $tree['name'];
			$link        = $this->siteUrl.'/export/rss/' . $tree['code'] . '.xml';
			$description = $title;
			$path        = $rss_dir . '/' . $tree['code'] . '.xml';
			$feed        = Articles::model()->visible()->recently(self::ITEMS_FOR_RSS_COUNT)->findAll('parent_id=:parent_id', array(':parent_id' => $tree['id']));
			$this->generateRss($feed, $title, $link, $description, $path);
		}

		


		$items = Articles::model()->visible()->toRss()->with(array('chapter' => array('condition' => 'order_num=1'),))->recently(self::ITEMS_FOR_RSS_COUNT)->findAll();


		// RSS для новостей Yandex, там свой формат с секцией для твиттер трансляции
		$path = $rss_dir . '/yandex.xml';
		$link = $this->siteUrl.'/export/rss/yandex.xml';
		$this->generateYandexRss($items, $this->siteHostname, $link, $this->siteTitle, $path);


		// RSS для Рамблера, используем туже выборку, что и для Яндекс
		$path = $rss_dir . '/rambler.xml';
		$link = $this->siteUrl.'/export/rss/rambler.xml';
		$this->generateRss($items, $this->siteHostname, $link, $this->siteTitle, $path);

		// RSS для Google
		$google_items = Articles::model()->visible()->toRss()->with(array('chapter' => array('condition' => 'order_num=1'),))->with('authors')->recently(self::ITEMS_FOR_RSS_GOOGLE_COUNT)->findAll();
		$path         = $rss_dir . '/google.xml';
		$link         = $this->siteUrl.'/export/rss/google.xml';
		$this->generateGoogleRss($google_items, $this->siteHostname, $link, $this->siteTitle, $path);


		// RSS для блогов
		/*$path = $rss_dir . '/blogs/all.xml';
		$link = 'http://bg.ru/export/rss/blogs/all.xml';
		$this->generateBlogsRss('Районные блоги большого города', $link, 'Большой Город', $path);
		*/
		echo "\r\nrss->update finished.\n\r";
	}


	private function generateBlogsRss($title, $link, $description, $path)
	{
		/*
		Yii::app()->setImport(array(
			'application.modules.blogs.models.*'
		));
		$result = '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"></rss>';
		$result = simplexml_load_string($result);

		$result->channel->title = $title;
		$result->channel->link  = $link;

		$result->channel->addChild('description');
		$dom   = dom_import_simplexml($result->channel->description);
		$cdata = $dom->ownerDocument->createCDATASection($description);
		$dom->appendChild($cdata);

		$result->channel->lastBuildDate = date('r');

		$criteria            = new CDbCriteria();
		$criteria->order     = 'date DESC';
		$criteria->condition = 'date <= NOW() AND status="public" AND is_delete=0';
		$criteria->limit     = 20;

		$items = BlogsPost::model()->findAll($criteria);

		foreach ($items as $item)
		{
			$entity = $result->channel->addChild('item');

			$entity->addChild('title');
			$dom   = dom_import_simplexml($entity->title);
			$cdata = $dom->ownerDocument->createCDATASection($item->title);
			$dom->appendChild($cdata);

			$entity->link = Yii::app()->params['baseUrl'] . '/blogs/posts/' . $item->id . '/?utm_source=rss&utm_medium=rss&utm_campaign=blogs';
			$entity->guid = $entity->link;

			$img = Yii::app()->params['baseUrl'] . $item->getImage('full', $item);

			$entity->addChild('description');
			$dom = dom_import_simplexml($entity->description);

			$htmlImg = '<p><img src="' . $img . '" alt=""/></p>';

			$cdata = $dom->ownerDocument->createCDATASection($htmlImg . $this->prepareText($item->text));
			$dom->appendChild($cdata);

			$entity->pubDate = date('r', strtotime($item->date));

			$enclosure = $entity->addChild('enclosure');
			$enclosure->addAttribute('url', $img);
			$enclosure->addAttribute('length', '');
			$enclosure->addAttribute('type', 'image/jpeg');
		}

		$result->asXML($path);
		*/
	}

	/**
	 * Генерация RSS из списка статей
	 *
	 * @param $items - список статей
	 * @param $title - заголовок RSS-канала
	 * @param $link - ссылка на RSS-канал
	 * @param $description - описание RSS-канала
	 * @param $path - путь до файла куда записать RSS файл на сервере
	 */
	private function generateRss($items, $title, $link, $description, $path)
	{
		$result = '<?xml version="1.0" encoding="utf-8"?><rss version="2.0"></rss>';

		$result = simplexml_load_string($result);


		$result->channel->title = $title;
		$result->channel->link  = $link;

		//$result->channel->description = $description;
		$result->channel->addChild('description');
		$dom   = dom_import_simplexml($result->channel->description);
		$cdata = $dom->ownerDocument->createCDATASection($description);
		$dom->appendChild($cdata);

		$result->channel->lastBuildDate = date('r');

		foreach ($items as $item)
		{
			$entity = $result->channel->addChild('item');

			$entity->addChild('title');
			$dom   = dom_import_simplexml($entity->title);
			$cdata = $dom->ownerDocument->createCDATASection($item->name);
			$dom->appendChild($cdata);

			$entity->link = $this->getUrl($item->url()) . '?utm_source=rss&utm_medium=rss&utm_campaign=basic';
			$entity->guid = $entity->link;

			$entity->addChild('description');
			$dom   = dom_import_simplexml($entity->description);
			$cdata = $dom->ownerDocument->createCDATASection($this->prepareText($item->preview_text));
			$dom->appendChild($cdata);

			$entity->pubDate = date('r', strtotime($item->date_active_start));

			$enclosure = $entity->addChild('enclosure');

			$img = ($item->preview_img) ? $item->img('320x180') : '/s/i/logo_beta.png';

			$enclosure->addAttribute('url', Yii::app()->params['baseUrl'] . $img);
			$enclosure->addAttribute('length', '');
			$enclosure->addAttribute('type', 'image/jpeg');
		}

		$result->asXML($path);
	}

	/**
	 * Генерация RSS из списка статей для Яндекса
	 *
	 * @param $items - список статей
	 * @param $title - заголовок RSS-канала
	 * @param $link - ссылка на RSS-канал
	 * @param $description - описание RSS-канала
	 * @param $path - путь до файла куда записать RSS файл на сервере
	 */
	private function generateYandexRss($items, $title, $link, $description, $path)
	{
		$result = '<?xml version="1.0" encoding="utf-8"?><rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" version="2.0"></rss>';

		$result = simplexml_load_string($result);


		$result->channel->title = $title;
		$result->channel->link  = $link;

		$result->channel->addChild('description');
		$dom   = dom_import_simplexml($result->channel->description);
		$cdata = $dom->ownerDocument->createCDATASection($description);
		$dom->appendChild($cdata);

		$datetime              = date_create(date('Y-m-d H:i:s'));
		$result->lastBuildDate = date_format($datetime, 'r');

		$image        = $result->channel->addChild('image');
		$image->title = $title;
		$image->url   = Yii::app()->params['baseUrl'] . '/s/i/logo_beta.png';
		$image->link  = Yii::app()->params['baseUrl'];


		foreach ($items as $item)
		{
			$entity = $result->channel->addChild('item');

			$entity->addChild('title');
			$dom   = dom_import_simplexml($entity->title);
			$cdata = $dom->ownerDocument->createCDATASection($item->name);
			$dom->appendChild($cdata);

			$entity->link = $this->getUrl($item->url()) . '?utm_source=rss&utm_medium=rss&utm_campaign=yandex';

			$img = ($item->preview_img) ? $item->img('320x180') : '/s/i/logo_beta.png';

			//$entity->{'media:group'}->{'media:thumbnail'}['url'] = Yii::app()->params['baseUrl'] . $img;
			$entity->{'media:content'}['url'] = Yii::app()->params['baseUrl'] . $img;
			

			$entity->{'yandex:full-text'} = '';
			$dom                          = dom_import_simplexml($entity->{'yandex:full-text'});
			$cdata                        = $dom->ownerDocument->createCDATASection($this->prepareText($item->chapter->detail_text));
			$dom->appendChild($cdata);

			$entity->pubDate = date('r', strtotime($item->date_publish));


		}

		$result->asXML($path);
	}


	/**
	 * Генерация RSS из списка статей для Google
	 *
	 * @param $items - список статей
	 * @param $title - заголовок RSS-канала
	 * @param $link - ссылка на RSS-канал
	 * @param $description - описание RSS-канала
	 * @param $path - путь до файла куда записать RSS файл на сервере
	 */
	private function generateGoogleRss($items, $title, $link, $description, $path)
	{
		$result = '<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0" xmlns:atom10=\'http://www.w3.org/2005/Atom\'></rss>';
		$result = simplexml_load_string($result);

		$result->channel->link = $link;

		$result->channel->addChild('description');
		$dom   = dom_import_simplexml($result->channel->description);
		$cdata = $dom->ownerDocument->createCDATASection($description);
		$dom->appendChild($cdata);

		$result->channel->title = $title;

		$image        = $result->channel->addChild('image');
		$image->title = $title;
		$image->url   = Yii::app()->params['baseUrl'] . '/s/i/logo_beta.png';
		$image->link  = Yii::app()->params['baseUrl'];

		foreach ($items as $item)
		{
			$entity        = $result->channel->addChild('item');
			$entity->title = $item->name;
			$entity->link  = $this->getUrl($item->url()) . '?utm_source=rss&utm_medium=rss&utm_campaign=google';

			$entity->{'description'} = '';
			$dom                     = dom_import_simplexml($entity->{'description'});
			$cdata                   = $dom->ownerDocument->createCDATASection($this->prepareText($item->chapter->detail_text));
			$dom->appendChild($cdata);

			$author_list = array();
			foreach ($item->authors as $authors)
			{
				$author_list[] = $authors->name . ' ' . $authors->surname;
			}
			$entity->author = implode(', ', $author_list);
		}
		$result->asXML($path);
	}


	public function prepareText($str)
	{
		// TODO: оптипизировать
		$str = strip_tags(preg_replace('#<style.+?</style>#s', ' ', $str));
		$str = strip_tags(preg_replace('#<script.+?</script>#s', ' ', $str));
		$str = preg_replace('# +#', ' ', preg_replace("#\n|\r|\t#", ' ', $str));
		return $str;
	}

	public function getUrl($url)
	{
		if (substr($url, 0, 4) == 'http')
		{
			return $url;
		}
		else
		{
			return $this->siteUrl.$url;
		}
	}
}