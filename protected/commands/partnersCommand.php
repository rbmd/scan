<?php
// /usr/local/bin/php /home/n1.bg.ru/www/protected/console.php partners update
// Import last news from slon.ru and tvrain.ru
class partnersCommand extends CConsoleCommand
{
    public $path = '';


    public function actionUpdate()
    {
        $this->path = Yii::getPathOfAlias('webroot') . '/../export/commands/partners';
        if (!is_dir($this->path))
        {
            mkdir($this->path, 0775, true);
        }
        $this->cityboom();
        $this->tvrain();
        $this->slon();
    }


    public function cityboom()
    {
        $xml = simplexml_load_file('http://yopolis.ru/feed/organizationNews/1?type=rss');
        $content = '';
        if ($xml) {
            $content .= '<div class="b-cityboom-news">
                     <div class="b-cityboom-logo"></div>
                     <ul class="b-cityboom-news-list">';

            foreach ($xml->channel->item as $item) {
                $date = date_format(new DateTime($item->pubDate), 'd.m.Y');
                $content .= '<li class="b-cityboom-news-list__item">
                    <div class="b-cityboom-news-list__date g-bigcity">' . $date . '</div>
                    <a class="b-cityboom-news-list__link" target="_blank" href="' . $item->link . '">
                      ' . $item->title . '
                    </a>
                  </li>';
            }
            $content .= '</ul>
                   </div>
                   <link rel="stylesheet" type="text/css" href="/static/css/pub/cityboom.css"/>';
        }
        file_put_contents($this->path . '/cityboom.html', $content);
    }


    public function tvrain()
    {
        $src = @file_get_contents('http://tvrain.ru/api/teleshow/articleslist/?teleshow_id=1804&page=1&limit=4');
        $tvrain = json_decode($src);
        $content = '';
        if (isset($tvrain->data) && count($tvrain->data) > 0) {
            ob_start();
            ?>
            <div class="b-partner">
                <a href="http://tvrain.ru/" target="_blank" rel="nofollow">
                    <img src="/static/css/pub/i/b-partner__logo_tvrain.png" alt="" class="b-partner__logo">
                </a>
                <i class="b-partner__line"></i>
                <?php foreach ($tvrain->data as $i => $item) : ?>
                    <div class="b-partner__item">
                        <table class="b-partner__item__layout">
                            <tbody>
                            <tr>
                                <td class="b-partner__item__image-handler">
                                    <?
                                    if (strlen($item->image) > 0) {
                                        ?>
                                        <a href="<?= $item->url ?>" target="_blank">
                                            <img src="<?= $item->image ?>" alt="" class="b-partner__item__image"/>
                                        </a>
                                    <?
                                    } else {
                                        ?>
                                        &nbsp;
                                    <?
                                    }
                                    ?>
                                </td>
                                <td class="b-partner__item__info-handler">
                                    <? /*<span class="b-partner__item__publish-date"><?=$item_date?></span>*/ ?>
                                    <a href="<?= $item->url ?>" target="_blank"
                                       class="b-partner__item__title"><?= $item->name ?></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endforeach; ?>
            </div>
            <?
            $content = ob_get_clean();
        }
        file_put_contents($this->path . '/tvrain.html', $content);
    }


    public function slon()
    {
        $src = @file_get_contents('http://slon.ru/export/tvrain.ru/themes2.php');
        $slon = json_decode($src);
        $content = '';
        if (count($slon) > 0) {
            ob_start();
            ?>
            <div class="b-partner">
                <a href="http://slon.ru/" target="_blank" rel="nofollow">
                    <img src="/static/css/pub/i/b-partner__logo_slon.png" alt="" class="b-partner__logo">
                </a>
                <i class="b-partner__line"></i>
                <?
                foreach ($slon as $i => $item) {
                    $item_date = date('d.m.Y H:i', strtotime($item->date_active_start));
                    ?>
                    <div class="b-partner__item">
                        <table class="b-partner__item__layout">
                            <tbody>
                            <tr>
                                <td class="b-partner__item__image-handler">
                                    <?
                                    if (strlen($item->image) > 0) {
                                        ?>
                                        <a href="http://slon.ru<?= $item->link ?>" target="_blank">
                                            <img src="http://slon.ru<?= $item->image ?>" alt=""
                                                 class="b-partner__item__image"/>
                                        </a>
                                    <?
                                    } else {
                                        ?>
                                        &nbsp;
                                    <?
                                    }
                                    ?>
                                </td>
                                <td class="b-partner__item__info-handler">
                                    <span class="b-partner__item__publish-date"><?= $item_date ?></span>
                                    <a href="http://slon.ru<?= $item->link ?>" target="_blank"
                                       class="b-partner__item__title"><?= $item->name ?></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                <?
                }
                ?>
            </div>
            <?
            $content = ob_get_clean();
        }
        file_put_contents($this->path . '/slon.html', $content);
    }
}