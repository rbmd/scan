<?php
/**
 * Генерация sitemap.xml
 *
 * Запускать так: /usr/local/bin/php /home/kp1.codemedia.ru/www/protected/console.php sitemap update
 *
 * User: Дмитрий Русаков
 * Date: 18.04.12
 * Time: 18:30
 */

class sitemapCommand extends CConsoleCommand
{
    /**
     * Адресс tv-rain'а
     */
    const HTTP_HOST = 'http://kavpolit.com';

    /**
     * Директория куда генерим sitemap
     */
    const PATH_TO_SITEMAP_XML = '/sitemap/xml';

    /**
     * Максимальное количество строк сущности(например Articles), которые выбираем из базы для генерации sitemap ЗА ОДИН РАЗ!
     * Если выборку за один раз не ограничить, то, скажем, попытка выборки из Articles падает по нехватке памятми.
     * Так как в Articles слишком большое количество записей.
     */
    const MAX_ROWS_BY_PASS = 1000;

    /*
     * Генерируем sitemap.xml для всего сайта
     */
    public function actionUpdate()
    {
        echo "sitemap.xml->update started...\n\r";

        $parts = array();

        /*ВСЕ МОДЕЛИ РАЗДЕЛОВ ДЛЯ ГЕНЕРАЦИИ SITEMAP.XML ДОЛЖНЫ СОДЕРЖАТЬ СКОУП VISIBLE()*/
        $parts[] = self::_generate_part_xml( new Articles() );
        $parts[] = self::_generate_part_xml( new Tags() );
        $parts[] = self::_generate_part_xml( new Tree() );
        $parts[] = self::_generate_part_xml( new Authors() );
        $parts[] = self::_generate_part_xml( new Blog() );
        $parts[] = self::_generate_part_xml( new Post() );

        self::_generate_common_xml($parts);

        echo "\n\rsitemap.xml->update finished.\n\r";
    }

    /**
     * Сгенерировать для определённого раздела сайта свой xml-sitemap файл
     * Результат записывается в файл, имя которого == имени класса в нижнем регистре
     *
     * @static
     * @param $model - на входе объект модели нужного раздела сайта
     * @return string - на выходе имя файла, куда записали url's
     */
    private static function _generate_part_xml($model)
    {
		 $criteria        = new CDbCriteria();
         if (get_class($model) == 'Blog' || get_class($model) == 'Post')
         {
                 $criteria->order = 't.date_create DESC';
         }
         else
         {
		      $criteria->order = 'x_timestamp DESC';
        }
		 //$criteria->offset = $i * self::MAX_ROWS_BY_PASS;
		 $criteria->limit  = self::MAX_ROWS_BY_PASS;
		 echo "--->", get_class($model), "\r\n";

        $result = '<?xml version="1.0" encoding="UTF-8"?><urlset></urlset>';

        $result = simplexml_load_string($result);

        $result['xmlns'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';

        $i = 0;

        //Выбираем из сущности по self::MAX_ROWS_BY_PASS записей и добавляем их в sitemap
        //И так пока, записи в сущности не закончатся.
        //Если сразу выбирать все записи, то скрипт падает по нехватке памяти
        do{
			  $model->visible();

              if (get_class($model) == 'Post')
              {
                 $model->with(array('blog'));
              }

			  if (get_class($model) == 'Articles')
			  {
				  $select = 'id, parent_id, code, x_timestamp';
			  }
			  elseif (get_class($model) == 'Tree')
			  {
				  $select = 'id, code, x_timestamp, url';
				  $model->getLevel(2);
				  $criteria->condition = 'code != "distribution" AND code !="how-to"';
			  }
              else if (get_class($model) == 'Blog')
              {
                  $select = 'id, code, t.date_create';
              }
              else if (get_class($model) == 'Post')
              {
                  $select = 'id, t.date_create';
              }
			  else
			  {
				  $select = 'id, code, x_timestamp';
			  }

			  $criteria->select = $select;

           $items = $model->findAll($criteria);

            ++$i;

            foreach($items as $item)
            {
                $url = $result->addChild('url');

                
                if (get_class($model) == 'Blog' || get_class($model) == 'Post')
                {
                    $url->addChild('loc', $item->url());
                    $url->addChild('lastmod', self::date_to_atom($item->date_create));
                }
                else
                { 
                    $url->addChild('loc', self::HTTP_HOST . $item->url());
                    $url->addChild('lastmod', self::date_to_atom($item->x_timestamp));
                }
            }

            $items = null; //Надеюсь, что это вызовет сборщик мусора!!!
            unset( $items );
        }
        while( !empty($items) );

        $filename = mb_strtolower(get_class($model)) . '.xml';

        $result->asXML(Yii::getPathOfAlias('webroot') . '/..' . self::PATH_TO_SITEMAP_XML . '/' . $filename);

        return $filename;
    }

    private static function _generate_common_xml(array $items)
    {
        echo "--->", "common_xml", "\r\n";

        $result = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex></sitemapindex>';

        $result = simplexml_load_string($result);

        $result['xmlns'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';

        foreach($items as $item)
        {
            $url = $result->addChild('sitemap');

            $url->addChild('loc', self::HTTP_HOST . self::PATH_TO_SITEMAP_XML . '/' . $item);
            $url->addChild('lastmod', date('Y-m-d'));
        }

        $result->asXML(Yii::getPathOfAlias('webroot') . '/..' . self::PATH_TO_SITEMAP_XML . '/sitemap.xml');
    }

    /**
     * Функция преобразования даты в нужный формат для sitemap.xml
     *
     * @static
     * @param $date - дата в формате Mysql
     */
    private static function date_to_atom($date){
        $tmp = date_create($date);
        return date_format($tmp, DATE_ATOM);
    }
}