<?
// php /home/kp1.codemedia.ru/www/protected/console.php users updaterating
class UsersCommand extends CConsoleCommand
{
	public function actionUpdateRating()
	{
		$users = User::model()->active()->findAll();

		echo "UsersCommand->actionUpdateRating started...\r\n";

		foreach ($users as $u)
		{
			$u->updateRating();
		}

		echo "UsersCommand->actionUpdateRating finished\r\n";
	}
}