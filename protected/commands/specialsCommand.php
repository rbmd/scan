<?php
// Запускать так: php /home/bg.ru/www/protected/console.php specials all
class specialsCommand extends RenderReadyConsoleCommand
{
	public function actionAll()
	{
		$this->actionResolutions();
	}


	// Рассылка напоминаний о новогодних обещаниях
	public function actionResolutions()
	{
		$where  = 'send=0 AND CURRENT_TIMESTAMP() > time + INTERVAL 6 MONTH';
		$table  = 'specprojects_resolutions_reminder';
		$emails = Yii::app()->db->createCommand()
			->selectDistinct('email')
			->from($table)
			->where($where)
			->queryColumn();

		if ($emails)
		{
			foreach ($emails as $email)
			{
				$email = trim($email);

				$aText = Yii::app()->db->createCommand()
					->selectDistinct('text')
					->from($table)
					->where($where . ' AND email=:email', array('email' => $email))
					->queryColumn();
				if ($aText)
				{
					echo $email . "\n";
					$subj  = 'Обещания «Большого города»';
					$text  = $this->renderPartial('specials_resolutions', array('list' => $aText), true);
					//$email = '771067@gmail.com';
					Yii::app()->mail->send($email, $subj, $text);
					Yii::app()->db->createCommand()->update($table, array('send' => 1), $where . ' AND email=:email', array('email' => $email));
				}
			}
		}
	}
}