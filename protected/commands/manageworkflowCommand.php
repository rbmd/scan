<?php
/**
 * Чистим блокировки на статьи, чтобы исключить ситуацию с мертвяками
 * Запускать так: /usr/local/bin/php /home/n.tvrain.ru/www/protected/console.php manageworkflow deleteoldlocks
 *
 * User: Дмитрий Русаков
 */
class manageworkflowCommand extends CConsoleCommand
{
	/*
	  * Время жизни блокировки статьи
	  */
	const LOCKS_LIFE_TIME_IN_MINUTES = 10;

	public function actionDeleteOldLocks()
	{
		echo "manageworkflowlocks->deleteoldlocks started...\n\r";
		$items    = WorkflowLock::model()->findAll('NOW() - INTERVAL :interval MINUTE >= lock_date', array(':interval' => self::LOCKS_LIFE_TIME_IN_MINUTES));
		$rowCount = 0;
		foreach ($items as $item)
		{
			WorkflowLock::breakLock($item->article_id);
			$rowCount++;
		}
		echo "manageworkflowlocks->deleteoldlocks finished. $rowCount locks was deleted!\n\r";
	}
}
