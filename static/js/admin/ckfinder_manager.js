var ckfinder_target = '';
ckfinderManager = new function() {
	this.init = function init() {}

	/**
	 * @param target - textarea id
	 */
	this.create = function(target) {
		
		var input = '<input class="dynamic btn" onclick="ckfinderManager.browseFinder(\'' + target + '\', \'/3rdparty/ckfinder/ckfinder.html?action=js&skin=kama&func=SetFileField&data=' + target + '&thumbFunc=SetFileField&tdata=' + target + '\'); return false;" name="yt0" type="button" value="Выбрать файл">';

		$('#' + target).after(input);
	}


	// показать finder
	this.browseFinder = function(target, back_url) {
		ckfinder_target = target;
		$('.popup-window .wrap').html('');
		$('.milk').fadeIn(500);
		$('.popup-window').show();
		data = '<iframe src="' + back_url + '" width="100%" height="500" frameborder="0" scrolling="no"></iframe>';
		$('.popup-window .wrap').html(data);
		$('.popup-window').width(800);
		realignPopup();
		
	}
}

/*This is a sample function which is called when a file is selected in CKFinder.*/
function SetFileField(fileUrl, data) {
	$('#' + ckfinder_target).val(fileUrl).focus();
	$('.milk').trigger('click');
}

