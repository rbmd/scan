//Отключаем авто-инлайн у редактора
CKEDITOR.disableAutoInline = true;
CKEDITOR.config.customConfig = '/static/js/admin/ckeditor/config.js';

/*ФУНКЦИИ ДЛЯ РАБОТЫ С WYSIWYG*/
wysiwygManager = new function() {
	//Количество запросов на создание редактора
	var ckeditor_instances;

	//Количество уже созданные редакторов
	var ckeditor_instances_ready;

	//Инстанс в процессе создания
	var ckeditor_creating;
	

	this.init = function init() {
		this.ckeditor_instances = 0;
		this.ckeditor_instances_ready = 0;
		this.ckeditor_creating = false;
	}


	//Уничтожает редакторы при перемещении элементов
	this.destroyEditor = function(element) {
		$(element).find('.wysiwyg_editor').each(function() {
			if (CKEDITOR.instances[$(this).attr('id')])
				CKEDITOR.instances[$(this).attr('id')].destroy(false);
		});
	}


	//Создает новый редактор, используется при сортировке элементов между собой
	this.replaceEditor = function(element) {
		$(element).find('.wysiwyg_editor').each(function() {
			if (!CKEDITOR.instances[$(this).attr('id')])
				wysiwygManager.createEditor($(this).attr('id'), {config: $(this).attr('data-wysiwyg_config'), inlineEditor: $(this).attr('data-wysiwyg_inline') } );
		});
	}

	//Создает редактор с заданым конфигом
	/*
	 * @param target - textarea id
	 * @param params.config - название конфига (mini.js, config.js и т.п.)
	 * @param params.inlineEditor - inline редактор(назначается на div) или обычный(на textarea)
	 * @param innerCall - если true, то не будет увеличен счетчик wysiwygManager.ckeditor_instances
	 */
	this.createEditor = function(target, params, innerCall) {
	
		//console.log(target);
		if(params.config == 'undefined')
			config = '/static/js/admin/ckeditor/config.js';
		else
			config = '/static/js/admin/ckeditor/' + params.config;

		//Увеличиваем число запросов на создание визредактора
		//if (!innerCall)
			//wysiwygManager.ckeditor_instances++;
		if (wysiwygManager.ckeditor_creating == false)
		{
			//Если инлайн редактор, то создаем другим конструктором
			if (params.inlineEditor == true || params.inlineEditor == "true")
				var neweditor = CKEDITOR.inline(target, {config: config, customConfig: config});
			else
				var neweditor = CKEDITOR.replace(target, {customConfig : config});

			//Подключаем ckfinder
			CKFinder.setupCKEditor(neweditor, '/3rdparty/ckfinder/');
				
			//Отмечаем, что визред в процессе создания
			wysiwygManager.ckeditor_creating = true;	
		}	
		//В другом случае берем таймаут и опять вызываем создание редактора
		else
		{
			setTimeout(function() {
				wysiwygManager.createEditor(target, params, true)
			}, 300);
		}
	}

	//Возвращает чистую длину текста, без учета html
	this.getCleanDataLength = function(editor) {
		return editor.getData()
			.replace(/<[^>]*>/g, '')
			.replace(/\s+/g, ' ')
			.replace(/&\w+;/g, 'X')
			.replace(/^\s*/g, '')
			.replace(/\s*$/g, '')
			.length;
	}
	

	this.init();
}

//Каждый раз при завершении создания визредактора увеличиваем счетчик созданных инстансев
CKEDITOR.on('instanceReady', function(e) {
	//console.log(wysiwygManager.ckeditor_instances + ': ' + wysiwygManager.ckeditor_instances_ready);

	//Количество уже созданных визредов
	//wysiwygManager.ckeditor_instances_ready++;

	//Процесс инстанцирования завершился => снимаем флаг
	wysiwygManager.ckeditor_creating = false;	
	
	//Если количество запрошенных на создание и созданных сравнивается, можно скрыть див загрузки
	//if (wysiwygManager.ckeditor_instances == wysiwygManager.ckeditor_instances_ready)
	{
		$('#wysiwyg_is_loading').hide();
	}
});