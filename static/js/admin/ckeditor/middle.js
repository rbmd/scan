var codemirror_rootpath = '/static/js/admin/';

CKEDITOR.editorConfig = function(config) {

	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.skin = 'v2';
	config.contentsCss = CKEDITOR.basePath + 'content.css';
	// config.enterMode = CKEDITOR.ENTER_BR;
	config.docType = '<!DOCTYPE html>';
	config.autoParagraph = false;
	config.fillEmptyBlocks = false;
	config.enableTabKeyTools = true;
	config.format_tags = 'p;h1;h2;h3';

	//чтобы insert вставлял в <p>, а не <div>
	//config.forceEnterMode=true;	
	config.forcePasteAsPlainText = true;

	config.extraPlugins = 'onchange';
	
	config.height = 60;
	config.toolbar =[{ name: 'document', items: [ 'Source', '-', 'Bold', 'Italic', 'Subscript', 'Superscript', '-', 'RemoveFormat', 'Link', 'Unlink' ]}];
	config.keystrokes =
	[
	    [ CKEDITOR.ALT + 121 /*F10*/, 'toolbarFocus' ],
	    [ CKEDITOR.ALT + 122 /*F11*/, 'elementsPathFocus' ],
	    [ CKEDITOR.SHIFT + 121 /*F10*/, 'contextMenu' ],
	    [ CKEDITOR.CTRL + 90 /*Z*/, 'undo' ],
	    [ CKEDITOR.CTRL + 89 /*Y*/, 'redo' ],
	    [ CKEDITOR.CTRL + CKEDITOR.SHIFT + 90 /*Z*/, 'redo' ],
	    [ CKEDITOR.CTRL + 76 /*L*/, 'link' ],
	    [ CKEDITOR.CTRL + 66 /*B*/, 'bold' ],
	  //[ CKEDITOR.ALT + 109 /*-*/, 'toolbarCollapse' ]
	    [ CKEDITOR.CTRL + 73 /*I*/, 'italic' ]
	];
};


CKEDITOR.on('instanceReady', function(ev) {
	var generic = {
		indent          : true,
		breakBeforeOpen : true,
		breakAfterOpen  : true,
		breakBeforeClose: true,
		breakAfterClose : true
	}
	ev.editor.dataProcessor.writer.setRules('p', generic);
	ev.editor.dataProcessor.writer.setRules('td', generic);
});
