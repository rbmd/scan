var tpl_sortable_item = '\
					<tr data-id="$id"> \
						<td> \
							<i class="icon-move"></i> \
						</td> \
						<td> \
							<a href="/admin/$sortableModel/edit/?id=$id">$name</a> \
							<input value="$id" name="$attachedToModel[$variableName][]" id="$attachedToModel_$sortableModelids" type="hidden"> \
						</td> \
						<td> \
							<a href="" class="dynamic" data-action="detach-from-sortable" data-sortable-model="$sortableModel" data-id="$id" data-name="$name" data-attach-to-model="$attachedToModel" onclick="dynamicClick($(this)); return false;">убрать</a> \
						</td> \
					</tr>';

$(document).ready(function() {


	(function($) {

		$.fn.newsGallery = function(options) {

			return this.each(function() {
				var cont = $(this),
					o = $.extend({}, $.fn.newsGallery.defaults, options, cont.data('newsGallery')),
					frame = $('.frame', cont),
					pc = $('.photo-container', cont),
					pics = $('.pic', cont),
					active = $('.pic:nth-child(1)', cont), // by default the active pic is the first one
					prev = $('.prev', cont),
					next = $('.next', cont),
					w = o.frameWidth;
				
				init()
				function init() {
					// We don't need this if there's only one pic
					if (pics.length <= 1) {
						return false;
					}
				};

			});

		};

		$.fn.newsGallery.defaults = {
			frameWidth: 620
		};

	})(jQuery);

	initSyncTranslit();

	if ($('select[data-fcbkcomplete]').length > 0)
	{
		$('select[data-fcbkcomplete]').fcbkcomplete({
			json_url: '/admin/tags/gethint/',
			addontab: true,
			height: 20,
			cache: true,
			filter_selected: true,
			newel: false
		});
	}

	if ($('table.sortable tbody').length > 0)
	{
		$('table.sortable tbody').sortable({
			helper: function(e, tr)
			{
				var $originals = tr.children();
				var $helper = tr.clone();
				$helper.children().each(function(index)
				{
					$(this).width($originals.eq(index).width())
				});
				return $helper;
			}
		});
	}
	
	$('.milk').click(function() {
		$(this).fadeOut(500);
		$('.popup-window').hide();
		document.cookie = 'CK_beseUrl=; expires=Fri, 3 Aug 2001 20:47:11 UTC; path=/';
		return false;
	});

	$('#ajax-attach-paginator a:not(.clicked)').live('click', function(e) {
		$(this).attr('data-action', 'refresh-popup');
		dynamicClick($(this));
		$(this).addClass('clicked');
		e.preventDefault();
	});

	$('input[data-limit-length]').live("keyup", function()
	{
		var $inf = $('.add-on[data-limit-length='+$(this).data('limit-length')+']');
		$inf.html($(this).val().length)
	});

	$('textarea[data-limit-length]').live("keyup", function() {
		var $inf = $('.add-on[data-limit-length=' + $(this).data('limit-length') + ']');
		$inf.html($(this).val().length)
		if ($(this).val().length > 155)
		{
			$inf.attr("class", "alert alert-error add-on");
		}
		else $inf.attr("class", "alert alert-success add-on");
	});

	/*$('.nav.nav-pills a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})*/
});


function dynamicClick($obj) {

	var action = $obj.data('action');
	switch (action) {
		case 'show-popup':
			$('.popup-window .wrap').html('');
			$('.milk').fadeIn(500);
			$('.popup-window').show();

			var popupType = $obj.data('popup-type');
			var data = popupType == 'attach-models' || true ? { 'attachToModel': $obj.data('attach-to-model'), 'variableName': $obj.data('variable-name') } : {};
			$.get(
				$obj.attr('href'),
				data,
				function(data) {
					$('.popup-window .wrap').html(data);
					$('.popup-window').css('width', 'auto');
					if ($obj.data('min-width'))
						$('.popup-window').width($obj.data('min-width'));

					realignPopup();
					if (popupType == 'attach-models') {
						bindAttachActions();
					}
				}
			);
		break;

		case 'attach-to-sortable':
			try {
				attachToSortable(String($obj.data('name')), $obj.data('id'), $obj.data('attach-to-model'), $obj.data('sortable-model'), $obj.data('variable-name'));
				bindAttachActions();
				refreshSortable();
			} catch (e) {
				alert(e)
			}
		break;

		case 'detach-from-sortable':
			$obj.data('action', 'attach-to-sortable');
			$obj.text('прикрепить');
			$('table[data-sortable-model='+$obj.data('sortable-model')+'] tr[data-id="'+$obj.data('id')+'"]').remove();
		break;

		case 'refresh-popup':
			var popupType = $obj.data('popup-type');
			var data = popupType == 'attach-models' ? { 'attachToModel': $obj.data('attach-to-model'), 'filter': $('.popup-window input[name=filter]').val(), 'variableName': $obj.data('variable-name') } : {};
			$.get(
				$obj.attr('href'),
				data,
				function(data) {
					$('.popup-window .wrap').html(data);
					realignPopup();
					bindAttachActions();
				}
			);
		break;


		case 'trigger-lock':
			// $obj.text( $obj.text() == 'w' ? 'x' : 'w' );

			if ($obj.text() == 'w') {
				 $obj.text('x');
				 $obj.removeClass('icon-unlock').addClass('icon-lock');
			} else {
				 $obj.text('w');
				 $obj.removeClass('icon-lock').addClass('icon-unlock')
			}

			document.cookie = 'transliterate_lock_state['+encodeURIComponent(location.pathname+location.search)+']='+$obj.text()+'; expires=Fri, 3 Aug 2021 20:47:11 UTC; path=/';
			initSyncTranslit();
		break;


		case 'refresh-snatask':
			var old_text = $obj.text();
			$obj.text('Обновляем...');
			$.get(
				$obj.attr('href'),
				function(data) {
					console.log(data);
					$obj.text(old_text);
				}
			);
		break;

		case 'clean-snatask':

			if (confirm('Удалить все записи?'))
			{
				var old_text = $obj.text();
				$obj.text('Удаляем...');
				$.get(
					$obj.attr('href'),
					function(data) {
						$obj.text(old_text);
					}
				);
			}
		break;

		case 'clone-seo-from-video':

			var old_text = $obj.text();
			$obj.text('Секундочку...');
			$.getJSON(
				$obj.attr('href'),
				function(data) {
					$('#Articles_seo_title').val(data.seo_title);
					$('#Articles_seo_keywords').val(data.seo_keywords);
					$('#Articles_seo_description').val(data.seo_description);
					$obj.text(old_text);
				}
			);
		break;

		case 'load-from-tvigle':

			var old_text = $obj.text();
			$obj.text('Секундочку...');
			$.getJSON(
				$obj.attr('href') + $('#Videos_tvigle_id').val(),
				function(data) {
					$('#Videos_tvigle_swf').val(data.swf);
					$('#Videos_tvigle_picture').val(data.img);
					$('#Videos_tvigle_picture').val(data.img);
					$('#Videos_tvigle_duration').val(data.duration);
					$('#Videos_tvigle_frame').val(data.frame);
					$('#tvigle_pic').attr('src', data.img);
					$obj.text(old_text);
				}
			);
		break;

		case 'toggle-snaitem':
			$.get(
				$obj.attr('href'),
				function(data) {
					$obj.replaceWith(data);
				}
			);
		break;

	}
}

function attachToSortable(name, id, attachedToModel, sortableModel, variableName)
{
	var $table = $('table[data-sortable-model='+sortableModel+']');

	var limit = Number($table.data('limit'));
	if ( !isNaN(limit) ) {
		if (limit == 1) {
			$table.find('tr').remove();
		} else if (limit <= $table.find('tr').length) {
			return;
		}
	}

	$table.find('tbody').append( tpl_sortable_item.replaceAll('$name', name.escapeHtml()).replaceAll('$id', id).replaceAll('$attachedToModel', attachedToModel).replaceAll('$sortableModel', sortableModel).replaceAll('$variableName', variableName) );

	if (isNaN(Number(id)))
		$table.find('tr:last a:first').replaceWith('<span>'+name.escapeHtml()+'</span>');
}

function refreshSortable() {
	$('table.sortable tbody').sortable('refresh');
}


function bindAttachActions() {
	$('#attach-list a[data-id]').each(function(){
		if ( $('table[data-sortable-model='+$(this).data('sortable-model')+'] tr[data-id="'+$(this).data('id')+'"]').length > 0 ) {
			$(this).text('убрать');
			$(this).data('action', 'detach-from-sortable');
		} else {
			$(this).text('прикрепить');
			$(this).data('action', 'attach-to-sortable');
		}
	});
}

function realignPopup() {
	$('.popup-window').css('margin-top',  - $('.popup-window').outerHeight() / 2 + $(window).scrollTop());
	if ($('.popup-window').offset().top < 150)	{
		$('.popup-window').css({ 'top': '150px', 'margin-top' : 0 });
	}
	$('.popup-window').css({ 'margin-left': -$('.popup-window').width() / 2 })
}

function initSyncTranslit()
{
	$('input[data-sync-to]').unbind('keyup');

	$('input[data-sync-to]').each(function() {
		var $lock = $('.icon[data-sync-to="'+$(this).data('sync-to')+'"]');

		if (($lock.length > 0 && $lock.text() == 'w') || $lock.length == 0)
		{
			$(this).syncTranslit({
				destination: $('input[data-sync-from]').attr('id'),
				type: 'url',
				caseStyle: 'lower',
				urlSeparator: '_'
			});
		}
	});
}

/* Extensions */

String.prototype.replaceAll = function(search, replace) {
	return this.split(search).join(replace);
}

String.prototype.escapeHtml = function() {
	return this.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}

// возвращает cookie если есть или undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}

// уcтанавливает cookie
function setCookie(name, value, props) {
    props = props || {}
    var exp = props.expires
    if (typeof exp == "number" && exp) {
        var d = new Date()
        d.setTime(d.getTime() + exp*1000)
        exp = props.expires = d
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

    value = encodeURIComponent(value)
    var updatedCookie = name + "=" + value
    for(var propName in props){
        updatedCookie += "; " + propName
        var propValue = props[propName]
        if(propValue !== true){ updatedCookie += "=" + propValue }
    }
    document.cookie = updatedCookie

}

// удаляет cookie
function deleteCookie(name) {
    setCookie(name, null, { expires: -1 })
}





gallery = new function Gallery()
{
	//Список добавленных галерей
	this.galleries = {};

	//Добавляем галерею в список
	/*this.add = function(placeholder, gallery_id, photo_count, photos)
	{
		this.galleries[gallery_id] = {gallery_id: gallery_id, photo_count: photo_count, photos: photos, current_photo: 0};

		var first_photo = gallery.galleries[gallery_id].photos[0];
		var image_holder = $(placeholder).find('.b-photogallery__viewport img');
		$(image_holder).attr('src', first_photo.img);
		$(image_holder).addClass('g-active');
	}*/

	//Создаем галерею из параметров элемента
	this.place = function(gallery_id, size)	{
		$.ajax({
			url: '/gallery/place/?id='+gallery_id + '&size=' + size,
			success: function(data){
				$('div#gallery_'+gallery_id).replaceWith(data);
			}
		});
	}
}


