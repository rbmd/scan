$(function() {
	$("#BlogsPostAdmin_region_id").change(function() {
		$.get('/blogs/admin/authors/listbyblog/', {id: $(this).val()},
				function(authors) {
					$(".authors_list p").remove();
					for (key in authors)
					{
						author = authors[key];
						$(".authors_list").append('<p><label style="display:inline"><input name="author[]" value="' + author.id + '" type="checkbox"/> — ' + author.name + '</label></p>');
					}
				});
	});


	$('.dot_address').keypress(function(e) {
		if (e.which == 13)
		{
			BlogsGMap.findDot();
		}
	});

	//Карта гидов. При клике на "Найти на карте"
	$('.BlogsGMap_findOnMap').live('click', function() {
		BlogsGMap.findDot();
	});
});


//Объект для работы с картой гидов
BlogsGMap = new function() {
	//Объект гугл-карты
	this.map;
	//объект гугл-геодекодера
	this.geocoder;
	//Инициализирована ли карта
	var initialized;

	this.findDot = function() {
		var input = $('.dot_address');
		//Получаем адрес из гугла
		geocoder.geocode({ 'address': 'Москва, ' + input.val()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK)
			{
				map.setCenter(results[0].geometry.location);
				marker.setPosition(results[0].geometry.location);
				BlogsGMap.updateMarkerCoord(marker);
			}
			else
				alert("Ошибка: " + status);
		});
	}

	this.initialize = function(address, x, y) {
		if (BlogsGMap.initialized == undefined)
		{
			if (!x)
			{
				x = 0;
				y = 0;
			}

			BlogsGMap.addMarker('address', new google.maps.LatLng(x, y));
			geocoder = new google.maps.Geocoder();

			//Центровка карты на Москву
			var latlng = new google.maps.LatLng(x, y);
			var myOptions = {
				scrollwheel: false,
				zoom       : 16,
				center     : latlng,
				mapTypeId  : google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		}

		//При инициализации проходим по массиву добавленных маркеров и рисуем их на карте
		BlogsGMap.markers.marker.setMap(map);
		BlogsGMap.initialized = true;
	}


	//Добавление маркера на карту
	this.addMarker = function(title, position) {
		//Точка содержит все параметры и объект googlemap - marker
		var dot = {};
		marker = new google.maps.Marker({
													  position : position,
													  draggable: true
												  });

		//Если инициализация прошла, отображаем на карте сразу
		if (BlogsGMap.initialized)
			marker.setMap(map);

		//При изменении положения необходимо пересчитывать координаты
		google.maps.event.addListener(marker, 'dragend', function() {
			BlogsGMap.updateMarkerCoord(marker)
		});

		dot.marker = marker;
		dot.title = title;

		//Добавляем в массив маркер
		BlogsGMap.markers = dot;
		BlogsGMap.updateMarkerCoord(marker);
	}


	//Обновляет значения координат при перемещении маркера на карте
	this.updateMarkerCoord = function(marker) {
		//Обновляем координаты в инпутах
		$('.dot_x').val(marker.getPosition().lat());
		$('.dot_y').val(marker.getPosition().lng());
	}
}