$(document).ready(function() {
	//При наведении на блок настроек точки подсвечиваем точку.
	//При наведении на точку подсвечиваем блок настроек
	$('.GuideGMap_dot').live('mouseover', function() {
		GuideGMap.highlightMarker($(this).attr('data-key'));
	});

	//При изменении выбранной точки элемента guidegmap необходимо дополнить скрытое поле элемента ключом выбранной точки
	$('.guidegmap_select').live('change', function() {
		chapter_element = chapter.getElementChapter_Element($(this));
		//ищем скрытое поле той же группы элементов и меняем ему параметр name, чтобы в post пришел корректный массив GuideGMap, содержащий chapter_order_num
		chapter.getElementHolder($(this)).find('input[type="hidden"]').attr('name', 'GuideGMap[' + $(this).val() + '][chapter_order_num]');
	});

	//При клике на галочку "Является гидом" показываем или скрываем блок
	$('.isGuideChecker').live('click', function() {
		elem = $('#guide_gmap_block');
		if (elem.hasClass('hidden'))
		{
			elem.removeClass('hidden');
			elem.addClass('visible');
		}
		else
		{
			elem.removeClass('visible');
			elem.addClass('hidden');
		}

	});

	//Инициализируем карту при клике на закладку ГЛАВЫ      
	//$('a[data-section="chapters"]').live('click', function(){
	$('button[pane="chapters"]').live('click', function(){
		GuideGMap.initialize();
		google.maps.event.trigger(map, 'resize');
	});

	
	//Карта гидов. При клике на "Найти на карте"
	$('.GuideGMap_findOnMap').live('click', function() {

		var input = $(this).parent().find('.dot_address');
		var dot_key = input.attr('data-key');

		//Если маркер уже установлен, сначала удаляем его
		if (GuideGMap.markers[dot_key] != undefined)
		{
			GuideGMap.markers[dot_key].marker.setMap(null);
		}

		//Получаем адрес из гугла
		geocoder.geocode({ 'address': 'Москва, ' + input.val()}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK)
			{

				map.setCenter(results[0].geometry.location);

				GuideGMap.addMarker(input.attr('data-key'), input.val(), results[0].geometry.location);

			}
			else
			{
				alert("Ошибка: " + status);
			}
		});
	});	
	
});



//Карта гидов. Добавление точки на карту гидов
$('.GuideGMap_dot_add').live('click', function() {
	$.ajax({
				 url     : '/admin/elements/getgmapdot/',
				 dataType: 'json',
				 success : function(data) {
					 $('.dot_params').append(data.view);
					 //Добавляем точку
					 GuideGMap.addMarker(data.key, data.title, new google.maps.LatLng(data.x, data.y));
					 //Обновляем селекты для элемента guidegmap
					 GuideGMap.updateMarkerSelect(data.key, data.title);
				 }
			 });
});

//Карта гидов. Удаление точки
$('.GuideGMap_remove_dot').live('click', function() {
	$(this).parent().remove();
	GuideGMap.removeMarker($(this).attr('data-key'));
});

//Ловим нажатие enter при вводе адреса
$('.dot_address').live('keypress', function(e) {
	if (e.charCode == 13)
	{
		e.preventDefault();
		key = $(this).attr('data-key');
		$('.GuideGMap_findOnMap[data-key="' + key + '"]').trigger('click');
		return false;
	}
});

//Обработчик на изменение названия(адреса)
$('.dot_address').live('keyup', function() {

	GuideGMap.updateMarker($(this).attr('data-key'), $(this).val());
	//Обновляем селекты для элемента guidegmap
	GuideGMap.updateMarkerSelect($(this).attr('data-key'), $(this).val());
});

//Объект для работы с картой гидов
GuideGMap = new function() {

	//Объект гугл-карты
	this.map;
	//объект гугл-геодекодера
	this.geocoder;
	//объект объектов маркеров карты
	this.markers = {};

	//Инициализирована ли карта
	var initialized;

	this.initialize = function() {

		if (GuideGMap.initialized == undefined)
		{
			geocoder = new google.maps.Geocoder();

			//Центровка карты на Москву
			var latlng = new google.maps.LatLng(55.75, 37.66);

			var myOptions = {
				scrollwheel: false,
				zoom       : 9,
				center     : latlng,
				mapTypeId  : google.maps.MapTypeId.ROADMAP
			};

			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		}

		//При инициализации проходим по массиву добавленных маркеров и рисуем их на карте
		$.each(GuideGMap.markers, function(key, value) {
			GuideGMap.markers[key].marker.setMap(map);
		});

		GuideGMap.initialized = true;

	}


	//Добавление маркера на карту
	this.addMarker = function(key, title, position) {
		//Точка содержит все параметры и объект googlemap -  marker
		var dot = {};
		var marker = new google.maps.Marker({
															position : position,
															draggable: true
															//map: map,
														});

		//Если инициализация прошла, отображаем на карте сразу
		if (GuideGMap.initialized)
			marker.setMap(map);

		//При изменении положения необходимо пересчитывать координаты
		google.maps.event.addListener(marker, 'dragend', function() {
			GuideGMap.updateMarkerCoord(key, marker)
		});

		//При наведении подсвечиваем маркер
		google.maps.event.addListener(marker, 'mouseover', function() {
			GuideGMap.highlightMarker(key)
		});

		dot.marker = marker;
		dot.title = title;
		dot.key = key;

		//Добавляем в массив маркер
		GuideGMap.markers[key] = dot;
		GuideGMap.updateMarkerCoord(key, marker);
	}

	//Удаляем маркер
	this.removeMarker = function(key) {
		GuideGMap.markers[key].marker.setMap(null);
		delete GuideGMap.markers[key];

		//Удаляем все option из select
		$('.guidegmap_select option[value="' + key + '"]').remove();
	}

	//Обновляет значения координат при перемещении маркера на карте
	this.updateMarkerCoord = function(key, marker) {
		//Обновляем координаты в инпутах
		$('.dot_x[data-key=' + key + ']').val(marker.getPosition().lat());
		$('.dot_y[data-key=' + key + ']').val(marker.getPosition().lng());
	}

	//Заполняет указаный select значениями из массива маркеров
	this.fillSelect = function(select_id, selected) {
		var s = '';

		//Проходим по всему массиву маркеров и добавляем их в селект
		$.each(GuideGMap.markers, function(key, value) {

			if (value.key == selected)
				s = 'selected';
			else
				s = '';

			$(select_id).append('<option value="' + value.key + '" ' + s + '>' + value.title + '</option>');
		});

		$('.guidegmap_select').trigger('change');
	}

	//Обновляет параметры маркера в массиве маркеров
	this.updateMarker = function(key, title) {
		GuideGMap.markers[key].title = title;
	}
	//Обновляет значения select для элементов guidegmap
	this.updateMarkerSelect = function(key, title) {

		var exists = false;
		//Проверяем наличие такой точки
		$.each($('.guidegmap_select:first option'), function(k, v) {
			if ($(v).attr('value') == key)
				exists = true;
		});

		//Если элемент существует, обновляем его значение
		if (exists)
		{
			$('.guidegmap_select option[value="' + key + '"]').html(title);
		}
		//В ином случае добавляем новый
		else
		{
			$('.guidegmap_select').append('<option value="' + key + '">' + title + '</option>');
		}
	}


	this.resetMarkersIcon = function() {
		$.each(GuideGMap.markers, function(key, value) {
			GuideGMap.markers[key].marker.setIcon('http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png');
		});
	}

	this.highlightMarker = function(key) {
		GuideGMap.resetMarkersIcon();
		GuideGMap.markers[key].marker.setIcon('http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png');
		GuideGMap.highlightMarkerBlock(key);
	}

	this.highlightMarkerBlock = function(key) {
		$('.GuideGMap_dot .highlighter').removeClass('on');
		$('.GuideGMap_dot[data-key="' + key + '"] .highlighter').addClass('on');
	}

	//Инициализация происходит при переключении вкладок, см шаблон редактирования статьи
	//this.initialize();
}