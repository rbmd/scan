/* element gallery*/

gallery = new function Gallery()
{
	//Список добавленных галерей
	this.galleries = {};

	//Добавляем галерею в список
	this.add = function(placeholder, gallery_id, photo_count, photos)
	{
		this.galleries[gallery_id] = {gallery_id: gallery_id, photo_count: photo_count, photos: photos, current_photo: 0};

		var first_photo = gallery.galleries[gallery_id].photos[0];
		var image_holder = $(placeholder).find('.b-photogallery__viewport img');
		$(image_holder).attr('src', first_photo.img);
		$(image_holder).addClass('g-active');
	}

	//Создаем галерею из параметров элемента
	this.place = function(gallery_id, size)	{
		$.ajax({
			url: '/gallery/place/?id='+gallery_id + '&size=' + size,
			success: function(data){
				$('div#gallery_'+gallery_id).replaceWith(data);
			}
		});
	}
}
/* element gallery END*/


/*element poll*/
poll = new function Poll()
{
	this.polls = {};

	//Добавляем голосование в список
	this.add = function(poll_id)
	{
		this.polls[poll_id] = {poll_id: poll_id};
	}


	this.place = function(poll_id)
	{
		$.ajax({
				url: '/polls/place/?poll_id='+poll_id,
				success: function(data){
					$('div#poll_'+poll_id).html(data);
				}
		});
	}
}
/* element poll END*/