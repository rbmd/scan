// Отступ для вьюпортпа, см. функцию fitPopularItems
var viewportOffset = 100;

adaptiveColumn = new function() {
	//Массив виджетов
	this.widgets = [];

	//Общая высота виджетов. Считается при добавлении каждого виджета
	this.widgetsHeight = 0;

	// Добавляем виджет. id = DOM id,
	// если он есть на странице и он не пустой

	this.addWidget = function(id) {

		var $widget = $('#'+id),
			widgetHeight = $widget.height();

		if ($widget.length && widgetHeight> 0) {
			this.widgets.push({id: id, start: 0});
			this.widgetsHeight += widgetHeight;
		}
	};





	//Вызывается при скролле. см метод render
	this.scroll = function() {

		//По середине экрана проходит линия, пересечение которой будет вызывать смену блоков
		var documentTop = $(document).scrollTop();
		var middleLine = documentTop + ($(window).height()/2);

		//Определяем, какой блок необходимо отображать на этой высоте
		$(adaptiveColumn.widgets).each(function(i, v) {
			if (middleLine >= this.start) {
				adaptiveColumn.blockId = i;
			}
		});

		//Если нужно показывать новый блок
		if (adaptiveColumn.lastBlockId != adaptiveColumn.blockId) {
			//Если новый блок ниже старого
			if (adaptiveColumn.blockId > adaptiveColumn.lastBlockId) {
				$('#' + adaptiveColumn.widgets[adaptiveColumn.lastBlockId].id).animate({'top': '-400px', 'opacity': 0}, 500);
			}

			//Если новый блок выше старого
			if (adaptiveColumn.blockId < adaptiveColumn.lastBlockId) {
				$('#' + adaptiveColumn.widgets[adaptiveColumn.lastBlockId].id).animate({'top': '800px', 'opacity': 0}, 500);
			}

			$('#' + adaptiveColumn.widgets[adaptiveColumn.blockId].id).animate({'top': '0', 'opacity': 1}, 500);

			adaptiveColumn.lastBlockId = adaptiveColumn.blockId;
		}

		//Если верх экрана выше колонки, значит блок не должен быть фиксирован




		if (documentTop >= $('.l-main__cols').position().top) {
			$('.fixed-container').addClass('fixed')
		} else {
			$('.fixed-container').removeClass('fixed')
		}

//		if (documentTop + 60 < adaptiveColumn.columnOffset.top) {
		if (documentTop + 40 <= $('.l-main__cols').position().top) {
			$('#' + adaptiveColumn.widgets[adaptiveColumn.lastBlockId].id).removeClass('b-adaptive-block_fixed');
			return;
		}

		//Для последнего блока проверяем, не пересек ли он границу родителя
		if (adaptiveColumn.blockId == adaptiveColumn.widgets.length - 1) {
			adaptiveColumn.lastBlock = $('#' + adaptiveColumn.widgets[adaptiveColumn.widgets.length - 1].id);

			//Если пересек, то фиксируем его по нижней кромке
			if (documentTop + adaptiveColumn.lastBlock.height() + 40 >= adaptiveColumn.columnOffset.top + $('[role="adaptive-column"]').height()) {
				adaptiveColum.lastBlock
					.removeClass('b-adaptive-block_fixed')
					.removeClass('b-adaptive-block_bottom')
					.addClass('b-adaptive-block_absolutebottom');
			}
			else {
				adaptiveColumn.lastBlock
					.addClass('b-adaptive-block_fixed')
					.addClass('b-adaptive-block_bottom')
					.removeClass('b-adaptive-block_absolutebottom');


			}
			return;
		}

		//В остальных случая блоки должны быть фиксированы
		$('#' + adaptiveColumn.widgets[adaptiveColumn.blockId].id).addClass('b-adaptive-block_fixed');

	}


	this.render = function() {

		//Если высота левой колонки недостаточна, чтобы прокручивать виджеты, то _НЕ_ включаем колонку

		if ($('.l-main__cols__left').height() - adaptiveColumn.widgetsHeight < 1000) {
			return;
		}

		var $column = $('[role="adaptive-column"]');

		//Для правой колонки назначаем высоту такую же, что и для левой
		$column.height( $('.l-main__cols__left').height() );


		//Параметры правой колонки
		this.columnOffset = $column.offset();

		//Количество блоков
		this.blocksCount = this.widgets.length;
		this.pixelsPerBlock =  $column.height() / this.widgets.length;

		//Текущий блок
		this.blockId = 0;

		//Последний показанный блок
		this.lastBlockId = 0;

		//Проходим по массиву виджетов, чтобы задать стартовые положения каждого в отдельности
		//Также назначаем для всех виджетов классы
		$(this.widgets).each(function(i, v) {
			adaptiveColumn.widgets[i].start = adaptiveColumn.pixelsPerBlock * i + adaptiveColumn.columnOffset.top;

			block = $('#'+v.id);

			//По умолчанию каждый виджет должен быть таким
			block.addClass('b-adaptive-block');

			var viewport = $(window).height() - $('.l-head__layout').height() - viewportOffset;

			fitPopularItems(block, viewport)

			//Все виджеты, кроме первого, скрываем и двигаем вниз
			if (i > 0) {
				block
					.addClass('b-adaptive-block_bottom')
					.addClass('b-adaptive-block_fixed');
			}


		});

		$(window).on('scroll.adaptive', function(e){
			clearTimeout(adaptiveColumn.ongoing);
			adaptiveColumn.ongoing = setTimeout(function() {
				adaptiveColumn.scroll()
			}, 50);
		});


	}


}



$(function(){




		adaptiveColumn.addWidget('first');
		adaptiveColumn.addWidget('second');
		adaptiveColumn.addWidget('third');
		adaptiveColumn.addWidget('fourth');
		adaptiveColumn.addWidget('fifth');
		adaptiveColumn.render();

		$(window).on('resize', function() {
//			adaptiveColumn.render();




			$('.b-adaptive-block').each(function(){

				var block = $(this);

				var viewport = $(window).height() - $('.l-head__layout').height() - viewportOffset;

				fitPopularItems(block, viewport);


			})




		});

});

// Функция для показа/скрытия блоков в Популярных статьях

var fitPopularItems = function(block, viewport) {


	var $items = block.find('.b-popular__item, .b-partner__item'),
		itemsCount = $items.length,
		itemsHeight = 0;

	// Проверям, есть ли в блоке дочерные элементы

	if (itemsCount > 0 ) {

		// Если есть, считает высоту каждого блока и приплюсовываем
		// к высоте всех дочених блоков, и сравниваем с высотой вьюпорта

		$items.each(function(){
			var $this = $(this),
					extraHeight = 0;

			if ($this.hasClass('b-partner__item')) {

				extraHeight = 50;
			}

			if (itemsHeight + $this.outerHeight() + extraHeight  < viewport) {
				itemsHeight += $this.outerHeight() + extraHeight;
				$this.show();
			} else {
				$this.hide();
			}
		});
	}
}